/*!
**  \file DataFlash.c
**  \author Alessandro Vagniluca
**  \date 06/10/2015
**  \brief Driver for SST25/SST26 data-flash memory
**  
**  \version 1.0
**/

#include "main.h"
#include "DataFlash.h"



#define CS_FLASH_PIN				(SPI2_NSS_Pin)
#define CS_FLASH_GPIO_PORT		(SPI2_NSS_GPIO_Port)
// #define CS_FLASH_2_PIN			(CS_FLASH_2_Pin)
// #define CS_FLASH_2_GPIO_PORT	(CS_FLASH_2_GPIO_Port)


#define SPI_TIMEOUT		10	//!< 10ms SPI timeout


extern SPI_HandleTypeDef hspi2;

#define VERIFY_BUFF_SIZE		DF_PAGE_SIZE
static unsigned char verbuf[VERIFY_BUFF_SIZE];	//!< write verify buffer


/*!
**  \fn void dfDelay( void )
**  \brief Delay data-flash access (min. 100 ns)
**  \return None
**/
static void dfDelay( void )
{
	unsigned long i = 20;

	for( i=0; i<20; i++ )
		__NOP();

}

/*!
**  \fn void BSP_CS_FLASH( unsigned char bank, unsigned char Stat )
**  \brief Drive the data-flsh chip-select
**  \param bank The data-flash bank
**  \param Stat the new chip-select state (0:low, 1:high)
**  \return None
**/
static void BSP_CS_FLASH( unsigned char bank, unsigned char Stat )
{
	switch( bank )
	{
		case EE_BANK_0:
			HAL_GPIO_WritePin( CS_FLASH_GPIO_PORT, CS_FLASH_PIN,
								Stat ? GPIO_PIN_SET : GPIO_PIN_RESET );
		break;
		
		/* case EE_BANK_1:
			HAL_GPIO_WritePin( CS_FLASH_2_GPIO_PORT, CS_FLASH_2_PIN,
								Stat ? GPIO_PIN_SET : GPIO_PIN_RESET );
		break; */
	}
}

/*!
**  \fn void dfWREN( unsigned char bank )
**  \brief Send WREN command
**  \param bank The data-flash bank
**  \return None
**/
static void dfWREN( unsigned char bank )
{
	unsigned char cmd = SPI_FLASH_INS_WREN;
	
	BSP_CS_FLASH( bank, 0 );			/* enable device */
	Wdog();
	HAL_SPI_Transmit(&hspi2, &cmd, 1, SPI_TIMEOUT);		/* send WREN command */
	Wdog();
	BSP_CS_FLASH( bank, 1 );			/* disable device */
	dfDelay();
}

/*!
**  \fn void dfWRDI( unsigned char bank )
**  \brief Send WRDI command
**  \param bank The data-flash bank
**  \return None
**/
static void dfWRDI( unsigned char bank )
{
	unsigned char cmd = SPI_FLASH_INS_WRDI;
	
	BSP_CS_FLASH( bank, 0 );			/* enable device */
	Wdog();
	HAL_SPI_Transmit(&hspi2, &cmd, 1, SPI_TIMEOUT);		/* send WRDI command */
	Wdog();
	BSP_CS_FLASH( bank, 1 );			/* disable device */
	dfDelay();
}

/*!
**  \fn void dfWaitBusy( unsigned char bank )
**  \brief Waits until device is no longer busy
**  \param bank The data-flash bank
**  \return None
**/
static void dfWaitBusy( unsigned char bank )
{
	unsigned char cmd = SPI_FLASH_INS_RDSR;
	STATUS_REG stat;
	
	stat.BYTE = 0xFF;
	do
	{
		BSP_CS_FLASH( bank, 0 );			/* enable device */
		Wdog();
		HAL_SPI_Transmit(&hspi2, &cmd, 1, SPI_TIMEOUT);		/* send RDSR command */
		Wdog();
		HAL_SPI_Receive(&hspi2, &stat.BYTE, 1, SPI_TIMEOUT);		/* receive byte */
		Wdog();
		BSP_CS_FLASH( bank, 1 );			/* disable device */
		dfDelay();
	} while( stat.BIT.Busy );
}


/*!
**  \fn short df_Init( unsigned char bank )
**  \brief Init data-flash driver
**  \param bank The data-flash bank
**  \return 1: OK, 0: FAILED
**/
short df_Init( unsigned char bank )
{
	short RetVal = 0;
	BYTE_UNION u;
	STATUS_REG stat;
#if defined(SST26VF032)
	// unsigned char block_protection_10[10];	/* array to store block_protection data */
#endif
	
	// lock DF
	if( osKernelRunning()
		&& (DFMutexHandle[bank] != NULL)
		)
	{
		osMutexWait(DFMutexHandle[bank], osWaitForever);
	}

	BSP_CS_FLASH( bank, 1 );		// disable device
	dfDelay();
	
	// power on device
	HAL_GPIO_WritePin( EN_FLASH_GPIO_Port, EN_FLASH_Pin, GPIO_PIN_RESET );
	DWT_Delay_us(200);	// TPU,min = 100us
	

#if defined(SST26VF032)
	/*!	No more needed: we have just powered up (and reset) the device. */
	/* // enable software reset
	BSP_CS_FLASH( bank, 0 );		// enable device
	u.b[0] = SPI_FLASH_INS_RSTEN;
	Wdog();
	HAL_SPI_Transmit(&hspi2, u.b, 1, SPI_TIMEOUT);
	Wdog();
	BSP_CS_FLASH( bank, 1 );		// disable device
	dfDelay();
	// software reset
	BSP_CS_FLASH( bank, 0 );		// enable device
	u.b[0] = SPI_FLASH_INS_RST;
	Wdog();
	HAL_SPI_Transmit(&hspi2, u.b, 1, SPI_TIMEOUT);
	Wdog();
	BSP_CS_FLASH( bank, 1 );		// disable device
	dfDelay(); */
#endif
	
	// read JEDEC-ID
	BSP_CS_FLASH( bank, 0 );		// enable device
	u.b[0] = SPI_FLASH_INS_RDID;
	Wdog();
	HAL_SPI_Transmit(&hspi2, u.b, 1, SPI_TIMEOUT);
	Wdog();
	HAL_SPI_Receive(&hspi2, u.b, 3, SPI_TIMEOUT);
	Wdog();
	BSP_CS_FLASH( bank, 1 );		// disable device
	dfDelay();
	
	// check JEDEC-ID
	if( (u.b[0] == 0xBF) &&		// Manufacturer ID 
#if defined(SST25VF032)			// Memory Type
		(u.b[1] == 0x25)
#elif defined(SST26VF032)	
		(u.b[1] == 0x26)
#endif
		)
	{
		// read STATUS
		BSP_CS_FLASH( bank, 0 );			/* enable device */
		u.b[0] = SPI_FLASH_INS_RDSR;		/* send RDSR command */
		Wdog();
		HAL_SPI_Transmit(&hspi2, u.b, 1, SPI_TIMEOUT);
		Wdog();
		HAL_SPI_Receive(&hspi2, &stat.BYTE, 1, SPI_TIMEOUT);		/* receive byte */
		Wdog();
		BSP_CS_FLASH( bank, 1 );			/* disable device */
		dfDelay();
		
#if defined(SST25VF032)
		// disable blocks protection
		/* stat.BIT.Bp0 = 0;
		stat.BIT.Bp1 = 0;
		stat.BIT.Bp2 = 0;
		stat.BIT.Bp3 = 0;
		stat.BIT.Bpl = 0; */
		stat.BYTE = 0x00;
		
		// enable write STATUS
		BSP_CS_FLASH( bank, 0 );			/* enable device */
		u.b[0] = SPI_FLASH_INS_EWSR;		/* send EWSR command */
		Wdog();
		HAL_SPI_Transmit(&hspi2, u.b, 1, SPI_TIMEOUT);
		Wdog();
		BSP_CS_FLASH( bank, 1 );			/* disable device */
		dfDelay();

		// write STATUS
		BSP_CS_FLASH( bank, 0 );			/* enable device */
		u.b[0] = SPI_FLASH_INS_WRSR;		/* send WRSR command */
		u.b[1] = stat.BYTE;	/* send new status value */
		Wdog();
		HAL_SPI_Transmit(&hspi2, u.b, 2, SPI_TIMEOUT);
		Wdog();
		BSP_CS_FLASH( bank, 1 );			/* disable device */
		dfDelay();
		
		dfWaitBusy( bank );
#elif defined(SST26VF032)	
		// // read block-protection register
		// BSP_CS_FLASH( bank, 0 );			/* enable device */
		// u.b[0] = SPI_FLASH_INS_RBPR;		/* send RBPR command */
		// HAL_SPI_Transmit(&hspi2, u.b, 1, SPI_TIMEOUT);
		// HAL_SPI_Receive(&hspi2, block_protection_10, 10, SPI_TIMEOUT);		/* receive byte */
		// BSP_CS_FLASH( bank, 1 );			/* disable device */
		// dfDelay();
		
		// // disable blocks protection
		// memset( block_protection_10, 0, sizeof(block_protection_10) );
		
		dfWREN( bank );
		
		// // write block-protection register
		// BSP_CS_FLASH( bank, 0 );			/* enable device */
		// u.b[0] = SPI_FLASH_INS_WBPR;		/* send WBPR command */
		// HAL_SPI_Transmit(&hspi2, u.b, 1, SPI_TIMEOUT);
		// HAL_SPI_Transmit(&hspi2, block_protection_10, 10, SPI_TIMEOUT);		/* receive byte */
		// BSP_CS_FLASH( bank, 1 );			/* disable device */
		// dfDelay();
		
		BSP_CS_FLASH( bank, 0 );			/* enable device */
		u.b[0] = SPI_FLASH_INS_ULBPR;		/* send ULBPR command */
		Wdog();
		HAL_SPI_Transmit(&hspi2, u.b, 1, SPI_TIMEOUT);
		Wdog();
		BSP_CS_FLASH( bank, 1 );			/* disable device */
		dfDelay();
		
		dfWaitBusy( bank );
		dfWRDI( bank );
#endif

		RetVal = 1;
	}

	// unlock DF
	if( osKernelRunning()
		&& (DFMutexHandle[bank] != NULL)
		)
	{
		osMutexRelease(DFMutexHandle[bank]);
	}
	
	return RetVal;
}

/*!
**  \fn short df_Deinit( unsigned char bank )
**  \brief Deinit data-flash driver
**  \param bank The data-flash bank
**  \return 1: OK, 0: FAILED
**/
short df_Deinit( unsigned char bank )
{
	// power off device
	HAL_GPIO_WritePin( EN_FLASH_GPIO_Port, EN_FLASH_Pin, GPIO_PIN_SET );
	// TPD,min = 100ms

	return 1;
}

/*!
**  \fn short df_EraseSubSector( unsigned char bank, unsigned short iSubSect )
**  \brief Erase the indicated data-flash subsector
**  \param bank The data-flash bank
**  \param iSubSect The subsector index
**  \return 1: OK, 0: FAILED
**/
short df_EraseSubSector( unsigned char bank, unsigned short iSubSect )
{
	short RetVal = 0;
	BYTE_UNION u, addr;
	
	// lock DF
	if( osKernelRunning()
		&& (DFMutexHandle[bank] != NULL)
		)
	{
		osMutexWait(DFMutexHandle[bank], osWaitForever);
	}
	
	// get the subsector address
	addr.d = (unsigned long)iSubSect * DF_SUBSECTOR_SIZE;
	if( addr.d < DF_CHIP_SIZE )
	{
		u.b[0] = SPI_FLASH_INS_SSE;			/* send Sector Erase command */
		u.b[1] = addr.b[2];	/* send 3 address bytes */
		u.b[2] = addr.b[1];
		u.b[3] = addr.b[0];

		dfWREN( bank );
		
		BSP_CS_FLASH( bank, 0 );		// enable device
		Wdog();
		HAL_SPI_Transmit(&hspi2, u.b, 4, SPI_TIMEOUT);
		Wdog();
		BSP_CS_FLASH( bank, 1 );		// disable device
		dfDelay();
		
		dfWaitBusy( bank );
		dfWRDI( bank );
		
		RetVal = 1;
	}

	// unlock DF
	if( osKernelRunning()
		&& (DFMutexHandle[bank] != NULL)
		)
	{
		osMutexRelease(DFMutexHandle[bank]);
	}
	
	return RetVal;
}

/*!
**  \fn short df_EraseSector( unsigned char bank, unsigned short iSect )
**  \brief Erase the indicated data-flash sector
**  \param bank The data-flash bank
**  \param iSect The sector index
**  \return 1: OK, 0: FAILED
**/
short df_EraseSector( unsigned char bank, unsigned short iSect )
{
	short RetVal = 0;
	BYTE_UNION u, addr;
	
	// get the sector address
	addr.d = (unsigned long)iSect * DF_SECTOR_SIZE;
	if( addr.d < DF_CHIP_SIZE )
	{
#if defined(SST26VF032)
		/*
			The first and the last 64KB sector don't exist.
			There are 1 32KB and 4 8KB blocks in each of the two 64KB area.
			Erase in 4KB subsectors.
		*/
		if( iSect == 0 )
		{
			// erase the first 64KB area
			for( iSect=0; iSect<NUM_SUBSECT_SECTOR; iSect++ )
			{
				if( !df_EraseSubSector( bank, iSect ) )
					break;
			}
			if( iSect == NUM_SUBSECT_SECTOR )
			{
				RetVal = 1;
			}
		}
		else if( iSect == 63 )
		{
			// erase the last 64KB area
			for( iSect=NUM_SUBSECT_CHIP-NUM_SUBSECT_SECTOR;
					iSect<NUM_SUBSECT_CHIP;
					iSect++ )
			{
				if( !df_EraseSubSector( bank, iSect ) )
					break;
			}
			if( iSect == NUM_SUBSECT_CHIP )
			{
				RetVal = 1;
			}
		}
		else
#endif		
		{
			// lock DF
			if( osKernelRunning()
				&& (DFMutexHandle[bank] != NULL)
				)
			{
				osMutexWait(DFMutexHandle[bank], osWaitForever);
			}

			u.b[0] = SPI_FLASH_INS_SE;			/* send Block Erase command */
			u.b[1] = addr.b[2];	/* send 3 address bytes */
			u.b[2] = addr.b[1];
			u.b[3] = addr.b[0];

			dfWREN( bank );
			
			BSP_CS_FLASH( bank, 0 );		// enable device
			Wdog();
			HAL_SPI_Transmit(&hspi2, u.b, 4, SPI_TIMEOUT);
			Wdog();
			BSP_CS_FLASH( bank, 1 );		// disable device
			dfDelay();
			
			dfWaitBusy( bank );
			dfWRDI( bank );

			// unlock DF
			if( osKernelRunning()
				&& (DFMutexHandle[bank] != NULL)
				)
			{
				osMutexRelease(DFMutexHandle[bank]);
			}
			
			RetVal = 1;
		}
	}
	
	return RetVal;
}

/*!
**  \fn short df_EraseChip( unsigned char bank )
**  \brief Erase the all data-flash
**  \param bank data-flash bank
**  \return 1: OK, 0: FAILED
**/
short df_EraseChip( unsigned char bank )
{
	short RetVal = 1;
	unsigned char cmd = SPI_FLASH_INS_DE;
	
	// lock DF
	if( osKernelRunning()
		&& (DFMutexHandle[bank] != NULL)
		)
	{
		osMutexWait(DFMutexHandle[bank], osWaitForever);
	}
	
	dfWREN( bank );
	
	BSP_CS_FLASH( bank, 0 );		// enable device
	Wdog();
	HAL_SPI_Transmit(&hspi2, &cmd, 1, SPI_TIMEOUT);		/* send Chip Erase command */
	Wdog();
	BSP_CS_FLASH( bank, 1 );		// disable device
	dfDelay();
	
	dfWaitBusy( bank );
	dfWRDI( bank );

	// unlock DF
	if( osKernelRunning()
		&& (DFMutexHandle[bank] != NULL)
		)
	{
		osMutexRelease(DFMutexHandle[bank]);
	}
	
	return RetVal;
}

/*!
**  \fn short df_Read( unsigned char bank, unsigned long addr,
**  							unsigned char *buf, unsigned short num )
**  \brief Read a buffer from the data-flash
**  \param bank The data-flash bank
**  \param addr The start address of the buffer
**  \param buf The buffer pointer
**  \param num The number of bytes to read
**  \return 1: OK, 0: FAILED
**/
short df_Read( unsigned char bank, unsigned long addr, 
					unsigned char *buf, unsigned short num )
{
	short RetVal = 0;
	BYTE_UNION u;
	
	// lock DF
	if( osKernelRunning()
		&& (DFMutexHandle[bank] != NULL)
		)
	{
		osMutexWait(DFMutexHandle[bank], osWaitForever);
	}
	
	if( num && (addr + num <= DF_CHIP_SIZE) )
	{
		u.b[0] = SPI_FLASH_INS_FAST_READ; 			/* read command */
		u.b[1] = (addr & 0x00FFFFFF) >> 16;	/* send 3 address bytes */
		u.b[2] = (addr & 0x0000FFFF) >> 8;
		u.b[3] = addr & 0x000000FF;
		
		BSP_CS_FLASH( bank, 0 );		// enable device
		Wdog();
		HAL_SPI_Transmit(&hspi2, u.b, 4, SPI_TIMEOUT);
		Wdog();
		u.b[0] = 0xFF;				/* dummy byte */
		HAL_SPI_Transmit(&hspi2, u.b, 1, SPI_TIMEOUT);
		Wdog();
		HAL_SPI_Receive(&hspi2, buf, num, num*((unsigned long)SPI_TIMEOUT));
		Wdog();
		BSP_CS_FLASH( bank, 1 );		// disable device
		dfDelay();
		
		RetVal = 1;
	}

	// unlock DF
	if( osKernelRunning()
		&& (DFMutexHandle[bank] != NULL)
		)
	{
		osMutexRelease(DFMutexHandle[bank]);
	}
	
	return RetVal;
}

/*!
**  \fn short df_Write( unsigned char bank, unsigned long addr, 
**  							unsigned char *buf, unsigned short num,
**  							unsigned char verify )
**  \brief Write a buffer from the data-flash
**  \param bank The data-flash bank
**  \param addr The start address of the buffer
**  \param buf The buffer pointer
**  \param num The number of bytes to write
**  \param verify The write verify command
**  \return 1: OK, 0: FAILED
**/
short df_Write( unsigned char bank, unsigned long addr, 
						unsigned char *buf, unsigned short num,
						unsigned char verify )
{
	short RetVal = 0;
	BYTE_UNION u;
#if defined(SST25VF032)
	unsigned char aai = 1;
	unsigned long addrSave = addr;
	unsigned char *bufSave = buf;
	unsigned short numSave = num;
#elif defined(SST26VF032)
	unsigned short nThisBytes;
#endif
	
	if( num && (addr + num <= DF_CHIP_SIZE) )
	{
#if defined(SST25VF032)
	
		// lock DF
		if( osKernelRunning()
			&& (DFMutexHandle[bank] != NULL)
			)
		{
			osMutexWait(DFMutexHandle[bank], osWaitForever);
		}

		dfWREN( bank );
		
		if( addr & 1 )
		{
			// write the first byte on the odd address
			u.b[0] = SPI_FLASH_INS_BP; 			/* byte-program command */
			u.b[1] = (addr & 0x00FFFFFF) >> 16;	/* send 3 address bytes */
			u.b[2] = (addr & 0x0000FFFF) >> 8;
			u.b[3] = addr & 0x000000FF;
			
			BSP_CS_FLASH( bank, 0 );		// enable device
			Wdog();
			HAL_SPI_Transmit(&hspi2, u.b, 4, SPI_TIMEOUT);
			Wdog();
			HAL_SPI_Transmit(&hspi2, buf, 1, SPI_TIMEOUT);	/* send byte to be programmed */
			Wdog();
			BSP_CS_FLASH( bank, 1 );		// disable device
			dfDelay();
			dfWaitBusy( bank );
			addr++;
			buf++;
			num--;
			if( num > 1 )
			{
				// terminate the current Byte Programming mode
				dfWRDI( bank );
				// start another write sequence
				dfWREN( bank );
			}
		}
		
		while( num )
		{
			Wdog();
			if( num == 1 )
			{
				// write the last byte
				if( aai == 0 )
				{
					// terminate the current AAI Word Programming mode
					dfWRDI( bank );
					// start another write sequence
					dfWREN( bank );
				}
				u.b[0] = SPI_FLASH_INS_BP; 			/* byte-program command */
				u.b[1] = (addr & 0x00FFFFFF) >> 16;	/* send 3 address bytes */
				u.b[2] = (addr & 0x0000FFFF) >> 8;
				u.b[3] = addr & 0x000000FF;
				
				BSP_CS_FLASH( bank, 0 );		// enable device
				Wdog();
				HAL_SPI_Transmit(&hspi2, u.b, 4, SPI_TIMEOUT);
				Wdog();
				HAL_SPI_Transmit(&hspi2, buf, 1, SPI_TIMEOUT);	/* send byte to be programmed */
				Wdog();
				BSP_CS_FLASH( bank, 1 );		// disable device
				dfDelay();
				dfWaitBusy( bank );
				addr++;
				buf++;
				num--;
			}
			else if( aai )
			{
				aai = 0;
				// write the first 2 byte
				u.b[0] = SPI_FLASH_INS_AAI; 			/* send AAI command */
				u.b[1] = (addr & 0x00FFFFFF) >> 16;	/* send 3 address bytes */
				u.b[2] = (addr & 0x0000FFFF) >> 8;
				u.b[3] = addr & 0x000000FF;
				
				BSP_CS_FLASH( bank, 0 );		// enable device
				Wdog();
				HAL_SPI_Transmit(&hspi2, u.b, 4, SPI_TIMEOUT);
				Wdog();
				HAL_SPI_Transmit(&hspi2, buf, 2, SPI_TIMEOUT);	/* send 2 byte to be programmed */
				Wdog();
				BSP_CS_FLASH( bank, 1 );		// disable device
				dfDelay();
				dfWaitBusy( bank );
				addr += 2;
				buf += 2;
				num -= 2;
			}
			else
			{
				// write the 2 byte
				u.b[0] = SPI_FLASH_INS_AAI; 			/* send AAI command */
				u.b[1] = *buf;	/* send 1st byte to be programmed */
				u.b[2] = *(buf+1);	/* send 2nd byte to be programmed */
				BSP_CS_FLASH( bank, 0 );		// enable device
				Wdog();
				HAL_SPI_Transmit(&hspi2, u.b, 3, SPI_TIMEOUT);
				Wdog();
				BSP_CS_FLASH( bank, 1 );		// disable device
				dfDelay();
				dfWaitBusy( bank );
				addr += 2;
				buf += 2;
				num -= 2;
			}
		}
		
		dfWRDI( bank );

		// unlock DF
		if( osKernelRunning()
			&& (DFMutexHandle[bank] != NULL)
			)
		{
			osMutexRelease(DFMutexHandle[bank]);
		}
		
		RetVal = 1;

		if( verify == WR_VER_YES )
		{
			// write verify
			addr = addrSave;
			buf = bufSave;
			num = numSave;
			while( num )
			{
				Wdog();
				numSave = num;
				if( numSave > DF_PAGE_SIZE )
					numSave = DF_PAGE_SIZE;

				memset( verbuf, 0xFF, numSave );
				df_Read( bank, addr, verbuf, numSave );
				if( memcmp( verbuf, buf, numSave ) )
				{
					RetVal = 0;
					break;
				}
				// adjust 'num' and 'addr'
				addr += numSave;
				buf += numSave;
				num -= numSave;
			}
		}

#elif defined(SST26VF032)	

		RetVal = 1;

		// eventually, we need to brake up transfers, if they span multiple pages
		while( num )
		{
			// determine how many bytes we have to a page boundary
			nThisBytes = DF_PAGE_SIZE - (addr % DF_PAGE_SIZE);
			if( nThisBytes > num )
				nThisBytes = num;
			// now nThisBytes contains the number of bytes within one page
	
			// lock DF
			if( osKernelRunning()
				&& (DFMutexHandle[bank] != NULL)
				)
			{
				osMutexWait(DFMutexHandle[bank], osWaitForever);
			}
		
			dfWREN( bank );

			// WRITE
			u.b[0] = SPI_FLASH_INS_PP; 			/* send Page Program command */
			u.b[1] = (addr & 0x00FFFFFF) >> 16;	/* send 3 address bytes */
			u.b[2] = (addr & 0x0000FFFF) >> 8;
			u.b[3] = addr & 0x000000FF;
			
			BSP_CS_FLASH( bank, 0 );		// enable device
			Wdog();
			HAL_SPI_Transmit(&hspi2, u.b, 4, SPI_TIMEOUT);
			Wdog();
			HAL_SPI_Transmit(&hspi2, buf, nThisBytes, nThisBytes*((unsigned long)SPI_TIMEOUT));	/* send bytes to be programmed */
			Wdog();
			BSP_CS_FLASH( bank, 1 );		// disable device
			dfDelay();
			dfWaitBusy( bank );
			
			dfWRDI( bank );

			// unlock DF
			if( osKernelRunning()
				&& (DFMutexHandle[bank] != NULL)
				)
			{
				osMutexRelease(DFMutexHandle[bank]);
			}

			if( verify == WR_VER_YES )
			{
				// write verify
				memset( verbuf, 0xFF, sizeof(verbuf) );
				df_Read( bank, addr, verbuf, nThisBytes );
				if( memcmp( verbuf, buf, nThisBytes ) )
				{
					RetVal = 0;
					break;
				}
			}
			// adjust 'num' and 'addr'
			addr += nThisBytes;
			buf += nThisBytes;
			num -= nThisBytes;
		}
		
#endif
	}
	
	return RetVal;
}




