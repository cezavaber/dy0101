/*!
** \file DynSizeVector.c
** \author Alessandro Vagniluca
** \date 18/09/2017
** \brief Gestione array a dimensione dinamica
** 
** 		Rif.: https://www.happybearsoftware.com/implementing-a-dynamic-array
** 
** \version 1.0
**/

#include "main.h"
#include "DynSizeVector.h"


/*!
** \fn short vector_init(Vector *vector)
** \brief Initializes a vector struct. 'dataSize' must be defined
** \param vector An allocated Vector struct
** \return 1: OK, 0: FAILED
** \note Init values:
** 		.size = 0;
** 		.capacity = VECTOR_INITIAL_CAPACITY;
** 		.data = malloc(vector->dataSize * VECTOR_INITIAL_CAPACITY);
**/
short vector_init(Vector *vector)
{
	short RetVal = 0;
	
	// initialize size and capacity
	vector->size = 0;
	vector->capacity = VECTOR_INITIAL_CAPACITY;

	// allocate memory for vector->data
	vector->data = malloc(vector->dataSize * vector->capacity);
	if( vector->data != NULL )
	{
		RetVal = 1;
	}
	
	return RetVal;
}

/*!
** \fn short vector_append(Vector *vector, void *value)
** \brief Appends the given element to the vector
** \param vector The Vector struct
** \param value The element to append
** \return 1: OK, 0: FAILED
** \note If full, the array is expanded.
**/
short vector_append(Vector *vector, void *value)
{
	short RetVal = 0;
	void *p;
	
	// make sure there's room to expand into
	if( vector_increment_capacity_if_full(vector) )
	{
		// append the value and increment vector->size
		p = (void *)((unsigned long)vector->data + (unsigned long)(vector->size * vector->dataSize));
		memcpy( p, value, vector->dataSize );
		vector->size++;
		RetVal = 1;
	}
	
	return RetVal;
}

/*!
** \fn void *vector_get(Vector *vector, unsigned short index)
** \brief Returns the element out of a vector at the given index
** \param vector The Vector struct
** \param index The element index
** \return The pointer to the element or NULL if out of bounds.
**/
void *vector_get(Vector *vector, unsigned short index)
{
	void *p = NULL;
	
	if( index < vector->size )
	{
		p = (void *)((unsigned long)vector->data + (unsigned long)(index * vector->dataSize));
	}
	
	return p;
}

/*!
** \fn short vector_set(Vector *vector, unsigned short index, void *value)
** \brief Sets the element at the given index to the given value
** \param vector The Vector struct
** \param index The element index
** \param value The element new value
** \return 1: OK, 0: FAILED
** \note If full, the array is expanded to the given index.
**/
short vector_set(Vector *vector, unsigned short index, void *value)
{
	short RetVal = 0;
	short i = 1;
	void *p;
	void *newValue;
	
	if( index >= vector->size )
	{
		newValue = malloc( vector->dataSize );
		if( newValue != NULL )
		{
			// zero fill the vector up to the desired index
			memset( newValue, 0, vector->dataSize );
			while( index >= vector->size )
			{
				if( !vector_append( vector, newValue ) )
				{
					i = 0;
					break;
				}
			}
			free( newValue );
		}
		else
		{
			i = 0;
		}
	}

	if( i )
	{
		// set the value at the desired index
		p = (void *)((unsigned long)vector->data + (unsigned long)(index * vector->dataSize));
		memcpy( p, value, vector->dataSize );
		RetVal = 1;
	}
	
	return RetVal;
}

/*!
** \fn short vector_increment_capacity_if_full(Vector *vector, unsigned short newCapacity)
** \brief Increments the underlying data array capacity
** \param vector The Vector struct
** \return 1: OK, 0: FAILED
**/
short vector_increment_capacity_if_full(Vector *vector)
{
	short RetVal = 1;
	Vector v = *vector;
	
	if( v.size >= v.capacity )
	{
		RetVal = 0;
		// increment vector->capacity and resize the allocated memory accordingly
		v.capacity += (VECTOR_INITIAL_CAPACITY/4);	// 25% of initial capacity
		//v.data = realloc(v.data, v.dataSize * v.capacity);	realloc() is unavailable in FreeRTOS heap handler
		v.data = malloc(v.dataSize * v.capacity);
		if( v.data != NULL )
		{
			// copy the old array in the new one
			memcpy( v.data, vector->data, vector->dataSize * vector->capacity );
			free( vector->data );
			// set the new array
			*vector = v;
			RetVal = 1;
		}
	}	
	
	return RetVal;
}

/*!
** \fn void vector_free(Vector *vector)
** \brief Frees the memory allocated for the data array
** \param vector The Vector struct
** \return None
**/
void vector_free(Vector *vector)
{
	free(vector->data);
}



