/*!
**  \file Utils.c
**  \author Alessandro Vagniluca
**  \date 22/06/2015
**  \brief Utility functions
**  
**  \version 1.0
**/

#include <stdlib.h>
#include <string.h>
#include "Utils.h"
#include "printf-stdarg.h"


char str[STR_MAXLEN+1];
char str1[STR_MAXLEN+1];
char str2[STR_MAXLEN+1];


/*! \fn unsigned char Ascii2Hex( char car )
    \brief Converte in HEX un carattere ASCII 0,1,..,9,A,..,F
	 \param car - carattere da convertire
	 \return Il valore del carattere convertito
*/
unsigned char Ascii2Hex( char car )
{
	if( (car >= '0') && (car <= '9') )
		car -= '0';
	else if( (car >= 'A') && (car <= 'F') )
		car -= ('A' - 0x0A);
	else if( (car >= 'a') && (car <= 'f') )
		car -= ('a' - 0x0A);

	return( (unsigned char)car );
}

/*! \fn char Hex2Ascii( unsigned char byte )
    \brief Converte in ASCII un byte HEX con valore 0,1,..,9,A,..,F
	 \param byte - byte da convertire
	 \return Il carattere corrispondente al byte da convertire
*/
char Hex2Ascii( unsigned char byte )
{
	if( byte <= 9 )
		byte += '0';
	else if( (byte >= 0x0A) && (byte <= 0x0F) )
		byte += ('A' - 0x0A);

	return( (char)byte );
}

// funzioni di conversione numero intero -> stringa

/*
char *itoa(int value, char *string, int radix)

Convert an integer to a string.
The itoa function converts value to a null-terminated character string
and stores the result in string. If radix equals 10 and value is negative,
the first character of the stored string is the minus sign ( � ).
Radix specifies the base of value; radix must be in the range 2 � 36.

Return Value
	A pointer to string. There is no error return.

Parameters
	value - Number to be converted
	string - String result
	radix - Base of value

Remarks
	Radix support: 10 and 16 only
*/
char *itoa(int value, char *string, int radix)
{
   char buf[8];
   char *p;
   short sign = (value < 0);

   if( sign )
   {
		switch( radix )
		{
			case 10:
		      value = -value;
			break;

			case 16:
				value = (int)(65536L+(long)value);
			break;
		}
	}
   p = buf+sizeof(buf);
   *--p = '\0';

   do
   {
		*--p = Hex2Ascii( (BYTE)(value%radix) );
		value /= radix;
   }
   while(value);

   if( sign )
   {
		switch( radix )
		{
			case 10:
		      *--p = '-';
			break;

			case 16:
			break;
		}
	}

   strcpy( string, p );
   return string;
}

/*
char *ultoa(unsigned long value, char *string, int radix)

Convert an unsigned long integer to a string.
The ultoa function converts value to a null-terminated character string
and stores the result in string.
No overflow checking is performed.
Radix specifies the base of value; radix must be in the range 2 � 36.

Return Value
	A pointer to string. There is no error return.

Parameters
	value - Number to be converted
	string - String result
	radix - Base of value

Remarks
	Radix support: 10 only
*/
char *ultoa(unsigned long value, char *string, int radix)
{
   char *p;
   char buf[16];

   p = buf+sizeof(buf);
   *--p = '\0';

   do
   {
      *--p = Hex2Ascii( (BYTE)(value%radix) );
      value /= radix;
   }
   while(value);

   strcpy( string, p );
   return string;
}

/*
char *ltoa(long value, char *string, int radix)

Convert a long integer to a string.
The ltoa function converts value to a null-terminated character string
and stores the result in string. If radix equals 10 and value is negative,
the first character of the stored string is the minus sign ( � ).
Radix specifies the base of value; radix must be in the range 2 � 36.

Return Value
	A pointer to string. There is no error return.

Parameters
	value - Number to be converted
	string - String result
	radix - Base of value

Remarks
	Radix support: 10 only
*/
char *ltoa(long value, char *string, int radix)
{
   char *p;
   char buf[16];
   short sign = (value < 0);

   if( sign )
		value = -value;
   p = ultoa(value, &buf[1], radix);
   if( sign )
   	*--p = '-';

   strcpy( string, p );
   return string;
}


// funzioni di controllo bitmap
// si intende: bit 0-7 nel primo byte, bit 8-15 nel secondo byte, ...
void SetBitBMap( unsigned char *bitmap, unsigned short ibit )
{
	unsigned short ibyte=ibit/8;
	unsigned char msk=ibit%8;

	msk = 0x01 << msk;
	bitmap[ibyte] |= msk;
}
void ResBitBMap( unsigned char *bitmap, unsigned short ibit )
{
	unsigned short ibyte=ibit/8;
	unsigned char msk=ibit%8;

	msk = 0x01 << msk;
	bitmap[ibyte] &= ~msk;
}
void NotBitBMap( unsigned char *bitmap, unsigned short ibit )
{
	unsigned short ibyte=ibit/8;
	unsigned char msk=ibit%8;

	msk = 0x01 << msk;
	bitmap[ibyte] ^= msk;
}
unsigned char GetBitBMap( unsigned char *bitmap, unsigned short ibit )
{
	unsigned short ibyte=ibit/8;
	unsigned char msk=ibit%8;

	msk = 0x01 << msk;
	if( bitmap[ibyte] & msk )
		return 1;
	else
		return 0;
}

// funzioni di conversione Little Endian (Intel) - Big Endian (Motorola)
unsigned short WordLittle2BigEndian( unsigned short w )
{
	BYTE_UNION u;
	unsigned char tmp;

	u.w[0] = w;
	tmp = u.b[0];
	u.b[0] = u.b[1];
	u.b[1] = tmp;

	return u.w[0];
}
unsigned long DWordLittle2BigEndian( unsigned long d )
{
	BYTE_UNION u;
	unsigned char tmp;

	u.d = d;
	tmp = u.b[0];
	u.b[0] = u.b[3];
	u.b[3] = tmp;
	tmp = u.b[1];
	u.b[1] = u.b[2];
	u.b[2] = tmp;

	return u.d;
}

// visualizza un buffer in forma esadecimale
char *VisHexMsg( unsigned char *pb, unsigned short len )
{
	char s1[4];
	unsigned char tronc = 0;

	if( len > STR_MAXLEN/3 )
	{
		len = STR_MAXLEN/3;	// troncamento di sicurezza
		tronc = 1;
	}

	if( len )
	{
		str[0] = '\0';
		for( ; len; len-- )
		{
			if( (len == 1) && tronc )
			{
				mysprintf( s1, "%02X_", (unsigned short)(*pb++) );
			}
			else
			{
				mysprintf( s1, "%02X ", (unsigned short)(*pb++) );
				//itoa( *pb++, s1, 16 );
				//strcat( s1, " " );
			}
			strcat( str, s1 );
		}
		return str;
	}
	else
		return NULL;
}

/*! \fn unsigned char CalcChecksum( unsigned char *pb, unsigned char len )
    \brief Calcolo checksum su un buffer
    \param pb - buffer
    \param len - lunghezza buffer
    \return Il valore calcolato del checksum
*/
unsigned char CalcChecksum( unsigned char *pb, unsigned char len )
{
	unsigned char chk = 0;

	for( ; len; len-- )
	{
		chk += *pb++;
	}

	return chk;
}

