/*!
** \file stringhe_ITA.c
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief ITA strings set
** 
** \version 1.0
**/

#include "stringhe_ITA.h"

//!< string set descriptor for each font group (ISO 3166-1 apha-3)
const char DescMsg_ITA[NUM_FONT_GROUP][MSGDESCLEN] =
{
//	 12345678 (MSGDESCLEN)
	"ITA\0\0\0\0\0",		// FONT_GROUP_EUROPE
	"NTA\0\0\0\0\0",		// FONT_GROUP_CYRILLIC
	"\0\0\0\0\0\0\0\0",		// FONT_GROUP_2
	"\0\0\0\0\0\0\0\0",		// FONT_GROUP_3
};


// week days
static const char sDomenica_ITA[]				= "Domenica";
static const char sLunedi_ITA[]					= "Lunedi";
static const char sMartedi_ITA[]					= "Martedi";
static const char sMercoledi_ITA[]				= "Mercoledi";
static const char sGiovedi_ITA[]					= "Giovedi";
static const char sVenerdi_ITA[]					= "Venerdi";
static const char sSabato_ITA[]					= "Sabato";

static const char sOff_ITA[]						= "Off";
static const char sOn_ITA[]						= "On ";
static const char sAuto_ITA[]						= "Auto";
static const char sDegC_ITA[]						= "\200C";
static const char sDegF_ITA[]						= "\200F";
static const char sH2O_ITA[]						= "H2O";

// stati della stufa
static const char Stufa_Accensione_ITA[] 		= "ACCENSIONE";
static const char Stufa_Accesa_ITA[]			= "ON";
static const char Stufa_Spegnimento_ITA[]		= "SPEGNIMENTO";
static const char Stufa_MCZ_ITA[]				= "OFF";
static const char Stufa_Allarme_ITA[]			= "ALLARME";
static const char Stufa_SpeRete_ITA[]			= "SPEGNIMENTO DOPO BLACKOUT";
static const char Stufa_AccRete_ITA[]			= "ACCENSIONE DOPO BLACKOUT";
static const char Stufa_Reset_ITA[]				= "RESET";
static const char Stufa_Collaudo_ITA[]			= "COLLAUDO";
static const char Stufa_NonDef_ITA[]			= " ";

// allarme
static const char Alarm_ITA[]						= "ALLARME";

// descrizione allarme
static const char AlarmSource00_ITA[]			= "";
static const char AlarmSource01_ITA[]			= "Mancata Accensione";
static const char AlarmSource02_ITA[]			= "Spegnimento Fiamma";
static const char AlarmSource03_ITA[]			= "Surriscaldamento Serbatoio Pellet";
static const char AlarmSource04_ITA[]			= "Temperatura Fumi Eccessiva";
static const char AlarmSource05_ITA[]			= "Allarme Pressostato";
static const char AlarmSource06_ITA[]			= "Allarme Aria Combustione";
static const char AlarmSource07_ITA[]			= "Porta Aperta";
static const char AlarmSource08_ITA[]			= "Guasto Espulsore Fumi";
static const char AlarmSource09_ITA[]			= "Guasto Sonda Fumi";
static const char AlarmSource10_ITA[]			= "Guasto Candeletta";
static const char AlarmSource11_ITA[]			= "Guasto Motore Coclea";
#define AlarmSource12_ITA		AlarmSource00_ITA
static const char AlarmSource13_ITA[]			= "Guasto Scheda Elettronica";
#define AlarmSource14_ITA		AlarmSource00_ITA
static const char AlarmSource15_ITA[]			= "Allarme Livello Pellet";
static const char AlarmSource16_ITA[]			= "Pressione Acqua fuori dai Limiti";
static const char AlarmSource17_ITA[]			= "Porta Aperta Serbatoio Pellet";
static const char AlarmSource18_ITA[]			= "Surriscaldamento Serbatoio Acqua";

// ripristino allarme
static const char AlarmRecovery00_ITA[]			= "";
static const char AlarmRecovery01_ITA[]			= "Pulire Braciere e Ritentare";
static const char AlarmRecovery02_ITA[]			= "Riempire Serbatoio Pellet";
static const char AlarmRecovery03_ITA[]			= "Controllare Libretto Istruzioni";
#define AlarmRecovery04_ITA		AlarmRecovery03_ITA
static const char AlarmRecovery05_ITA[]			= "Liberare da una Eventuale Ostruzione";
static const char AlarmRecovery06_ITA[]			= "Verificare Pulizia Braciere/Ingresso Aria/Canna Fumaria";
static const char AlarmRecovery07_ITA[]			= "Verificare Chiusura Porta";
static const char AlarmRecovery08_ITA[]			= "Chiamare Assistenza";
#define AlarmRecovery09_ITA		AlarmRecovery08_ITA
#define AlarmRecovery10_ITA		AlarmRecovery08_ITA
#define AlarmRecovery11_ITA		AlarmRecovery08_ITA
#define AlarmRecovery12_ITA		AlarmRecovery08_ITA
#define AlarmRecovery13_ITA		AlarmRecovery08_ITA
#define AlarmRecovery14_ITA		AlarmRecovery08_ITA
static const char AlarmRecovery15_ITA[]			= "Verificare Livello Pellet";
static const char AlarmRecovery16_ITA[]			= "Ripristinare la Corretta Pressione dell'Impianto";
static const char AlarmRecovery17_ITA[]			= "Verificare Chiusura Sportello Pellet";
#define AlarmRecovery18_ITA		AlarmRecovery03_ITA

// sleep
static const char Menu_Sleep_ITA[]				= "Sleep";

// avvio
static const char Settings_Time_ITA[]			= "Ora";
static const char Menu_DataOra_ITA[]				= "Data";
static const char Potenza_ITA[]					= "Potenza";
static const char Temperatura_ITA[]				= "Temperatura";
static const char Menu_Idro_ITA[]					= "Temperatura Acqua";
static const char Fan_ITA[]							= "Fan";
	
// network
static const char Menu_Network_ITA[]				= "Network";
//static const char WiFi_ITA[]						= "Wi-Fi";
static const char Ssid_ITA[]						= "SSID";
static const char Keyword_ITA[]					= "Keyword";
//static const char TCPPort_ITA[]					= "TCP Port";
//static const char WPSPin_ITA[]						= "WPS PIN";

// anomalie
static const char Menu_Anomalie_ITA[]				= "Anomalie";
static const char RichiestaService_ITA[]			= "Service";
static const char GuastoSondaTempAria_ITA[]		= "Sonda Temp. Aria Guasta";
static const char GuastoSondaTempAcqua_ITA[]		= "Sonda Temp. Acqua Guasta";
static const char GuastoPressostAcqua_ITA[]		= "Pressostato Acqua Guasto";
static const char SovraPressAcqua_ITA[]				= "Pressione Acqua Oltre i Limiti";
static const char GuastoSensorePortata_ITA[]		= "Guasto Sensore di Portata Aria";
static const char PelletEsaurito_ITA[]				= "Pellet in Esaurimento";
static const char PortaAperta_ITA[]					= "Sportello Aperto";

// info
static const char Menu_Infos_ITA[]				= "Info";
static const char CodiceScheda_ITA[]				= "Codice Scheda";
static const char CodiceSicurezza_ITA[]			= "Codice Sicurezza";
static const char CodicePannello_ITA[]			= "Codice Display";
static const char CodiceParametri_ITA[]			= "Codice Parametri";
static const char OreFunzionamento_ITA[]		= "Ore Funzionamento";
static const char OreSat_ITA[]						= "Ore Service";
static const char Service_ITA[]					= "Assistenza";
static const char VentilatoreFumi_ITA[]			= "Giri Espulsore 1";
static const char PortataAriaMisurata_ITA[]	= "Portata Aria Misurata";
static const char OssigenoResiduo_ITA[]			= "Ossigeno Residuo";
static const char Consumo_ITA[]					= "Consumo Pellet";
static const char TemperaturaFumi_ITA[]			= "Temperatura Fumi";
static const char FotoResistenza_ITA[]			= "Fotoresistenza";
static const char TempoCoclea_ITA[]				= "Tempo Coclea 1";
static const char GiriCoclea_ITA[]				= "Giri Coclea 1";
static const char MotoreScambiatore_ITA[] 		= "Attuazione Fan1";
static const char MotoreScambiatore2_ITA[] 	= "Attuazione Fan2";
static const char IdroPress_ITA[]					= "Pressione Acqua";
static const char NumAccensioni_ITA[]			= "Numero Accensioni";
static const char IPAddress_ITA[]					= "IP Address";
static const char StoricoAllarmi_ITA[]			= "Storico Allarmi";

// menu principale
static const char MainMenu_ITA[]					= "Menu Principale";
static const char Menu_Impostazioni_ITA[]		= "Impostazioni";
static const char Menu_Tecnico_ITA[]				= "Menu Tecnico";

// settings
static const char Lingua_ITA[]						= "Lingua";
static const char EcoMode_ITA[]					= "Eco";
static const char Settings_Backlight_ITA[]		= "Display";
static const char Settings_ShowTemp_ITA[]		= "\200C / \200F";
static const char Precarica_Pellet_ITA[]    	= "Carica Coclea";
static const char Attiva_Pulizia_ITA[]			= "Pulizia";
static const char Attiva_Pompa_ITA[]				= "Attiva Pompa";
static const char RFPanel_ITA[]					= "Identificativo Radio";

// ricette
static const char Ricette_ITA[]						= "Ricette Combustione";
static const char OffsetEspulsore_ITA[]				= "Aria";
static const char RicettaPelletON_ITA[]				= "Pellet";
static const char Ossigeno_ITA[]						= "Ossigeno";

// temp H2O
static const char TempH2O_SetRisc_ITA[]			= "Set Riscaldamento";
static const char TempH2O_SetSan_ITA[]			= "Set Sanitari";

// menu nascosto
static const char HidPassword_ITA[]				= "Password";

// menu tecnico - livello 1					012345678901234567890
static const char ConfigSystem_ITA[]				= "Configurazione";
static const char Controllo_ITA[]					= "Controllo";
static const char MenuIdro_ITA[]					= "Idro";
static const char AttuaTransitorie_ITA[]		= "Accensione/Spegnim.";
static const char AttuaPotenza_ITA[]				= "Potenza";
static const char GestAllarmi_ITA[]				= "Gestione Allarmi";
static const char Collaudo_ITA[]					= "Collaudo";
static const char RicercaCodParam_ITA[]			= "Ricerca per Codice";

// menu tecnico - livello 2							012345678901234567890
static const char MenuParamGen_ITA[]						= "Impostazioni";
static const char Blackout_ITA[]							= "Blackout";
static const char MenuCoclea_ITA[]						= "Coclea";
static const char MenuScuotitore_ITA[]					= "Pulitore/Scuotitore";
static const char FanAmbiente_ITA[]						= "Fan Ambiente";
static const char Fan1_ITA[]								= "Fan 1";
static const char Fan2_ITA[]								= "Fan 2";
static const char Fan3_ITA[]								= "Fan 3";

static const char MenuEco_ITA[]							= "Funzione Eco";
static const char TempiFunz_ITA[]							= "Tempi Funzionamento";

static const char ParamTransitori_ITA[]					= "Impostazioni";
static const char Preacc1_ITA[]							= "Preaccensione 1";
static const char Preacc2_ITA[]							= "Preaccensione 2";
static const char PreaccCaldo_ITA[]						= "Preaccensione a Caldo";
static const char AccA_ITA[]								= "Accensione A";
static const char AccB_ITA[]								= "Accensione B";
static const char FireON_ITA[]								= "Fire ON";
static const char SpeA_ITA[]								= "Spegnimento A";
static const char SpeB_ITA[]								= "Spegnimento B";
static const char Raff_ITA[]								= "Raffreddamento";

static const char ParamPotenza_ITA[]						= "Parametri di Potenza";
static const char MenuPB_ITA[]								= "Ciclo Pulizia";
static const char RitardoAttuaEsp_ITA[]					= "Ritardo Espulsore";
static const char RitardoStepPot_ITA[]					= "Ritardo Step Potenza";
static const char Pot1_ITA[]								= "Potenza 1";
static const char Pot2_ITA[]								= "Potenza 2";
static const char Pot3_ITA[]								= "Potenza 3";
static const char Pot4_ITA[]								= "Potenza 4";
static const char Pot5_ITA[]								= "Potenza 5";
static const char Pot6_ITA[]								= "Potenza 6";
static const char Pulizia_ITA[]							= "Pulizia";
static const char Interpolazione_ITA[]					= "Interpolazione";

static const char AllarmeFumi_ITA[]						= "Allarme Fumi";
static const char SensPellet_ITA[]						= "Sensore Pellet";
// static const char AssenzaFiamma_ITA[]					= "Assenza Fiamma";
static const char SensAria_ITA[]							= "Portata Aria";

static const char Comandi_ITA[]							= "Comandi";
static const char Carichi_ITA[]							= "Carichi";
static const char Attuazioni_ITA[]						= "Attuazioni";
	
// parametri generali
static const char TipoStufa_ITA[]						= "Tipo Stufa";
static const char TipoStufaDef_ITA[]					= "Ripristino Tipo Stufa Default";
static const char MotoreEspulsore_ITA[]				= "Tipo Motore 1";
static const char MotoreEspulsore2_ITA[]			= "Tipo Motore 2";
static const char Multifan_ITA[]						= "Numero Fan";
static const char IdroMode_ITA[]						= "Abilitazione Idro";
static const char VisSanitari_ITA[]					= "Vis. Sanitari";
static const char VisTipoStufa_ITA[]					= "Vis. Tipo Stufa";
static const char SensorePortataAria_ITA[]			= "Controllo Portata";
static const char SensoreHall_ITA[]					= "Controllo Giri";
static const char ControlloLambda_ITA[]				= "Controllo Lambda";
static const char LimGiriMinimi_ITA[]				= "Limitatore Giri Minimi";
static const char AccTermocoppia_ITA[]				= "Termocoppia";
static const char AccFotoResistenza_ITA[]			= "Fotoresistenza";
static const char InibAccB_ITA[]						= "Inibizione Acc B";
//static const char RampaCandeletta_ITA[]				= "Rampa Candeletta";
static const char Termostato_Ambiente_ITA[] 		= "Termostato";
static const char GestioneIbrida_ITA[]		 		= "Modalita' Fire/Temp";
//static const char GestioneLegna_ITA[]		 		= "Gestione Legna/Pellet";
static const char FScuotitore_ITA[]					= "Pulizia Meccanica";
static const char Coclea2_ITA[]						= "Coclea 2 / Pulitore";
static const char Esp2Enable_ITA[]					= "Abilitazione Espulsore 2";
static const char SensoreHall_2_ITA[]				= "Controllo Giri Espulsore 2";
static const char SensorePellet_ITA[]				= "Sensore Livello Pellet";

// parametri blackout
static const char BlackoutRipristino_ITA[]			= "Abilitazione Ripristino";
static const char BlackoutDurata_ITA[]				= "Durata Ripristino";

// parametri coclea
static const char FrenataCoclea_ITA[]				= "Frenata Coclea 1";
static const char TipoFrenataCoclea_ITA[]			= "Tipo Frenata Coclea 1";
static const char FrenataCoclea_2_ITA[]				= "Frenata Coclea 2";
static const char TipoFrenataCoclea_2_ITA[]		= "Tipo Frenata Coclea 2";
static const char CtrlGiriCoclea_ITA[]				= "Controllo Giri Coclea 1";
static const char PeriodoCoclea_ITA[]				= "Periodo Coclea 1";
static const char PeriodoCoclea2_ITA[]				= "Periodo Coclea 2";
static const char AbilPeriodoCoclea_ITA[]			= "Abilitazione Periodo Coclea 1";
static const char AbilPeriodoCoclea2_ITA[]			= "Abilitazione Periodo Coclea 2";

// parametri scuotitore
static const char ScuotDurata_ITA[]					= "Durata Semi-Ciclo";
static const char ScuotNCicli_ITA[]					= "Numero Cicli";
//static const char ScuotPeriodo_ITA[]					= "Intervallo Cicli";

// parametri fan ambiente
static const char FanCtrlTempFumi_ITA[]				= "Controllo da Temp. Fumi";
static const char FanTempFumiON_ITA[]				= "Temp. On Fan";
static const char FanTempFumiOFF_ITA[]				= "Temp. Fan Min.";

// parametri fan 
static const char FanAttuaL1_ITA[]					= "Livello 1";
static const char FanAttuaL2_ITA[]					= "Livello 2";
static const char FanAttuaL3_ITA[]					= "Livello 3";
static const char FanAttuaL4_ITA[]					= "Livello 4";
static const char FanAttuaL5_ITA[]					= "Livello 5";

// parametri ricette

// parametri eco
static const char EcoStop_ITA[]						= "Ecostop";
static const char EcoAttesaAccensione_ITA[]		= "Attesa On";
static const char Settings_TShutdown_Eco_ITA[] 	= "Attesa Off";
static const char EcoDeltaTemperatura_ITA[]		= "Delta Temp.";

// parametri tempi funzionamento

// parametri idro
static const char IdroIndipendente_ITA[]			= "Idro Indipendente";
static const char SpegnimentoIdro_ITA[]				= "Spegnimento Idro";
//static const char FAccumulo_ITA[]						= "Accumulo";
static const char InibSensing_ITA[]					= "Inibizione Sensing Pompa";
static const char PompaModulante_ITA[]				= "Pompa Modulante";
static const char FPressIdro_ITA[]					= "Pressostato Acqua";
//static const char FlussostatoDigitale_ITA[] 		= "Flussostato Secondario";
static const char GainRisc_ITA[]						= "Guadagno Riscaldamento";
static const char IsteresiTempAcqua_ITA[]			= "Isteresi Temp. Acqua";
static const char DeltaSanitari_ITA[]				= "Delta Sanitari";
static const char GainSanit_ITA[]						= "Guadagno Sanitari";
static const char TempAux_ITA[]						= "Temp. Attivazione AUX 2";
static const char MaxPressH2O_ITA[]					= "Pressione Max Acqua";
static const char TSETON_Pompa_ITA[]					= "Temp. On Pompa";
static const char TSETOFF_Pompa_ITA[]				= "Temp. Off Pompa";

// parametri transitori
static const char AccTempoPreacc_ITA[]				= "Durata Preacc. 1";
static const char AccTempoPreLoad_ITA[]				= "Durata Preacc. 2";
static const char AccTempoPreacc2_ITA[]				= "Durata Preacc. a Caldo";
static const char AccTempOnFumi_ITA[]				= "Temp. Fumi On";
static const char AccTempOffFumi_ITA[]				= "Temp. Fumi Off";
static const char AccTempSogliaFumi_ITA[]			= "Soglia Legna";
static const char AccTempoInc_ITA[]					= "Attesa per Acc. a Caldo";
static const char AccDeltaTempCaldo_ITA[]			= "Delta Temp. Caldo";
static const char AccMaxTimeWarmUp_ITA[]			= "Durata Max. Accensione";
static const char AccMaxTimeFireOn_ITA[]			= "Durata Fire On";
static const char AccMaxTimeSpe_ITA[]				= "Durata Spegnimento";
	
// parametri di attuazione
static const char AttuaTempoOnCoclea1_ITA[]		= "T. On Coclea 1";
static const char AttuaTempoOffCoclea1_ITA[]		= "T. Off Coclea 1";
static const char AttuaPortata_ITA[]					= "Portata Aria";
static const char AttuaGiriEsp_ITA[]					= "Giri Espulsore 1";
static const char AttuaGiriEsp2_ITA[]				= "Giri Espulsore 2";
static const char AttuaTempoOnCoclea2_ITA[]		= "T. On Coclea 2";
static const char AttuaTempoOffCoclea2_ITA[]		= "T. Off Coclea 2";
static const char AttuaEspulsore2_ITA[]				= "Espulsore 2";
	
// parametri di potenza
static const char LivPotenzaMax_ITA[]				= "Livelli di Potenza";
	
// parametri pulizia braciere
static const char AttesaPB_ITA[]						= "Intervallo";
static const char DurataPB_ITA[]						= "Durata";
static const char MinPowerPB_ITA[]					= "Soglia";
	
// parametri ritardo espulsore
static const char RitardoEspInc_ITA[]				= "Ritardo Incremento";
static const char RitardoEspDec_ITA[]				= "Ritardo Decremento";
	
// parametri ritardo step potenza
static const char RitardoPotInc_ITA[]				= "Ritardo Incremento";
static const char RitardoPotDec_ITA[]				= "Ritardo Decremento";
	
// parametri allarme fumi
static const char AlmFumi_InibPreAlm_ITA[]			= "Inibizione Pre-Allarme";
static const char AlmFumi_TempPreAlm_ITA[]			= "T.Fumi Pre-Allarme";
static const char AlmFumi_DurataPreAlm_ITA[]		= "Durata Max Pre-Allarme";
static const char AlmFumi_GradientePreAlm_ITA[]	= "Isteresi Pre-Allarme";
static const char AlmFumi_TempAlm_ITA[]				= "T.Fumi Allarme";
	
// parametri sensore pellet
static const char SensPellet_DurataPreAlm_ITA[]	= "Durata Pre-Allarme";
	
// parametri assenza fiamma
static const char NoFiamma_DeltaTemp_ITA[]			= "Delta Fumi Off";
	
// parametri sensore portata aria
static const char Aria_PortataCritica_ITA[]			= "Soglia Porta Aperta";
static const char Aria_TempoAlmPorta_ITA[]				= "Durata Porta Aperta";
static const char Aria_TempoPreAlmPorta_ITA[]			= "Durata Pre-Allarme Porta";
static const char Aria_InibAlmAriaCombust_ITA[]		= "Inibizione Allarme Aria Combustione";
static const char Aria_TempoPreAlmAriaCombust_ITA[]	= "Durata Pre-Allarme Aria Combustione";
static const char Aria_DeltaPortAriaCombust_ITA[]	= "Delta Portata Aria Combustione";
	
// comandi
static const char Test_StartStop_ITA[]				= "Collaudo Manuale";
static const char Test_Sequenza_ITA[]				= "Collaudo Automatico";
static const char Test_Bypass_ITA[]					= "Bypass";
static const char Test_CalibTC_ITA[]					= "Taratura Termocoppia";
static const char Test_CalibFotoresOn_ITA[]		= "Taratura Fotoresistenza On";
static const char Test_CalibFotoresOff_ITA[]		= "Taratura Fotoresistenza Off";
static const char Test_DurataStep_ITA[]				= "Durata Step Collaudo";
	
// carichi
static const char Test_Coclea_ITA[]					= "Coclea 1";
static const char Test_Candeletta_ITA[]				= "Candeletta";
static const char Test_Fan1_ITA[]						= "Fan 1";
static const char Test_Fan2_ITA[]						= "Fan 2";
static const char Test_Fan3_ITA[]						= "Fan 3";
static const char Test_Espulsore_ITA[]				= "Espulsore 1";
static const char Test_Pompa_ITA[]					= "Pompa";
static const char Test_3Vie_ITA[]						= "3-Vie";
static const char Test_Aux1_ITA[]						= "Aux 1";
static const char Test_Aux2_ITA[]						= "Aux 2";
static const char Test_AuxA_ITA[]						= "Aux A";
	
// attuazioni
static const char AttuaFan1_ITA[]						= "Attuazione Fan 1";
static const char AttuaFan2_ITA[]						= "Attuazione Fan 2";
static const char AttuaFan3_ITA[]						= "Attuazione Fan 3";

// richieste di conferma
static const char ReqConf_ITA[]						= "Conferma?";
static const char TC_ReqConf_ITA[]					= "Corto circuitare Morsettiera JE e DOPO Abilitare";

// Crono
static const char Menu_Crono_ITA[]					= "Crono";
static const char Abilitazione_ITA[]					= "Abilitazione";
static const char CaricaProfilo_ITA[]				= "Carica Profilo";
static const char Azzera_ITA[]							= "Azzeramento";

// prog. crono settimanale
static const char PrgSettimanale_ITA[]				= "Programma";
static const char PrgAbilita_ITA[]					= "Abilitazione";
static const char PrgStart_ITA[]						= "Start";
static const char PrgStop_ITA[]						= "Stop";
static const char PrgTempAria_ITA[]					= "Temperatura Aria";
static const char PrgTempH2O_ITA[]					= "Temperatura Acqua";
static const char PrgPotenza_ITA[]					= "Fire";
	
// sigle fan
//													   12
static const char sF1_ITA[]							= "F1";
static const char sF2_ITA[]							= "F2";

// stati di attuazione
//													   1234
static const char sAttuaStat00_ITA[]				= " OFF";
static const char sAttuaStat01_ITA[]				= "PSU1";
static const char sAttuaStat02_ITA[]				= "PSU2";
static const char sAttuaStat03_ITA[]				= "WPSU";
static const char sAttuaStat04_ITA[]				= "SU A";
static const char sAttuaStat05_ITA[]				= "SU B";
static const char sAttuaStat06_ITA[]				= "SU C";
static const char sAttuaStat07_ITA[]				= "FONA";
static const char sAttuaStat08_ITA[]				= "FONB";
static const char sAttuaStat09_ITA[]				= "SD A";
static const char sAttuaStat10_ITA[]				= "SD B";
static const char sAttuaStat11_ITA[]				= "SD C";
static const char sAttuaStat12_ITA[]				= "CD A";
static const char sAttuaStat13_ITA[]				= "CD B";
static const char sAttuaStat14_ITA[]				= "BCLA";
static const char sAttuaStat15_ITA[]				= "BCLB";
static const char sAttuaStat16_ITA[]				= "PL 1";
static const char sAttuaStat17_ITA[]				= "PL 2";
static const char sAttuaStat18_ITA[]				= "PL 3";
static const char sAttuaStat19_ITA[]				= "PL 4";
static const char sAttuaStat20_ITA[]				= "PL 5";
static const char sAttuaStat21_ITA[]				= "PL 6";
static const char sAttuaStat22_ITA[]				= "PL 7";
static const char sAttuaStat23_ITA[]				= "PL 8";
static const char sAttuaStat24_ITA[]				= "PL 9";
static const char sAttuaStat25_ITA[]				= "PLSA";



// ITA strings set
const PCSTR pStr_ITA[NUMMSG] =
{
	// week days
	sDomenica_ITA,
	sLunedi_ITA,
	sMartedi_ITA,
	sMercoledi_ITA,
	sGiovedi_ITA,
	sVenerdi_ITA,
	sSabato_ITA,

	sOff_ITA,
	sOn_ITA,
	sAuto_ITA,
	sDegC_ITA,
	sDegF_ITA,
	sH2O_ITA,

	// stati della stufa
	Stufa_Accensione_ITA,
	Stufa_Accesa_ITA,
	Stufa_Spegnimento_ITA,
	Stufa_MCZ_ITA,
	Stufa_Allarme_ITA,
	Stufa_SpeRete_ITA,
	Stufa_AccRete_ITA,
	Stufa_Reset_ITA,
	Stufa_Collaudo_ITA,
	Stufa_NonDef_ITA,

	// allarme
	Alarm_ITA,

	// descrizione allarme
	AlarmSource00_ITA,
	AlarmSource01_ITA,
	AlarmSource02_ITA,
	AlarmSource03_ITA,
	AlarmSource04_ITA,
	AlarmSource05_ITA,
	AlarmSource06_ITA,
	AlarmSource07_ITA,
	AlarmSource08_ITA,
	AlarmSource09_ITA,
	AlarmSource10_ITA,
	AlarmSource11_ITA,
	AlarmSource12_ITA,
	AlarmSource13_ITA,
	AlarmSource14_ITA,
	AlarmSource15_ITA,
	AlarmSource16_ITA,
	AlarmSource17_ITA,
	AlarmSource18_ITA,

	// ripristino allarme
	AlarmRecovery00_ITA,
	AlarmRecovery01_ITA,
	AlarmRecovery02_ITA,
	AlarmRecovery03_ITA,
	AlarmRecovery04_ITA,
	AlarmRecovery05_ITA,
	AlarmRecovery06_ITA,
	AlarmRecovery07_ITA,
	AlarmRecovery08_ITA,
	AlarmRecovery09_ITA,
	AlarmRecovery10_ITA,
	AlarmRecovery11_ITA,
	AlarmRecovery12_ITA,
	AlarmRecovery13_ITA,
	AlarmRecovery14_ITA,
	AlarmRecovery15_ITA,
	AlarmRecovery16_ITA,
	AlarmRecovery17_ITA,
	AlarmRecovery18_ITA,

	// sleep
	Menu_Sleep_ITA,

	// avvio
	Settings_Time_ITA,
	Menu_DataOra_ITA,
	Potenza_ITA,
	Temperatura_ITA,
	Menu_Idro_ITA,
	Fan_ITA,
	
	// network
	Menu_Network_ITA,
	//WiFi_ITA,
	Ssid_ITA,
	Keyword_ITA,
	//TCPPort_ITA,
	//WPSPin_ITA,
	
	// anomalie
	Menu_Anomalie_ITA,
	RichiestaService_ITA,
	GuastoSondaTempAria_ITA,	
	GuastoSondaTempAcqua_ITA,	
	GuastoPressostAcqua_ITA,	
	SovraPressAcqua_ITA,	
	GuastoSensorePortata_ITA,
	PelletEsaurito_ITA,
	PortaAperta_ITA,

	// info
	Menu_Infos_ITA,
	CodiceScheda_ITA,
	CodiceSicurezza_ITA,
	CodicePannello_ITA,
	CodiceParametri_ITA,
	OreFunzionamento_ITA,
	OreSat_ITA,
	Service_ITA,
	VentilatoreFumi_ITA,
	PortataAriaMisurata_ITA,
	OssigenoResiduo_ITA,
	Consumo_ITA,
	TemperaturaFumi_ITA,
	FotoResistenza_ITA,
	TempoCoclea_ITA,
	GiriCoclea_ITA,
	MotoreScambiatore_ITA,
	MotoreScambiatore2_ITA,
	IdroPress_ITA,
	NumAccensioni_ITA,
	IPAddress_ITA,
	StoricoAllarmi_ITA,
	
	// menu principale
	MainMenu_ITA,
	Menu_Impostazioni_ITA,
	Menu_Tecnico_ITA,

	// settings
	Lingua_ITA,
	EcoMode_ITA,
	Settings_Backlight_ITA,
	Settings_ShowTemp_ITA,
	Precarica_Pellet_ITA,
	Attiva_Pulizia_ITA,
	Attiva_Pompa_ITA,
	RFPanel_ITA,

	// ricette
	Ricette_ITA,
	OffsetEspulsore_ITA,
	RicettaPelletON_ITA,
	Ossigeno_ITA,

	// temp H2O
	TempH2O_SetRisc_ITA,
	TempH2O_SetSan_ITA,

	// menu nascosto
	HidPassword_ITA,

	// menu tecnico - livello 1
	ConfigSystem_ITA,
	Controllo_ITA,
	MenuIdro_ITA,
	AttuaTransitorie_ITA,
	AttuaPotenza_ITA,
	GestAllarmi_ITA,
	Collaudo_ITA,
	RicercaCodParam_ITA,
	
	// menu tecnico - livello 2
	MenuParamGen_ITA,
	Blackout_ITA,
	MenuCoclea_ITA,
	MenuScuotitore_ITA,
	FanAmbiente_ITA,
	Fan1_ITA,
	Fan2_ITA,
	Fan3_ITA,
	
	MenuEco_ITA,
	TempiFunz_ITA,
	
	ParamTransitori_ITA,
	Preacc1_ITA,
	Preacc2_ITA,
	PreaccCaldo_ITA,
	AccA_ITA,
	AccB_ITA,
	FireON_ITA,
	SpeA_ITA,
	SpeB_ITA,
	Raff_ITA,
	
	ParamPotenza_ITA,
	MenuPB_ITA,
	RitardoAttuaEsp_ITA,
	RitardoStepPot_ITA,
	Pot1_ITA,
	Pot2_ITA,
	Pot3_ITA,
	Pot4_ITA,
	Pot5_ITA,
	Pot6_ITA,
	Pulizia_ITA,
	Interpolazione_ITA,
	
	AllarmeFumi_ITA,
	SensPellet_ITA,
	// AssenzaFiamma_ITA,
	SensAria_ITA,
	
	Comandi_ITA,
	Carichi_ITA,
	Attuazioni_ITA,
	
	// parametri generali
	TipoStufa_ITA,
	TipoStufaDef_ITA,
	MotoreEspulsore_ITA,
	MotoreEspulsore2_ITA,
	Multifan_ITA,
	IdroMode_ITA,
	VisSanitari_ITA,
	VisTipoStufa_ITA,
	SensorePortataAria_ITA,
	SensoreHall_ITA,
	ControlloLambda_ITA,
	LimGiriMinimi_ITA,
	AccTermocoppia_ITA,
	AccFotoResistenza_ITA,
	InibAccB_ITA,
	//RampaCandeletta_ITA,
	Termostato_Ambiente_ITA,
	GestioneIbrida_ITA,
	//GestioneLegna_ITA,
	FScuotitore_ITA,
	Coclea2_ITA,
	Esp2Enable_ITA,
	SensoreHall_2_ITA,
	SensorePellet_ITA,
	
	// parametri blackout
	BlackoutRipristino_ITA,
	BlackoutDurata_ITA,

	// parametri coclea
	FrenataCoclea_ITA,
	TipoFrenataCoclea_ITA,
	FrenataCoclea_2_ITA,
	TipoFrenataCoclea_2_ITA,
	CtrlGiriCoclea_ITA,
	PeriodoCoclea_ITA,
	PeriodoCoclea2_ITA,
	AbilPeriodoCoclea_ITA,
	AbilPeriodoCoclea2_ITA,

	// parametri scuotitore
	ScuotDurata_ITA,
	ScuotNCicli_ITA,
	//ScuotPeriodo_ITA,

	// parametri fan ambiente
	FanCtrlTempFumi_ITA,
	FanTempFumiON_ITA,
	FanTempFumiOFF_ITA,

	// parametri fan 
	FanAttuaL1_ITA,
	FanAttuaL2_ITA,
	FanAttuaL3_ITA,
	FanAttuaL4_ITA,
	FanAttuaL5_ITA,

	// parametri ricette

	// parametri eco
	EcoStop_ITA,
	EcoAttesaAccensione_ITA,
	Settings_TShutdown_Eco_ITA,
	EcoDeltaTemperatura_ITA,

	// parametri tempi funzionamento

	// parametri idro
	IdroIndipendente_ITA,	
	SpegnimentoIdro_ITA,	
	//FAccumulo_ITA,
	InibSensing_ITA,
	PompaModulante_ITA,
	FPressIdro_ITA,
	//FlussostatoDigitale_ITA,
	GainRisc_ITA,			
	IsteresiTempAcqua_ITA,
	DeltaSanitari_ITA,		
	GainSanit_ITA,			
	TempAux_ITA,
	MaxPressH2O_ITA,
	TSETON_Pompa_ITA,
	TSETOFF_Pompa_ITA,

	// parametri transitori
	AccTempoPreacc_ITA,
	AccTempoPreLoad_ITA,
	AccTempoPreacc2_ITA,
	AccTempOnFumi_ITA,
	AccTempOffFumi_ITA,
	AccTempSogliaFumi_ITA,
	AccTempoInc_ITA,
	AccDeltaTempCaldo_ITA,
	AccMaxTimeWarmUp_ITA,
	AccMaxTimeFireOn_ITA,
	AccMaxTimeSpe_ITA,
	
	// parametri di attuazione
	AttuaTempoOnCoclea1_ITA,	
	AttuaTempoOffCoclea1_ITA,
	AttuaPortata_ITA,			
	AttuaGiriEsp_ITA,			
	AttuaGiriEsp2_ITA,			
	AttuaTempoOnCoclea2_ITA,	
	AttuaTempoOffCoclea2_ITA,
	AttuaEspulsore2_ITA,
	
	// parametri di potenza
	LivPotenzaMax_ITA,
	
	// parametri pulizia braciere
	AttesaPB_ITA,
	DurataPB_ITA,
	MinPowerPB_ITA,
	
	// parametri ritardo
	RitardoEspInc_ITA,
	RitardoEspDec_ITA,
	
	// parametri ritardo step potenza
	RitardoPotInc_ITA,
	RitardoPotDec_ITA,
	
	// parametri allarme fumi
	AlmFumi_InibPreAlm_ITA,
	AlmFumi_TempPreAlm_ITA,
	AlmFumi_DurataPreAlm_ITA,
	AlmFumi_GradientePreAlm_ITA,
	AlmFumi_TempAlm_ITA,
	
	// parametri sensore pellet
	SensPellet_DurataPreAlm_ITA,
	
	// parametri assenza fiamma
	NoFiamma_DeltaTemp_ITA,
	
	// parametri sensore portata aria
	Aria_PortataCritica_ITA,
	Aria_TempoAlmPorta_ITA,
	Aria_TempoPreAlmPorta_ITA,
	Aria_InibAlmAriaCombust_ITA,
	Aria_TempoPreAlmAriaCombust_ITA,
	Aria_DeltaPortAriaCombust_ITA,
	
	// comandi
	Test_StartStop_ITA,
	Test_Sequenza_ITA,
	Test_Bypass_ITA,
	Test_CalibTC_ITA,
	Test_CalibFotoresOn_ITA,
	Test_CalibFotoresOff_ITA,
	Test_DurataStep_ITA,
	
	// carichi
	Test_Coclea_ITA,
	Test_Candeletta_ITA,	
	Test_Fan1_ITA,
	Test_Fan2_ITA,
	Test_Fan3_ITA,
	Test_Espulsore_ITA,
	Test_Pompa_ITA,
	Test_3Vie_ITA,
	Test_Aux1_ITA,
	Test_Aux2_ITA,
	Test_AuxA_ITA,
	
	// attuazioni
	AttuaFan1_ITA,
	AttuaFan2_ITA,
	AttuaFan3_ITA,

	// richieste di conferma
	ReqConf_ITA,
	TC_ReqConf_ITA,
	
	// Crono
	Menu_Crono_ITA,
	Abilitazione_ITA,
	CaricaProfilo_ITA,
	Azzera_ITA,	

	// prog. crono settimanale
	PrgSettimanale_ITA,
	PrgAbilita_ITA,
	PrgStart_ITA,	
	PrgStop_ITA,	
	PrgTempAria_ITA,	
	PrgTempH2O_ITA,	
	PrgPotenza_ITA,	
	
	// sigle fan
	sF1_ITA,
	sF2_ITA,

	// stati di attuazione
	sAttuaStat00_ITA,
	sAttuaStat01_ITA,
	sAttuaStat02_ITA,
	sAttuaStat03_ITA,
	sAttuaStat04_ITA,
	sAttuaStat05_ITA,
	sAttuaStat06_ITA,
	sAttuaStat07_ITA,
	sAttuaStat08_ITA,
	sAttuaStat09_ITA,
	sAttuaStat10_ITA,
	sAttuaStat11_ITA,
	sAttuaStat12_ITA,
	sAttuaStat13_ITA,
	sAttuaStat14_ITA,
	sAttuaStat15_ITA,
	sAttuaStat16_ITA,
	sAttuaStat17_ITA,
	sAttuaStat18_ITA,
	sAttuaStat19_ITA,
	sAttuaStat20_ITA,
	sAttuaStat21_ITA,
	sAttuaStat22_ITA,
	sAttuaStat23_ITA,
	sAttuaStat24_ITA,
	sAttuaStat25_ITA,

	
};

