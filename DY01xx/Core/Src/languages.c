/*!
** \file languages.c
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief Common strings to all languages
** 
** \version 1.0
**/

#include "languages.h"
#include "stringhe_ITA.h"
#include "stringhe_ENG.h"


unsigned char NumLanguage;	// total number of languages

/*!
** \var StringSet_RawData_Header
** \brief The string set reference structure pointer.
**/
MSG_TAB *pStringSet_RawData_Header;

const char sBitmapLoadError[] = "BITMAP LOAD ERROR: %u\r\n";

const PPCSTR pSetMsg[NROMLANG] =
{
	(PPCSTR)pStr_ITA,		// Italiano - Ita
	(PPCSTR)pStr_ENG,		// Inglese - Eng
};

const char pChLogoDefault[] = "\xCA\xCA\xCA\xCA\xCA\xCA\xCA\xCA\xCA\xCA\xCA\xCA\xCA\xCA\xCA\xCA";

char pChLogo[LENGTH_LOGO];
char pChTipoDesc[NTYPE][LENGTH_TYPEDESC];
//																012345678901234567890
const char sBlank[DISPLAY_MAX_COL+1]			= "                     ";

const char sOPEN[]	= "Open ";
const char sCLOSE[]	= "Close";
const char sSubMenu[] = "----->";
const char sDescParInit[] = "C000";	// descrizione parametro a inizio immissione

const char sESM[]	= "ESM";

// giorni abbreviati della settimana in bulgaro
const char * const BULwday[7] =
{
//	 12
	"Hg",		// Do
	"Sh",		// Lu
	"Bt",		// Ma
	"Cp",		// Me
	"Yt",		// Gi
	"St",		// Ve
	"Cd",		// Sa
};


