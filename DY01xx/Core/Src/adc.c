/*!
	\file adc.c
	\brief Modulo adc.c

	Gestione ADC.

*/

#include "main.h"
#include "adc.h"

/* variabili inizializzate */


/* variabili non inizializzate */

/*! \var Anain
    \brief Struttura dati A/D converter.
*/
ANAIN_STRUCT Anain;

static ANAIN_STRUCT AinValAccu;	//!< accumulatori delle letture
static unsigned char nLetture;	//!<; contatore letture

/* Variable used to get converted value */
__IO uint16_t ADCxConvertedValue[NADCCH];


/* costanti */


/* codice */

static void adcFiltro4Ana( unsigned short *Dest, unsigned short *Src );
//static void adcFiltro4Dig( unsigned short *Dest, unsigned short *Src );





/*! \fn void adcInit( void )
    \brief Inizializzazione gestore ADC
*/
void adcInit( void )
{
	// azzero accumulatori delle letture
	memset( &AinValAccu, 0, sizeof(AinValAccu) );
	nLetture = 0;

	// alimenta VBATT OPA
	HAL_GPIO_WritePin(EN_V_BATT_GPIO_Port, EN_V_BATT_Pin, GPIO_PIN_SET);
}

/*! \fn void adcStart( void )
    \brief Avvio scan ingressi ADC
*/
void adcStart( void )
{
	// avvia conversione
	memset( (void *)ADCxConvertedValue, 0, sizeof(ADCxConvertedValue) );
	
	if( HAL_ADC_Start_DMA(&hadc1, (uint32_t *)ADCxConvertedValue, NADCCH) != HAL_OK )
	{
		HAL_ADC_ErrorCallback(&hadc1);
	}
}

/**
  * @brief  Error ADC callback.
  * @param  hadc: pointer to a ADC_HandleTypeDef structure that contains
  *         the configuration information for the specified ADC.
  * @retval None
  */
void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc)
{
	HAL_ADC_Stop_DMA( hadc );
	// disalimenta VBATT OPA
	//HAL_GPIO_WritePin(EN_V_BATT_GPIO_Port, EN_V_BATT_Pin, GPIO_PIN_RESET);
	// segnalo fine conversione
	ELAB_INP = 1;
}

/**
  * @brief  Regular conversion complete callback in non blocking mode
  * @param  hadc: pointer to a ADC_HandleTypeDef structure that contains
  *         the configuration information for the specified ADC.
  * @retval None
  */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	// disalimenta VBATT OPA
	//HAL_GPIO_WritePin(EN_V_BATT_GPIO_Port, EN_V_BATT_Pin, GPIO_PIN_RESET);
	
	AinValAccu.VBatt += (ADCxConvertedValue[0] & 0x0FFF);	// accumulo lettura
	AinValAccu.VrefInt += (ADCxConvertedValue[1] & 0x0FFF);	// accumulo lettura
	AinValAccu.TempSensor += (ADCxConvertedValue[2] & 0x0FFF);	// accumulo lettura
	
	if( ++nLetture >= ACCU_NUM_LETT )
	{
		// fine accumulo
		nLetture = 0;

		// filtraggio valori accumulati e aggiornamento in tabella
		adcFiltro4Ana( &Anain.VBatt, &AinValAccu.VBatt );
		adcFiltro4Ana( &Anain.VrefInt, &AinValAccu.VrefInt );
		//adcFiltro4Ana( &Anain.TempSensor, &AinValAccu.TempSensor ); NO FILTRO PER TEMP.SENSOR
		Anain.TempSensor = AinValAccu.TempSensor;

		// azzero accumulatori delle letture
		memset( &AinValAccu, 0, sizeof(AinValAccu) );
	}
	// attesa prossimo avvio scan ingressi ADC
	//HAL_ADC_Stop_DMA( hadc );

	// segnalo nuovi dati acquisiti
	ELAB_INP = 1;
}

/*! \fn void adcFiltro4Ana( unsigned short *Dest, unsigned short *Src )
    \brief Filtraggio valore accumulato ingresso analogico
    \param Dest puntatore al valore filtrato
    \param Src puntatore al valore da filtrare
	\note Formula del filtro digitale al passo k:
	
		Z(k) = (NF * Z(k-1) + x(k)) / (NF + 1)
*/
static void adcFiltro4Ana( unsigned short *Dest, unsigned short *Src )
{
	#define NF		31		// grado del filtraggio (0: disabilitato)
	unsigned long d = (*Dest);

	d *= NF;
	d += (unsigned long)(*Src);
	/*
		Arrotondamento di un rapporto X/Y:
		
		((10*X)/Y + 5)/10 = ((10*X) + (5*Y)) / (10*Y)
	*/
	d *= 10;
	d += (5*(NF + 1));
	d /= (10*(NF + 1));
	
	*Dest = (unsigned short)d;
}

/*! \fn void adcFiltro4Dig( unsigned short *Dest, unsigned short *Src )
    \brief Filtraggio valore accumulato ingresso digitale
    \param Dest puntatore al valore filtrato
    \param Src puntatore al valore da filtrare
*/
/*static void adcFiltro4Dig( unsigned short *Dest, unsigned short *Src )
{
	unsigned short w;

	w = (*Src)/32;
	*Dest = LOBYTE( w );
}*/

