/*!
	\file OT_Plus_tabelle.c
	\brief Modulo OT_Plus_tabelle.c

	Tabelle dati per la comunicazione su canale OT/+ .

*/

#include "main.h"
#include "nvram.h"
#include "OT_Plus_tabelle.h"


/* defines */


/* variabili inizializzate */
//#pragma SECTION data limits_data

static unsigned char PanVerFW[4] =
{
	codSW,
	verSW,
	revSW,
	buildSW
};

/*!
	\var Limits_tab
	\brief Tabella dei limiti per gli Holding Registers
*/
LIMITS_STRUCT Limits_tab[THOLREG_NREG] =
{
		{ 0, 255, 1 },								// 0x0000
		{ 0, 255, 1 },								// 0x0001
		{ 0, 255, 1 },								// 0x0002
		{ 0, 255, 1 },								// 0x0003
		{ 0, 255, 1 },								// 0x0004
		{ 0, 255, 1 },								// 0x0005
	{ 0, 0xFFFF, 1 },								// 0x0006	// bitmap
	{ 0, 0xFFFF, 1 },								// 0x0007
	{ MINSETTEMP, MAXSETTEMP, 1 },			// 0x0008
	{ H2O_TMIN, H2O_TMAX, 1 },					// 0x0009
	{ H2OSAN_TMIN, H2OSAN_TMAX, 1 },			// 0x000A
	{ ZERO_FAN, TRE_FAN, 1 },					// 0x000B
	{ 0, 10, 1 },									// 0x000C
	{ 0, 60, 1 },									// 0x000D
	{ 0, 60, 1 },									// 0x000E
	{ 30, 85, 1 },									// 0x000F
	{ 30, 85, 1 },									// 0x0010
	{ 30, 85, 1 },									// 0x0011
	{ 0, 20, 1 },									// 0x0012
	{ 0, 15, 1 },									// 0x0013
	{ (unsigned short)(-10), 10, 1 },		// 0x0014
	{ (unsigned short)(-10), 10, 1 },		// 0x0015
	{ 25, 47, 1 },									// 0x0016	// in step di 0.1bar
	{ 0, 0xFFFF, 1 },								// 0x0017	// bitmap
		{ 0, 255, 1 },								// 0x0018
		{ 0, 255, 1 },								// 0x0019
		{ 0, 255, 1 },								// 0x001A
		{ 0, 255, 1 },								// 0x001B
		{ 0, 255, 1 },								// 0x001C
	{ (unsigned short)(-10), 10, 1 },		// 0x001D
	{ (unsigned short)(-10), 10, 1 },		// 0x001E
	{ (unsigned short)(-10), 10, 1 },		// 0x001F
	{ (unsigned short)(-10), 10, 1 },		// 0x0020
	{ 0, 0xFFFF, 1 },								// 0x0021	// bitmap
	{ 0, 12000, 2 },								// 0x0022	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x0023
	{ 300, 2750, 50 },							// 0x0024
	{ 0, 12000, 2 },								// 0x0025	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x0026
	{ 300, 2750, 50 },							// 0x0027
	{ 0, 1000, 1 },								// 0x0028
	{ 0, 1000, 1 },								// 0x0029
	{ 0, 0xFFFF, 1 },								// 0x002A	// bitmap
	{ 0, 12000, 2 },								// 0x002B	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x002C
	{ 300, 2750, 50 },							// 0x002D
	{ 0, 12000, 2 },								// 0x002E	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x002F
	{ 300, 2750, 50 },							// 0x0030
	{ 0, 1000, 1 },								// 0x0031
	{ 0, 1000, 1 },								// 0x0032
	{ 0, 0xFFFF, 1 },								// 0x0033	// bitmap
	{ 0, 12000, 2 },								// 0x0034	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x0035
	{ 300, 2750, 50 },							// 0x0036
	{ 0, 12000, 2 },								// 0x0037	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x0038
	{ 300, 2750, 50 },							// 0x0039
	{ 0, 1000, 1 },								// 0x003A
	{ 0, 1000, 1 },								// 0x003B
	{ 0, 0xFFFF, 1 },								// 0x003C	// bitmap
	{ 0, 12000, 2 },								// 0x003D	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x003E
	{ 300, 2750, 50 },							// 0x003F
	{ 0, 12000, 2 },								// 0x0040	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x0041
	{ 300, 2750, 50 },							// 0x0042
	{ 0, 1000, 1 },								// 0x0043
	{ 0, 1000, 1 },								// 0x0044
	{ 0, 0xFFFF, 1 },								// 0x0045	// bitmap
	{ 0, 12000, 2 },								// 0x0046	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x0047
	{ 300, 2750, 50 },							// 0x0048
	{ 0, 12000, 2 },								// 0x0049	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x004A
	{ 300, 2750, 50 },							// 0x004B
	{ 0, 1000, 1 },								// 0x004C
	{ 0, 1000, 1 },								// 0x004D
	{ 0, 0xFFFF, 1 },								// 0x004E	// bitmap
	{ 0, 12000, 2 },								// 0x004F	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x0050
	{ 300, 2750, 50 },							// 0x0051
	{ 0, 12000, 2 },								// 0x0052	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x0053
	{ 300, 2750, 50 },							// 0x0054
	{ 0, 1000, 1 },								// 0x0055
	{ 0, 1000, 1 },								// 0x0056
	{ 0, 0xFFFF, 1 },								// 0x0057	// bitmap
	{ 0, 12000, 2 },								// 0x0058	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x0059
	{ 300, 2750, 50 },							// 0x005A
	{ 0, 12000, 2 },								// 0x005B	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x005C
	{ 300, 2750, 50 },							// 0x005D
	{ 0, 1000, 1 },								// 0x005E
	{ 0, 1000, 1 },								// 0x005F
	{ 0, 0xFFFF, 1 },								// 0x0060	// bitmap
	{ 0, 12000, 2 },								// 0x0061	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x0062
	{ 300, 2750, 50 },							// 0x0063
	{ 0, 12000, 2 },								// 0x0064	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x0065
	{ 300, 2750, 50 },							// 0x0066
	{ 0, 1000, 1 },								// 0x0067
	{ 0, 1000, 1 },								// 0x0068
	{ 0, 0xFFFF, 1 },								// 0x0069	// bitmap
	{ 0, 12000, 2 },								// 0x006A	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x006B
	{ 300, 2750, 50 },							// 0x006C
	{ 0, 12000, 2 },								// 0x006D	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x006E
	{ 300, 2750, 50 },							// 0x006F
	{ 0, 1000, 1 },								// 0x0070
	{ 0, 1000, 1 },								// 0x0071
	{ 0, 0xFFFF, 1 },								// 0x0072	// bitmap
	{ 0, 12000, 2 },								// 0x0073	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x0074
	{ 300, 2750, 50 },							// 0x0075
	{ 0, 12000, 2 },								// 0x0076	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x0077
	{ 300, 2750, 50 },							// 0x0078
	{ 0, 1000, 1 },								// 0x0079
	{ 0, 1000, 1 },								// 0x007A
	{ 0, 0xFFFF, 1 },								// 0x007B	// bitmap
	{ 0, 12000, 2 },								// 0x007C	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x007D
	{ 300, 2750, 50 },							// 0x007E
	{ 0, 12000, 2 },								// 0x007F	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x0080
	{ 300, 2750, 50 },							// 0x0081
	{ 0, 1000, 1 },								// 0x0082
	{ 0, 1000, 1 },								// 0x0083
	{ 0, 0xFFFF, 1 },								// 0x0084	// bitmap
	{ 0, 12000, 2 },								// 0x0085	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x0086
	{ 300, 2750, 50 },							// 0x0087
	{ 0, 12000, 2 },								// 0x0088	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x0089
	{ 300, 2750, 50 },							// 0x008A
	{ 0, 1000, 1 },								// 0x008B
	{ 0, 1000, 1 },								// 0x008C
	{ 0, 0xFFFF, 1 },								// 0x008D	// bitmap
	{ 0, 12000, 2 },								// 0x008E	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x008F
	{ 300, 2750, 50 },							// 0x0090
	{ 0, 12000, 2 },								// 0x0091	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x0092
	{ 300, 2750, 50 },							// 0x0093
	{ 0, 1000, 1 },								// 0x0094
	{ 0, 1000, 1 },								// 0x0095
	{ 0, 0xFFFF, 1 },								// 0x0096	// bitmap
	{ 0, 12000, 2 },								// 0x0097	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x0098
	{ 300, 2750, 50 },							// 0x0099
	{ 0, 12000, 2 },								// 0x009A	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x009B
	{ 300, 2750, 50 },							// 0x009C
	{ 0, 1000, 1 },								// 0x009D
	{ 0, 1000, 1 },								// 0x009E
	{ 0, 50, 1 },									// 0x009F
	{ 0, 300, 1 },									// 0x00A0
	{ 0, 300, 1 },									// 0x00A1
	{ 0, 300, 1 },									// 0x00A2
	{ 0, 3600, 1 },								// 0x00A3
	{ 0, 3600, 1 },								// 0x00A4
	{ 0, 3600, 1 },								// 0x00A5
	{ 0, 3600, 1 },								// 0x00A6
	{ 0, 3600, 1 },								// 0x00A7
	{ 0, 3600, 1 },								// 0x00A8
	{ 0, 3600, 1 },								// 0x00A9
	{ 0, 3600, 1 },								// 0x00AA
	{ 0, 3600, 1 },								// 0x00AB
	{ 0, 3600, 1 },								// 0x00AC
	{ 0, 300, 2 },									// 0x00AD	// il valore e' espresso in step di 50ms
	{ 0, 300, 2 },									// 0x00AE	// il valore e' espresso in step di 50ms
	{ 0, 10, 1 },									// 0x00AF
	{ 0, 10, 1 },									// 0x00B0
	{ 0, 8100, 1 },								// 0x00B1
	{ 0, 8100, 1 },								// 0x00B2
	{ 0, 0xFFFF, 1 },								// 0x00B3	// bitmap
	{ 0, 12000, 2 },								// 0x00B4	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x00B5
	{ 300, 2750, 50 },							// 0x00B6
	{ 0, 12000, 2 },								// 0x00B7	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x00B8
	{ 300, 2750, 50 },							// 0x00B9
	{ 0, 1000, 1 },								// 0x00BA
	{ 0, 1000, 1 },								// 0x00BB
	{ 0, 0xFFFF, 1 },								// 0x00BC	// bitmap
	{ 0, 12000, 2 },								// 0x00BD	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x00BE
	{ 300, 2750, 50 },							// 0x00BF
	{ 0, 12000, 2 },								// 0x00C0	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x00C1
	{ 300, 2750, 50 },							// 0x00C2
	{ 0, 1000, 1 },								// 0x00C3
	{ 0, 1000, 1 },								// 0x00C4
	{ 0, 0xFFFF, 1 },								// 0x00C5	// bitmap
	{ 0, 12000, 2 },								// 0x00C6	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x00C7
	{ 300, 2750, 50 },							// 0x00C8
	{ 0, 12000, 2 },								// 0x00C9	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x00CA
	{ 300, 2750, 50 },							// 0x00CB
	{ 0, 1000, 1 },								// 0x00CC
	{ 0, 1000, 1 },								// 0x00CD
	{ 0, 0xFFFF, 1 },								// 0x00CE	// bitmap
	{ 0, 12000, 2 },								// 0x00CF	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x00D0
	{ 300, 2750, 50 },							// 0x00D1
	{ 0, 12000, 2 },								// 0x00D2	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x00D3
	{ 300, 2750, 50 },							// 0x00D4
	{ 0, 1000, 1 },								// 0x00D5
	{ 0, 1000, 1 },								// 0x00D6
	{ 0, 0xFFFF, 1 },								// 0x00D7	// bitmap
	{ 0, 12000, 2 },								// 0x00D8	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x00D9
	{ 300, 2750, 50 },							// 0x00DA
	{ 0, 12000, 2 },								// 0x00DB	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x00DC
	{ 300, 2750, 50 },							// 0x00DD
	{ 0, 1000, 1 },								// 0x00DE
	{ 0, 1000, 1 },								// 0x00DF
	{ 0, 0xFFFF, 1 },								// 0x00E0	// bitmap
	{ 0, 12000, 2 },								// 0x00E1	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x00E2
	{ 300, 2750, 50 },							// 0x00E3
	{ 0, 12000, 2 },								// 0x00E4	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x00E5
	{ 300, 2750, 50 },							// 0x00E6
	{ 0, 1000, 1 },								// 0x00E7
	{ 0, 1000, 1 },								// 0x00E8
	{ 0, 0xFFFF, 1 },								// 0x00E9	// bitmap
	{ 0, 12000, 2 },								// 0x00EA	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x00EB
	{ 300, 2750, 50 },							// 0x00EC
	{ 0, 12000, 2 },								// 0x00ED	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x00EE
	{ 300, 2750, 50 },							// 0x00EF
	{ 0, 1000, 1 },								// 0x00F0
	{ 0, 1000, 1 },								// 0x00F1
	{ 0, 0xFFFF, 1 },								// 0x00F2	// bitmap
	{ 0, 12000, 2 },								// 0x00F3	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x00F4
	{ 300, 2750, 50 },							// 0x00F5
	{ 0, 12000, 2 },								// 0x00F6	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x00F7
	{ 300, 2750, 50 },							// 0x00F8
	{ 0, 1000, 1 },								// 0x00F9
	{ 0, 1000, 1 },								// 0x00FA
	{ 0, 0xFFFF, 1 },								// 0x00FB	// bitmap
	{ 0, 12000, 2 },								// 0x00FC	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x00FD
	{ 300, 2750, 50 },							// 0x00FE
	{ 0, 12000, 2 },								// 0x00FF	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x0100
	{ 300, 2750, 50 },							// 0x0101
	{ 0, 1000, 1 },								// 0x0102
	{ 0, 1000, 1 },								// 0x0103
	{ 0, 0xFFFF, 1 },								// 0x0104	// bitmap
	{ 0, 12000, 2 },								// 0x0105	// il valore e' espresso in step di 50ms
	{ 0, 400, 1 },									// 0x0106
	{ 300, 2750, 50 },							// 0x0107
	{ 0, 12000, 2 },								// 0x0108	// il valore e' espresso in step di 50ms
	{ 0, 1000, 1 },								// 0x0109
	{ 300, 2750, 50 },							// 0x010A
	{ 0, 1000, 1 },								// 0x010B
	{ 0, 1000, 1 },								// 0x010C
	{ 3, POTMAXLEV, 1 },							// 0x010D
	{ 0, 300, 1 },									// 0x010E
	{ 0, 300, 1 },									// 0x010F
	{ 0, 300, 1 },									// 0x0110
	{ 0, 300, 1 },									// 0x0111
	{ 0, 300, 1 },									// 0x0112
	{ 0, 300, 1 },									// 0x0113
	{ 0, 3600, 1 },								// 0x0114
	{ 0, 200, 1 },									// 0x0115	// il valore e` espresso in 0.5 %
	{ 1, 12, 1 },									// 0x0116	// il valore e` espresso in 0.5
		{ 0, 255, 1 },								// 0x0117
		{ 0, 255, 1 },								// 0x0118
		{ 0, 255, 1 },								// 0x0119
		{ 0, 255, 1 },								// 0x011A
		{ 0, 255, 1 },								// 0x011B
		{ 0, 255, 1 },								// 0x011C
		{ 0, 255, 1 },								// 0x011D
	{ 0, 0xFFFF, 1 },								// 0x011E	// bitmap
		{ 0, 255, 1 },								// 0x011F
	{ 0, 50, 1 },									// 0x0120
	{ 0, 14400, 10 },								// 0x0121
	{ 0, 14400, 10 },								// 0x0122
	{ 0, 50, 1 },									// 0x0123
	{ 0, 300, 1 },									// 0x0124
	{ 0, 300, 1 },									// 0x0125
	{ 0, 14400, 10 },								// 0x0126
	{ 0, 400, 1 },									// 0x0127
	{ 0, 50, 1 },									// 0x0128
		{ 0, 255, 1 },								// 0x0129
		{ 0, 255, 1 },								// 0x012A
		{ 0, 255, 1 },								// 0x012B
		{ 0, 255, 1 },								// 0x012C
		{ 0, 255, 1 },								// 0x012D
		{ 0, 255, 1 },								// 0x012E
		{ 0, 255, 1 },								// 0x012F
		{ 0, 255, 1 },								// 0x0130
	{ 0, 0xFFFF, 1 },								// 0x0131	// bitmap
	{ 0, 0xFFFF, 1 },								// 0x0132	// bitmap
	{ 0, 255, 1 },									// 0x0133
	{ 0, 255, 1 },									// 0x0134
	{ 0, 255, 1 },									// 0x0135
	{ 0, 255, 1 },									// 0x0136
	{ 0, 9999, 1 },								// 0x0137	// il valore e' espresso in g/h
		{ 0, 255, 1 },								// 0x0138
	{ 0, 3600, 1 },								// 0x0139
	{ 0, 9960, 1 },								// 0x013A
	{ 0, 600, 1 },									// 0x013B
	{ 0, 255, 1 },									// 0x013C
	{ 0, 0, 0 },									// 0x013D	// read-only
	{ 0, 0, 0 },									// 0x013E	// read-only
	{ 0, 65535, 1 },								// 0x013F
	{ 0, 0, 1 },									// 0x0140	// solo azzerabile
		{ 0, 255, 1 },								// 0x0141
	{ 0, 0, 0 },									// 0x0142	// TBLACKOUT
	{ 0, 0, 0 },									// 0x0143	//	LastStatoFunz
	{ 0, 0, 0 },									// 0x0144	// TEMP_AMB_PAN
	{ 0, 0, 0 },									// 0x0145	// PAN codSW
	{ 0, 0, 0 },									// 0x0146   // PAN verSW
	{ 0, 0, 0 },									// 0x0147   // PAN revSW
	{ 0, 0, 0 },									// 0x0148   // PAN buildSW
	
};
		
/*!
	\var Limits2_tab
	\brief Tabella dei limiti variabili per gli Holding Registers 2 inizializzata al default
*/
LIMITS_STRUCT Limits2_tab[THOLREG2_NREG] =
{
	{ 0, 12000, 2 },								// 0x0400	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0401	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0402	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0403	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0404	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0405	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0406	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0407	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0408	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0409	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x040A	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x040B	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x040C	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x040D	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x040E	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x040F	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0410	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0411	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0412	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0413	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0414	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0415	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0416	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0417	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0418	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0419	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x041A	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x041B	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x041C	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x041D	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x041E	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x041F	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0420	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0421	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0422	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0423	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0424	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0425	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0426	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0427	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0428	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x0429	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x042A	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x042B	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x042C	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x042D	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x042E	// il valore e' espresso in step di 50ms
	{ 0, 12000, 2 },								// 0x042F	// il valore e' espresso in step di 50ms
	{ POTMINLEV, POTMAXLEV, 1 },				// 0x0430
	{ FANMINLEV, FANAUTO, 1 },					// 0x0431
	{ FANMINLEV, FANAUTO, 1 },					// 0x0432
	{ FANMINLEV, FANAUTO, 1 },					// 0x0433
		{ 0, 255, 1 },								// 0x0434
		{ 0, 255, 1 },								// 0x0435
		{ 0, 255, 1 },								// 0x0436
		{ 0, 255, 1 },								// 0x0437
		{ 0, 255, 1 },								// 0x0438
		{ 0, 255, 1 },								// 0x0439
		{ 0, 255, 1 },								// 0x043A
		{ 0, 255, 1 },								// 0x043B
	{ POTMINLEV, POTMAXLEV, 1 },				// 0x043C
	{ 10, 1300, 1 },								// 0x043D
	{ 10, 1300, 1 },								// 0x043E
	{ 10, 1300, 1 },								// 0x043F
	{ 10, 1300, 1 },								// 0x0440
	{ 10, 1300, 1 },								// 0x0441
	{ 10, 1300, 1 },								// 0x0442
	{ 10, 1300, 1 },								// 0x0443

};


/* variabili non inizializzate */
//#pragma SECTION bss

static unsigned short DummyHoldingReg;


/* costanti */
//#pragma SECTION rom

static const unsigned short NullReg = 0;

/*!
	\var Coil_tab
	\brief Tabella MB dei coils
*/
const MBDATA Coil_tab[TCOIL_NREG] =
{
	{ MBTYPE_BYTE_MASK, 0x0001, (unsigned short *)&SETTINGS },			// 0x0000
	{ MBTYPE_BYTE_MASK, 0x0002, (unsigned short *)&SETTINGS },			// 0x0001

};

/*!
	\var InpReg_tab
	\brief Tabella MB degli Input Registers
*/
const MBDATA InpReg_tab[TINPREG_NREG] =
{
	{ MBTYPE_BYTE, 0, (unsigned short *)&gBySchedaCod },					// 0x0000
	{ MBTYPE_BYTE, 0, (unsigned short *)&gBySchedaVers },					// 0x0001
	{ MBTYPE_BYTE, 0, (unsigned short *)&gBySchedaRev },					// 0x0002
	{ MBTYPE_BYTE, 0, (unsigned short *)&gBySchedaBuild },					// 0x0003
	{ MBTYPE_BYTE, 0, (unsigned short *)&gBySicurezzaCod },				// 0x0004
	{ MBTYPE_BYTE, 0, (unsigned short *)&gBySicurezzaVers },				// 0x0005
	{ MBTYPE_BYTE, 0, (unsigned short *)&gBySicurezzaRev },				// 0x0006
	{ MBTYPE_BYTE, 0, (unsigned short *)&gBySicurezzaBuild },				// 0x0007
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0008
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0009
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x000A
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x000B
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x000C
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x000D
	{ MBTYPE_WORD, 0, (unsigned short *)&SetpSanitari },					// 0x000E
	{ MBTYPE_WORD, 0, (unsigned short *)&TEMP_FUMI },						// 0x000F
	{ MBTYPE_INT, 0, (unsigned short *)&TEMP_AMB_MB },						// 0x0010	// il valore e' espresso in step di 0.5 �C
	{ MBTYPE_INT, 0, (unsigned short *)&H2O_TEMP },							// 0x0011	// il valore e' espresso in step di 0.5 �C
	{ MBTYPE_BYTE, 0, (unsigned short *)&StatoAttuazioni },				// 0x0012
	{ MBTYPE_BYTE, 0, (unsigned short *)&STATO_STUFA },						// 0x0013
	{ MBTYPE_BYTE, 0, (unsigned short *)&TIPO_ALLARME },					// 0x0014
	{ MBTYPE_WORD, 0, (unsigned short *)&INPBMAP21 },						// 0x0015
	{ MBTYPE_WORD, 0, (unsigned short *)&INPBMAP22 },						// 0x0016
	{ MBTYPE_WORD, 0, (unsigned short *)&INPBMAP23 },						// 0x0017
	{ MBTYPE_WORD, 0, (unsigned short *)&INPBMAP24 },						// 0x0018
	{ MBTYPE_WORD, 0, (unsigned short *)&INPBMAP25 },						// 0x0019
	{ MBTYPE_WORD, 0, (unsigned short *)&TOnCocleaSet },					// 0x001A	// il valore e' espresso in step di 50ms
	{ MBTYPE_WORD, 0, (unsigned short *)&TOffCocleaSet },					// 0x001B	// il valore e' espresso in step di 50ms
	{ MBTYPE_WORD, 0, (unsigned short *)&PortataAriaSet },					// 0x001C
	{ MBTYPE_WORD, 0, (unsigned short *)&GiriMinEspSet },					// 0x001D
	{ MBTYPE_WORD, 0, (unsigned short *)&AttEsp },							// 0x001E
	{ MBTYPE_WORD, 0, (unsigned short *)&TOnFan1Set },						// 0x001F	// il valore e' espresso in step di 50ms
	{ MBTYPE_WORD, 0, (unsigned short *)&TOffFan1Set },						// 0x0020	// il valore e' espresso in step di 50ms
	{ MBTYPE_WORD, 0, (unsigned short *)&Fan1Set },							// 0x0021
	{ MBTYPE_WORD, 0, (unsigned short *)&Giri2Set },							// 0x0022
	{ MBTYPE_WORD, 0, (unsigned short *)&Fan2Set },							// 0x0023
	{ MBTYPE_WORD, 0, (unsigned short *)&Fan3Set },							// 0x0024
	{ MBTYPE_WORD, 0, (unsigned short *)&PORTATA },							// 0x0025
	{ MBTYPE_WORD, 0, (unsigned short *)&ESP_FUMI },							// 0x0026
	{ MBTYPE_WORD, 0, (unsigned short *)&VelocitaEspulsore2 },			// 0x0027
	{ MBTYPE_BYTE, 0, (unsigned short *)&H2O_FLOW },							// 0x0028	// il valore e` espresso in 0.1 bar
	{ MBTYPE_BYTE, 0, (unsigned short *)&StatoLogico },						// 0x0029
	{ MBTYPE_BYTE, 0, (unsigned short *)&StatoLogicoPassato },			// 0x002A
	{ MBTYPE_BYTE, 0, (unsigned short *)&ContaDurata1 },					// 0x002B
	{ MBTYPE_WORD, 0, (unsigned short *)&ContaDurata2 },					// 0x002C
	{ MBTYPE_WORD, 0, (unsigned short *)&ContaDurataEco },					// 0x002D
	{ MBTYPE_BYTE, 0, (unsigned short *)&PotCalc },							// 0x002E
	{ MBTYPE_BYTE, 0, (unsigned short *)&PotIdro },							// 0x002F
	{ MBTYPE_BYTE, 0, (unsigned short *)&PotAttuata },						// 0x0030
	{ MBTYPE_BYTE, 0, (unsigned short *)&ContaPID },							// 0x0031
	{ MBTYPE_WORD, 0, (unsigned short *)&LivFotores },						// 0x0032
	{ MBTYPE_BYTE, 0, (unsigned short *)&OssigenoResiduo },				// 0x0033	// il valore e' espresso in 0.5 %
	{ MBTYPE_WORD, 0, (unsigned short *)&Consumo },							// 0x0034	// il valore e' espresso in 0.05 kg/h
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0035
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0036
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0037
	{ MBTYPE_WORD, 0, (unsigned short *)&ParamCode },						// 0x0038
	{ MBTYPE_WORD, 0, (unsigned short *)&ParamRel },							// 0x0039
	{ MBTYPE_BYTE, 0, (unsigned short *)&DriverCod },						// 0x003A
	{ MBTYPE_BYTE, 0, (unsigned short *)&DriverVers },						// 0x003B
	{ MBTYPE_BYTE, 0, (unsigned short *)&DriverRev },						// 0x003C
	{ MBTYPE_BYTE, 0, (unsigned short *)&DriverBuild },						// 0x003D
	{ MBTYPE_CHAR, 0, (unsigned short *)&pByPassLettera },					// 0x003E
	{ MBTYPE_BYTE, 0, (unsigned short *)&pByPassCodice },					// 0x003F
	{ MBTYPE_BYTE, 1, (unsigned short *)&ServNumber[0].BYTE },			// 0x0040
	{ MBTYPE_BYTE, 3, (unsigned short *)&ServNumber[2].BYTE },			// 0x0041
	{ MBTYPE_BYTE, 5, (unsigned short *)&ServNumber[4].BYTE },			// 0x0042
	{ MBTYPE_BYTE, 7, (unsigned short *)&ServNumber[6].BYTE },			// 0x0043
	{ MBTYPE_CHAR, 1, (unsigned short *)&pChLogo[0] },						// 0x0044
	{ MBTYPE_CHAR, 3, (unsigned short *)&pChLogo[2] },						// 0x0045
	{ MBTYPE_CHAR, 5, (unsigned short *)&pChLogo[4] },						// 0x0046
	{ MBTYPE_CHAR, 7, (unsigned short *)&pChLogo[6] },						// 0x0047
	{ MBTYPE_CHAR, 9, (unsigned short *)&pChLogo[8] },						// 0x0048
	{ MBTYPE_CHAR, 11, (unsigned short *)&pChLogo[10] },					// 0x0049
	{ MBTYPE_CHAR, 13, (unsigned short *)&pChLogo[12] },					// 0x004A
	{ MBTYPE_CHAR, 15, (unsigned short *)&pChLogo[14] },					// 0x004B
	{ MBTYPE_CHAR, 1, (unsigned short *)&pChTipoDesc[TYPE_1][0] },		// 0x004C
	{ MBTYPE_CHAR, 3, (unsigned short *)&pChTipoDesc[TYPE_1][2] },		// 0x004D
	{ MBTYPE_CHAR, 5, (unsigned short *)&pChTipoDesc[TYPE_1][4] },		// 0x004E
	{ MBTYPE_CHAR, 7, (unsigned short *)&pChTipoDesc[TYPE_1][6] },		// 0x004F
	{ MBTYPE_CHAR, 1, (unsigned short *)&pChTipoDesc[TYPE_2][0] },		// 0x0050
	{ MBTYPE_CHAR, 3, (unsigned short *)&pChTipoDesc[TYPE_2][2] },		// 0x0051
	{ MBTYPE_CHAR, 5, (unsigned short *)&pChTipoDesc[TYPE_2][4] },		// 0x0052
	{ MBTYPE_CHAR, 7, (unsigned short *)&pChTipoDesc[TYPE_2][6] },		// 0x0053
	{ MBTYPE_CHAR, 1, (unsigned short *)&pChTipoDesc[TYPE_3][0] },		// 0x0054
	{ MBTYPE_CHAR, 3, (unsigned short *)&pChTipoDesc[TYPE_3][2] },		// 0x0055
	{ MBTYPE_CHAR, 5, (unsigned short *)&pChTipoDesc[TYPE_3][4] },		// 0x0056
	{ MBTYPE_CHAR, 7, (unsigned short *)&pChTipoDesc[TYPE_3][6] },		// 0x0057
	{ MBTYPE_CHAR, 1, (unsigned short *)&pChTipoDesc[TYPE_4][0] },		// 0x0058
	{ MBTYPE_CHAR, 3, (unsigned short *)&pChTipoDesc[TYPE_4][2] },		// 0x0059
	{ MBTYPE_CHAR, 5, (unsigned short *)&pChTipoDesc[TYPE_4][4] },		// 0x005A
	{ MBTYPE_CHAR, 7, (unsigned short *)&pChTipoDesc[TYPE_4][6] },		// 0x005B
	{ MBTYPE_CHAR, 1, (unsigned short *)&pChTipoDesc[TYPE_5][0] },		// 0x005C
	{ MBTYPE_CHAR, 3, (unsigned short *)&pChTipoDesc[TYPE_5][2] },		// 0x005D
	{ MBTYPE_CHAR, 5, (unsigned short *)&pChTipoDesc[TYPE_5][4] },		// 0x005E
	{ MBTYPE_CHAR, 7, (unsigned short *)&pChTipoDesc[TYPE_5][6] },		// 0x005F
	{ MBTYPE_CHAR, 1, (unsigned short *)&pChTipoDesc[TYPE_6][0] },		// 0x0060
	{ MBTYPE_CHAR, 3, (unsigned short *)&pChTipoDesc[TYPE_6][2] },		// 0x0061
	{ MBTYPE_CHAR, 5, (unsigned short *)&pChTipoDesc[TYPE_6][4] },		// 0x0062
	{ MBTYPE_CHAR, 7, (unsigned short *)&pChTipoDesc[TYPE_6][6] },		// 0x0063
	{ MBTYPE_CHAR, 1, (unsigned short *)&pChTipoDesc[TYPE_7][0] },		// 0x0064
	{ MBTYPE_CHAR, 3, (unsigned short *)&pChTipoDesc[TYPE_7][2] },		// 0x0065
	{ MBTYPE_CHAR, 5, (unsigned short *)&pChTipoDesc[TYPE_7][4] },		// 0x0066
	{ MBTYPE_CHAR, 7, (unsigned short *)&pChTipoDesc[TYPE_7][6] },		// 0x0067
	{ MBTYPE_CHAR, 1, (unsigned short *)&pChTipoDesc[TYPE_8][0] },		// 0x0068
	{ MBTYPE_CHAR, 3, (unsigned short *)&pChTipoDesc[TYPE_8][2] },		// 0x0069
	{ MBTYPE_CHAR, 5, (unsigned short *)&pChTipoDesc[TYPE_8][4] },		// 0x006A
	{ MBTYPE_CHAR, 7, (unsigned short *)&pChTipoDesc[TYPE_8][6] },		// 0x006B
	{ MBTYPE_BYTE, 0, (unsigned short *)&TIPO_STUFA },						// 0x006C
	{ MBTYPE_BYTE, 0, (unsigned short *)&LinguaDef },						// 0x006D
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x006E
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x006F
	{ MBTYPE_BYTE, 0, (unsigned short *)&SecOrologio },						// 0x0070	// il valore e' espresso in 0.5s
	{ MBTYPE_BYTE, 0, (unsigned short *)&MinOrologio },						// 0x0071
	{ MBTYPE_WORD, 0, (unsigned short *)&TempoOn },							// 0x0072
	{ MBTYPE_WORD, 0, (unsigned short *)&INPBMAP115 },						// 0x0073
		
};

/*!
	\var HoldReg_tab
	\brief Tabella MB degli Holding Registers
	
		PARAMETRI CON LIMITI VARIABILI TRA UN FIRMWARE E UN ALTRO
*/
const MBDATA HoldReg_tab[THOLREG_NREG] =
{
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0000
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0001
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0002
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0003
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0004
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0005
	{ MBTYPE_WORD, 0, (unsigned short *)&HOLDBMAP6 },						// 0x0006
	{ MBTYPE_WORD, 0, (unsigned short *)&SETTINGS },							// 0x0007
	{ MBTYPE_BYTE, 0, (unsigned short *)&TEMP_PAN },							// 0x0008
	{ MBTYPE_BYTE, 0, (unsigned short *)&H2O_TSET },							// 0x0009
	{ MBTYPE_BYTE, 0, (unsigned short *)&H2O_TSETSAN },						// 0x000A
	{ MBTYPE_BYTE, 0, (unsigned short *)&F_MULTIFAN },						// 0x000B
	{ MBTYPE_BYTE, 0, (unsigned short *)&GRADIENTE_ECO },					// 0x000C
	{ MBTYPE_WORD, 0, (unsigned short *)&TSWITCHON_ECO },					// 0x000D
	{ MBTYPE_WORD, 0, (unsigned short *)&TSHUTDOWN_ECO },					// 0x000E
	{ MBTYPE_BYTE, 0, (unsigned short *)&SETON_POMPA },						// 0x000F
	{ MBTYPE_BYTE, 0, (unsigned short *)&SetIdroPompaOff },				// 0x0010
	{ MBTYPE_BYTE, 0, (unsigned short *)&TOnAux },							// 0x0011
	{ MBTYPE_BYTE, 0, (unsigned short *)&DeltaSanitari },					// 0x0012
	{ MBTYPE_BYTE, 0, (unsigned short *)&IstTempAcqua },					// 0x0013
	{ MBTYPE_INT, 0, (unsigned short *)&GainRisc },							// 0x0014
	{ MBTYPE_INT, 0, (unsigned short *)&GainSanitari },						// 0x0015
	{ MBTYPE_BYTE, 0, (unsigned short *)&PressH2OMax },						// 0x0016
	{ MBTYPE_WORD, 0, (unsigned short *)&HOLDBMAP23 },						// 0x0017
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0018
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0019
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x001A
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x001B
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x001C
	{ MBTYPE_INT, 0, (unsigned short *)&RicPelletAcc },						// 0x001D
	{ MBTYPE_INT, 0, (unsigned short *)&RicPellet },							// 0x001E
	{ MBTYPE_INT, 0, (unsigned short *)&RicEspAcc },							// 0x001F
	{ MBTYPE_INT, 0, (unsigned short *)&RicEsp },								// 0x0020
	{ MBTYPE_WORD, 0, (unsigned short *)&PREACC1_BMAP },					// 0x0021
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc1_TOffCoclea },			// 0x0022
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc1_Portata },				// 0x0023
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc1_Esp },						// 0x0024
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc1_TOffFan1 },				// 0x0025
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc1_AttuaFan1 },				// 0x0026
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc1_Giri2 },					// 0x0027
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc1_AttuaFan2 },				// 0x0028
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc1_AttuaFan3 },				// 0x0029
	{ MBTYPE_WORD, 0, (unsigned short *)&PREACC2_BMAP },					// 0x002A
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc2_TOffCoclea },			// 0x002B
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc2_Portata },				// 0x002C
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc2_Esp },						// 0x002D
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc2_TOffFan1 },				// 0x002E
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc2_AttuaFan1 },				// 0x002F
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc2_Giri2 },					// 0x0030
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc2_AttuaFan2 },				// 0x0031
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc2_AttuaFan3 },				// 0x0032
	{ MBTYPE_WORD, 0, (unsigned short *)&PREACCCALDO_BMAP },				// 0x0033
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAccCaldo_TOffCoclea },		// 0x0034
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAccCaldo_Portata },			// 0x0035
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAccCaldo_Esp },				// 0x0036
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAccCaldo_TOffFan1 },			// 0x0037
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAccCaldo_AttuaFan1 },		// 0x0038
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAccCaldo_Giri2 },				// 0x0039
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAccCaldo_AttuaFan2 },		// 0x003A
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAccCaldo_AttuaFan3 },		// 0x003B
	{ MBTYPE_WORD, 0, (unsigned short *)&ACCA_BMAP },						// 0x003C
	{ MBTYPE_WORD, 0, (unsigned short *)&AccA_TOffCoclea },				// 0x003D
	{ MBTYPE_WORD, 0, (unsigned short *)&AccA_Portata },					// 0x003E
	{ MBTYPE_WORD, 0, (unsigned short *)&AccA_Esp },							// 0x003F
	{ MBTYPE_WORD, 0, (unsigned short *)&AccA_TOffFan1 },					// 0x0040
	{ MBTYPE_WORD, 0, (unsigned short *)&AccA_AttuaFan1 },					// 0x0041
	{ MBTYPE_WORD, 0, (unsigned short *)&AccA_Giri2 },						// 0x0042
	{ MBTYPE_WORD, 0, (unsigned short *)&AccA_AttuaFan2 },					// 0x0043
	{ MBTYPE_WORD, 0, (unsigned short *)&AccA_AttuaFan3 },					// 0x0044
	{ MBTYPE_WORD, 0, (unsigned short *)&ACCB_BMAP },						// 0x0045
	{ MBTYPE_WORD, 0, (unsigned short *)&AccB_TOffCoclea },				// 0x0046
	{ MBTYPE_WORD, 0, (unsigned short *)&AccB_Portata },					// 0x0047
	{ MBTYPE_WORD, 0, (unsigned short *)&AccB_Esp },							// 0x0048
	{ MBTYPE_WORD, 0, (unsigned short *)&AccB_TOffFan1 },					// 0x0049
	{ MBTYPE_WORD, 0, (unsigned short *)&AccB_AttuaFan1 },					// 0x004A
	{ MBTYPE_WORD, 0, (unsigned short *)&AccB_Giri2 },						// 0x004B
	{ MBTYPE_WORD, 0, (unsigned short *)&AccB_AttuaFan2 },					// 0x004C
	{ MBTYPE_WORD, 0, (unsigned short *)&AccB_AttuaFan3 },					// 0x004D
	{ MBTYPE_WORD, 0, (unsigned short *)&FIREONA_BMAP },					// 0x004E
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnA_TOffCoclea },			// 0x004F
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnA_Portata },				// 0x0050
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnA_Esp },						// 0x0051
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnA_TOffFan1 },				// 0x0052
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnA_AttuaFan1 },				// 0x0053
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnA_Giri2 },					// 0x0054
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnA_AttuaFan2 },				// 0x0055
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnA_AttuaFan3 },				// 0x0056
	{ MBTYPE_WORD, 0, (unsigned short *)&FIREONB_BMAP },					// 0x0057
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnB_TOffCoclea },			// 0x0058
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnB_Portata },				// 0x0059
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnB_Esp },						// 0x005A
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnB_TOffFan1 },				// 0x005B
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnB_AttuaFan1 },				// 0x005C
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnB_Giri2 },					// 0x005D
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnB_AttuaFan2 },				// 0x005E
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnB_AttuaFan3 },				// 0x005F
	{ MBTYPE_WORD, 0, (unsigned short *)&ACCC_BMAP },						// 0x0060
	{ MBTYPE_WORD, 0, (unsigned short *)&AccC_TOffCoclea },				// 0x0061
	{ MBTYPE_WORD, 0, (unsigned short *)&AccC_Portata },					// 0x0062
	{ MBTYPE_WORD, 0, (unsigned short *)&AccC_Esp },							// 0x0063
	{ MBTYPE_WORD, 0, (unsigned short *)&AccC_TOffFan1 },					// 0x0064
	{ MBTYPE_WORD, 0, (unsigned short *)&AccC_AttuaFan1 },					// 0x0065
	{ MBTYPE_WORD, 0, (unsigned short *)&AccC_Giri2 },						// 0x0066
	{ MBTYPE_WORD, 0, (unsigned short *)&AccC_AttuaFan2 },					// 0x0067
	{ MBTYPE_WORD, 0, (unsigned short *)&AccC_AttuaFan3 },					// 0x0068
	{ MBTYPE_WORD, 0, (unsigned short *)&SPENTA_BMAP },						// 0x0069
	{ MBTYPE_WORD, 0, (unsigned short *)&Spenta_TOffCoclea },				// 0x006A
	{ MBTYPE_WORD, 0, (unsigned short *)&Spenta_Portata },					// 0x006B
	{ MBTYPE_WORD, 0, (unsigned short *)&Spenta_Esp },						// 0x006C
	{ MBTYPE_WORD, 0, (unsigned short *)&Spenta_TOffFan1 },				// 0x006D
	{ MBTYPE_WORD, 0, (unsigned short *)&Spenta_AttuaFan1 },				// 0x006E
	{ MBTYPE_WORD, 0, (unsigned short *)&Spenta_Giri2 },					// 0x006F
	{ MBTYPE_WORD, 0, (unsigned short *)&Spenta_AttuaFan2 },				// 0x0070
	{ MBTYPE_WORD, 0, (unsigned short *)&Spenta_AttuaFan3 },				// 0x0071
	{ MBTYPE_WORD, 0, (unsigned short *)&SPEA_BMAP },						// 0x0072
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeA_TOffCoclea },				// 0x0073
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeA_Portata },					// 0x0074
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeA_Esp },							// 0x0075
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeA_TOffFan1 },					// 0x0076
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeA_AttuaFan1 },					// 0x0077
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeA_Giri2 },						// 0x0078
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeA_AttuaFan2 },					// 0x0079
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeA_AttuaFan3 },					// 0x007A
	{ MBTYPE_WORD, 0, (unsigned short *)&SPEB_BMAP },						// 0x007B
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeB_TOffCoclea },				// 0x007C
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeB_Portata },					// 0x007D
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeB_Esp },							// 0x007E
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeB_TOffFan1 },					// 0x007F
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeB_AttuaFan1 },					// 0x0080
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeB_Giri2 },						// 0x0081
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeB_AttuaFan2 },					// 0x0082
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeB_AttuaFan3 },					// 0x0083
	{ MBTYPE_WORD, 0, (unsigned short *)&RAFFA_BMAP },						// 0x0084
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffA_TOffCoclea },				// 0x0085
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffA_Portata },					// 0x0086
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffA_Esp },						// 0x0087
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffA_TOffFan1 },					// 0x0088
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffA_AttuaFan1 },				// 0x0089
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffA_Giri2 },						// 0x008A
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffA_AttuaFan2 },				// 0x008B
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffA_AttuaFan3 },				// 0x008C
	{ MBTYPE_WORD, 0, (unsigned short *)&RAFFB_BMAP },						// 0x008D
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffB_TOffCoclea },				// 0x008E
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffB_Portata },					// 0x008F
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffB_Esp },						// 0x0090
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffB_TOffFan1 },					// 0x0091
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffB_AttuaFan1 },				// 0x0092
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffB_Giri2 },						// 0x0093
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffB_AttuaFan2 },				// 0x0094
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffB_AttuaFan3 },				// 0x0095
	{ MBTYPE_WORD, 0, (unsigned short *)&SPEC_BMAP },						// 0x0096
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeC_TOffCoclea },				// 0x0097
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeC_Portata },					// 0x0098
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeC_Esp },							// 0x0099
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeC_TOffFan1 },					// 0x009A
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeC_AttuaFan1 },					// 0x009B
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeC_Giri2 },						// 0x009C
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeC_AttuaFan2 },					// 0x009D
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeC_AttuaFan3 },					// 0x009E
	{ MBTYPE_BYTE, 0, (unsigned short *)&DeltaTempCaldo },					// 0x009F
	{ MBTYPE_WORD, 0, (unsigned short *)&TPreAcc1 },							// 0x00A0
	{ MBTYPE_WORD, 0, (unsigned short *)&TPreAcc2 },							// 0x00A1
	{ MBTYPE_WORD, 0, (unsigned short *)&TPreAccCaldo },					// 0x00A2
	{ MBTYPE_WORD, 0, (unsigned short *)&TWarmUp },							// 0x00A3
	{ MBTYPE_WORD, 0, (unsigned short *)&TAccB },								// 0x00A4
	{ MBTYPE_WORD, 0, (unsigned short *)&TAccC },								// 0x00A5
	{ MBTYPE_WORD, 0, (unsigned short *)&TFireOn },							// 0x00A6
	{ MBTYPE_WORD, 0, (unsigned short *)&TFireOnB },							// 0x00A7
	{ MBTYPE_WORD, 0, (unsigned short *)&TSpeA },								// 0x00A8
	{ MBTYPE_WORD, 0, (unsigned short *)&TSpeB },								// 0x00A9
	{ MBTYPE_WORD, 0, (unsigned short *)&TSpeC },								// 0x00AA
	{ MBTYPE_WORD, 0, (unsigned short *)&TRaffA },							// 0x00AB
	{ MBTYPE_WORD, 0, (unsigned short *)&TRaffB },							// 0x00AC
	{ MBTYPE_WORD, 0, (unsigned short *)&PeriodoCoclea },					// 0x00AD
	{ MBTYPE_WORD, 0, (unsigned short *)&PeriodoFan1 },						// 0x00AE
	{ MBTYPE_BYTE, 0, (unsigned short *)&PreAcc1NumExec },					// 0x00AF
	{ MBTYPE_BYTE, 0, (unsigned short *)&PreAcc2NumExec },					// 0x00B0
	{ MBTYPE_WORD, 0, (unsigned short *)&FotoresThresOn },					// 0x00B1
	{ MBTYPE_WORD, 0, (unsigned short *)&FotoresThresOff },				// 0x00B2
	{ MBTYPE_WORD, 0, (unsigned short *)&PULIZIA_BMAP },					// 0x00B3
	{ MBTYPE_WORD, 0, (unsigned short *)&Pulizia_TOffCoclea },			// 0x00B4
	{ MBTYPE_WORD, 0, (unsigned short *)&Pulizia_Portata },				// 0x00B5
	{ MBTYPE_WORD, 0, (unsigned short *)&Pulizia_Esp },						// 0x00B6
	{ MBTYPE_WORD, 0, (unsigned short *)&Pulizia_TOffFan1 },				// 0x00B7
	{ MBTYPE_WORD, 0, (unsigned short *)&Pulizia_AttuaFan1 },				// 0x00B8
	{ MBTYPE_WORD, 0, (unsigned short *)&Pulizia_Giri2 },					// 0x00B9
	{ MBTYPE_WORD, 0, (unsigned short *)&Pulizia_AttuaFan2 },				// 0x00BA
	{ MBTYPE_WORD, 0, (unsigned short *)&Pulizia_AttuaFan3 },				// 0x00BB
	{ MBTYPE_WORD, 0, (unsigned short *)&POT1_BMAP },						// 0x00BC
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot1_TOffCoclea },				// 0x00BD
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot1_Portata },					// 0x00BE
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot1_Esp },							// 0x00BF
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot1_TOffFan1 },					// 0x00C0
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot1_AttuaFan1 },					// 0x00C1
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot1_Giri2 },						// 0x00C2
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot1_AttuaFan2 },					// 0x00C3
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot1_AttuaFan3 },					// 0x00C4
	{ MBTYPE_WORD, 0, (unsigned short *)&POT2_BMAP },						// 0x00C5
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot2_TOffCoclea },				// 0x00C6
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot2_Portata },					// 0x00C7
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot2_Esp },							// 0x00C8
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot2_TOffFan1 },					// 0x00C9
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot2_AttuaFan1 },					// 0x00CA
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot2_Giri2 },						// 0x00CB
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot2_AttuaFan2 },					// 0x00CC
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot2_AttuaFan3 },					// 0x00CD
	{ MBTYPE_WORD, 0, (unsigned short *)&POT3_BMAP },						// 0x00CE
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot3_TOffCoclea },				// 0x00CF
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot3_Portata },					// 0x00D0
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot3_Esp },							// 0x00D1
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot3_TOffFan1 },					// 0x00D2
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot3_AttuaFan1 },					// 0x00D3
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot3_Giri2 },						// 0x00D4
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot3_AttuaFan2 },					// 0x00D5
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot3_AttuaFan3 },					// 0x00D6
	{ MBTYPE_WORD, 0, (unsigned short *)&POT4_BMAP },						// 0x00D7
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot4_TOffCoclea },				// 0x00D8
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot4_Portata },					// 0x00D9
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot4_Esp },							// 0x00DA
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot4_TOffFan1 },					// 0x00DB
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot4_AttuaFan1 },					// 0x00DC
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot4_Giri2 },						// 0x00DD
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot4_AttuaFan2 },					// 0x00DE
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot4_AttuaFan3 },					// 0x00DF
	{ MBTYPE_WORD, 0, (unsigned short *)&POT5_BMAP },						// 0x00E0
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot5_TOffCoclea },				// 0x00E1
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot5_Portata },					// 0x00E2
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot5_Esp },							// 0x00E3
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot5_TOffFan1 },					// 0x00E4
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot5_AttuaFan1 },					// 0x00E5
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot5_Giri2 },						// 0x00E6
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot5_AttuaFan2 },					// 0x00E7
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot5_AttuaFan3 },					// 0x00E8
	{ MBTYPE_WORD, 0, (unsigned short *)&POT6_BMAP },						// 0x00E9
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot6_TOffCoclea },				// 0x00EA
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot6_Portata },					// 0x00EB
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot6_Esp },							// 0x00EC
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot6_TOffFan1 },					// 0x00ED
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot6_AttuaFan1 },					// 0x00EE
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot6_Giri2 },						// 0x00EF
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot6_AttuaFan2 },					// 0x00F0
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot6_AttuaFan3 },					// 0x00F1
	{ MBTYPE_WORD, 0, (unsigned short *)&POT7_BMAP },						// 0x00F2
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot7_TOffCoclea },				// 0x00F3
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot7_Portata },					// 0x00F4
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot7_Esp },							// 0x00F5
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot7_TOffFan1 },					// 0x00F6
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot7_AttuaFan1 },					// 0x00F7
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot7_Giri2 },						// 0x00F8
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot7_AttuaFan2 },					// 0x00F9
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot7_AttuaFan3 },					// 0x00FA
	{ MBTYPE_WORD, 0, (unsigned short *)&POT8_BMAP },						// 0x00FB
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot8_TOffCoclea },				// 0x00FC
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot8_Portata },					// 0x00FD
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot8_Esp },							// 0x00FE
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot8_TOffFan1 },					// 0x00FF
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot8_AttuaFan1 },					// 0x0100
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot8_Giri2 },						// 0x0101
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot8_AttuaFan2 },					// 0x0102
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot8_AttuaFan3 },					// 0x0103
	{ MBTYPE_WORD, 0, (unsigned short *)&PULIZIAB_BMAP },					// 0x0104
	{ MBTYPE_WORD, 0, (unsigned short *)&PuliziaB_TOffCoclea },			// 0x0105
	{ MBTYPE_WORD, 0, (unsigned short *)&PuliziaB_Portata },				// 0x0106
	{ MBTYPE_WORD, 0, (unsigned short *)&PuliziaB_Esp },					// 0x0107
	{ MBTYPE_WORD, 0, (unsigned short *)&PuliziaB_TOffFan1 },				// 0x0108
	{ MBTYPE_WORD, 0, (unsigned short *)&PuliziaB_AttuaFan1 },			// 0x0109
	{ MBTYPE_WORD, 0, (unsigned short *)&PuliziaB_Giri2 },					// 0x010A
	{ MBTYPE_WORD, 0, (unsigned short *)&PuliziaB_AttuaFan2 },			// 0x010B
	{ MBTYPE_WORD, 0, (unsigned short *)&PuliziaB_AttuaFan3 },			// 0x010C
	{ MBTYPE_BYTE_MASK, 0x0F, (unsigned short *)&POWER_BYTE },			// 0x010D
	{ MBTYPE_WORD, 0, (unsigned short *)&EspRitardoInc },					// 0x010E
	{ MBTYPE_WORD, 0, (unsigned short *)&EspRitardoDec },					// 0x010F
	{ MBTYPE_WORD, 0, (unsigned short *)&PotRitardoInc },					// 0x0110
	{ MBTYPE_WORD, 0, (unsigned short *)&PotRitardoDec },					// 0x0111
	{ MBTYPE_WORD, 0, (unsigned short *)&TPB },								// 0x0112
	{ MBTYPE_WORD, 0, (unsigned short *)&TPBB },								// 0x0113
	{ MBTYPE_WORD, 0, (unsigned short *)&TWaitPB },							// 0x0114
	{ MBTYPE_BYTE, 0, (unsigned short *)&Ossigeno },							// 0x0115
	{ MBTYPE_BYTE, 0, (unsigned short *)&K_Sonda },							// 0x0116
	{ MBTYPE_BYTE, 0, (unsigned short *)&ConfigIdro },						// 0x0117
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0118
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0119
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x011A
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x011B
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x011C
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x011D
	{ MBTYPE_WORD, 0, (unsigned short *)&HOLDBMAP286 },						// 0x011E
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x011F
	{ MBTYPE_BYTE, 0, (unsigned short *)&GradienteAlmFumi },				// 0x0120
	{ MBTYPE_WORD, 0, (unsigned short *)&DurataMaxPreAllFumi },			// 0x0121
	{ MBTYPE_WORD, 0, (unsigned short *)&DurataPreAllPellet },			// 0x0122
	{ MBTYPE_BYTE, 0, (unsigned short *)&DeltaTAssenzaFiamma },			// 0x0123
	{ MBTYPE_WORD, 0, (unsigned short *)&TAlarmPorta },						// 0x0124
	{ MBTYPE_WORD, 0, (unsigned short *)&TPreAlmPorta },					// 0x0125
	{ MBTYPE_WORD, 0, (unsigned short *)&TPreAlmAriaCombust },			// 0x0126
	{ MBTYPE_WORD, 0, (unsigned short *)&PortataCritica },					// 0x0127
	{ MBTYPE_BYTE, 0, (unsigned short *)&DeltaPortata },					// 0x0128
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0129
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x012A
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x012B
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x012C
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x012D
	{ MBTYPE_BYTE, 0, (unsigned short *)&MinSetFan1 },						// 0x012E
	{ MBTYPE_BYTE, 0, (unsigned short *)&MinSetFan2 },						// 0x012F
	{ MBTYPE_BYTE, 0, (unsigned short *)&MinSetFan3 },						// 0x0130
	{ MBTYPE_WORD, 0, (unsigned short *)&HOLDBMAP305 },						// 0x0131
	{ MBTYPE_WORD, 0, (unsigned short *)&HOLDBMAP306 },						// 0x0132
	{ MBTYPE_BYTE, 0, (unsigned short *)&TipoMotoreEsp2 },					// 0x0133
	{ MBTYPE_BYTE, 0, (unsigned short *)&TipoMotoreEsp },					// 0x0134
	{ MBTYPE_BYTE, 0, (unsigned short *)&TipoFrenataCoclea },				// 0x0135
	{ MBTYPE_BYTE, 0, (unsigned short *)&TipoFrenataFan1 },				// 0x0136
	{ MBTYPE_WORD, 0, (unsigned short *)&PesataPellet },					// 0x0137
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0138
	{ MBTYPE_WORD, 0, (unsigned short *)&DurataBlkoutRecovery },			// 0x0139
	{ MBTYPE_WORD, 0, (unsigned short *)&TWaitScuot },						// 0x013A
	{ MBTYPE_WORD, 0, (unsigned short *)&ScuotDurataCiclo },				// 0x013B
	{ MBTYPE_BYTE, 0, (unsigned short *)&ScuotNCicli },						// 0x013C
	{ MBTYPE_DWORD, 1, (unsigned short *)&OreFunz+1 },						// 0x013D
	{ MBTYPE_DWORD, 0, (unsigned short *)&OreFunz },							// 0x013E
	{ MBTYPE_WORD, 0, (unsigned short *)&OreSATLimite },					// 0x013F
	{ MBTYPE_WORD, 0, (unsigned short *)&OreSAT },							// 0x0140
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0141
	{ MBTYPE_WORD, 0, (unsigned short *)&TBLACKOUT },						// 0x0142	// TBLACKOUT
	{ MBTYPE_BYTE, 0, (unsigned short *)&StatSec.LastStatoFunz },		// 0x0143	//	LastStatoFunz
	{ MBTYPE_INT, 0, (unsigned short *)&TEMP_AMB_PAN },						// 0x0144	// TEMP_AMB_PAN
	{ MBTYPE_BYTE, 0, (unsigned short *)&PanVerFW[0] },						// 0x0145	// PAN codSW
	{ MBTYPE_BYTE, 0, (unsigned short *)&PanVerFW[1] },						// 0x0146	// PAN verSW
	{ MBTYPE_BYTE, 0, (unsigned short *)&PanVerFW[2] },						// 0x0147	// PAN revSW
	{ MBTYPE_BYTE, 0, (unsigned short *)&PanVerFW[3] },						// 0x0148	// PAN buildSW

};

/*!
	\var LimitsReq_tab
	\brief Tabella delle richieste dei limiti per gli Holding Registers
	\note Richiesta limiti di max 4 registri
*/
const LIMITSREQ_STRUCT LimitsReq_tab[LIMITSREQ_NUM] =
{
	{ 0x0008, 4 },
	{ 0x000C, 4 },
	{ 0x0010, 4 },
	{ 0x0014, 3 },
	{ 0x001D, 4 },
	{ 0x0022, 4 },
	{ 0x0026, 4 },
	{ 0x002B, 4 },
	{ 0x002F, 4 },
	{ 0x0034, 4 },
	{ 0x0038, 4 },
	{ 0x003D, 4 },
	{ 0x0041, 4 },
	{ 0x0046, 4 },
	{ 0x004A, 4 },
	{ 0x004F, 4 },
	{ 0x0053, 4 },
	{ 0x0058, 4 },
	{ 0x005C, 4 },
	{ 0x0061, 4 },
	{ 0x0065, 4 },
	{ 0x006A, 4 },
	{ 0x006E, 4 },
	{ 0x0073, 4 },
	{ 0x0077, 4 },
	{ 0x007C, 4 },
	{ 0x0080, 4 },
	{ 0x0085, 4 },
	{ 0x0089, 4 },
	{ 0x008E, 4 },
	{ 0x0092, 4 },
	{ 0x0097, 4 },
	{ 0x009B, 4 },
	{ 0x009F, 4 },
	{ 0x00A3, 4 },
	{ 0x00A7, 4 },
	{ 0x00AB, 4 },
	{ 0x00AF, 4 },
	{ 0x00B4, 4 },
	{ 0x00B8, 4 },
	{ 0x00BD, 4 },
	{ 0x00C1, 4 },
	{ 0x00C6, 4 },
	{ 0x00CA, 4 },
	{ 0x00CF, 4 },
	{ 0x00D3, 4 },
	{ 0x00D8, 4 },
	{ 0x00DC, 4 },
	{ 0x00E1, 4 },
	{ 0x00E5, 4 },
	{ 0x00EA, 4 },
	{ 0x00EE, 4 },
	{ 0x00F3, 4 },
	{ 0x00F7, 4 },
	{ 0x00FC, 4 },
	{ 0x0100, 4 },
	{ 0x0105, 4 },
	{ 0x0109, 4 },
	{ 0x010D, 4 },
	{ 0x0111, 4 },
	{ 0x0115, 3 },
	{ 0x0120, 4 },
	{ 0x0124, 4 },
	{ 0x0128, 1 },
	{ 0x012E, 3 },
	{ 0x0133, 4 },
	{ 0x0137, 1 },
	{ 0x0139, 4 },
	{ 0x013D, 4 },
};

/*!
	\var HoldReg2_tab
	\brief Tabella MB degli Holding Registers 2
	
		PARAMETRI CON LIMITI VARIABILI DURANTE IL FUNZIONAMENTO
*/
const MBDATA HoldReg2_tab[THOLREG2_NREG] =
{
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc1_TOnCoclea },				// 0x0400
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc1_TOnFan1 },				// 0x0401
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc2_TOnCoclea },				// 0x0402
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAcc2_TOnFan1 },				// 0x0403
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAccCaldo_TOnCoclea },		// 0x0404
	{ MBTYPE_WORD, 0, (unsigned short *)&PreAccCaldo_TOnFan1 },			// 0x0405
	{ MBTYPE_WORD, 0, (unsigned short *)&AccA_TOnCoclea },					// 0x0406
	{ MBTYPE_WORD, 0, (unsigned short *)&AccA_TOnFan1 },					// 0x0407
	{ MBTYPE_WORD, 0, (unsigned short *)&AccB_TOnCoclea },					// 0x0408
	{ MBTYPE_WORD, 0, (unsigned short *)&AccB_TOnFan1 },					// 0x0409
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnA_TOnCoclea },				// 0x040A
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnA_TOnFan1 },				// 0x040B
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnB_TOnCoclea },				// 0x040C
	{ MBTYPE_WORD, 0, (unsigned short *)&FireOnB_TOnFan1 },				// 0x040D
	{ MBTYPE_WORD, 0, (unsigned short *)&AccC_TOnCoclea },					// 0x040E
	{ MBTYPE_WORD, 0, (unsigned short *)&AccC_TOnFan1 },					// 0x040F
	{ MBTYPE_WORD, 0, (unsigned short *)&Spenta_TOnCoclea },				// 0x0410
	{ MBTYPE_WORD, 0, (unsigned short *)&Spenta_TOnFan1 },					// 0x0411
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeA_TOnCoclea },					// 0x0412
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeA_TOnFan1 },					// 0x0413
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeB_TOnCoclea },					// 0x0414
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeB_TOnFan1 },					// 0x0415
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffA_TOnCoclea },				// 0x0416
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffA_TOnFan1 },					// 0x0417
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffB_TOnCoclea },				// 0x0418
	{ MBTYPE_WORD, 0, (unsigned short *)&RaffB_TOnFan1 },					// 0x0419
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeC_TOnCoclea },					// 0x041A
	{ MBTYPE_WORD, 0, (unsigned short *)&SpeC_TOnFan1 },					// 0x041B
	{ MBTYPE_WORD, 0, (unsigned short *)&Pulizia_TOnCoclea },				// 0x041C
	{ MBTYPE_WORD, 0, (unsigned short *)&Pulizia_TOnFan1 },				// 0x041D
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot1_TOnCoclea },					// 0x041E
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot1_TOnFan1 },					// 0x041F
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot2_TOnCoclea },					// 0x0420
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot2_TOnFan1 },					// 0x0421
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot3_TOnCoclea },					// 0x0422
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot3_TOnFan1 },					// 0x0423
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot4_TOnCoclea },					// 0x0424
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot4_TOnFan1 },					// 0x0425
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot5_TOnCoclea },					// 0x0426
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot5_TOnFan1 },					// 0x0427
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot6_TOnCoclea },					// 0x0428
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot6_TOnFan1 },					// 0x0429
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot7_TOnCoclea },					// 0x042A
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot7_TOnFan1 },					// 0x042B
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot8_TOnCoclea },					// 0x042C
	{ MBTYPE_WORD, 0, (unsigned short *)&Pot8_TOnFan1 },					// 0x042D
	{ MBTYPE_WORD, 0, (unsigned short *)&PuliziaB_TOnCoclea },			// 0x042E
	{ MBTYPE_WORD, 0, (unsigned short *)&PuliziaB_TOnFan1 },				// 0x042F
	{ MBTYPE_BYTE_MASK, 0xF0, (unsigned short *)&POWER_BYTE },			// 0x0430
	{ MBTYPE_BYTE_MASK, 0x07, (unsigned short *)&MODE_BYTE },				// 0x0431
	{ MBTYPE_BYTE_MASK, 0x38, (unsigned short *)&MODE_BYTE },				// 0x0432
	{ MBTYPE_BYTE_MASK, 0x07, (unsigned short *)&FAN3_BYTE },				// 0x0433
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0434
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0435
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0436
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0437
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0438
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x0439
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x043A
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x043B
	{ MBTYPE_BYTE, 0, (unsigned short *)&MinPowerPB },						// 0x043C
	{ MBTYPE_WORD, 0, (unsigned short *)&TempOnFumi },						// 0x043D
	{ MBTYPE_WORD, 0, (unsigned short *)&TempSogliaFumi },					// 0x043E
	{ MBTYPE_WORD, 0, (unsigned short *)&TempOffFumi },						// 0x043F
	{ MBTYPE_WORD, 0, (unsigned short *)&TempPreAlarmFumi },				// 0x0440
	{ MBTYPE_WORD, 0, (unsigned short *)&TempAlarmFumi },					// 0x0441
	{ MBTYPE_WORD, 0, (unsigned short *)&TempOnScamb },						// 0x0442
	{ MBTYPE_WORD, 0, (unsigned short *)&TempSogliaScamb },				// 0x0443
	
};
		
/*!
	\var HoldReg3_tab
	\brief Tabella MB degli Holding Registers 3
	
		PARAMETRI CON LIMITI STANDARD CONOSCIUTI DAI DISPOSITIVI REMOTI
*/
const MBDATA HoldReg3_tab[THOLREG3_NREG] =
{
	{ MBTYPE_BYTE, 0, (unsigned short *)&IdRF },								// 0x2000
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2001
	{ MBTYPE_WORD, 0, (unsigned short *)&HOLDBMAP8194 },					// 0x2002
	{ MBTYPE_WORD, 0, (unsigned short *)&PortaIP },							// 0x2003
	{ MBTYPE_BYTE, 1, (unsigned short *)&Ip[0] },								// 0x2004
	{ MBTYPE_BYTE, 3, (unsigned short *)&Ip[2] },								// 0x2005
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2006
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2007
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2008
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2009
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x200A
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x200B
	{ MBTYPE_BYTE, 1, (unsigned short *)&Dns[0] },							// 0x200C
	{ MBTYPE_BYTE, 3, (unsigned short *)&Dns[2] },							// 0x200D
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x200E
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x200F
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2010
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2011
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2012
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2013
	{ MBTYPE_BYTE, 1, (unsigned short *)&Gateway[0] },						// 0x2014
	{ MBTYPE_BYTE, 3, (unsigned short *)&Gateway[2] },						// 0x2015
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2016
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2017
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2018
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2019
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x201A
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x201B
	{ MBTYPE_BYTE, 1, (unsigned short *)&Subnet[0] },						// 0x201C
	{ MBTYPE_BYTE, 3, (unsigned short *)&Subnet[2] },						// 0x201D
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x201E
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x201F
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2020
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2021
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2022
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2023
	{ MBTYPE_CHAR, 1, (unsigned short *)&Ssid[0] },							// 0x2024
	{ MBTYPE_CHAR, 3, (unsigned short *)&Ssid[2] },							// 0x2025
	{ MBTYPE_CHAR, 5, (unsigned short *)&Ssid[4] },							// 0x2026
	{ MBTYPE_CHAR, 7, (unsigned short *)&Ssid[6] },							// 0x2027
	{ MBTYPE_CHAR, 9, (unsigned short *)&Ssid[8] },							// 0x2028
	{ MBTYPE_CHAR, 11, (unsigned short *)&Ssid[10] },						// 0x2029
	{ MBTYPE_CHAR, 13, (unsigned short *)&Ssid[12] },						// 0x202A
	{ MBTYPE_CHAR, 15, (unsigned short *)&Ssid[14] },						// 0x202B
	{ MBTYPE_CHAR, 17, (unsigned short *)&Ssid[16] },						// 0x202C
	{ MBTYPE_CHAR, 19, (unsigned short *)&Ssid[18] },						// 0x202D
	{ MBTYPE_CHAR, 21, (unsigned short *)&Ssid[20] },						// 0x202E
	{ MBTYPE_CHAR, 23, (unsigned short *)&Ssid[22] },						// 0x202F
	{ MBTYPE_CHAR, 25, (unsigned short *)&Ssid[24] },						// 0x2030
	{ MBTYPE_CHAR, 27, (unsigned short *)&Ssid[26] },						// 0x2031
	{ MBTYPE_CHAR, 29, (unsigned short *)&Ssid[28] },						// 0x2032
	{ MBTYPE_CHAR, 31, (unsigned short *)&Ssid[30] },						// 0x2033
	{ MBTYPE_CHAR, 1, (unsigned short *)&CryptoKey[0] },					// 0x2034
	{ MBTYPE_CHAR, 3, (unsigned short *)&CryptoKey[2] },					// 0x2035
	{ MBTYPE_CHAR, 5, (unsigned short *)&CryptoKey[4] },					// 0x2036
	{ MBTYPE_CHAR, 7, (unsigned short *)&CryptoKey[6] },					// 0x2037
	{ MBTYPE_CHAR, 9, (unsigned short *)&CryptoKey[8] },					// 0x2038
	{ MBTYPE_CHAR, 11, (unsigned short *)&CryptoKey[10] },					// 0x2039
	{ MBTYPE_CHAR, 13, (unsigned short *)&CryptoKey[12] },					// 0x203A
	{ MBTYPE_CHAR, 15, (unsigned short *)&CryptoKey[14] },					// 0x203B
	{ MBTYPE_CHAR, 17, (unsigned short *)&CryptoKey[16] },					// 0x203C
	{ MBTYPE_CHAR, 19, (unsigned short *)&CryptoKey[18] },					// 0x203D
	{ MBTYPE_CHAR, 21, (unsigned short *)&CryptoKey[20] },					// 0x203E
	{ MBTYPE_CHAR, 23, (unsigned short *)&CryptoKey[22] },					// 0x203F
	{ MBTYPE_CHAR, 25, (unsigned short *)&CryptoKey[24] },					// 0x2040
	{ MBTYPE_CHAR, 27, (unsigned short *)&CryptoKey[26] },					// 0x2041
	{ MBTYPE_CHAR, 29, (unsigned short *)&CryptoKey[28] },					// 0x2042
	{ MBTYPE_CHAR, 31, (unsigned short *)&CryptoKey[30] },					// 0x2043
	{ MBTYPE_CHAR, 33, (unsigned short *)&CryptoKey[32] },					// 0x2044
	{ MBTYPE_CHAR, 35, (unsigned short *)&CryptoKey[34] },					// 0x2045
	{ MBTYPE_CHAR, 37, (unsigned short *)&CryptoKey[36] },					// 0x2046
	{ MBTYPE_CHAR, 39, (unsigned short *)&CryptoKey[38] },					// 0x2047
	{ MBTYPE_CHAR, 41, (unsigned short *)&CryptoKey[40] },					// 0x2048
	{ MBTYPE_CHAR, 43, (unsigned short *)&CryptoKey[42] },					// 0x2049
	{ MBTYPE_CHAR, 45, (unsigned short *)&CryptoKey[44] },					// 0x204A
	{ MBTYPE_CHAR, 47, (unsigned short *)&CryptoKey[46] },					// 0x204B
	{ MBTYPE_CHAR, 49, (unsigned short *)&CryptoKey[48] },					// 0x204C
	{ MBTYPE_CHAR, 51, (unsigned short *)&CryptoKey[50] },					// 0x204D
	{ MBTYPE_CHAR, 53, (unsigned short *)&CryptoKey[52] },					// 0x204E
	{ MBTYPE_CHAR, 55, (unsigned short *)&CryptoKey[54] },					// 0x204F
	{ MBTYPE_CHAR, 57, (unsigned short *)&CryptoKey[56] },					// 0x2050
	{ MBTYPE_CHAR, 59, (unsigned short *)&CryptoKey[58] },					// 0x2051
	{ MBTYPE_CHAR, 61, (unsigned short *)&CryptoKey[60] },					// 0x2052
	{ MBTYPE_CHAR, 63, (unsigned short *)&CryptoKey[62] },					// 0x2053
	{ MBTYPE_CHAR, 1, (unsigned short *)&WPSPin[0] },						// 0x2054
	{ MBTYPE_CHAR, 3, (unsigned short *)&WPSPin[2] },						// 0x2055
	{ MBTYPE_CHAR, 1, (unsigned short *)&Local_ISO3166_1_Code[0] },	// 0x2056
	{ MBTYPE_BYTE, 0, (unsigned short *)&OTdelay },							// 0x2057
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2058
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2059
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x205A
	{ MBTYPE_WORD, 0, (unsigned short *)&NumAcc },							// 0x205B
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x205C
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x205D
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x205E
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x205F
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2060
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2061
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2062
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2063
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2064
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2065
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2066
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2067
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2068
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2069
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x206A
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x206B
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x206C
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x206D
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x206E
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x206F
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2070
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2071
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2072
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2073
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2074
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2075
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2076
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2077
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2078
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x2079
		{ MBTYPE_BYTE, 0, (unsigned short *)&DummyHoldingReg },			// 0x207A
	{ MBTYPE_WORD, 0, (unsigned short *)&HOLDBMAP8315 },					// 0x207B
	{ MBTYPE_WORD, 0, (unsigned short *)&HOLDBMAP8316 },					// 0x207C
	{ MBTYPE_INT, 0, (unsigned short *)&TempP0 },								// 0x207D	// il valore e' espresso in step di 0.5 �C
	{ MBTYPE_INT, 0, (unsigned short *)&TempP1 },								// 0x207E	// il valore e' espresso in step di 0.5 �C
	{ MBTYPE_INT, 0, (unsigned short *)&TempP2 },								// 0x207F	// il valore e' espresso in step di 0.5 �C
	{ MBTYPE_INT, 0, (unsigned short *)&TempP3 },								// 0x2080	// il valore e' espresso in step di 0.5 �C
	{ MBTYPE_INT, 0, (unsigned short *)&TempP4 },								// 0x2081	// il valore e' espresso in step di 0.5 �C


};

/*!
	\var Limits3_tab
	\brief Tabella dei limiti variabili per gli Holding Registers 3
*/
const LIMITS_STRUCT Limits3_tab[THOLREG3_NREG] =
{
	{ 0, 255, 1 },									// 0x2000
		{ 0, 255, 1 },								// 0x2001
	{ 0, 0xFFFF, 1 },								// 0x2002	// bitmap
	{ 0, 0xFFFF, 1 },								// 0x2003
	{ 0, 0xFFFF, 1 },								// 0x2004
	{ 0, 0xFFFF, 1 },								// 0x2005
		{ 0, 255, 1 },								// 0x2006
		{ 0, 255, 1 },								// 0x2007
		{ 0, 255, 1 },								// 0x2008
		{ 0, 255, 1 },								// 0x2009
		{ 0, 255, 1 },								// 0x200A
		{ 0, 255, 1 },								// 0x200B
	{ 0, 0xFFFF, 1 },								// 0x200C
	{ 0, 0xFFFF, 1 },								// 0x200D
		{ 0, 255, 1 },								// 0x200E
		{ 0, 255, 1 },								// 0x200F
		{ 0, 255, 1 },								// 0x2010
		{ 0, 255, 1 },								// 0x2011
		{ 0, 255, 1 },								// 0x2012
		{ 0, 255, 1 },								// 0x2013
	{ 0, 0xFFFF, 1 },								// 0x2014
	{ 0, 0xFFFF, 1 },								// 0x2015
		{ 0, 255, 1 },								// 0x2016
		{ 0, 255, 1 },								// 0x2017
		{ 0, 255, 1 },								// 0x2018
		{ 0, 255, 1 },								// 0x2019
		{ 0, 255, 1 },								// 0x201A
		{ 0, 255, 1 },								// 0x201B
	{ 0, 0xFFFF, 1 },								// 0x201C
	{ 0, 0xFFFF, 1 },								// 0x201D
		{ 0, 255, 1 },								// 0x201E
		{ 0, 255, 1 },								// 0x201F
		{ 0, 255, 1 },								// 0x2020
		{ 0, 255, 1 },								// 0x2021
		{ 0, 255, 1 },								// 0x2022
		{ 0, 255, 1 },								// 0x2023
	{ 0x2020, 0x7F7F, 1 },						// 0x2024
	{ 0x2020, 0x7F7F, 1 },						// 0x2025
	{ 0x2020, 0x7F7F, 1 },						// 0x2026
	{ 0x2020, 0x7F7F, 1 },						// 0x2027
	{ 0x2020, 0x7F7F, 1 },						// 0x2028
	{ 0x2020, 0x7F7F, 1 },						// 0x2029
	{ 0x2020, 0x7F7F, 1 },						// 0x202A
	{ 0x2020, 0x7F7F, 1 },						// 0x202B
	{ 0x2020, 0x7F7F, 1 },						// 0x202C
	{ 0x2020, 0x7F7F, 1 },						// 0x202D
	{ 0x2020, 0x7F7F, 1 },						// 0x202E
	{ 0x2020, 0x7F7F, 1 },						// 0x202F
	{ 0x2020, 0x7F7F, 1 },						// 0x2030
	{ 0x2020, 0x7F7F, 1 },						// 0x2031
	{ 0x2020, 0x7F7F, 1 },						// 0x2032
	{ 0x2020, 0x7F7F, 1 },						// 0x2033
	{ 0, 0xFFFF, 1 },								// 0x2034
	{ 0, 0xFFFF, 1 },								// 0x2035
	{ 0, 0xFFFF, 1 },								// 0x2036
	{ 0, 0xFFFF, 1 },								// 0x2037
	{ 0, 0xFFFF, 1 },								// 0x2038
	{ 0, 0xFFFF, 1 },								// 0x2039
	{ 0, 0xFFFF, 1 },								// 0x203A
	{ 0, 0xFFFF, 1 },								// 0x203B
	{ 0, 0xFFFF, 1 },								// 0x203C
	{ 0, 0xFFFF, 1 },								// 0x203D
	{ 0, 0xFFFF, 1 },								// 0x203E
	{ 0, 0xFFFF, 1 },								// 0x203F
	{ 0, 0xFFFF, 1 },								// 0x2040
	{ 0, 0xFFFF, 1 },								// 0x2041
	{ 0, 0xFFFF, 1 },								// 0x2042
	{ 0, 0xFFFF, 1 },								// 0x2043
	{ 0, 0xFFFF, 1 },								// 0x2044
	{ 0, 0xFFFF, 1 },								// 0x2045
	{ 0, 0xFFFF, 1 },								// 0x2046
	{ 0, 0xFFFF, 1 },								// 0x2047
	{ 0, 0xFFFF, 1 },								// 0x2048
	{ 0, 0xFFFF, 1 },								// 0x2049
	{ 0, 0xFFFF, 1 },								// 0x204A
	{ 0, 0xFFFF, 1 },								// 0x204B
	{ 0, 0xFFFF, 1 },								// 0x204C
	{ 0, 0xFFFF, 1 },								// 0x204D
	{ 0, 0xFFFF, 1 },								// 0x204E
	{ 0, 0xFFFF, 1 },								// 0x204F
	{ 0, 0xFFFF, 1 },								// 0x2050
	{ 0, 0xFFFF, 1 },								// 0x2051
	{ 0, 0xFFFF, 1 },								// 0x2052
	{ 0, 0xFFFF, 1 },								// 0x2053
	{ 0, 0xFFFF, 1 },								// 0x2054
	{ 0, 0xFFFF, 1 },								// 0x2055
		{ 0, 255, 1 },								// 0x2056
		{ 0, 255, 1 },								// 0x2057
		{ 0, 255, 1 },								// 0x2058
		{ 0, 255, 1 },								// 0x2059
		{ 0, 255, 1 },								// 0x205A
	{ 0, 0xFFFF, 1 },								// 0x205B
		{ 0, 255, 1 },								// 0x205C
		{ 0, 255, 1 },								// 0x205D
		{ 0, 255, 1 },								// 0x205E
		{ 0, 255, 1 },								// 0x205F
		{ 0, 255, 1 },								// 0x2060
		{ 0, 255, 1 },								// 0x2061
		{ 0, 255, 1 },								// 0x2062
		{ 0, 255, 1 },								// 0x2063
		{ 0, 255, 1 },								// 0x2064
		{ 0, 255, 1 },								// 0x2065
		{ 0, 255, 1 },								// 0x2066
		{ 0, 255, 1 },								// 0x2067
		{ 0, 255, 1 },								// 0x2068
		{ 0, 255, 1 },								// 0x2069
		{ 0, 255, 1 },								// 0x206A
		{ 0, 255, 1 },								// 0x206B
		{ 0, 255, 1 },								// 0x206C
		{ 0, 255, 1 },								// 0x206D
		{ 0, 255, 1 },								// 0x206E
		{ 0, 255, 1 },								// 0x206F
		{ 0, 255, 1 },								// 0x2070
		{ 0, 255, 1 },								// 0x2071
		{ 0, 255, 1 },								// 0x2072
		{ 0, 255, 1 },								// 0x2073
		{ 0, 255, 1 },								// 0x2074
		{ 0, 255, 1 },								// 0x2075
		{ 0, 255, 1 },								// 0x2076
		{ 0, 255, 1 },								// 0x2077
		{ 0, 255, 1 },								// 0x2078
		{ 0, 255, 1 },								// 0x2079
		{ 0, 255, 1 },								// 0x207A
	{ 0, 0xFFFF, 1 },								// 0x207B	// bitmap
	{ 0, 0xFFFF, 1 },								// 0x207C	// bitmap
	{ 0, 0xFFFF, 1 },								// 0x207D
	{ 0, 0xFFFF, 1 },								// 0x207E
	{ 0, 0xFFFF, 1 },								// 0x207F
	{ 0, 0xFFFF, 1 },								// 0x2080
	{ 0, 0xFFFF, 1 },								// 0x2081


};


/* codice */
//#pragma SECTION program



