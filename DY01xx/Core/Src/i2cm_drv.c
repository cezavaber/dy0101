/****************************************************************************
* File      : I2CM_DRV.C
* Scopo     : Driver per I2C Master
****************************************************************************/

#include "main.h"
#include "i2cm_drv.h"


static short Esito;



/***************************************************************************
* FUNZIONE  : short i2cInit( void )
* ARGOMENTI : 
* SCOPO     : Inizializza driver I2C Master
* RETURN    : Il codice di esito
***************************************************************************/
short i2cInit( void )
{
	short RetVal = I2C_OK;
	
	Esito = I2C_OK;
	
	// I2C gia' inizializzata

	return RetVal;
}

/***************************************************************************
* FUNZIONE  : short i2cWriteBuffer( unsigned char *pBufWr, unsigned short sizeWr,
*								unsigned char *pData, unsigned short sizeData );
* SCOPO     : Scrittura di un buffer su canale I2C
* ARGOMENTI : pBufWr - buffer da scrivere (devAddr + memAddr)
*			  sizeWr - dimensione buffer da scrivere
*			  pData - buffer dati da scrivere
*			  sizeData - dimensione buffer dati
* RETURN    : Il codice di esito
***************************************************************************/
short i2cWriteBuffer( unsigned char *pBufWr, unsigned short sizeWr,
						unsigned char *pData, unsigned short sizeData )
{
	short RetVal = I2C_FAIL;
	const I2C_HandleTypeDef *I2cHandle = NULL;
	uint16_t DevAddress;
	uint16_t MemAddress;
	uint16_t MemAddSize;
	short *esito;
	
	if( sizeWr >= 1 )
	{
		I2cHandle = &hi2c1;
		esito = &Esito;
		if( (*esito != I2C_RUN)
			&& (HAL_I2C_GetState((I2C_HandleTypeDef *)I2cHandle) == HAL_I2C_STATE_READY)
			)
		{
			*esito = I2C_RUN;
			DevAddress = pBufWr[0] << 1;
			if( sizeWr == 1 )
			{
				// Check if the device is ready for a new operation
				if(
					HAL_I2C_IsDeviceReady(
											(I2C_HandleTypeDef *)I2cHandle,
											DevAddress, 
											1, 
											2
											) == HAL_OK
					)
				{
					*esito = I2C_OK;
				}
				else
				{
					*esito = I2C_FAIL;
				}
			}
			else
			{
				if( sizeWr < 3 )
				{
					// 8-bit address
					MemAddress = pBufWr[1];
					MemAddSize = I2C_MEMADD_SIZE_8BIT;
				}
				else
				{
					// 16-bit address
					MemAddress = MAKEWORD( pBufWr[2], pBufWr[1] );
					MemAddSize = I2C_MEMADD_SIZE_16BIT;
				}
				if(
					HAL_I2C_Mem_Write(
										(I2C_HandleTypeDef *)I2cHandle,
										DevAddress,
										MemAddress,
										MemAddSize,
										pData,
										sizeData,
										100
									) == HAL_OK
					)
				{
					*esito = I2C_OK;
				}
				else
				{
					*esito = I2C_FAIL;
				}
			}
			
			RetVal = *esito;
		}
	}
	
	return RetVal;
}

/***************************************************************************
* FUNZIONE  : short i2cWriteReadBuffer( unsigned char *pBufWr, unsigned short sizeWr,
*									unsigned char *pBufRd, unsigned short sizeRd )
* SCOPO     : Lettura di un buffer su canale I2C (RESTART)
* ARGOMENTI : pBufWr - buffer da scrivere (devAddr + memAddr)
*			  sizeWr - dimensione buffer da scrivere
*			  pBufRd - buffer da leggere
*			  sizeRd - dimensione buffer da leggere
* RETURN    : Il codice di esito
***************************************************************************/
short i2cWriteReadBuffer( unsigned char *pBufWr, unsigned short sizeWr,
						unsigned char *pBufRd, unsigned short sizeRd )
{
	short RetVal = I2C_FAIL;
	const I2C_HandleTypeDef *I2cHandle = NULL;
	uint16_t DevAddress;
	uint16_t MemAddress;
	uint16_t MemAddSize;
	short *esito;
	
	if( (sizeWr >= 2)
		&& (sizeRd > 0)
		)
	{
		I2cHandle = &hi2c1;
		esito = &Esito;
		if( (*esito != I2C_RUN)
			&& (HAL_I2C_GetState((I2C_HandleTypeDef *)I2cHandle) == HAL_I2C_STATE_READY)
			)
		{
			*esito = I2C_RUN;
			DevAddress = pBufWr[0] << 1;
			if( sizeWr < 3 )
			{
				// 8-bit address
				MemAddress = pBufWr[1];
				MemAddSize = I2C_MEMADD_SIZE_8BIT;
			}
			else
			{
				// 16-bit address
				MemAddress = MAKEWORD( pBufWr[2], pBufWr[1] );
				MemAddSize = I2C_MEMADD_SIZE_16BIT;
			}
			if(
				HAL_I2C_Mem_Read(
									(I2C_HandleTypeDef *)I2cHandle,
									DevAddress,
									MemAddress,
									MemAddSize,
									pBufRd,
									sizeRd,
									100
								) == HAL_OK
				)
			{
				*esito = I2C_OK;
			}
			else
			{
				*esito = I2C_FAIL;
			}
			
			RetVal = *esito;
		}
	}
		
	return RetVal;
}


