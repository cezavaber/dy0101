/*!
**  \file Led.c
**  \author Alessandro Vagniluca
**  \date 24/07/2015
**  \brief Led handler
**  
**  \version 1.0
**/

#include <stdlib.h>
#include <string.h>
#include "Led.h"
#include "timebase.h"




static unsigned char StLed[NLED];	//!< led state
static unsigned char cntBlink;		//!< led counter for blinking

static void DriveLed( unsigned char iLed, unsigned char state );
static void ToggleLed( unsigned char iLed );


/*!
** \fn void DriveLed( unsigned char iLed, unsigned char state )
** \brief Set the state to the led
** \param iLed led index
** \param state off (LED_OFF) / on (LED_ON) state
** \return None
**/
static void DriveLed( unsigned char iLed, unsigned char state )
{
	switch( iLed )
	{
		case BUZ:
			BSP_BUZ_OnOff( state );
		break;
	}
}

/*!
** \fn void ToggleLed( unsigned char iLed )
** \brief Led toggle
** \param iLed led index
** \return None
**/
static void ToggleLed( unsigned char iLed )
{
	switch( iLed )
	{
		case BUZ:
			BSP_BUZ_Toggle;
		break;
	}
}

/*!
** \fn void led_Init( void )
** \brief Init led handler
** \return None
**/
void led_Init( void )
{
	unsigned char i;
	
	//starttimer( TLED, T1DEC );
	cntBlink = 0;
	/* led OFF */
	memset( StLed, LEDSTAT_OFF, NLED );
	for( i=0; i<NLED; i++ )
	{
		/* led OFF */
		DriveLed( i, LED_OFF );
	}
}

/*!
** \fn void led_Handler(void)
** \brief Led handler
** \return None
**/
void led_Handler( void )
{
	unsigned char i;

	//if( checktimer( TLED ) == SCADUTO )
	{
		//starttimer( TLED, T1DEC );	// 100ms time risolution
		if( ++cntBlink >= 20 )	// period 2s=20*100ms (m.c.m. of all the possible periods)
		{
			cntBlink = 0;
			// at each period start with all leds off (only those driven automatically)
			for( i=0; i<NLED; i++ )
			{
				if( StLed[i] != LEDSTAT_MANUAL )
				{
					DriveLed( i, LED_OFF );
				}
			}
		}
		for( i=0; i<NLED; i++ )
		{
			switch( StLed[i] )
			{
				case LEDSTAT_OFF:
					/* led OFF */
					DriveLed( i, LED_OFF );
				break;

				case LEDSTAT_ON:
					/* led ON */
					DriveLed( i, LED_ON );
				break;

				case LEDSTAT_LOW_BLINK:
					/* led BLINKING Ton=Toff=1s */
					if( !(cntBlink % 10) )
						ToggleLed( i );
				break;

				case LEDSTAT_BLINK:
					/* led BLINKING Ton=Toff=500ms */
					if( !(cntBlink % 5) )
						ToggleLed( i );
				break;

				case LEDSTAT_HIGH_BLINK:
					/* led BLINKING Ton=Toff=200ms */
					if( !(cntBlink % 2) )
						ToggleLed( i );
				break;

				case LEDSTAT_VERY_HIGH_BLINK:
					/* led BLINKING Ton=Toff=100ms */
					ToggleLed( i );
				break;

				case LEDSTAT_1_IMPULSE_SHORT:
					/* led 1 IMPULSE Ton=200ms, Toff=1800ms */
					if( cntBlink < 2 )
						DriveLed( i, LED_ON );
					else
						DriveLed( i, LED_OFF );
				break;

				case LEDSTAT_1_IMPULSE:
					/* led 1 IMPULSE Ton=500ms, Toff=1500ms */
					if( cntBlink < 5 )
						DriveLed( i, LED_ON );
					else
						DriveLed( i, LED_OFF );
				break;

				case LEDSTAT_1_IMPULSE_LONG:
					/* led 1 IMPULSE Ton=1500ms, Toff=500ms */
					if( cntBlink < 15 )
						DriveLed( i, LED_ON );
					else
						DriveLed( i, LED_OFF );
				break;
			}
		}
	}
}

/*!
** \fn short led_Set( unsigned char iLed, unsigned char mode )
** \brief Set the led state
** \param iLed led index
** \param mode led mode
** \return 1: OK, 0: FAILED
**/
short led_Set( unsigned char iLed, unsigned char mode )
{
	short RetVal = 0;

	if( (iLed < NLED) && (mode < NLEDSTAT) )
	{
		StLed[iLed] = mode;
		RetVal = 1;
	}

	return RetVal;
}








