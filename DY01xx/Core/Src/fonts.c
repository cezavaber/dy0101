/*!
** \file fonts.c
** \author Alessandro Vagniluca
** \date 15/10/2015
** \brief Fonts definitions
** 
** \version 1.0
**/

/* Includes */
#include "main.h"
#include "fonts.h"


/* defines */


/* variables */

unsigned char NumFontGroup;	//!< number of fonts' groups
	// NumFontGroup = CtrlArea.param.FontNum / NFONT_PER_GROUP

/*!
** \var Font_RawData_Header
** \brief The font metric blocks.
**/
Font_header_t Font_RawData_Header;

/*!
** \var RAMFontTableAddr
** \brief The RAM font metric blocks GRAM address.
**/
ft_uint32_t RAMFontTableAddr[NUM_RAM_FONT];



/* costants */


/* function prototypes */



