/*!
	\file OT_Plus_ph_layer.c
	\brief Modulo OT_Plus_ph_layer.c

	Gestione del livello fisico di comunicazione su canale OT/+ .

*/

#include "main.h"
#include "nvram.h"
#include "mbcrc.h"
#include "OT_Plus_ph_layer.h"


/*!
	\var PwrMode
	\brief Indice power mode
*/
unsigned char PwrMode;

/*!
	\var rfID
	\brief Identificativo radio corrente
*/
unsigned char rfID;



#define TX_BUFF_MAX_LEN		(MAX_PACKET_SIZE+2)	// comprende il CRC16
static unsigned char pTxBuff[TX_BUFF_MAX_LEN];	// buffer da trasmettere
static unsigned short LenTxBuff;	// dimensione buffer da trasmettere
#define RX_BUFF_MAX_LEN		(MAX_PACKET_SIZE+2)	// comprende il CRC16
//static unsigned char pRxBuff[RX_BUFF_MAX_LEN];	// buffer di ricezione
#define pRxBuff		pTxBuff	// stesso buffer per RX e TX (half-duplex)
static unsigned short LenRxBuff;	// dimensione pacchetto ricevuto
#define RX_BUFF_MIN_LEN		1		// minima dimensione del pacchetto ricevuto
#define RX_BUFF_OVER		(RX_BUFF_MAX_LEN+1)		// segnala rx buffer overflow (deve essere > RX_BUFF_MAX_LEN)
static unsigned char rxChar;



#define TIMEOUT_RX			3	// 2ms (garantito), timeout di ricezione
static unsigned char cntMsec;	// contatore msec

#define T_RTS					2	// 1ms (garantito), tempo RTS attivo
#define T_CONF					100	// 10ms, ritardo fra attivazione CONF e invio richiesta config
static unsigned long wTime;
#define N_RETRY				3		// numero massimo tentativi configurazione
static unsigned char nRetry;


/*!
	\var bRxStat
	\brief Funzione call-back di ricezione
*/
static OTRxPkt pRx_CallBack;

static OTRxPkt pSaveRx_CallBack;
static unsigned char StCfg;


static void OT_PH_RxCfg( unsigned char *pkt, unsigned short pktSize );



/*!
	\fn short OT_PH_Init( OTRxPkt Rx_CallBack )
   \brief Inizializzazione physical layer OT/+
	\param Rx_CallBack Funzione call-back di ricezione
	\return 1: OK, 0: FAILED
*/
short OT_PH_Init( OTRxPkt Rx_CallBack )
{
	short RetVal = 1;

	//HAL_GPIO_WritePin(RESET_RF_GPIO_Port, RESET_RF_Pin, GPIO_PIN_RESET);	// RADIO reset on
	HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_SET);	// disable RADIO config
	HAL_GPIO_WritePin(PD_RF_GPIO_Port, PD_RF_Pin, GPIO_PIN_RESET);	// RADIO power-down on
	HAL_GPIO_WritePin(USART2_RTS_GPIO_Port, USART2_RTS_Pin, GPIO_PIN_SET);		// disable RADIO TX to host
	
	rfID = RFID_DEF;

	OT_ResetRXTX();
	
	PwrMode = PWR_MODE_HIGH;		// start with high-power mode
	pRx_CallBack = Rx_CallBack;
	
	HAL_GPIO_WritePin(RESET_RF_GPIO_Port, RESET_RF_Pin, GPIO_PIN_SET);	// RADIO reset off
	DWT_Delay_us( 5 );
	HAL_GPIO_WritePin(USART2_RTS_GPIO_Port, USART2_RTS_Pin, GPIO_PIN_RESET);	// enable RADIO TX to host
	HAL_GPIO_WritePin(PD_RF_GPIO_Port, PD_RF_Pin, GPIO_PIN_SET);	// RADIO power-down off

	return RetVal;
}

/*!
	\fn void OT_ResetRX( void )
   \brief Reset della ricezione
*/
void OT_ResetRX( void )
{
	// FlushRX
	while( HAL_UART_Receive(&huart2, &rxChar, 1, 0) == HAL_OK );
	LenRxBuff = 0;

	/* Initialize the UART state */
	huart2.ErrorCode = HAL_UART_ERROR_NONE;
	huart2.gState= HAL_UART_STATE_READY;
	huart2.RxState= HAL_UART_STATE_READY;
}

/*!
	\fn void OT_ResetRXTX( void )
   \brief Reset della comunicazione
*/
void OT_ResetRXTX( void )
{
#ifndef RX_ONLY
	// disable RX interrupt
	__HAL_UART_DISABLE_IT(&huart2, UART_IT_RXNE);
	// disable TX interrupt
	__HAL_UART_DISABLE_IT(&huart2, UART_IT_TXE);
	__HAL_UART_DISABLE_IT(&huart2, UART_IT_TC);
	
	LenTxBuff = 0;
	cntMsec = 0;
	OT_ResetRX();
	//HAL_GPIO_WritePin(USART2_RTS_GPIO_Port, USART2_RTS_Pin, GPIO_PIN_SET);		// disable RADIO TX
	
#endif
}

/**
  * @brief  Rx Transfer completed callbacks.
  * @param  huart: pointer to a UART_HandleTypeDef structure that contains
  *                the configuration information for the specified UART module.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if( huart->Instance == USART2 )
	{
		pRxBuff[LenRxBuff] = rxChar;
		if( ++LenRxBuff >= RX_BUFF_MAX_LEN )
		{
			// RX buffer overflow -> suspend reception and wait timeout
			LenRxBuff = RX_BUFF_OVER;
		}
		else
		{
			// predispone nuova Rx dato
			HAL_UART_Receive_IT( huart, &rxChar, 1 );
		}
		cntMsec = TIMEOUT_RX;	// ricarica timeout inter-carattere
	}
	else if( huart->Instance == USART6 )
	{
		//USART6_RxCpltCallback();
	}
}

/**
  * @brief  Tx Transfer completed callbacks.
  * @param  huart: pointer to a UART_HandleTypeDef structure that contains
  *                the configuration information for the specified UART module.
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if( huart->Instance == USART2 )
	{
		/* abilita ricezione */
		cntMsec = 0;
		LenRxBuff = 0;
		HAL_UART_Receive_IT( huart, &rxChar, 1 );
		if( HAL_GPIO_ReadPin(CONF_RF_GPIO_Port,CONF_RF_Pin) == GPIO_PIN_SET )
		{
			HAL_GPIO_WritePin(USART2_RTS_GPIO_Port, USART2_RTS_Pin, GPIO_PIN_SET);		// enable RADIO TX on air
			wTime = osKernelSysTick();	// start RTS time
		}
	}
	else if( huart->Instance == USART6 )
	{
		//USART6_TxCpltCallback();
	}
}

/**
  * @brief  UART error callbacks.
  * @param  huart: pointer to a UART_HandleTypeDef structure that contains
  *                the configuration information for the specified UART module.
  * @retval None
  */
 void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	 // try to clear all the UART status flags
	 while( huart->Instance->SR &
			 (UART_FLAG_CTS | UART_FLAG_LBD | UART_FLAG_RXNE |
			UART_FLAG_IDLE | UART_FLAG_ORE | UART_FLAG_NE | UART_FLAG_FE | UART_FLAG_PE)
		)
	 {
	    __HAL_UART_CLEAR_PEFLAG(huart);
	    huart->Instance->SR &= ~(UART_FLAG_CTS|UART_FLAG_LBD|UART_FLAG_RXNE);
	 }
}

/*!
	\fn short OT_PH_Send( unsigned char *pkt, unsigned short pktSize,
								unsigned char enCRC )
   \brief Invio di un pacchetto
	\param pkt pacchetto da inviare
	\param pktSize dimensione pacchetto
	\param enCRC flag di abilitazione aggiunta CRC16 al pacchetto
	\return 1: OK, 0: FAILED
*/
short OT_PH_Send( unsigned char *pkt, unsigned short pktSize, unsigned char enCRC )
{
	short RetVal = 0;
	unsigned short w;
	
	if( pktSize && (pktSize <= TX_BUFF_MAX_LEN) 
		&& (huart2.gState != HAL_UART_STATE_BUSY_TX)
		&& (HAL_GPIO_ReadPin(USART2_CTS_GPIO_Port, USART2_CTS_Pin) == GPIO_PIN_RESET)
		)
	{
		// copia pacchetto da inviare
		memcpy( pTxBuff, pkt, pktSize );
		LenTxBuff = pktSize;
		// RFID corrente
		pTxBuff[0] = rfID;
		if( enCRC && (pktSize <= MAX_PACKET_SIZE) )
		{
			// aggiunge il CRC16
			w = CalcCrcBuff( &pTxBuff[1], LenTxBuff-1 );
			pTxBuff[LenTxBuff++] = LOBYTE( w );
			pTxBuff[LenTxBuff++] = HIBYTE( w );
		}

		HAL_GPIO_WritePin(USART2_RTS_GPIO_Port, USART2_RTS_Pin, GPIO_PIN_RESET);	// enable RADIO TX to host
		// disable RX interrupt
		__HAL_UART_DISABLE_IT(&huart2, UART_IT_RXNE);
		cntMsec = 0;
		OT_ResetRX();
		
		// send packet
		if( HAL_UART_Transmit_IT(&huart2, (uint8_t*)pTxBuff, LenTxBuff ) == HAL_OK )
		{
		#ifdef OT_LOGGING
			/* if( enCRC == 2 )
			{
				// richiesta ciclica
			}
			else */
			{
				myprintf( "%u OT_TX: %s\r\n", TimeSec, VisHexMsg( pTxBuff, LenTxBuff ) );
			}
		#endif
			RetVal = 1;
		}
	}
	
	return RetVal;
}

/*!
	\fn void OT_RxTimeout( void )
   \brief Gestione timeout ricezione carattere
*/
void OT_RxTimeout( void )
{
	if( HAL_GPIO_ReadPin(USART2_RTS_GPIO_Port, USART2_RTS_Pin) == GPIO_PIN_SET )
	{
		if( (unsigned long)(osKernelSysTick() - wTime) >= T_RTS )
		{
			HAL_GPIO_WritePin(USART2_RTS_GPIO_Port, USART2_RTS_Pin, GPIO_PIN_RESET);	// enable RADIO TX to host
		}
	}
	else if( cntMsec )
	{
		if( --cntMsec == 0 )
		{
			// timeout inter-carattere
			if( LenRxBuff && (LenRxBuff <= RX_BUFF_MAX_LEN) )
			{
				// check the received packet size
				if( LenRxBuff >= RX_BUFF_MIN_LEN )
				{
					// received packet OK
					// disable RX interrupt
					__HAL_UART_DISABLE_IT(&huart2, UART_IT_RXNE);
					if( pRx_CallBack != NULL )
					{
						pRx_CallBack( pRxBuff, LenRxBuff );
					}
				}
			}
			OT_ResetRX();
		}
	}
}

/*!
	\fn short OT_PH_GetRCQ2Cfg( RCQ2_868_STRUCT *pCfg )
   \brief Rileva la configurazione del radio-modem RCQ2-868
	\param pCfg La struttura di configurazione rilevata (NULL: abort procedura)
	\return 1: OK, 0: RUNNING, -1: FAILED
*/
short OT_PH_GetRCQ2Cfg( RCQ2_868_STRUCT *pCfg )
{
	short RetVal = 0;
	
	switch( StCfg )
	{
		case 0:
			if( (huart2.gState != HAL_UART_STATE_BUSY_TX)
				&& (HAL_GPIO_ReadPin(USART2_CTS_GPIO_Port, USART2_CTS_Pin) == GPIO_PIN_RESET)
				&& (pCfg != NULL)
				)
			{
				HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_RESET);	// enable RADIO config
				pSaveRx_CallBack = pRx_CallBack;
				pRx_CallBack = OT_PH_RxCfg;
				wTime = osKernelSysTick();	// start CONF delay
				nRetry = N_RETRY;
				StCfg = 1;
			}
			else
			{
				// failed
				RetVal = -1;
			}
		break;
		
		case 1:
			// ritardo da attivazione CONF
			if( (unsigned long)(osKernelSysTick() - wTime) >= T_CONF )
			{
				StCfg = 101;
			}
			else if( pCfg == NULL )
			{
				// abort procedure
				StCfg = 2;
			}
		break;
		
		default:
		case 2:
			// start abort procedure
			pRx_CallBack = pSaveRx_CallBack;
			HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_SET);	// disable RADIO config
			wTime = osKernelSysTick();	// start CONF delay
			StCfg = 20;
		break;
		
		case 3:
			// start end procedure
			pRx_CallBack = pSaveRx_CallBack;
			HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_SET);	// disable RADIO config
			wTime = osKernelSysTick();	// start CONF delay
			StCfg = 30;
		break;
		
		case 20:
			// ABORT: ritardo da disattivazione CONF
		case 30:
			// END: ritardo da disattivazione CONF
			if( (unsigned long)(osKernelSysTick() - wTime) >= T_CONF )
			{
				if( StCfg == 20 )
				{
					// end procedure as FAILED
					RetVal = -1;
				}
				else
				{
					// end procedure as OK
					RetVal = 1;
				}
				StCfg = 0;
			}
		break;
		
		case 101:
			if( (unsigned long)(osKernelSysTick() - wTime) >= T_CONF )
			{
				// send one single byte
				LenTxBuff = 0;
				pTxBuff[LenTxBuff++] = 0x00;
				HAL_GPIO_WritePin(USART2_RTS_GPIO_Port, USART2_RTS_Pin, GPIO_PIN_RESET);	// enable RADIO TX to host
				// disable RX interrupt
				__HAL_UART_DISABLE_IT(&huart2, UART_IT_RXNE);
				cntMsec = 0;
				OT_ResetRX();
				HAL_UART_Transmit_IT(&huart2, (uint8_t*)pTxBuff, LenTxBuff );
				StCfg = 102;
			}
			else if( pCfg == NULL )
			{
				// abort procedure
				StCfg = 2;
			}
		break;
		
		case 102:
			// wait for RCQ2-868 config structure
			if( pCfg == NULL )
			{
				// abort procedure
				StCfg = 2;
			}
		break;
		
		case 103:
			if( pCfg != NULL )
			{
				memcpy( pCfg, pRxBuff, sizeof(RCQ2_868_STRUCT) );
				// procedure end OK
				StCfg = 3;
			}
			else
			{
				// abort procedure
				StCfg = 2;
			}
		break;
	}
	
	return RetVal;
}

/*!
	\fn void OT_PH_RxCfg( unsigned char *pkt, unsigned short pktSize );
	\brief Funzione call-back per ricezione configurazione.
	\param pkt pacchetto ricevuto
	\param pktSize dimensione pacchetto
*/
static void OT_PH_RxCfg( unsigned char *pkt, unsigned short pktSize )
{
	if( pktSize >= sizeof(RCQ2_868_STRUCT) )
	{
		// segnala configurazione ricevuta
		StCfg = 103;
	}
}

/*!
	\fn short OT_PH_SetRCQ2Cfg( RCQ2_868_STRUCT *pCfg )
   \brief Imposta la configurazione del radio-modem RCQ2-868
	\param pCfg La struttura di configurazione da impostare (NULL: abort procedura)
	\return 1: OK, 0: RUNNING, -1: FAILED
*/
short OT_PH_SetRCQ2Cfg( RCQ2_868_STRUCT *pCfg )
{
	short RetVal = 0;
	
	switch( StCfg )
	{
		case 0:
			if( (huart2.gState != HAL_UART_STATE_BUSY_TX)
				&& (HAL_GPIO_ReadPin(USART2_CTS_GPIO_Port, USART2_CTS_Pin) == GPIO_PIN_RESET)
				&& (pCfg != NULL)
				)
			{
				HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_RESET);	// enable RADIO config
				pSaveRx_CallBack = pRx_CallBack;
				pRx_CallBack = OT_PH_RxCfg;
				wTime = osKernelSysTick();	// start CONF delay
				nRetry = N_RETRY;
				StCfg = 1;
			}
			else
			{
				// failed
				RetVal = -1;
			}
		break;
		
		case 1:
			// ritardo da attivazione CONF
			if( (unsigned long)(osKernelSysTick() - wTime) >= T_CONF )
			{
				StCfg = 101;
			}
			else if( pCfg == NULL )
			{
				// abort procedure
				StCfg = 2;
			}
		break;
		
		default:
		case 2:
			// start abort procedure
			pRx_CallBack = pSaveRx_CallBack;
			HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_SET);	// disable RADIO config
			wTime = osKernelSysTick();	// start CONF delay
			StCfg = 20;
		break;
		
		case 3:
			// start end procedure
			pRx_CallBack = pSaveRx_CallBack;
			HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_SET);	// disable RADIO config
			wTime = osKernelSysTick();	// start CONF delay
			StCfg = 30;
		break;
		
		case 20:
			// ABORT: ritardo da disattivazione CONF
		case 30:
			// END: ritardo da disattivazione CONF
			if( (unsigned long)(osKernelSysTick() - wTime) >= T_CONF )
			{
				if( StCfg == 20 )
				{
					// end procedure as FAILED
					RetVal = -1;
				}
				else
				{
					// end procedure as OK
					RetVal = 1;
				}
				StCfg = 0;
			}
		break;
		
		case 101:
			if( (unsigned long)(osKernelSysTick() - wTime) >= T_CONF )
			{
				// send new configuration
				LenTxBuff = sizeof(RCQ2_868_STRUCT);
				memcpy( pTxBuff, pCfg, LenTxBuff );
				HAL_GPIO_WritePin(USART2_RTS_GPIO_Port, USART2_RTS_Pin, GPIO_PIN_RESET);	// enable RADIO TX to host
				// disable RX interrupt
				__HAL_UART_DISABLE_IT(&huart2, UART_IT_RXNE);
				cntMsec = 0;
				OT_ResetRX();
				HAL_UART_Transmit_IT(&huart2, (uint8_t*)pTxBuff, LenTxBuff );
				StCfg = 102;
			}
			else if( pCfg == NULL )
			{
				// abort procedure
				StCfg = 2;
			}
		break;
		
		case 102:
			// wait for RCQ2-868 config structure
			if( pCfg == NULL )
			{
				// abort procedure
				StCfg = 2;
			}
		break;
		
		case 103:
			if( pCfg != NULL )
			{
				if( !memcmp( pCfg, pRxBuff, sizeof(RCQ2_868_STRUCT) ) )
				{
					// successful
					StCfg = 3;
				}
				else if( --nRetry == 0 )
				{
					// failed
					StCfg = 2;
				}
				else
				{
					// retry
					wTime = osKernelSysTick();	// start CONF delay
					StCfg = 101;
				}
			}
			else
			{
				// abort procedure
				StCfg = 2;
			}
		break;
	}
	
	return RetVal;
}

/*!
	\fn void OT_PH_SetLowPower( void )
   \brief Radio-modem RCQ2-868 in low-power
*/
void OT_PH_SetLowPower( void )
{
	HAL_GPIO_WritePin(RESET_RF_GPIO_Port, RESET_RF_Pin, GPIO_PIN_SET);	// RADIO reset off
	HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_SET);	// disable RADIO config
	HAL_GPIO_WritePin(PD_RF_GPIO_Port, PD_RF_Pin, GPIO_PIN_RESET);	// RADIO power-down on
}

/*!
	\fn void OT_PH_SetFullPower( void )
   \brief Radio-modem RCQ2-868 in full-power
*/
void OT_PH_SetFullPower( void )
{
	//HAL_GPIO_WritePin(RESET_RF_GPIO_Port, RESET_RF_Pin, GPIO_PIN_SET);	// RADIO reset off	COMMENTATO perchè altrimenti non trasmette nulla
	//HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_SET);	// disable RADIO config	COMMENTATO perchè altrimenti non trasmette nulla
	HAL_GPIO_WritePin(PD_RF_GPIO_Port, PD_RF_Pin, GPIO_PIN_SET);	// RADIO power-down off
}








