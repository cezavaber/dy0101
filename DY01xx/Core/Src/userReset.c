/*!
** \file userReset.c
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief User Interface USER_STATE_RESET state handler
** 
** \version 1.0
**/

#include "user.h"
#include "User_Parametri.h"
#include "userMenu.h"
#include "userInit.h"
#include "userReset.h"
#include "userAvvio.h"



/*!
** \fn unsigned char userVisReset( void )
** \brief Starts the USER_STATE_RESET User Interface state handling
** \return The next User Interface state 
**/
unsigned char userVisReset( void )
{
	// full LCD backlight
	SetBacklight( LCD_BACKLIGHT_MAX );

	if( userCurrentState != USER_STATE_INIT )
	{
		// start viewing room temperature
		lByTogVisTH2O = 1;

		SBLOCCO_PAN = 0;

		Sleep.Ore = SLEEP_OFF;
		Sleep.Minuti = 0;

		/* bRipetizione = 0;
		TargetFAN = TARGET_FAN_1;
		CommutaFan = 0; */

		if( STATO_STUFA == STUFASTAT_IN_RESET )
		{
			STATO_ALL_BLACK = PAN_RESET;

			TIPO_ALLARME = NO_ALARM;

			TEMP_PAN = DEFAULTSETTEMP;
			FAN_PAN = FANAUTO;
			FAN_PAN_2 = FANAUTO;
			POWER_LEVEL = POTDEFAULT;

			MAX_POWER = POTMAXLEV;

			TEMP_AMB_MB = 255;

			H2O_TSET = H2O_DEFAULT;

			InitPassword();
			InitLogo();
			InitTipoDesc();
			TIPO_STUFA = NO_TYPE;
			OTdelay = 255;
		}
	}

	userSetTempUM();

	userSubState = 0;
	userCounterTime = TIMEOUT_AVVIO_8SEC;
	userNextPointer = NULL;
	
	return USER_STATE_RESET;
}

/*!
** \fn unsigned char userReset( void )
** \brief Handler function for the USER_STATE_RESET User Interface state
** \return The next User Interface state 
**/
unsigned char userReset( void )
{
	unsigned char RetVal = USER_STATE_RESET;
	char s[DISPLAY_MAX_COL+1];
	long i;
#ifdef EN_WALLPAPER
	BM_HEADER * p_bmhdrLOGO_c0 = NULL;	// logo bitmap can be not valid
	volatile BM_HEADER * p_bmhdrLOGO_c1;
	BM_HEADER * p_bmhdrLOGO_b0;
	volatile BM_HEADER * p_bmhdrLOGO_b1;
#else
	BM_HEADER * p_bmhdrLOGO = NULL;	// logo bitmap can be not valid
	short red = 0;
	short green = 0;
	short blue= 0;
	short iRed = 1;
	short iGreen = 1;
	short iBlue = 1;
	unsigned char step = 2;
	unsigned char maxlev = 128;
#endif
	
	// push bitmap LOGO into Graphic RAM
#ifdef EN_WALLPAPER
	if( res_bm_Load( BITMAP_LOGO_c0, res_bm_GetGRAM() ) > 0 )
	{
		if( res_bm_Load( BITMAP_LOGO_c1, res_bm_GetGRAM() ) > 0 )
		{
			if( res_bm_Load( BITMAP_LOGO_b0, res_bm_GetGRAM() ) > 0 )
			{
				if( res_bm_Load( BITMAP_LOGO_b1, res_bm_GetGRAM() ) > 0 )
				{
					p_bmhdrLOGO_c0 = res_bm_GetHeader( BITMAP_LOGO_c0 );
					p_bmhdrLOGO_c1 = res_bm_GetHeader( BITMAP_LOGO_c1 );
					p_bmhdrLOGO_b0 = res_bm_GetHeader( BITMAP_LOGO_b0 );
					p_bmhdrLOGO_b1 = res_bm_GetHeader( BITMAP_LOGO_b1 );
				}
				else
				{
					// invalid logo bitmap
					p_bmhdrLOGO_c0 = NULL;
				#ifdef LOGGING
					myprintf( sBitmapLoadError, BITMAP_LOGO_b1 );
				#endif
				}
			}
			else
			{
				// invalid logo bitmap
				p_bmhdrLOGO_c0 = NULL;
			#ifdef LOGGING
				myprintf( sBitmapLoadError, BITMAP_LOGO_b0 );
			#endif
			}
		}
		else
		{
			// invalid logo bitmap
			p_bmhdrLOGO_c0 = NULL;
		#ifdef LOGGING
			myprintf( sBitmapLoadError, BITMAP_LOGO_c1 );
		#endif
		}
	}
	else
	{
		// invalid logo bitmap
		p_bmhdrLOGO_c0 = NULL;
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_LOGO_c0 );
	#endif
	}
#else
	if( res_bm_Load( BITMAP_LOGO, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_LOGO );
	#endif
	}
	else
	{
		p_bmhdrLOGO = res_bm_GetHeader( BITMAP_LOGO );
		if( (p_bmhdrLOGO->bmhdr.Width < BITMAP_LOGO_MIN_WIDTH)
			|| (p_bmhdrLOGO->bmhdr.Height < BITMAP_LOGO_MIN_HEIGHT) )
		{
			// invalid logo bitmap
			p_bmhdrLOGO = NULL;
		}
	}
#endif
	
	/* Send command screen saver */
	// Ft_App_WrCoCmd_Buffer(pHost, CMD_SCREENSAVER);//screen saver command will continuously update the macro0 with vertex2f command
	
#ifndef EN_WALLPAPER
	/* First write a valid macro instruction into macro1 */
	Ft_Gpu_Hal_Wr32(pHost, REG_MACRO_1, CLEAR_COLOR_RGB(red,green,blue));	// Set the default clear color
#endif
	
	Ft_Gpu_CoCmd_Dlstart(pHost);
#ifdef EN_WALLPAPER
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
#else
	//Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
	Ft_App_WrCoCmd_Buffer(pHost,MACRO(1));
#endif
	Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
	Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
	
	memcpy( s, pChLogo, LENGTH_LOGO );
	s[LENGTH_LOGO] = '\0';
#ifdef EN_WALLPAPER
	if( p_bmhdrLOGO_c0 != NULL )
	{
		// draw LOGO bitmap
		
		Ft_App_WrCoCmd_Buffer(pHost,SAVE_CONTEXT());
		
		//B0&B1 Handle
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_HANDLE(11));	// handle 0,..,(NFONT_PER_GROUP-1) are for app custom fonts, NFONT_PER_GROUP,..,14 are free
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrLOGO_b0->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrLOGO_b0->bmhdr.Format, p_bmhdrLOGO_b0->bmhdr.Stride, p_bmhdrLOGO_b0->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, FT_DispWidth, FT_DispHeight));
		
		//C0&C1 Handle
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_HANDLE(13));	// handle 0,..,(NFONT_PER_GROUP-1) are for app custom fonts, NFONT_PER_GROUP,..,14 are free
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrLOGO_c0->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrLOGO_c0->bmhdr.Format, p_bmhdrLOGO_c0->bmhdr.Stride, p_bmhdrLOGO_c0->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, FT_DispWidth, FT_DispHeight));	//scale to 480x272

		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		
		Ft_App_WrCoCmd_Buffer(pHost,BLEND_FUNC(ONE,ZERO));
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(0x55));
		Ft_App_WrCoCmd_Buffer(pHost,VERTEX2II(0, 0, 11, 0));
		Ft_App_WrCoCmd_Buffer(pHost,BLEND_FUNC(ONE,ONE));
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(0xAA));
		Ft_App_WrCoCmd_Buffer(pHost,VERTEX2II(0, 0, 11, 1));
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_MASK(1,1,1,0));
		Ft_Gpu_CoCmd_LoadIdentity(pHost);
		Ft_Gpu_CoCmd_Scale(pHost, 65536*FT_DispWidth/p_bmhdrLOGO_c0->bmhdr.Width, 65536*FT_DispHeight/p_bmhdrLOGO_c0->bmhdr.Height);	//scale to 480x272
		Ft_Gpu_CoCmd_SetMatrix(pHost);
		Ft_App_WrCoCmd_Buffer(pHost,BLEND_FUNC(DST_ALPHA,ZERO));
		Ft_App_WrCoCmd_Buffer(pHost,VERTEX2II(0, 0, 13, 1));
		Ft_App_WrCoCmd_Buffer(pHost,BLEND_FUNC(ONE_MINUS_DST_ALPHA,ONE));
		Ft_App_WrCoCmd_Buffer(pHost,VERTEX2II(0, 0, 13, 0));
		
		Ft_App_WrCoCmd_Buffer( pHost, END() );
		
		Ft_App_WrCoCmd_Buffer(pHost,RESTORE_CONTEXT());
		
		/*
		Ft_App_WrCoCmd_Buffer(pHost,COLOR_A(255));
		Ft_App_WrCoCmd_Buffer(pHost, BLEND_FUNC( SRC_ALPHA, SRC_ALPHA ) );	// overlay on background image
		*/
		// draw LOGO string (per eventuale "P.ERR: xxxxxxxx")
		Ft_Gpu_CoCmd_Text(pHost,(FT_DispWidth/2), (FT_DispHeight-50+12), fontDesc, OPT_CENTER, String2Font(s, fontDesc));
	}
#else
	if( p_bmhdrLOGO != NULL )
	{
		// draw LOGO bitmap
		// specify the starting address of the bitmap in graphics RAM
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrLOGO->GramAddr));
		// specify the bitmap format, linestride and height
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrLOGO->bmhdr.Format, p_bmhdrLOGO->bmhdr.Stride, p_bmhdrLOGO->bmhdr.Height));
		// set filtering, wrapping and on-screen size
		Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, p_bmhdrLOGO->bmhdr.Width, p_bmhdrLOGO->bmhdr.Height));
		// start drawing bitmap
		Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
		//Ft_App_WrCoCmd_Buffer(pHost,MACRO(0));
		Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( ((FT_DispWidth-p_bmhdrLOGO->bmhdr.Width)/2)*16, (((FT_DispHeight-50)-p_bmhdrLOGO->bmhdr.Height)/2)*16 ) );
		Ft_App_WrCoCmd_Buffer(pHost,END());
		
		// draw LOGO string (per eventuale "P.ERR: xxxxxxxx")
		Ft_Gpu_CoCmd_Text(pHost,(FT_DispWidth/2), (FT_DispHeight-50+12), fontDesc, OPT_CENTER, String2Font(s, fontDesc));
	}
#endif
	else
	{
		// draw LOGO string
		Ft_Gpu_CoCmd_Text(pHost,(FT_DispWidth/2), ((FT_DispHeight-50)/2), fontMax, OPT_CENTER, String2Font(s, fontMax));
	}
	
	if( (TIPO_STUFA < NTYPE) && (STATO_ALL_BLACK != PAN_NO_TIPO) )
	{
		// draw Tipo Stufa
		userGetPar( P_TIPOSTUFA );
		userVisPar( P_TIPOSTUFA );
		Ft_Gpu_CoCmd_Text(pHost,(FT_DispWidth/2), (FT_DispHeight-13), fontDesc, OPT_CENTER, String2Font(str2, fontDesc));
	}

#ifdef EN_WALLPAPER
	//Ft_App_WrCoCmd_Buffer(pHost, BLEND_FUNC( SRC_ALPHA, ONE_MINUS_SRC_ALPHA ) );	// default blend
#endif
	
	Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
	Ft_Gpu_CoCmd_Swap( pHost );
	Ft_App_Flush_Co_Buffer( pHost );
	Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );

	// wait for the reset end
	for( i=0; i>=0; i++ )
	{
		Wdog();
		Ft_Gpu_Hal_Sleep(16);	// 16.667ms the period of automatic macro0 update
		
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			break;
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			break;
		}
		else if( userCollectParam() && (i >= 300) )	// remain here for at least 5s
		{
			if( STATO_STUFA != STUFASTAT_IN_RESET )
			{
				// Finite le comunicazioni di reset entro nello stato online.
				i = -2;
				break;
			}
			else if( STATO_ALL_BLACK == PAN_NO_TIPO )
			{
				// we need to set Tipo Stufa
				i = -1;
				break;
			}
			else if( !(i % 60) )	// every 1s
			{
				// mantengo aggiornati Logo e Tipo Stufa
				OT_Send( (PKT_STRUCT *)&reqLogo, SEND_MODE_ONE_SHOT, 0 );
				OT_Send( (PKT_STRUCT *)&reqTipoStufa, SEND_MODE_ONE_SHOT, 0 );
				// check OT delay
				userGetPar( P_OT_DELAY );
				if( userParValue.b[0] != OTDELAY_PAN )
				{
					userParValue.b[0] = OTDELAY_PAN;
					userSetPar( P_OT_DELAY );
				}
			}
		}
	
	#ifndef EN_WALLPAPER
		if( iBlue )
		{
			blue += step;
			if( blue > maxlev )
			{
				blue = maxlev;
				iBlue = 0;
			}
		}
		else
		{
			blue -= step;
			if( blue < 0 )
			{
				blue = 0;
				iBlue = 1;
			}
		}
		if( i >= 30 )
		{
			if( iRed )
			{
				red += step;
				if( red > maxlev )
				{
					red = maxlev;
					iRed = 0;
				}
			}
			else
			{
				red -= step;
				if( red < 0 )
				{
					red = 0;
					iRed = 1;
				}
			}
		}
		if( i >= 60 )
		{
			if( iGreen )
			{
				green += step;
				if( green > maxlev )
				{
					green = maxlev;
					iGreen = 0;
				}
			}
			else
			{
				green -= step;
				if( green < 0 )
				{
					green = 0;
					iGreen = 1;
				}
			}
		}
		/*  update just macro1 */
		Ft_Gpu_Hal_Wr32(pHost, REG_MACRO_1, CLEAR_COLOR_RGB(red,green,blue));	// Set the default clear color
	#endif
	}

	// Send the stop command
	// Ft_Gpu_Hal_WrCmd32(pHost, CMD_STOP);

	// pop bitmap LOGO from Graphic RAM
#ifdef EN_WALLPAPER
	res_bm_Release( BITMAP_LOGO_b1 );
	res_bm_Release( BITMAP_LOGO_b0 );
	res_bm_Release( BITMAP_LOGO_c1 );
	res_bm_Release( BITMAP_LOGO_c0 );
#else
	res_bm_Release( BITMAP_LOGO );
#endif
	
	if( i == -1 )
	{
		// set Tipo Stufa
		userSubState = 1;	// parameter modify (to set the start value for Tipo Stufa directly without an increment/decrement user action)
		// parameter code as title
		userGetParCode( P_TIPOSTUFA );
		strcpy( s, str2 );
		userParValue.b[0] = TYPE_1;	// start value for Tipo Stufa
		if( WinSetParam(		// oppure WinSetList() ????
								P_TIPOSTUFA,
								s,
								(char *)GetMsg( 2, NULL, iLanguage, d_TIPOSTUFA )
								) )
		{
			ExecTipoStufa();
			STATO_ALL_BLACK = PAN_ONLINE;
		}

		// restart userReset()
	}
	else if( i == -2 )
	{
		// start user session
		RetVal = userVisAvvio();
	}
	
	return RetVal;
}




