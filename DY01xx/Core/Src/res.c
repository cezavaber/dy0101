/*!
** \file res.c
** \author Alessandro Vagniluca
** \date 20/10/2015
** \brief Resources handler functions module.
** 
** The resources are bitmaps, fonts and language strings sets stored in the 
** dataflash memory.
** 
** \version 1.0
**/

#include "main.h"
#include "FT_Gpu.h"
#include "FT_Gpu_Hal.h"
#include "FT_Hal_Utils.h"
#include "FT_CoPro_Cmds.h"
#include "DataFlash.h"
#include "res.h"
#include "stringhe_ITA.h"
#include "stringhe_ENG.h"


#define COPY_BUFF_SIZE			1024		//!< 1KB buffer size for raw data copy

/*!
** \var CtrlArea
** \brief The control area copy in ram
**/
CTRL_AREA CtrlArea;

unsigned char ResVersion;	//!< Resources version number acquired
unsigned char ResRevision;	//!< Resources revision number acquired
unsigned char ResMaintenance;	//!< Resources maintenance number acquired from uSD

ft_uint8_t fontMax;		//!< window max font
ft_uint8_t fontTitle;	//!< window title font
ft_uint8_t fontDesc;	//!< parameter description font
ft_uint8_t fontValue;	//!< parameter value font

/*!
** \var iFontGroup
** \brief The current font group
**/
unsigned char iFontGroup;

/*!
** \typedef BM_ITEM
** \brief Bitmap item of a linked list
**/
typedef struct bm_item
{
	BM_HEADER bmHeader;
	struct bm_item *next;
} BM_ITEM;
static BM_ITEM *bm_head = NULL;
static BM_ITEM *bm_curr = NULL;

static unsigned long bm_GRam;	//!< the next available GRAM address for a bitmap load

static char sBuffRiga[NUM_ROW_BUFFER][MAX_LEN_STRINGA+1];	// buffer for strings in a row

/*!
** \typedef LINGUA_TAB
** \brief Language strings set references 
**/
typedef struct __packed
{
	unsigned char eeBank;	//!< memory bank (=255 not available)
	unsigned long eeAddr;	//!< strings set start address
	unsigned short Font_Group;			//!< font group index
	char DescMsg[NUM_FONT_GROUP][MSGDESCLEN];	//!< string set descriptor for each font group
} LINGUA_TAB;
static Vector Lingua_tab;

// first strings set address in each memory bank
static const unsigned long eeOffset[NEEBANK] =
{
	LANG_HEADER_ARRAY_ADDR,
	//LANG_HEADER_ARRAY_ADDR,
}; 

/*!
** \var SigleStati
** \brief Strings list for language selection
**/
Vector SigleStati;

extern char *String2Font( char *s, ft_uint8_t font );

static BM_ITEM* bm_create_list( BM_HEADER *ps );
static BM_ITEM* bm_add_to_list_end( BM_HEADER *ps );
static BM_ITEM* bm_search_in_list( unsigned char tag, BM_ITEM **prev );
static short bm_delete_from_list( unsigned char tag );
static void bm_delete_list( void );

static short InitSetMsg( void );
static short CheckSetMsg( void );
static void DefaultFont( unsigned short iFnt );





/******************************************************************************
LOADED BITMAPS LINKED LIST HANDLING FUNCTIONS
*******************************************************************************/

// create the first item of the linked list
static BM_ITEM* bm_create_list( BM_HEADER *ps )
{
	BM_ITEM *ptr = (BM_ITEM *)malloc(sizeof(BM_ITEM));
	
	if( NULL != ptr )
	{
		ptr->bmHeader = *ps;
		ptr->next = NULL;

		bm_head = bm_curr = ptr;
	}
	
	return ptr;
}

// append an item to the linked list
static BM_ITEM* bm_add_to_list_end( BM_HEADER *ps )
{
	BM_ITEM *ptr = NULL;
	
	if( NULL == bm_head )
	{
		// create the linked list with the item
		ptr = bm_create_list(ps);
	}
	else
	{
		// create the new item
		ptr = (BM_ITEM *)malloc(sizeof(BM_ITEM));
		if( NULL != ptr )
		{
			ptr->bmHeader = *ps;
			ptr->next = NULL;
		
			// add the item to the linked list end
			bm_curr->next = ptr;
			bm_curr = ptr;
		}
	}
	
	return ptr;
}

// search an item and the previous one in the linked list
// return NULL if not found
static BM_ITEM* bm_search_in_list( unsigned char tag, BM_ITEM **prev )
{
	BM_ITEM *ptr = bm_head;
	BM_ITEM *tmp = NULL;
	unsigned char found = 0;
	
	// search the item
	while( ptr != NULL )
	{
		if( ptr->bmHeader.tag == tag )
		{
			// found
			found = 1;
			break;
		}
		else
		{
			// save the bm_current item and continue to the next item
			tmp = ptr;
			ptr = ptr->next;
		}
	}
	
	if( found )
	{
		// 'ptr' is the searched item
		if( prev )
		{
			// pass also the previous item
			*prev = tmp;
		}
	}
	else
	{
		ptr = NULL;
	}
	
	return ptr;
}

// delete an item from the linked list
// return: 1: OK, 0: FAILED
static short bm_delete_from_list( unsigned char tag )
{
	short RetVal = 0;
	BM_ITEM *prev = NULL;
	BM_ITEM *del = NULL;
	
	del = bm_search_in_list( tag, &prev );
	if( del != NULL )
	{
		if( prev != NULL )	// link the previous to the next
		{
			prev->next = del->next;
		}

		if( del == bm_curr )	// is it the last item?
		{ 
			bm_curr = prev;
			// update the GRAM address for the next bitmap load
			bm_GRam = del->bmHeader.GramAddr;
		}
 		/* --else--
 		 * fix-bug cancellazione linked-list 20180529 - A. BERTINI
 		 */
 		if( del == bm_head )	// is it the first item?
		{
			bm_head = del->next;
		}

		free( del );
		
		RetVal = 1;
	}
	
	return RetVal;
}

// delete the entire linked list
static void bm_delete_list( void )
{
	BM_ITEM *next;
	
	// delete the linked list from the beginning
	while( bm_head != NULL )
	{
		next = bm_head->next;
		free( bm_head );
		bm_head = next;
	}
}




/*!
** \fn short res_Init( void )
** \brief Init the resources handler
** \return 1: OK, 0: FAILED
**/
short res_Init( void )
{
	short RetVal = 0;
	unsigned char crc;
	
	memset( &CtrlArea, 0, sizeof(CtrlArea) );
	res_bm_ReleaseAll();

	// default to ROM fonts
	memset( RAMFontTableAddr, 0, sizeof(RAMFontTableAddr) );
	fontMax = FONT_MAX_DEF;
	fontTitle = FONT_TITLE_DEF;
	fontDesc = FONT_DESC_DEF;
	fontValue = FONT_VALUE_DEF;
	
	// set the current font group as undefined to force fonts' loading
	iFontGroup = NUM_FONT_GROUP;

	// init language strings sets
	if( InitSetMsg() )
	{
		// check data-flash bank #0
		if( df_Init( EE_BANK_0 ) )	// dataflash power-on
		{
			if( df_Read( EE_BANK_0, CTRL_AREA_ADDR, (unsigned char *)&CtrlArea, sizeof(CtrlArea) ) )
			{
				crc = CalcChecksum( (unsigned char *)&CtrlArea.param, sizeof(CTRL_PARAM) );
				// check control area
				if( CtrlArea.crc == crc )
				{
					if( 
						(CtrlArea.param.Version == RES_VER)
						&& (CtrlArea.param.Revision == RES_REV)
						//&& (CtrlArea.param.Maintenance == RES_MAINT)
						&& (CtrlArea.param.BitmapNum >= NUM_BITMAP)
						&& !(CtrlArea.param.FontNum % NFONT_PER_GROUP)
						)
					{
						// valid control area
						NumFontGroup = CtrlArea.param.FontNum / NFONT_PER_GROUP;
						// Search for available language strings sets
						if( CheckSetMsg() )
						{
							RetVal = 1;
						}
						// check if the current language is in the range
						if( iLanguage >= NumLanguage )
						{
							iLanguage = 0;
						}
						// fonts loading
						LoadFonts();	// the current language sets the current font group
					}
				}
				else
				{
					// invalidate control area
					memset( &CtrlArea, 0, sizeof(CtrlArea) );
				}
			}
		}
		
		// check data-flash bank #1
		//if( df_Init( EE_BANK_1 ) )
		{
		}
	}
	
	return RetVal;
}

/*!
** \fn unsigned long res_bm_Load( unsigned char iBitmap, unsigned long gram )
** \brief Copy a bitmap from dataflash to GRAM
** 
** If one or more bitmaps are loaded into GRAM, at the end of their use they
** must be all released to free the used GRAM.
** This allows to have a bitmaps stack loaded into GRAM: the last 'pushed'
** bitmap is the first bitmap to be 'popped'.
** 
** \param iBitmap The bitmap index
** \param gram The GRAM start address
** \return The number > 0 of the copied bytes if OK, 0 if FAILED
**/
unsigned long res_bm_Load( unsigned char iBitmap, unsigned long gram )
{
	unsigned long RetVal = 0;
	BM_HEADER bmHeader;
	BM_ITEM *ptr;
	unsigned long addr;
	unsigned char *RawData;
	unsigned long RawDataSize;
	unsigned long blocklen;
	
	if( gram >= BITMAP_GRAM_BASE_ADDR )
	{
		bmHeader.GramAddr = gram;
		bmHeader.tag = iBitmap;
		// read the bitmap property structure from the dataflash
		Wdog();
		addr = BITMAP_HEADER_ARRAY_ADDR + iBitmap*sizeof(Bitmap_header_t);
		if( df_Read( EE_BANK_0, addr, (unsigned char *)&bmHeader.bmhdr, sizeof(Bitmap_header_t) ) )
		{
			addr = bmHeader.bmhdr.Bitmap_RawData + bmHeader.bmhdr.Arrayoffset;
			RawDataSize = (unsigned long)bmHeader.bmhdr.Stride * (unsigned long)bmHeader.bmhdr.Height;
			// check for enough GRAM space for the bitmap
			if( (gram + RawDataSize) <= (RAM_G + RAM_G_SIZE) )
			{
				if( (RawData = (unsigned char *)malloc(COPY_BUFF_SIZE)) != NULL )
				{
					RetVal = RawDataSize;	// return raw data size
					// copy the raw data to GRAM in COPY_BUFF_SIZE blocks
					while( RawDataSize )
					{
						Wdog();
						blocklen = RawDataSize > COPY_BUFF_SIZE ? COPY_BUFF_SIZE : RawDataSize;
						if( !df_Read( EE_BANK_0, addr, RawData, blocklen ) )
						{
							break;
						}
						else
						{
							Wdog();
							Ft_Gpu_Hal_WrMem(pHost, gram, RawData, blocklen);
							RawDataSize -= blocklen;
							addr += blocklen;
							gram += blocklen;
						}
					}
					free( RawData );
					if( !RawDataSize )
					{
						// add bitmap to the linked list
						if( (ptr = bm_add_to_list_end( &bmHeader )) == NULL )
						{
							// bitmap add failed
							RetVal = 0;
						}
						else
						{
							// update the GRAM address for the next bitmap load
							bm_GRam = gram;
						}
					}
					else
					{
						// raw data copy failed
						RetVal = 0;
					}
				}
			}
		}
	}
	
	return RetVal;
}

/*!
** \fn short res_bm_Release( unsigned char iBitmap )
** \brief Free the GRAM from the loaded bitmap
** 
** If one or more bitmaps are loaded into GRAM, at the end of their use they
** must be all released to free the used GRAM.
** This allows to have a bitmaps stack loaded into GRAM: the last 'pushed'
** bitmap is the first bitmap to be 'popped'.
** 
** \param iBitmap The bitmap index
** \return 1: OK, 0: FAILED
**/
short res_bm_Release( unsigned char iBitmap )
{
	short RetVal = 0;
	
	if( bm_delete_from_list( iBitmap ) )
	{
		RetVal = 1;
	}
	
	return RetVal;
}

/*!
** \fn BM_HEADER *res_bm_GetHeader( unsigned char iBitmap )
** \brief Get the bitmap header for the loaded bitmap
** \param iBitmap The bitmap index
** \return The pointer to the bitmap header, NULL otherwise.
**/
BM_HEADER *res_bm_GetHeader( unsigned char iBitmap )
{
	BM_HEADER *RetVal = NULL;
	BM_ITEM *ptr = NULL;
	BM_ITEM *prev = NULL;
	
	ptr = bm_search_in_list( iBitmap, &prev );
	if( ptr != NULL )
	{
		RetVal = &ptr->bmHeader;
	}
	
	return RetVal;
}

/*!
** \fn unsigned long res_bm_GetGRAM( void )
** \brief Get the available GRAM address for a new bitmap load
** 
** This function returns the next available GRAM address after the last loaded 
** bitmap.
** 
** \return The available GRAM address value
**/
unsigned long res_bm_GetGRAM( void )
{
	return bm_GRam;
}

/*!
** \fn void res_bm_ReleaseAll( void )
** \brief Free the GRAM from all the loaded bitmaps
** \return None
**/
void res_bm_ReleaseAll( void )
{
	bm_delete_list();
	bm_GRam = BITMAP_GRAM_BASE_ADDR;
}

/*!
** \fn void res_bm_Reset( void )
** \brief Reset the GRAM to the starting operative state
** \return None
**/
void res_bm_Reset( void )
{
#ifdef LOGGING
	myprintf( "Reset bitmap GRAM." );
#endif

	res_bm_ReleaseAll();

	// push bitmap QUIT into Graphic RAM
	if( res_bm_Load( BITMAP_QUIT, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_QUIT );
	#endif
	}
	// push bitmap OK into Graphic RAM
	if( res_bm_Load( BITMAP_OK, res_bm_GetGRAM() ) == 0 )
	{
	#ifdef LOGGING
		myprintf( sBitmapLoadError, BITMAP_OK );
	#endif
	}
}


/*!
** \fn short InitSetMsg( void )
** \brief Init language strings sets
** \return None
** \return 1: OK, 0: FAILED
** \note To be called at startup, before calling 'CheckSetMsg()' function
**/
static short InitSetMsg( void )
{
	short RetVal = 0;
	LINGUA_TAB lt;
	
	// start with no available language
	NumLanguage = 0;
	vector_free( &Lingua_tab );
	// create the dynamic array for language strings set references
	Lingua_tab.dataSize = sizeof( LINGUA_TAB );	// the size of the dynamic array elements
	if( vector_init( &Lingua_tab ) )
	{
		/*! set the NROMLANG languages references */
		
		// language 0
		lt.eeBank = NEEBANK;
		lt.eeAddr = 0xFFFFFFFF;
		lt.Font_Group = FONT_GROUP_ITA;
		memcpy( lt.DescMsg, DescMsg_ITA, sizeof(DescMsg_ITA) );
		if( vector_set( &Lingua_tab, LANG_ITA, &lt ) )
		{
			NumLanguage++;
			RetVal = 1;
		}
		
		// language 1
		lt.eeBank = NEEBANK;
		lt.eeAddr = 0xFFFFFFFF;
		lt.Font_Group = FONT_GROUP_ENG;
		memcpy( lt.DescMsg, DescMsg_ENG, sizeof(DescMsg_ENG) );
		if( vector_set( &Lingua_tab, LANG_ENG, &lt ) )
		{
			NumLanguage++;
			RetVal = 1;
		}
	}
	
	return RetVal;
}

/*!
** \fn short CheckSetMsg( void )
** \brief Search for language strings sets
** \return 1: OK, 0: FAILED
** \note To be called at startup, before using the first language string
**/
static short CheckSetMsg( void )
{
	short RetVal = 0;
	unsigned long i;
	LINGUA_TAB lt;
	unsigned long eeAddr;
	unsigned char bank;
	
	// Search for language strings set headers
	//for( bank=EE_BANK_0; bank<NEEBANK; bank++ )
	bank = EE_BANK_0;	// per ora solo il primo banco
	{
		eeAddr = eeOffset[bank];
		for( i=0; i<CtrlArea.param.LangNum; i++ )
		{
			Wdog();
			// read 'i-th' strings set descriptors and font group
			lt.eeBank = bank;
			lt.eeAddr = eeAddr;
			memset( &lt.Font_Group, 0xFF, sizeof(short)+NUM_FONT_GROUP*MSGDESCLEN );
			df_Read( bank, eeAddr, (unsigned char *)&lt.Font_Group, sizeof(short)+NUM_FONT_GROUP*MSGDESCLEN );
			if( lt.Font_Group < NUM_FONT_GROUP )
			{
				// language (NROMLANG+i)
				if( vector_set( &Lingua_tab, NROMLANG+i, &lt ) )
				{
					NumLanguage++;
				}
			}
			eeAddr += MSG_TAB_SIZE;
		}
	}
	
	// create the dynamic strings array for language selection
	vector_free( &SigleStati );
	SigleStati.dataSize = MSGDESCLEN;	// the size of the dynamic array elements
	if( vector_init( &SigleStati ) )
	{
		RetVal = 1;
	}
	// the array will be filled at every font change

	return RetVal;
}

/*!
** \fn const char * GetMsg( unsigned char Indice_Riga, char *sDest,
** 								unsigned char Indice_Lingua,
** 								unsigned short Indice_Stringa )
** \brief Get a language string 
** \param Indice_Riga row index (1: row 1, 2: row 2, ..., NUM_ROW_BUFFER: row NUM_ROW_BUFFER)
** \param sDest Destination buffer; if NULL, the row string buffer is used.
** \param Indice_Lingua Language index
** \param Indice_Stringa String index
** \return The pointer to the string buffer, NULL if not found.
**/
const char * GetMsg( unsigned char Indice_Riga, char *sDest,
									unsigned char Indice_Lingua,
									unsigned short Indice_Stringa )
{
	static unsigned char iLang[NUM_ROW_BUFFER] = { 255, 255, 255, 255, 255, 255, 255, 255 };
	static unsigned short iStr[NUM_ROW_BUFFER] = { NUMMSG, NUMMSG, NUMMSG, NUMMSG, NUMMSG, NUMMSG, NUMMSG, NUMMSG };
	unsigned char *pb;
	unsigned short *pw;
	char *p = NULL;
	RIF_MSG RifMsg;
	unsigned long eeAddr;
	LINGUA_TAB lt;
	
	if( Indice_Lingua < NROMLANG )
	{
		// the string is in ROM
		p = (char *)pSetMsg[Indice_Lingua][Indice_Stringa];
		if( sDest != NULL )
		{
			// copy the string from ROM to the destination buffer
			//strcpy( sDest, p );	NO! GetMsg() can be called as a strcpy() parameter: recursion.
			for( eeAddr=0; p[eeAddr]; eeAddr++ )
			{
				sDest[eeAddr] = p[eeAddr];
			}
			sDest[eeAddr] = '\0';
			// return the pointer to the destination buffer
			p = sDest;
		}
		else if( Indice_Riga )	// row buffer index start from 0
		{
			Indice_Riga--;
			// copy the string from ROM to the indicated string buffer
			sDest = sBuffRiga[Indice_Riga];
			//strcpy( sDest, p );	NO! GetMsg() can be called as a strcpy() parameter: recursion.
			for( eeAddr=0; p[eeAddr]; eeAddr++ )
			{
				sDest[eeAddr] = p[eeAddr];
			}
			sDest[eeAddr] = '\0';
			// return the pointer to the string buffer
			p = sDest;
		}
		// otherwise (Indice_Riga = 0): return the pointer to the string in ROM
	}
	else if( Indice_Lingua < NumLanguage )
	{
		// the string is in the data-flash
		if( Indice_Riga )	// row buffer index start from 0
			Indice_Riga--;
		pb = &iLang[Indice_Riga];
		pw = &iStr[Indice_Riga];
		// return the pointer to the destination buffer or to the indicated string buffer
		if( sDest != NULL )
			p = sDest;
		else
			p = sBuffRiga[Indice_Riga];
			
		if( (sDest != NULL) || (*pb != Indice_Lingua) || (*pw != Indice_Stringa) )
		{
			// get the string from the data-flash
			lt = *(LINGUA_TAB *)vector_get( &Lingua_tab, Indice_Lingua);
			eeAddr = lt.eeAddr + sizeof(short) + NUM_FONT_GROUP*(unsigned long)MSGDESCLEN + Indice_Stringa*(unsigned long)sizeof(RifMsg);
			df_Read( lt.eeBank, eeAddr, (unsigned char *)&RifMsg, sizeof(RifMsg) );
			// the string in the data-flash is null-terminated
			if( RifMsg.MsgLen > MAX_LEN_STRINGA+1 )
				RifMsg.MsgLen = MAX_LEN_STRINGA+1;
			df_Read( lt.eeBank, RifMsg.MsgAddr, (unsigned char *)p, RifMsg.MsgLen );
			p[RifMsg.MsgLen-1] = '\0';	// null terminator for safety
			
			if( sDest == NULL )
			{
				*pb = Indice_Lingua;
				*pw = Indice_Stringa;
			}
		}
	}
	
	return (const char *)p;
}									


/*!
** \fn void DefaultFont( unsigned short iFnt )
** \brief Set the indicated default font
** \param iFnt The font index
** \return None
**/
static void DefaultFont( unsigned short iFnt )
{
	switch( iFnt )
	{
		case FONT_ARIAL_L1_12:
		break;

		case FONT_ARIAL_L1_16:
			fontValue = FONT_VALUE_DEF;
		break;

		case FONT_ARIAL_L1_18:
			fontDesc = FONT_DESC_DEF;
		break;

		case FONT_ARIAL_L1_22:
			fontTitle = FONT_TITLE_DEF;
		break;

		case FONT_ARIAL_L1_28:
		break;

		case FONT_ARIAL_L1_36:
			fontMax = FONT_MAX_DEF;
		break;
	}
}

/*!
** \fn short LoadFonts( void )
** \brief Load the fonts to be used with the currente language
** \return The number of the fonts loaded with no errors
**/
short LoadFonts( void )
{
	short RetVal = 0;
	LINGUA_TAB lt;
	unsigned char i;
	unsigned long addr, gram;
	unsigned char *RawData;
	unsigned long RawDataSize;
	unsigned long blocklen;
	char s[40];
	
	// check if all the used fonts are present in the data-flash
	if(
		(CtrlArea.param.Version == RES_VER)
		&& (CtrlArea.param.Revision == RES_REV)
		//&& (CtrlArea.param.Maintenance == RES_MAINT)
		&& !(CtrlArea.param.FontNum % NFONT_PER_GROUP)
		)
	{
		NumFontGroup = CtrlArea.param.FontNum / NFONT_PER_GROUP;
		// check if fonts loading is needed
		lt = *(LINGUA_TAB *)vector_get( &Lingua_tab, iLanguage);
		if( iFontGroup != lt.Font_Group )
		{
			if( (RawData = (unsigned char *)malloc(COPY_BUFF_SIZE)) != NULL )
			{
				// fonts loading
				memset( RAMFontTableAddr, 0, sizeof(RAMFontTableAddr) );
				
				for( i=0; i<NFONT_PER_GROUP; i++ )
				{
					Wdog();
					Ft_Gpu_Hal_Sleep(5);
					// read the font metric blocks structure from the dataflash
					addr = FONT_HEADER_ARRAY_ADDR + (lt.Font_Group*NFONT_PER_GROUP + i)*sizeof(Font_header_t);
					if( df_Read( EE_BANK_0, addr, (unsigned char *)&Font_RawData_Header, sizeof(Font_header_t) ) )
					{
						// copy the font metric blocks structure to GRAM
						gram = FONT_GRAM_BASE_ADDR + Font_RawData_Header.FontTableAddr;
						RawDataSize = Font_RawData_Header.Font_RawDataSize;
						// check for enough GRAM space for the font
						if( (gram + sizeof(FT_Gpu_Fonts_t) + RawDataSize) <= BITMAP_GRAM_BASE_ADDR )
						{
							Ft_Gpu_Hal_WrMem(pHost, gram, (ft_uint8_t *)&Font_RawData_Header.FontTable, sizeof(FT_Gpu_Fonts_t) );
							// copy the raw data to GRAM in COPY_BUFF_SIZE blocks
							addr = Font_RawData_Header.Font_RawData;
							gram += sizeof(FT_Gpu_Fonts_t);
							while( RawDataSize )
							{
								Wdog();
								Ft_Gpu_Hal_Sleep(5);
								blocklen = RawDataSize > COPY_BUFF_SIZE ? COPY_BUFF_SIZE : RawDataSize;
								if( !df_Read( EE_BANK_0, addr, RawData, blocklen ) )
								{
									break;
								}
								else
								{
									Ft_Gpu_Hal_WrMem(pHost, gram, RawData, blocklen);
									RawDataSize -= blocklen;
									addr += blocklen;
									gram += blocklen;
								}
							}
							if( !RawDataSize )
							{
								RAMFontTableAddr[i] = FONT_GRAM_BASE_ADDR + Font_RawData_Header.FontTableAddr;
								
								// register custom font with handle 'i'
								Ft_Gpu_CoCmd_Dlstart(pHost);
								Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
								Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
								Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE

								Ft_App_WrCoCmd_Buffer(pHost,BITMAP_HANDLE(i));
								Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(Font_RawData_Header.FontTable.PointerToFontGraphicsData));
								Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(Font_RawData_Header.FontTable.FontBitmapFormat, Font_RawData_Header.FontTable.FontLineStride, Font_RawData_Header.FontTable.FontHeightInPixels));
								Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(NEAREST, BORDER, BORDER, Font_RawData_Header.FontTable.FontWidthInPixels, Font_RawData_Header.FontTable.FontHeightInPixels));
								Ft_Gpu_CoCmd_SetFont(pHost, i, FONT_GRAM_BASE_ADDR+Font_RawData_Header.FontTableAddr);

								mysprintf( s, "Font %u loaded", i );
								Ft_Gpu_CoCmd_Text( pHost, 0, 0, i, 0, String2Font( s, i ) );
								
								Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
								Ft_Gpu_CoCmd_Swap( pHost );
								Ft_App_Flush_Co_Buffer( pHost );
								Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
								
								// update the related font index
								switch( i )
								{
									case FONT_ARIAL_L1_12:
									break;

									case FONT_ARIAL_L1_16:
										fontValue = i;
									break;

									case FONT_ARIAL_L1_18:
										fontDesc = i;
									break;

									case FONT_ARIAL_L1_22:
										fontTitle = i;
									break;

									case FONT_ARIAL_L1_28:
									break;

									case FONT_ARIAL_L1_36:
										fontMax = i;
									break;
								}
								
								RetVal++;
							}
							else
							{
								// default the related font index
								DefaultFont( i );
							}
						}
						else
						{
							// default the related font index
							DefaultFont( i );
						}
					}
				}
				
				free( RawData );
				
				if( RetVal >= NFONT_PER_GROUP )
				{
					// confirm the new font group
					iFontGroup = lt.Font_Group;
					
					/* 
						Update the strings list for language selection.
						The list ordering is as follow:
						0 - the first ROM language
						1 - the second ROM language
						...
						(NROMLANG-1) - the last ROM language
						(NROMLANG) - the first language in the LANG.INI file
						(NROMLANG+1) - the second language in the LANG.INI file
						...
						(NumLanguage-1) - the last language in the LANG.INI file
					*/
					for( i=0; i<NumLanguage; i++ )
					{
						lt = *(LINGUA_TAB *)vector_get( &Lingua_tab, i);
						vector_set( &SigleStati, i, lt.DescMsg[iFontGroup] );
					}
					
				}
			}
		}
	}
	
	return RetVal;
}



