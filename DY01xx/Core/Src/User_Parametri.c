/*!
** \file User_Parametri.c
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief Riferimenti ai parametri gestiti dall'interfaccia utente
** 
** \version 1.0
**/

#include "user.h"
#include "User_Parametri.h"
#include "userMenu.h"


// tabella degli indici alle stringhe correlate ai valori di alcuni parametri
const WORD sWeekDay_tab[7] =
{
	sDOMENICA,
	sLUNEDI,
	sMARTEDI,
	sMERCOLEDI,
	sGIOVEDI,
	sVENERDI,
	sSABATO
};
const WORD sOnOff_tab[2] =
{
	sOFF,
	sON
};
const WORD ShowTemp_StrInd_tab[NSHOWTEMP] =
{
	sAUTO,
	sDEGC,
	sDEGF
};

// tabella dei gruppi di parametri
const PAR_GROUP Group_tab[NUM_GROUP_PAR+1] =
{
	{ 'C', P_TIPOSTUFA, P_TEST },
	{ 'D', P_CODICE_PANNELLO, P_DURATA_BLACKOUT },
	{ 'E', P_TEMPERATURE, P_RICETTA_PELLET_P },
	{ 'F', P_LOGIC_ONOFF, P_EN_WIFI },
	{ 'H', P_SPE_COCLEA, P_PULIZIAB_AUX2 },
	{ 'L', P_PREACC1_COCLEA, P_ACCC_AUX2 },
	{ 'P', P_POT1_COCLEA, P_POT8_AUX2 },

	{ 'Z', P_LINGUA, P_SLEEP-1 },
};

// Tabella dei riferimenti ai parametri
const PARTAB_STRUCT Param_tab[NUMPAR] =
{
	// Gruppo C
	{ TYPE_BYTE, 0, {(unsigned long)TYPE_1}, {(unsigned long)NTYPE-1}, /*{(unsigned long)NO_TYPE},*/ 1, {(void *)&TIPO_STUFA}, 0, /*d_TIPOSTUFA,*/ NULL, 0x006C, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)255}, /*{(unsigned long)0},*/ 1, {(void *)&TipoMotoreEsp}, 1, /*NUMMSG,*/ NULL, 0x0134, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)ZERO_FAN}, {(unsigned long)TRE_FAN}, /*{(unsigned long)ZERO_FAN},*/ 1, {(void *)&F_MULTIFAN}, 2, /*NUMMSG,*/ NULL, 0x000B, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x0F, {(unsigned long)3}, {(unsigned long)POTMAXLEV}, /*{(unsigned long)POTDEFAULT},*/ 1, {(void *)&POWER_BYTE}, 3, /*NUMMSG,*/ NULL, 0x010D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)9999}, /*{(unsigned long)0},*/ 1, {(void *)&PesataPellet}, 4, /*NUMMSG,*/ NULL, 0x0137, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },	// il valore e' espresso in g/h
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)255}, /*{(unsigned long)0},*/ 1, {(void *)&TipoMotoreEsp2}, 5, /*NUMMSG,*/ NULL, 0x0133, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)255}, /*{(unsigned long)0},*/ 1, {(void *)&ConfigIdro}, 7, /*NUMMSG,*/ NULL, 0x0117, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 1, {(void *)&NumAcc}, 8, /*NUMMSG,*/ NULL, THOLREG3_FIRST_ADDR+0x005B, MBTAB_HOLD3_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 2, {(void *)&PeriodoCoclea}, 10, /*NUMMSG,*/ NULL, 0x00AD, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)255}, /*{(unsigned long)0},*/ 1, {(void *)&TipoFrenataCoclea}, 11, /*NUMMSG,*/ NULL, 0x0135, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 2, {(void *)&PeriodoFan1}, 30, /*NUMMSG,*/ NULL, 0x00AE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)255}, /*{(unsigned long)0},*/ 1, {(void *)&TipoFrenataFan1}, 31, /*NUMMSG,*/ NULL, 0x0136, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)255}, /*{(unsigned long)0},*/ 1, {(void *)&MinSetFan1}, 35, /*NUMMSG,*/ NULL, 0x012E, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)255}, /*{(unsigned long)0},*/ 1, {(void *)&MinSetFan2}, 36, /*NUMMSG,*/ NULL, 0x012F, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)255}, /*{(unsigned long)0},*/ 1, {(void *)&MinSetFan3}, 37, /*NUMMSG,*/ NULL, 0x0130, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&LinguaDef}, 100, /*NUMMSG,*/ NULL, 0x006D, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	//{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)65535}, /*{(unsigned long)0},*/ 1, {(void *)&OreSATLimite}, 101, /*NUMMSG,*/ NULL, 0x013F, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)ServNumber}, 104, /*d_SERVICE,*/ NULL, 0x0040, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },	// codifica service number
	{ TYPE_CHAR, 0, {(unsigned long)0x20}, {(unsigned long)0x7F}, /*{(unsigned long)0x20},*/ 1, {(void *)Ssid}, 200, /*d_SSID,*/ NULL, THOLREG3_FIRST_ADDR+0x0024, MBTAB_HOLD3_REG, /*LOGIN_FREE*/ },	// stringa SSID
	{ TYPE_CHAR, 0, {(unsigned long)0}, {(unsigned long)255}, /*{(unsigned long)0},*/ 1, {(void *)CryptoKey}, 201, /*d_KEYWORD,*/ NULL, THOLREG3_FIRST_ADDR+0x0034, MBTAB_HOLD3_REG, /*LOGIN_FREE*/ },	// stringa Keyword
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0xFFFF}, /*{(unsigned long)0},*/ 1, {(void *)&PortaIP}, 202, /*d_TCP_PORT,*/ NULL, THOLREG3_FIRST_ADDR+0x0003, MBTAB_HOLD3_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)Ip}, 203, /*d_IP_ADDRESS,*/ NULL, THOLREG3_FIRST_ADDR+0x0004, MBTAB_HOLD3_REG, /*LOGIN_FREE*/ },	// vettore IP Address
	{ TYPE_CHAR, 0, {(unsigned long)0x30}, {(unsigned long)0x39}, /*{(unsigned long)0x30},*/ 1, {(void *)WPSPin}, 207, /*d_WPS_PIN,*/ NULL, THOLREG3_FIRST_ADDR+0x0054, MBTAB_HOLD3_REG, /*LOGIN_FREE*/ },	// stringa WPS PIN
	{ TYPE_CHAR, 0, {(unsigned long)0x20}, {(unsigned long)0x7F}, /*{(unsigned long)0x20},*/ 1, {(void *)Local_ISO3166_1_Code}, 208, /*NUMMSG,*/ NULL, THOLREG3_FIRST_ADDR+0x0056, MBTAB_HOLD3_REG, /*LOGIN_FREE*/ },	// stringa ISO3166-1 Paese locale
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)255}, /*{(unsigned long)0},*/ 1, {(void *)&OTdelay}, 209, /*NUMMSG,*/ NULL, THOLREG3_FIRST_ADDR+0x0057, MBTAB_HOLD3_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BOOL, 12, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 999, /*d_TEST_START_STOP,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	//{ TYPE_BYTE_MASK, 0x80, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ByCMD_MB}, 999, /*d_TEST_START_STOP,*/ sOnOff_tab, 16, NUM_MBTAB, /*LOGIN_SERVICE*/ },	quando sar� implementato sulla stufa

	// Gruppo D
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)NULL}, 1, /*d_CODICE_PANNELLO,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },	// PanCode+PanVer+PanRev+PanBuild
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&gBySchedaVers}, 2, /*d_CODICE_SCHEDA,*/ NULL, 0, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },	// Comfort+Ver_Comf+Rev_Comf+Build_Comf
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&gBySicurezzaVers}, 3, /*d_CODICE_SICUREZZA,*/ NULL, 4, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },	// Ver_Sic+Rev_Sic+Build_Sic
	{ TYPE_DWORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&OreFunz}, 4, /*NUMMSG,*/ NULL, 0x013D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// Read-Only
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 1, {(void *)&OreSAT}, 5, /*d_ORESAT,*/ NULL, 0x0140, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// Clear-Only
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&StatoLogico}, 6, /*NUMMSG,*/ NULL, 0x0029, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&StatoAttuazioni}, 7, /*NUMMSG,*/ NULL, 0x0012, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&STATO_STUFA}, 9, /*NUMMSG,*/ NULL, 0x0013, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&DriverCod}, 10, /*NUMMSG,*/ NULL, 0x003A, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },	// Driver+Ver_Drv+Rev_Drv+Build_Drv
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&ParamCode}, 11, /*NUMMSG,*/ NULL, 0x0038, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },	// cod/ver+rev/build
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TIPO_ALLARME}, 12, /*NUMMSG,*/ NULL, 0x0014, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP23_LOW}, 30, /*NUMMSG,*/ sOnOff_tab, 0x0017, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP23_LOW}, 31, /*NUMMSG,*/ sOnOff_tab, 0x0017, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP23_LOW}, 33, /*NUMMSG,*/ sOnOff_tab, 0x0017, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP23_LOW}, 34, /*NUMMSG,*/ sOnOff_tab, 0x0017, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TOnCocleaSet}, 150, /*NUMMSG,*/ NULL, 0x001A, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TOffCocleaSet}, 151, /*NUMMSG,*/ NULL, 0x001B, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&Fan1Set}, 152, /*NUMMSG,*/ NULL, 0x0021, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&Fan2Set}, 153, /*NUMMSG,*/ NULL, 0x0023, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&PortataAriaSet}, 154, /*NUMMSG,*/ NULL, 0x001C, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&GiriMinEspSet}, 155, /*NUMMSG,*/ NULL, 0x001D, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&AttEsp}, 156, /*NUMMSG,*/ NULL, 0x001E, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TOnFan1Set}, 161, /*NUMMSG,*/ NULL, 0x001F, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TOffFan1Set}, 162, /*NUMMSG,*/ NULL, 0x0020, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&Giri2Set}, 163, /*NUMMSG,*/ NULL, 0x0022, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&Fan3Set}, 164, /*NUMMSG,*/ NULL, 0x0024, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP286_LOW}, 400, /*NUMMSG,*/ sOnOff_tab, 0x011E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP22_LOW}, 402, /*RICHIESTASERVICE,*/ sOnOff_tab, 0x0016, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP21_HIGH}, 409, /*GUASTO_SENSORE_PORTATA,*/ sOnOff_tab, 0x0016, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP21_HIGH}, 410, /*SOVRAPRESSACQUA,*/ sOnOff_tab, 0x0016, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP22_LOW}, 411, /*GUASTOPRESSOSTACQUA,*/ sOnOff_tab, 0x0016, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP22_LOW}, 412, /*GUASTOSONDATEMPARIA,*/ sOnOff_tab, 0x0016, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP22_LOW}, 413, /*GUASTOSONDATEMPACQUA,*/ sOnOff_tab, 0x0016, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP22_LOW}, 415, /*PELLET_ESAURITO,*/ sOnOff_tab, 0x0016, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP23_HIGH}, 501, /*NUMMSG,*/ sOnOff_tab, 0x0017, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&PORTATA}, 505, /*d_PORTATAARIAMISURATA,*/ NULL, 0x0025, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&ESP_FUMI}, 506, /*d_VENTILATOREFUMI,*/ NULL, 0x0026, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TEMP_FUMI}, 507, /*d_TEMPERATURAFUMI,*/ NULL, 0x000F, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_INTEGER, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TEMP_AMB_MB}, 508, /*NUMMSG,*/ NULL, 0x0010, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },	// il valore e' espresso in step di 0.5�C
	{ TYPE_INTEGER, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&H2O_TEMP}, 509, /*NUMMSG,*/ NULL, 0x0011, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 0.5�C
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&LivFotores}, 510, /*d_FOTORESISTENZA,*/ NULL, 0x0032, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&H2O_FLOW}, 511, /*NUMMSG,*/ NULL, 0x0028, MBTAB_INPUT_REG, /*LOGIN_SERVICE*/ },	// il valore e` espresso in 0.1 bar
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&INPBMAP21_LOW}, 514, /*PORTA_APERTA,*/ sOnOff_tab, 0x0016, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&VelocitaEspulsore2}, 518, /*NUMMSG,*/ NULL, 0x0027, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&OssigenoResiduo}, 519, /*d_OSSIGENORESIDUO,*/ NULL, 0x0033, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },	// il valore e` espresso in 0.5 %
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&Consumo}, 520, /*NUMMSG,*/ NULL, 0x0034, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },	// il valore e` espresso in 0.05 kg/h
	{ TYPE_INTEGER, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TEMP_AMB_PAN}, 700, /*NUMMSG,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 0.1�C
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&StatSec.LastStatoFunz}, 750, /*NUMMSG,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TBLACKOUT}, 751, /*NUMMSG,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },

	// Gruppo E
	{ TYPE_BYTE, 0, {(unsigned long)MINSETTEMP}, {(unsigned long)MAXSETTEMP}, /*{(unsigned long)DEFAULTSETTEMP},*/ 1, {(void *)&TEMP_PAN}, 1, /*NUMMSG,*/ NULL, 0x0008, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)10}, /*{(unsigned long)0},*/ 1, {(void *)&GRADIENTE_ECO}, 2, /*NUMMSG,*/ NULL, 0x000C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)10}, {(unsigned long)1300}, /*{(unsigned long)0},*/ 1, {(void *)&TempOnFumi}, 150, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x003D, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)10}, {(unsigned long)1300}, /*{(unsigned long)0},*/ 1, {(void *)&TempSogliaFumi}, 151, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x003E, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)10}, {(unsigned long)1300}, /*{(unsigned long)0},*/ 1, {(void *)&TempOffFumi}, 152, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x003F, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)50}, /*{(unsigned long)0},*/ 1, {(void *)&DeltaTempCaldo}, 153, /*NUMMSG,*/ NULL, 0x009F, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)10}, {(unsigned long)1300}, /*{(unsigned long)0},*/ 1, {(void *)&TempPreAlarmFumi}, 154, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0040, MBTAB_HOLD2_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)50}, /*{(unsigned long)0},*/ 1, {(void *)&GradienteAlmFumi}, 155, /*NUMMSG,*/ NULL, 0x0120, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)10}, {(unsigned long)1300}, /*{(unsigned long)0},*/ 1, {(void *)&TempAlarmFumi}, 156, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0041, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)8100}, /*{(unsigned long)0},*/ 1, {(void *)&FotoresThresOn}, 160, /*NUMMSG,*/ NULL, 0x00B1, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)8100}, /*{(unsigned long)0},*/ 1, {(void *)&FotoresThresOff}, 161, /*NUMMSG,*/ NULL, 0x00B2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)H2O_TMIN}, {(unsigned long)H2O_TMAX}, /*{(unsigned long)H2O_DEFAULT},*/ 1, {(void *)&H2O_TSET}, 200, /*NUMMSG,*/ NULL, 0x0009, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)30}, {(unsigned long)85}, /*{(unsigned long)0},*/ 1, {(void *)&SETON_POMPA}, 201, /*NUMMSG,*/ NULL, 0x000F, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)30}, {(unsigned long)85}, /*{(unsigned long)0},*/ 1, {(void *)&SetIdroPompaOff}, 202, /*NUMMSG,*/ NULL, 0x0010, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)30}, {(unsigned long)85}, /*{(unsigned long)0},*/ 1, {(void *)&TOnAux}, 203, /*NUMMSG,*/ NULL, 0x0011, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)H2OSAN_TMIN}, {(unsigned long)H2OSAN_TMAX}, /*{(unsigned long)H2O_DEFAULT},*/ 1, {(void *)&H2O_TSETSAN}, 204, /*NUMMSG,*/ NULL, 0x000A, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)5}, {(unsigned long)20}, /*{(unsigned long)0},*/ 1, {(void *)&DeltaSanitari}, 205, /*NUMMSG,*/ NULL, 0x0012, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)4}, {(unsigned long)15}, /*{(unsigned long)0},*/ 1, {(void *)&IstTempAcqua}, 206, /*NUMMSG,*/ NULL, 0x0013, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)10}, {(unsigned long)1300}, /*{(unsigned long)0},*/ 1, {(void *)&TempOnScamb}, 207, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0042, MBTAB_HOLD2_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)10}, {(unsigned long)1300}, /*{(unsigned long)0},*/ 1, {(void *)&TempSogliaScamb}, 208, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0043, MBTAB_HOLD2_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&SetpSanitari}, 209, /*NUMMSG,*/ NULL, 0x000E, MBTAB_INPUT_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)14400}, /*{(unsigned long)0},*/ 10, {(void *)&DurataMaxPreAllFumi}, 300, /*NUMMSG,*/ NULL, 0x0121, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)14400}, /*{(unsigned long)0},*/ 10, {(void *)&DurataPreAllPellet}, 301, /*NUMMSG,*/ NULL, 0x0122, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)50}, /*{(unsigned long)0},*/ 1, {(void *)&DeltaTAssenzaFiamma}, 302, /*NUMMSG,*/ NULL, 0x0123, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)60}, /*{(unsigned long)0},*/ 1, {(void *)&TSWITCHON_ECO}, 303, /*NUMMSG,*/ NULL, 0x000D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)60}, /*{(unsigned long)0},*/ 1, {(void *)&TSHUTDOWN_ECO}, 304, /*NUMMSG,*/ NULL, 0x000E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 1, {(void *)&TAlarmPorta}, 305, /*NUMMSG,*/ NULL, 0x0124, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 1, {(void *)&TPreAlmPorta}, 306, /*NUMMSG,*/ NULL, 0x0125, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)14400}, /*{(unsigned long)0},*/ 10, {(void *)&TPreAlmAriaCombust}, 307, /*NUMMSG,*/ NULL, 0x0126, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)3600}, /*{(unsigned long)0},*/ 1, {(void *)&DurataBlkoutRecovery}, 308, /*NUMMSG,*/ NULL, 0x0139, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 1, {(void *)&TPreAcc1}, 309, /*NUMMSG,*/ NULL, 0x00A0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 1, {(void *)&TPreAcc2}, 310, /*NUMMSG,*/ NULL, 0x00A1, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 1, {(void *)&TPreAccCaldo}, 311, /*NUMMSG,*/ NULL, 0x00A2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)30}, /*{(unsigned long)0},*/ 1, {(void *)&TRaffA}, 312, /*NUMMSG,*/ NULL, 0x00AB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)3600}, /*{(unsigned long)0},*/ 1, {(void *)&TWarmUp}, 313, /*NUMMSG,*/ NULL, 0x00A3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)3600}, /*{(unsigned long)0},*/ 1, {(void *)&TFireOn}, 314, /*NUMMSG,*/ NULL, 0x00A6, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)3600}, /*{(unsigned long)0},*/ 1, {(void *)&TSpeA}, 315, /*NUMMSG,*/ NULL, 0x00A8, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 1, {(void *)&EspRitardoInc}, 316, /*NUMMSG,*/ NULL, 0x010E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 1, {(void *)&EspRitardoDec}, 317, /*NUMMSG,*/ NULL, 0x010F, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 1, {(void *)&PotRitardoInc}, 318, /*NUMMSG,*/ NULL, 0x0110, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 1, {(void *)&PotRitardoDec}, 319, /*NUMMSG,*/ NULL, 0x0111, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 1, {(void *)&TPB}, 320, /*NUMMSG,*/ NULL, 0x0112, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)3600}, /*{(unsigned long)0},*/ 1, {(void *)&TWaitPB}, 321, /*NUMMSG,*/ NULL, 0x0114, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)9960}, /*{(unsigned long)0},*/ 1, {(void *)&TWaitScuot}, 322, /*NUMMSG,*/ NULL, 0x013A, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)600}, /*{(unsigned long)0},*/ 1, {(void *)&ScuotDurataCiclo}, 323, /*NUMMSG,*/ NULL, 0x013B, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)3600}, /*{(unsigned long)0},*/ 1, {(void *)&TAccB}, 324, /*NUMMSG,*/ NULL, 0x00A4, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)3600}, /*{(unsigned long)0},*/ 1, {(void *)&TAccC}, 325, /*NUMMSG,*/ NULL, 0x00A5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)3600}, /*{(unsigned long)0},*/ 1, {(void *)&TFireOnB}, 326, /*NUMMSG,*/ NULL, 0x00A7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)3600}, /*{(unsigned long)0},*/ 1, {(void *)&TSpeB}, 327, /*NUMMSG,*/ NULL, 0x00A9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)3600}, /*{(unsigned long)0},*/ 1, {(void *)&TSpeC}, 328, /*NUMMSG,*/ NULL, 0x00AA, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)3600}, /*{(unsigned long)0},*/ 1, {(void *)&TRaffB}, 329, /*NUMMSG,*/ NULL, 0x00AC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)300}, /*{(unsigned long)0},*/ 1, {(void *)&TPBB}, 332, /*NUMMSG,*/ NULL, 0x0113, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)10}, /*{(unsigned long)0},*/ 1, {(void *)&PreAcc1NumExec}, 333, /*NUMMSG,*/ NULL, 0x00AF, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)10}, /*{(unsigned long)0},*/ 1, {(void *)&PreAcc2NumExec}, 334, /*NUMMSG,*/ NULL, 0x00B0, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)200}, /*{(unsigned long)0},*/ 1, {(void *)&Ossigeno}, 340, /*d_OSSIGENO,*/ NULL, 0x0115, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e` espresso in 0.5 %
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&HOLDBMAP8315_LOW}, 446, /*NUMMSG,*/ sOnOff_tab, THOLREG3_FIRST_ADDR+0x007B, MBTAB_HOLD3_REG, /*LOGIN_SERVICE*/ },	// Read-Only
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&HOLDBMAP8315_LOW}, 447, /*NUMMSG,*/ sOnOff_tab, THOLREG3_FIRST_ADDR+0x007B, MBTAB_HOLD3_REG, /*LOGIN_SERVICE*/ },	// Read-Only
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&HOLDBMAP8315_LOW}, 448, /*NUMMSG,*/ sOnOff_tab, THOLREG3_FIRST_ADDR+0x007B, MBTAB_HOLD3_REG, /*LOGIN_SERVICE*/ },	// Read-Only
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&HOLDBMAP8315_LOW}, 449, /*NUMMSG,*/ sOnOff_tab, THOLREG3_FIRST_ADDR+0x007B, MBTAB_HOLD3_REG, /*LOGIN_SERVICE*/ },	// Read-Only
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&HOLDBMAP8315_LOW}, 450, /*NUMMSG,*/ sOnOff_tab, THOLREG3_FIRST_ADDR+0x007B, MBTAB_HOLD3_REG, /*LOGIN_SERVICE*/ },	// Read-Only
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&HOLDBMAP8316_LOW}, 462, /*NUMMSG,*/ sOnOff_tab, THOLREG3_FIRST_ADDR+0x007C, MBTAB_HOLD3_REG, /*LOGIN_SERVICE*/ },	// Read-Only
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&HOLDBMAP8316_LOW}, 463, /*NUMMSG,*/ sOnOff_tab, THOLREG3_FIRST_ADDR+0x007C, MBTAB_HOLD3_REG, /*LOGIN_SERVICE*/ },	// Read-Only
	{ TYPE_INTEGER, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TempP0}, 478, /*NUMMSG,*/ NULL, THOLREG3_FIRST_ADDR+0x007D, MBTAB_HOLD3_REG, /*LOGIN_FREE*/ },	// Read-Only, il valore e' espresso in step di 0.5�C
	{ TYPE_INTEGER, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TempP1}, 479, /*NUMMSG,*/ NULL, THOLREG3_FIRST_ADDR+0x007E, MBTAB_HOLD3_REG, /*LOGIN_FREE*/ },	// Read-Only, il valore e' espresso in step di 0.5�C
	{ TYPE_INTEGER, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TempP2}, 480, /*NUMMSG,*/ NULL, THOLREG3_FIRST_ADDR+0x007F, MBTAB_HOLD3_REG, /*LOGIN_FREE*/ },	// Read-Only, il valore e' espresso in step di 0.5�C
	{ TYPE_INTEGER, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TempP3}, 481, /*NUMMSG,*/ NULL, THOLREG3_FIRST_ADDR+0x0080, MBTAB_HOLD3_REG, /*LOGIN_FREE*/ },	// Read-Only, il valore e' espresso in step di 0.5�C
	{ TYPE_INTEGER, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&TempP4}, 482, /*NUMMSG,*/ NULL, THOLREG3_FIRST_ADDR+0x0081, MBTAB_HOLD3_REG, /*LOGIN_FREE*/ },	// Read-Only, il valore e' espresso in step di 0.5�C
	{ TYPE_BYTE_MASK, 0xF0, {(unsigned long)POTMINLEV}, {(unsigned long)POTMAXLEV}, /*{(unsigned long)POTDEFAULT},*/ 1, {(void *)&POWER_BYTE}, 600, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0030, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x07, {(unsigned long)FANMINLEV}, {(unsigned long)FANAUTO}, /*{(unsigned long)FANAUTO},*/ 1, {(void *)&MODE_BYTE}, 601, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0031, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x38, {(unsigned long)FANMINLEV}, {(unsigned long)FANAUTO}, /*{(unsigned long)FANAUTO},*/ 1, {(void *)&MODE_BYTE}, 602, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0032, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_INTEGER, 0, {(unsigned long)(-10)}, {(unsigned long)10}, /*{(unsigned long)0},*/ 1, {(void *)&GainRisc}, 603, /*NUMMSG,*/ NULL, 0x0014, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_INTEGER, 0, {(unsigned long)(-10)}, {(unsigned long)10}, /*{(unsigned long)0},*/ 1, {(void *)&GainSanitari}, 604, /*NUMMSG,*/ NULL, 0x0015, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)25}, {(unsigned long)47}, /*{(unsigned long)0},*/ 1, {(void *)&PressH2OMax}, 605, /*NUMMSG,*/ NULL, 0x0016, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },	// il valore e` espresso in 0.1 bar
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)255}, /*{(unsigned long)0},*/ 1, {(void *)&ScuotNCicli}, 606, /*NUMMSG,*/ NULL, 0x013C, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)POTMINLEV}, {(unsigned long)POTMAXLEV}, /*{(unsigned long)POTDEFAULT},*/ 1, {(void *)&MinPowerPB}, 607, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x003C, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x07, {(unsigned long)FANMINLEV}, {(unsigned long)FANAUTO}, /*{(unsigned long)FANAUTO},*/ 1, {(void *)&FAN3_BYTE}, 608, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0033, MBTAB_HOLD2_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE, 0, {(unsigned long)1}, {(unsigned long)12}, /*{(unsigned long)0},*/ 1, {(void *)&K_Sonda}, 610, /*NUMMSG,*/ NULL, 0x0116, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e` espresso in 0.5
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&PortataCritica}, 613, /*NUMMSG,*/ NULL, 0x0127, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)50}, /*{(unsigned long)0},*/ 1, {(void *)&DeltaPortata}, 614, /*NUMMSG,*/ NULL, 0x0128, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_INTEGER, 0, {(unsigned long)(-10)}, {(unsigned long)10}, /*{(unsigned long)0},*/ 1, {(void *)&RicEspAcc}, 615, /*d_OFFSETESPULSORE_ON,*/ NULL, 0x001F, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_INTEGER, 0, {(unsigned long)(-10)}, {(unsigned long)10}, /*{(unsigned long)0},*/ 1, {(void *)&RicEsp}, 616, /*d_OFFSETESPULSORE_ON,*/ NULL, 0x0020, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_INTEGER, 0, {(unsigned long)(-10)}, {(unsigned long)10}, /*{(unsigned long)0},*/ 1, {(void *)&RicPelletAcc}, 617, /*d_RICETTAPELLET_ON,*/ NULL, 0x001D, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_INTEGER, 0, {(unsigned long)(-10)}, {(unsigned long)10}, /*{(unsigned long)0},*/ 1, {(void *)&RicPellet}, 618, /*d_RICETTAPELLET_ON,*/ NULL, 0x001E, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },

	// Gruppo F
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SETTINGS_LOW}, 0, /*NUMMSG,*/ sOnOff_tab, 0x0000, MBTAB_COIL, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP6_LOW}, 1, /*NUMMSG,*/ sOnOff_tab, 0x0006, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_HIGH}, 2, /*NUMMSG,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_LOW}, 3, /*d_TERMAMB,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP6_LOW}, 4, /*d_ECO,*/ sOnOff_tab, 0x0006, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_LOW}, 100, /*NUMMSG,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_HIGH}, 101, /*NUMMSG,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_LOW}, 102, /*NUMMSG,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_LOW}, 103, /*NUMMSG,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SETTINGS_LOW}, 104, /*NUMMSG,*/ sOnOff_tab, 0x0001, MBTAB_COIL, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP286_LOW}, 106, /*NUMMSG,*/ sOnOff_tab, 0x011E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP306_LOW}, 200, /*NUMMSG,*/ sOnOff_tab, 0x0132, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP306_LOW}, 220, /*NUMMSG,*/ sOnOff_tab, 0x0132, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_HIGH}, 221, /*d_EN_PERIODO_COCLEA,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP306_LOW}, 222, /*NUMMSG,*/ sOnOff_tab, 0x0132, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP306_LOW}, 240, /*NUMMSG,*/ sOnOff_tab, 0x0132, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_HIGH}, 241, /*NUMMSG,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_HIGH}, 242, /*d_EN_PERIODO_COCLEA_2,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x80, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP306_LOW}, 251, /*NUMMSG,*/ sOnOff_tab, 0x0132, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP306_LOW}, 252, /*NUMMSG,*/ sOnOff_tab, 0x0132, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_HIGH}, 260, /*NUMMSG,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP306_LOW}, 280, /*NUMMSG,*/ sOnOff_tab, 0x0132, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP286_LOW}, 281, /*NUMMSG,*/ sOnOff_tab, 0x011E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP306_LOW}, 320, /*NUMMSG,*/ sOnOff_tab, 0x0132, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x80, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_LOW}, 321, /*NUMMSG,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x80, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_HIGH}, 322, /*NUMMSG,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_LOW}, 323, /*d_CONTROLLOLAMBDA,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_LOW}, 400, /*NUMMSG,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP286_LOW}, 500, /*NUMMSG,*/ sOnOff_tab, 0x011E, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP305_LOW}, 700, /*NUMMSG,*/ sOnOff_tab, 0x0131, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP23_LOW}, 701, /*NUMMSG,*/ sOnOff_tab, 0x0017, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP23_LOW}, 702, /*NUMMSG,*/ sOnOff_tab, 0x0017, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP23_LOW}, 703, /*NUMMSG,*/ sOnOff_tab, 0x0017, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP23_LOW}, 704, /*NUMMSG,*/ sOnOff_tab, 0x0017, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP23_LOW}, 705, /*NUMMSG,*/ sOnOff_tab, 0x0017, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP23_LOW}, 706, /*NUMMSG,*/ sOnOff_tab, 0x0017, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&HOLDBMAP23_LOW}, 707, /*NUMMSG,*/ sOnOff_tab, 0x0017, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 0, {(void *)&HOLDBMAP8194_LOW}, 800, /*d_WIFI,*/ sOnOff_tab, THOLREG3_FIRST_ADDR+0x0002, MBTAB_HOLD3_REG, /*LOGIN_FREE*/ },	// Read-Only

	// Gruppo H
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_LOW}, 0, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_LOW}, 1, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_LOW}, 2, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_LOW}, 3, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_LOW}, 4, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_LOW}, 5, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_LOW}, 6, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Spenta_TOnCoclea}, 10, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0010, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Spenta_TOffCoclea}, 11, /*NUMMSG,*/ NULL, 0x006A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Spenta_Giri2}, 12, /*NUMMSG,*/ NULL, 0x006F, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&Spenta_Portata}, 20, /*NUMMSG,*/ NULL, 0x006B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Spenta_Esp}, 21, /*NUMMSG,*/ NULL, 0x006C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Spenta_TOnFan1}, 30, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0011, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Spenta_TOffFan1}, 31, /*NUMMSG,*/ NULL, 0x006D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Spenta_AttuaFan1}, 32, /*NUMMSG,*/ NULL, 0x006E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Spenta_Giri2}, 40, /*NUMMSG,*/ NULL, 0x006F, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Spenta_AttuaFan2}, 41, /*NUMMSG,*/ NULL, 0x0070, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Spenta_AttuaFan2}, 42, /*NUMMSG,*/ NULL, 0x0070, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Spenta_AttuaFan3}, 52, /*NUMMSG,*/ NULL, 0x0071, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_HIGH}, 58, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_HIGH}, 59, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_HIGH}, 60, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_HIGH}, 61, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_HIGH}, 62, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPENTA_HIGH}, 63, /*NUMMSG,*/ sOnOff_tab, 0x0069, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_LOW}, 100, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_LOW}, 101, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_LOW}, 102, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_LOW}, 103, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_LOW}, 104, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_LOW}, 105, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_LOW}, 106, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&SpeA_TOnCoclea}, 110, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0012, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&SpeA_TOffCoclea}, 111, /*NUMMSG,*/ NULL, 0x0073, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&SpeA_Giri2}, 112, /*NUMMSG,*/ NULL, 0x0078, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&SpeA_Portata}, 120, /*NUMMSG,*/ NULL, 0x0074, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&SpeA_Esp}, 121, /*NUMMSG,*/ NULL, 0x0075, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&SpeA_TOnFan1}, 130, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0013, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&SpeA_TOffFan1}, 131, /*NUMMSG,*/ NULL, 0x0076, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&SpeA_AttuaFan1}, 132, /*NUMMSG,*/ NULL, 0x0077, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&SpeA_Giri2}, 140, /*NUMMSG,*/ NULL, 0x0078, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&SpeA_AttuaFan2}, 141, /*NUMMSG,*/ NULL, 0x0079, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&SpeA_AttuaFan2}, 142, /*NUMMSG,*/ NULL, 0x0079, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&SpeA_AttuaFan3}, 152, /*NUMMSG,*/ NULL, 0x007A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_HIGH}, 158, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_HIGH}, 159, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_HIGH}, 160, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_HIGH}, 161, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_HIGH}, 162, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEA_HIGH}, 163, /*NUMMSG,*/ sOnOff_tab, 0x0072, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_LOW}, 200, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_LOW}, 201, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_LOW}, 202, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_LOW}, 203, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_LOW}, 204, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_LOW}, 205, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_LOW}, 206, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&SpeB_TOnCoclea}, 210, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0014, MBTAB_HOLD2_REG, /*LOGIN_MANUFACTURER*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&SpeB_TOffCoclea}, 211, /*NUMMSG,*/ NULL, 0x007C, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&SpeB_Giri2}, 212, /*NUMMSG,*/ NULL, 0x0081, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&SpeB_Portata}, 220, /*NUMMSG,*/ NULL, 0x007D, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&SpeB_Esp}, 221, /*NUMMSG,*/ NULL, 0x007E, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&SpeB_TOnFan1}, 230, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0015, MBTAB_HOLD2_REG, /*LOGIN_MANUFACTURER*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&SpeB_TOffFan1}, 231, /*NUMMSG,*/ NULL, 0x007F, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&SpeB_AttuaFan1}, 232, /*NUMMSG,*/ NULL, 0x0080, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&SpeB_Giri2}, 240, /*NUMMSG,*/ NULL, 0x0081, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&SpeB_AttuaFan2}, 241, /*NUMMSG,*/ NULL, 0x0082, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&SpeB_AttuaFan2}, 242, /*NUMMSG,*/ NULL, 0x0082, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&SpeB_AttuaFan3}, 252, /*NUMMSG,*/ NULL, 0x0083, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_HIGH}, 258, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_HIGH}, 259, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_HIGH}, 260, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_HIGH}, 261, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_HIGH}, 262, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEB_HIGH}, 263, /*NUMMSG,*/ sOnOff_tab, 0x007B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_LOW}, 300, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_LOW}, 301, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_LOW}, 302, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_LOW}, 303, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_LOW}, 304, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_LOW}, 305, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_LOW}, 306, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&RaffA_TOnCoclea}, 310, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0016, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&RaffA_TOffCoclea}, 311, /*NUMMSG,*/ NULL, 0x0085, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&RaffA_Giri2}, 312, /*NUMMSG,*/ NULL, 0x008A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&RaffA_Portata}, 320, /*NUMMSG,*/ NULL, 0x0086, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&RaffA_Esp}, 321, /*NUMMSG,*/ NULL, 0x0087, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&RaffA_TOnFan1}, 330, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0017, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&RaffA_TOffFan1}, 331, /*NUMMSG,*/ NULL, 0x0088, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&RaffA_AttuaFan1}, 332, /*NUMMSG,*/ NULL, 0x0089, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&RaffA_Giri2}, 340, /*NUMMSG,*/ NULL, 0x008A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&RaffA_AttuaFan2}, 341, /*NUMMSG,*/ NULL, 0x008B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&RaffA_AttuaFan2}, 342, /*NUMMSG,*/ NULL, 0x008B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&RaffA_AttuaFan3}, 352, /*NUMMSG,*/ NULL, 0x008C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_HIGH}, 358, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_HIGH}, 359, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_HIGH}, 360, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_HIGH}, 361, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_HIGH}, 362, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFA_HIGH}, 363, /*NUMMSG,*/ sOnOff_tab, 0x0084, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_LOW}, 400, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_LOW}, 401, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_LOW}, 402, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_LOW}, 403, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_LOW}, 404, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_LOW}, 405, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_LOW}, 406, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pulizia_TOnCoclea}, 410, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x001C, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pulizia_TOffCoclea}, 411, /*NUMMSG,*/ NULL, 0x00B4, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pulizia_Giri2}, 412, /*NUMMSG,*/ NULL, 0x00B9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&Pulizia_Portata}, 420, /*NUMMSG,*/ NULL, 0x00B5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pulizia_Esp}, 421, /*NUMMSG,*/ NULL, 0x00B6, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pulizia_TOnFan1}, 430, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x001D, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pulizia_TOffFan1}, 431, /*NUMMSG,*/ NULL, 0x00B7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pulizia_AttuaFan1}, 432, /*NUMMSG,*/ NULL, 0x00B8, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pulizia_Giri2}, 440, /*NUMMSG,*/ NULL, 0x00B9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pulizia_AttuaFan2}, 441, /*NUMMSG,*/ NULL, 0x00BA, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pulizia_AttuaFan2}, 442, /*NUMMSG,*/ NULL, 0x00BA, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pulizia_AttuaFan3}, 452, /*NUMMSG,*/ NULL, 0x00BB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_HIGH}, 458, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_HIGH}, 459, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_HIGH}, 460, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_HIGH}, 461, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_HIGH}, 462, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIA_HIGH}, 463, /*NUMMSG,*/ sOnOff_tab, 0x00B3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_LOW}, 500, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_LOW}, 501, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_LOW}, 502, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_LOW}, 503, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_LOW}, 504, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_LOW}, 505, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_LOW}, 506, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&SpeC_TOnCoclea}, 510, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x001A, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&SpeC_TOffCoclea}, 511, /*NUMMSG,*/ NULL, 0x0097, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&SpeC_Giri2}, 512, /*NUMMSG,*/ NULL, 0x009C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&SpeC_Portata}, 520, /*NUMMSG,*/ NULL, 0x0098, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&SpeC_Esp}, 521, /*NUMMSG,*/ NULL, 0x0099, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&SpeC_TOnFan1}, 530, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x001B, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&SpeC_TOffFan1}, 531, /*NUMMSG,*/ NULL, 0x009A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&SpeC_AttuaFan1}, 532, /*NUMMSG,*/ NULL, 0x009B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&SpeC_Giri2}, 540, /*NUMMSG,*/ NULL, 0x009C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&SpeC_AttuaFan2}, 541, /*NUMMSG,*/ NULL, 0x009D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&SpeC_AttuaFan2}, 542, /*NUMMSG,*/ NULL, 0x009D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&SpeC_AttuaFan3}, 552, /*NUMMSG,*/ NULL, 0x009E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_HIGH}, 558, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_HIGH}, 559, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_HIGH}, 560, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_HIGH}, 561, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_HIGH}, 562, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SPEC_HIGH}, 563, /*NUMMSG,*/ sOnOff_tab, 0x0096, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_LOW}, 600, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_LOW}, 601, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_LOW}, 602, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_LOW}, 603, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_LOW}, 604, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_LOW}, 605, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_LOW}, 606, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&RaffB_TOnCoclea}, 610, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0018, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&RaffB_TOffCoclea}, 611, /*NUMMSG,*/ NULL, 0x008E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&RaffB_Giri2}, 612, /*NUMMSG,*/ NULL, 0x0093, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&RaffB_Portata}, 620, /*NUMMSG,*/ NULL, 0x008F, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&RaffB_Esp}, 621, /*NUMMSG,*/ NULL, 0x0090, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&RaffB_TOnFan1}, 630, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0019, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&RaffB_TOffFan1}, 631, /*NUMMSG,*/ NULL, 0x0091, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&RaffB_AttuaFan1}, 632, /*NUMMSG,*/ NULL, 0x0092, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&RaffB_Giri2}, 640, /*NUMMSG,*/ NULL, 0x0093, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&RaffB_AttuaFan2}, 641, /*NUMMSG,*/ NULL, 0x0094, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&RaffB_AttuaFan2}, 642, /*NUMMSG,*/ NULL, 0x0094, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&RaffB_AttuaFan3}, 652, /*NUMMSG,*/ NULL, 0x0095, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_HIGH}, 658, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_HIGH}, 659, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_HIGH}, 660, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_HIGH}, 661, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_HIGH}, 662, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&RAFFB_HIGH}, 663, /*NUMMSG,*/ sOnOff_tab, 0x008D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_LOW}, 700, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_LOW}, 701, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_LOW}, 702, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_LOW}, 703, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_LOW}, 704, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_LOW}, 705, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_LOW}, 706, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PuliziaB_TOnCoclea}, 710, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x002E, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PuliziaB_TOffCoclea}, 711, /*NUMMSG,*/ NULL, 0x0105, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&PuliziaB_Giri2}, 712, /*NUMMSG,*/ NULL, 0x010A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&PuliziaB_Portata}, 720, /*NUMMSG,*/ NULL, 0x0106, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&PuliziaB_Esp}, 721, /*NUMMSG,*/ NULL, 0x0107, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PuliziaB_TOnFan1}, 730, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x002F, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PuliziaB_TOffFan1}, 731, /*NUMMSG,*/ NULL, 0x0108, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PuliziaB_AttuaFan1}, 732, /*NUMMSG,*/ NULL, 0x0109, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&PuliziaB_Giri2}, 740, /*NUMMSG,*/ NULL, 0x010A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PuliziaB_AttuaFan2}, 741, /*NUMMSG,*/ NULL, 0x010B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PuliziaB_AttuaFan2}, 742, /*NUMMSG,*/ NULL, 0x010B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PuliziaB_AttuaFan3}, 752, /*NUMMSG,*/ NULL, 0x010C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_HIGH}, 758, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_HIGH}, 759, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_HIGH}, 760, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_HIGH}, 761, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_HIGH}, 762, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PULIZIAB_HIGH}, 763, /*NUMMSG,*/ sOnOff_tab, 0x0104, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },

	// Gruppo L
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_LOW}, 100, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_LOW}, 101, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_LOW}, 102, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_LOW}, 103, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_LOW}, 104, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_LOW}, 105, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_LOW}, 106, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PreAcc1_TOnCoclea}, 110, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0000, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PreAcc1_TOffCoclea}, 111, /*NUMMSG,*/ NULL, 0x0022, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&PreAcc1_Giri2}, 112, /*NUMMSG,*/ NULL, 0x0027, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&PreAcc1_Portata}, 120, /*NUMMSG,*/ NULL, 0x0023, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&PreAcc1_Esp}, 121, /*NUMMSG,*/ NULL, 0x0024, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PreAcc1_TOnFan1}, 130, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0001, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PreAcc1_TOffFan1}, 131, /*NUMMSG,*/ NULL, 0x0025, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PreAcc1_AttuaFan1}, 132, /*NUMMSG,*/ NULL, 0x0026, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&PreAcc1_Giri2}, 140, /*NUMMSG,*/ NULL, 0x0027, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PreAcc1_AttuaFan2}, 141, /*NUMMSG,*/ NULL, 0x0028, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PreAcc1_AttuaFan2}, 142, /*NUMMSG,*/ NULL, 0x0028, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PreAcc1_AttuaFan3}, 152, /*NUMMSG,*/ NULL, 0x0029, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_HIGH}, 158, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_HIGH}, 159, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_HIGH}, 160, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_HIGH}, 161, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_HIGH}, 162, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC1_HIGH}, 163, /*NUMMSG,*/ sOnOff_tab, 0x0021, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_LOW}, 200, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_LOW}, 201, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_LOW}, 202, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_LOW}, 203, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_LOW}, 204, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_LOW}, 205, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_LOW}, 206, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PreAcc2_TOnCoclea}, 210, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0002, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PreAcc2_TOffCoclea}, 211, /*NUMMSG,*/ NULL, 0x002B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&PreAcc2_Giri2}, 212, /*NUMMSG,*/ NULL, 0x0030, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&PreAcc2_Portata}, 220, /*NUMMSG,*/ NULL, 0x002C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&PreAcc2_Esp}, 221, /*NUMMSG,*/ NULL, 0x002D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PreAcc2_TOnFan1}, 230, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0003, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PreAcc2_TOffFan1}, 231, /*NUMMSG,*/ NULL, 0x002E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PreAcc2_AttuaFan1}, 232, /*NUMMSG,*/ NULL, 0x002F, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&PreAcc2_Giri2}, 240, /*NUMMSG,*/ NULL, 0x0030, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PreAcc2_AttuaFan2}, 241, /*NUMMSG,*/ NULL, 0x0031, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PreAcc2_AttuaFan2}, 242, /*NUMMSG,*/ NULL, 0x0031, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PreAcc2_AttuaFan3}, 252, /*NUMMSG,*/ NULL, 0x0032, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_HIGH}, 258, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_HIGH}, 259, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_HIGH}, 260, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_HIGH}, 261, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_HIGH}, 262, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACC2_HIGH}, 263, /*NUMMSG,*/ sOnOff_tab, 0x002A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_LOW}, 300, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_LOW}, 301, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_LOW}, 302, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_LOW}, 303, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_LOW}, 304, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_LOW}, 305, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_LOW}, 306, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PreAccCaldo_TOnCoclea}, 310, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0004, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PreAccCaldo_TOffCoclea}, 311, /*NUMMSG,*/ NULL, 0x0034, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&PreAccCaldo_Giri2}, 312, /*NUMMSG,*/ NULL, 0x0039, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&PreAccCaldo_Portata}, 320, /*NUMMSG,*/ NULL, 0x0035, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&PreAccCaldo_Esp}, 321, /*NUMMSG,*/ NULL, 0x0036, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PreAccCaldo_TOnFan1}, 330, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0005, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&PreAccCaldo_TOffFan1}, 331, /*NUMMSG,*/ NULL, 0x0037, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PreAccCaldo_AttuaFan1}, 332, /*NUMMSG,*/ NULL, 0x0038, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&PreAccCaldo_Giri2}, 340, /*NUMMSG,*/ NULL, 0x0039, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PreAccCaldo_AttuaFan2}, 341, /*NUMMSG,*/ NULL, 0x003A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PreAccCaldo_AttuaFan2}, 342, /*NUMMSG,*/ NULL, 0x003A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&PreAccCaldo_AttuaFan3}, 352, /*NUMMSG,*/ NULL, 0x003B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_HIGH}, 358, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_HIGH}, 359, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_HIGH}, 360, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_HIGH}, 361, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_HIGH}, 362, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PREACCCALDO_HIGH}, 363, /*NUMMSG,*/ sOnOff_tab, 0x0033, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_LOW}, 400, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_LOW}, 401, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_LOW}, 402, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_LOW}, 403, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_LOW}, 404, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_LOW}, 405, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_LOW}, 406, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&AccA_TOnCoclea}, 410, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0006, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&AccA_TOffCoclea}, 411, /*NUMMSG,*/ NULL, 0x003D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&AccA_Giri2}, 412, /*NUMMSG,*/ NULL, 0x0042, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&AccA_Portata}, 420, /*NUMMSG,*/ NULL, 0x003E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&AccA_Esp}, 421, /*NUMMSG,*/ NULL, 0x003F, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&AccA_TOnFan1}, 430, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0007, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&AccA_TOffFan1}, 431, /*NUMMSG,*/ NULL, 0x0040, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&AccA_AttuaFan1}, 432, /*NUMMSG,*/ NULL, 0x0041, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&AccA_Giri2}, 440, /*NUMMSG,*/ NULL, 0x0042, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&AccA_AttuaFan2}, 441, /*NUMMSG,*/ NULL, 0x0043, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&AccA_AttuaFan2}, 442, /*NUMMSG,*/ NULL, 0x0043, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&AccA_AttuaFan3}, 452, /*NUMMSG,*/ NULL, 0x0044, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_HIGH}, 458, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_HIGH}, 459, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_HIGH}, 460, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_HIGH}, 461, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_HIGH}, 462, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCA_HIGH}, 463, /*NUMMSG,*/ sOnOff_tab, 0x003C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_LOW}, 500, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_LOW}, 501, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_LOW}, 502, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_LOW}, 503, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_LOW}, 504, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_LOW}, 505, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_LOW}, 506, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&AccB_TOnCoclea}, 510, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0008, MBTAB_HOLD2_REG, /*LOGIN_MANUFACTURER*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&AccB_TOffCoclea}, 511, /*NUMMSG,*/ NULL, 0x0046, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&AccB_Giri2}, 512, /*NUMMSG,*/ NULL, 0x004B, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&AccB_Portata}, 520, /*NUMMSG,*/ NULL, 0x0047, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&AccB_Esp}, 521, /*NUMMSG,*/ NULL, 0x0048, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&AccB_TOnFan1}, 530, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0009, MBTAB_HOLD2_REG, /*LOGIN_MANUFACTURER*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&AccB_TOffFan1}, 531, /*NUMMSG,*/ NULL, 0x0049, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&AccB_AttuaFan1}, 532, /*NUMMSG,*/ NULL, 0x004A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&AccB_Giri2}, 540, /*NUMMSG,*/ NULL, 0x004B, MBTAB_HOLD_REG, /*LOGIN_MANUFACTURER*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&AccB_AttuaFan2}, 541, /*NUMMSG,*/ NULL, 0x004C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&AccB_AttuaFan2}, 542, /*NUMMSG,*/ NULL, 0x004C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&AccB_AttuaFan3}, 552, /*NUMMSG,*/ NULL, 0x004D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_HIGH}, 558, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_HIGH}, 559, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_HIGH}, 560, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_HIGH}, 561, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_HIGH}, 562, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCB_HIGH}, 563, /*NUMMSG,*/ sOnOff_tab, 0x0045, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_LOW}, 600, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_LOW}, 601, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_LOW}, 602, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_LOW}, 603, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_LOW}, 604, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_LOW}, 605, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_LOW}, 606, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&FireOnA_TOnCoclea}, 610, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x000A, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&FireOnA_TOffCoclea}, 611, /*NUMMSG,*/ NULL, 0x004F, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&FireOnA_Giri2}, 612, /*NUMMSG,*/ NULL, 0x0054, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&FireOnA_Portata}, 620, /*NUMMSG,*/ NULL, 0x0050, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&FireOnA_Esp}, 621, /*NUMMSG,*/ NULL, 0x0051, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&FireOnA_TOnFan1}, 630, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x000B, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&FireOnA_TOffFan1}, 631, /*NUMMSG,*/ NULL, 0x0052, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&FireOnA_AttuaFan1}, 632, /*NUMMSG,*/ NULL, 0x0053, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&FireOnA_Giri2}, 640, /*NUMMSG,*/ NULL, 0x0054, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&FireOnA_AttuaFan2}, 641, /*NUMMSG,*/ NULL, 0x0055, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&FireOnA_AttuaFan2}, 642, /*NUMMSG,*/ NULL, 0x0055, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&FireOnA_AttuaFan3}, 652, /*NUMMSG,*/ NULL, 0x0056, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_HIGH}, 658, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_HIGH}, 659, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_HIGH}, 660, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_HIGH}, 661, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_HIGH}, 662, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONA_HIGH}, 663, /*NUMMSG,*/ sOnOff_tab, 0x004E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_LOW}, 700, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_LOW}, 701, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_LOW}, 702, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_LOW}, 703, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_LOW}, 704, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_LOW}, 705, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_LOW}, 706, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&FireOnB_TOnCoclea}, 710, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x000C, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&FireOnB_TOffCoclea}, 711, /*NUMMSG,*/ NULL, 0x0058, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&FireOnB_Giri2}, 712, /*NUMMSG,*/ NULL, 0x005D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&FireOnB_Portata}, 720, /*NUMMSG,*/ NULL, 0x0059, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&FireOnB_Esp}, 721, /*NUMMSG,*/ NULL, 0x005A, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&FireOnB_TOnFan1}, 730, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x000D, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&FireOnB_TOffFan1}, 731, /*NUMMSG,*/ NULL, 0x005B, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&FireOnB_AttuaFan1}, 732, /*NUMMSG,*/ NULL, 0x005C, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&FireOnB_Giri2}, 740, /*NUMMSG,*/ NULL, 0x005D, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&FireOnB_AttuaFan2}, 741, /*NUMMSG,*/ NULL, 0x005E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&FireOnB_AttuaFan2}, 742, /*NUMMSG,*/ NULL, 0x005E, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&FireOnB_AttuaFan3}, 752, /*NUMMSG,*/ NULL, 0x005F, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_HIGH}, 758, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_HIGH}, 759, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_HIGH}, 760, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_HIGH}, 761, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_HIGH}, 762, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&FIREONB_HIGH}, 763, /*NUMMSG,*/ sOnOff_tab, 0x0057, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_LOW}, 800, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_LOW}, 801, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_LOW}, 802, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_LOW}, 803, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_LOW}, 804, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_LOW}, 805, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_LOW}, 806, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&AccC_TOnCoclea}, 810, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x000E, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&AccC_TOffCoclea}, 811, /*NUMMSG,*/ NULL, 0x0061, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&AccC_Giri2}, 812, /*NUMMSG,*/ NULL, 0x0066, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&AccC_Portata}, 820, /*NUMMSG,*/ NULL, 0x0062, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&AccC_Esp}, 821, /*NUMMSG,*/ NULL, 0x0063, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&AccC_TOnFan1}, 830, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x000F, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&AccC_TOffFan1}, 831, /*NUMMSG,*/ NULL, 0x0064, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&AccC_AttuaFan1}, 832, /*NUMMSG,*/ NULL, 0x0065, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&AccC_Giri2}, 840, /*NUMMSG,*/ NULL, 0x0066, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&AccC_AttuaFan2}, 841, /*NUMMSG,*/ NULL, 0x0067, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&AccC_AttuaFan2}, 842, /*NUMMSG,*/ NULL, 0x0067, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&AccC_AttuaFan3}, 852, /*NUMMSG,*/ NULL, 0x0068, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_HIGH}, 858, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_HIGH}, 859, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_HIGH}, 860, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_HIGH}, 861, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_HIGH}, 862, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&ACCC_HIGH}, 863, /*NUMMSG,*/ sOnOff_tab, 0x0060, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
                                 
	// Gruppo P                   
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_LOW}, 100, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_LOW}, 101, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_LOW}, 102, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_LOW}, 103, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_LOW}, 104, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_LOW}, 105, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_LOW}, 106, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot1_TOnCoclea}, 110, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x001E, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot1_TOffCoclea}, 111, /*NUMMSG,*/ NULL, 0x00BD, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot1_Giri2}, 112, /*NUMMSG,*/ NULL, 0x00C2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&Pot1_Portata}, 120, /*NUMMSG,*/ NULL, 0x00BE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot1_Esp}, 121, /*NUMMSG,*/ NULL, 0x00BF, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot1_TOnFan1}, 130, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x001F, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot1_TOffFan1}, 131, /*NUMMSG,*/ NULL, 0x00C0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot1_AttuaFan1}, 132, /*NUMMSG,*/ NULL, 0x00C1, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot1_Giri2}, 140, /*NUMMSG,*/ NULL, 0x00C2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot1_AttuaFan2}, 141, /*NUMMSG,*/ NULL, 0x00C3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot1_AttuaFan2}, 142, /*NUMMSG,*/ NULL, 0x00C3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot1_AttuaFan3}, 152, /*NUMMSG,*/ NULL, 0x00C4, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_HIGH}, 158, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_HIGH}, 159, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_HIGH}, 160, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_HIGH}, 161, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_HIGH}, 162, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT1_HIGH}, 163, /*NUMMSG,*/ sOnOff_tab, 0x00BC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_LOW}, 200, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_LOW}, 201, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_LOW}, 202, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_LOW}, 203, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_LOW}, 204, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_LOW}, 205, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_LOW}, 206, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot2_TOnCoclea}, 210, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0020, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot2_TOffCoclea}, 211, /*NUMMSG,*/ NULL, 0x00C6, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot2_Giri2}, 212, /*NUMMSG,*/ NULL, 0x00CB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&Pot2_Portata}, 220, /*NUMMSG,*/ NULL, 0x00C7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot2_Esp}, 221, /*NUMMSG,*/ NULL, 0x00C8, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot2_TOnFan1}, 230, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0021, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot2_TOffFan1}, 231, /*NUMMSG,*/ NULL, 0x00C9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot2_AttuaFan1}, 232, /*NUMMSG,*/ NULL, 0x00CA, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot2_Giri2}, 240, /*NUMMSG,*/ NULL, 0x00CB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot2_AttuaFan2}, 241, /*NUMMSG,*/ NULL, 0x00CC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot2_AttuaFan2}, 242, /*NUMMSG,*/ NULL, 0x00CC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot2_AttuaFan3}, 252, /*NUMMSG,*/ NULL, 0x00CD, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_HIGH}, 258, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_HIGH}, 259, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_HIGH}, 260, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_HIGH}, 261, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_HIGH}, 262, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT2_HIGH}, 263, /*NUMMSG,*/ sOnOff_tab, 0x00C5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_LOW}, 300, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_LOW}, 301, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_LOW}, 302, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_LOW}, 303, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_LOW}, 304, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_LOW}, 305, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_LOW}, 306, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot3_TOnCoclea}, 310, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0022, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot3_TOffCoclea}, 311, /*NUMMSG,*/ NULL, 0x00CF, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot3_Giri2}, 312, /*NUMMSG,*/ NULL, 0x00D4, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&Pot3_Portata}, 320, /*NUMMSG,*/ NULL, 0x00D0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot3_Esp}, 321, /*NUMMSG,*/ NULL, 0x00D1, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot3_TOnFan1}, 330, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0023, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot3_TOffFan1}, 331, /*NUMMSG,*/ NULL, 0x00D2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot3_AttuaFan1}, 332, /*NUMMSG,*/ NULL, 0x00D3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot3_Giri2}, 340, /*NUMMSG,*/ NULL, 0x00D4, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot3_AttuaFan2}, 341, /*NUMMSG,*/ NULL, 0x00D5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot3_AttuaFan2}, 342, /*NUMMSG,*/ NULL, 0x00D5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot3_AttuaFan3}, 352, /*NUMMSG,*/ NULL, 0x00D6, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_HIGH}, 358, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_HIGH}, 359, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_HIGH}, 360, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_HIGH}, 361, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_HIGH}, 362, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT3_HIGH}, 363, /*NUMMSG,*/ sOnOff_tab, 0x00CE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_LOW}, 400, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_LOW}, 401, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_LOW}, 402, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_LOW}, 403, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_LOW}, 404, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_LOW}, 405, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_LOW}, 406, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot4_TOnCoclea}, 410, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0024, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot4_TOffCoclea}, 411, /*NUMMSG,*/ NULL, 0x00D8, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot4_Giri2}, 412, /*NUMMSG,*/ NULL, 0x00DD, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&Pot4_Portata}, 420, /*NUMMSG,*/ NULL, 0x00D9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot4_Esp}, 421, /*NUMMSG,*/ NULL, 0x00DA, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot4_TOnFan1}, 430, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0025, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot4_TOffFan1}, 431, /*NUMMSG,*/ NULL, 0x00DB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot4_AttuaFan1}, 432, /*NUMMSG,*/ NULL, 0x00DC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot4_Giri2}, 440, /*NUMMSG,*/ NULL, 0x00DD, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot4_AttuaFan2}, 441, /*NUMMSG,*/ NULL, 0x00DE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot4_AttuaFan2}, 442, /*NUMMSG,*/ NULL, 0x00DE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot4_AttuaFan3}, 452, /*NUMMSG,*/ NULL, 0x00DF, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_HIGH}, 458, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_HIGH}, 459, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_HIGH}, 460, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_HIGH}, 461, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_HIGH}, 462, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT4_HIGH}, 463, /*NUMMSG,*/ sOnOff_tab, 0x00D7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_LOW}, 500, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_LOW}, 501, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_LOW}, 502, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_LOW}, 503, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_LOW}, 504, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_LOW}, 505, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_LOW}, 506, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot5_TOnCoclea}, 510, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0026, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot5_TOffCoclea}, 511, /*NUMMSG,*/ NULL, 0x00E1, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot5_Giri2}, 512, /*NUMMSG,*/ NULL, 0x00E6, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&Pot5_Portata}, 520, /*NUMMSG,*/ NULL, 0x00E2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot5_Esp}, 521, /*NUMMSG,*/ NULL, 0x00E3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot5_TOnFan1}, 530, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0027, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot5_TOffFan1}, 531, /*NUMMSG,*/ NULL, 0x00E4, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot5_AttuaFan1}, 532, /*NUMMSG,*/ NULL, 0x00E5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot5_Giri2}, 540, /*NUMMSG,*/ NULL, 0x00E6, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot5_AttuaFan2}, 541, /*NUMMSG,*/ NULL, 0x00E7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot5_AttuaFan2}, 542, /*NUMMSG,*/ NULL, 0x00E7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot5_AttuaFan3}, 552, /*NUMMSG,*/ NULL, 0x00E8, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_HIGH}, 558, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_HIGH}, 559, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_HIGH}, 560, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_HIGH}, 561, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_HIGH}, 562, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT5_HIGH}, 563, /*NUMMSG,*/ sOnOff_tab, 0x00E0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_LOW}, 600, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_LOW}, 601, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_LOW}, 602, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_LOW}, 603, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_LOW}, 604, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_LOW}, 605, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_LOW}, 606, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot6_TOnCoclea}, 610, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0028, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot6_TOffCoclea}, 611, /*NUMMSG,*/ NULL, 0x00EA, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot6_Giri2}, 612, /*NUMMSG,*/ NULL, 0x00EF, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&Pot6_Portata}, 620, /*NUMMSG,*/ NULL, 0x00EB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot6_Esp}, 621, /*NUMMSG,*/ NULL, 0x00EC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot6_TOnFan1}, 630, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x0029, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot6_TOffFan1}, 631, /*NUMMSG,*/ NULL, 0x00ED, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot6_AttuaFan1}, 632, /*NUMMSG,*/ NULL, 0x00EE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot6_Giri2}, 640, /*NUMMSG,*/ NULL, 0x00EF, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot6_AttuaFan2}, 641, /*NUMMSG,*/ NULL, 0x00F0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot6_AttuaFan2}, 642, /*NUMMSG,*/ NULL, 0x00F0, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot6_AttuaFan3}, 652, /*NUMMSG,*/ NULL, 0x00F1, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_HIGH}, 658, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_HIGH}, 659, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_HIGH}, 660, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_HIGH}, 661, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_HIGH}, 662, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT6_HIGH}, 663, /*NUMMSG,*/ sOnOff_tab, 0x00E9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_LOW}, 700, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_LOW}, 701, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_LOW}, 702, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_LOW}, 703, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_LOW}, 704, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_LOW}, 705, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_LOW}, 706, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot7_TOnCoclea}, 710, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x002A, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot7_TOffCoclea}, 711, /*NUMMSG,*/ NULL, 0x00F3, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot7_Giri2}, 712, /*NUMMSG,*/ NULL, 0x00F8, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&Pot7_Portata}, 720, /*NUMMSG,*/ NULL, 0x00F4, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot7_Esp}, 721, /*NUMMSG,*/ NULL, 0x00F5, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot7_TOnFan1}, 730, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x002B, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot7_TOffFan1}, 731, /*NUMMSG,*/ NULL, 0x00F6, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot7_AttuaFan1}, 732, /*NUMMSG,*/ NULL, 0x00F7, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot7_Giri2}, 740, /*NUMMSG,*/ NULL, 0x00F8, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot7_AttuaFan2}, 741, /*NUMMSG,*/ NULL, 0x00F9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot7_AttuaFan2}, 742, /*NUMMSG,*/ NULL, 0x00F9, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot7_AttuaFan3}, 752, /*NUMMSG,*/ NULL, 0x00FA, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_HIGH}, 758, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_HIGH}, 759, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_HIGH}, 760, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_HIGH}, 761, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_HIGH}, 762, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT7_HIGH}, 763, /*NUMMSG,*/ sOnOff_tab, 0x00F2, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_LOW}, 800, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_LOW}, 801, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_LOW}, 802, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_LOW}, 803, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_LOW}, 804, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_LOW}, 805, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_LOW}, 806, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot8_TOnCoclea}, 810, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x002C, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot8_TOffCoclea}, 811, /*NUMMSG,*/ NULL, 0x00FC, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot8_Giri2}, 812, /*NUMMSG,*/ NULL, 0x0101, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)400}, /*{(unsigned long)0},*/ 1, {(void *)&Pot8_Portata}, 820, /*NUMMSG,*/ NULL, 0x00FD, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot8_Esp}, 821, /*NUMMSG,*/ NULL, 0x00FE, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot8_TOnFan1}, 830, /*NUMMSG,*/ NULL, THOLREG2_FIRST_ADDR+0x002D, MBTAB_HOLD2_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)12000}, /*{(unsigned long)0},*/ 2, {(void *)&Pot8_TOffFan1}, 831, /*NUMMSG,*/ NULL, 0x00FF, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },	// il valore e' espresso in step di 50ms
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot8_AttuaFan1}, 832, /*NUMMSG,*/ NULL, 0x0100, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)300}, {(unsigned long)2750}, /*{(unsigned long)0},*/ 50, {(void *)&Pot8_Giri2}, 840, /*NUMMSG,*/ NULL, 0x0101, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot8_AttuaFan2}, 841, /*NUMMSG,*/ NULL, 0x0102, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot8_AttuaFan2}, 842, /*NUMMSG,*/ NULL, 0x0102, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_WORD, 0, {(unsigned long)500}, {(unsigned long)4000}, /*{(unsigned long)0},*/ 1, {(void *)&Pot8_AttuaFan3}, 852, /*NUMMSG,*/ NULL, 0x0103, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_HIGH}, 858, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_HIGH}, 859, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_HIGH}, 860, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_HIGH}, 861, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_HIGH}, 862, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&POT8_HIGH}, 863, /*NUMMSG,*/ sOnOff_tab, 0x00FB, MBTAB_HOLD_REG, /*LOGIN_SERVICE*/ },

	
	// Gruppo Z (parametri locali)
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)(NROMLANG-1)}, /*{(unsigned long)0},*/ 1, {(void *)&iLanguage}, 0, /*d_LINGUA,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_WORD, 0, {(unsigned long)MIN_YEAR_RTC}, {(unsigned long)MAX_YEAR}, /*{(unsigned long)MIN_YEAR},*/ 1, {(void *)&DateTime.tm_year}, 1, /*d_YEAR,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)1}, {(unsigned long)12}, /*{(unsigned long)1},*/ 1, {(void *)&DateTime.tm_mon}, 2, /*d_MONTH,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)1}, {(unsigned long)31}, /*{(unsigned long)1},*/ 1, {(void *)&DateTime.tm_mday}, 3, /*d_DAY,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)6}, /*{(unsigned long)6},*/ 1, {(void *)&DateTime.tm_wday}, 4, /*d_WEEKDAY,*/ sWeekDay_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)23}, /*{(unsigned long)0},*/ 1, {(void *)&DateTime.tm_hour}, 5, /*d_HOUR,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)59}, /*{(unsigned long)0},*/ 1, {(void *)&DateTime.tm_min}, 6, /*d_MINUTE,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)1}, {(unsigned long)6}, /*{(unsigned long)1},*/ 1, {(void *)&StatSec.BackLightTime}, 7, /*d_BACKLIGHT,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	//{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&PAN_BYTE_SETS}, 8, /*d_TONE,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x30, {(unsigned long)0}, {(unsigned long)(NSHOWTEMP-1)}, /*{(unsigned long)SHOW_TEMP_AUTO},*/ 1, {(void *)&PAN_BYTE_SETS}, 9, /*d_SHOWTEMP,*/ ShowTemp_StrInd_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)RFID_MIN}, {(unsigned long)RFID_MAX}, /*{(unsigned long)RFID_DEF},*/ 1, {(void *)&RFid}, 10, /*d_RFPANEL,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)1},*/ 1, {(void *)&PAN_BYTE_SETS}, 11, /*d_VISSANITARI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x80, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)1},*/ 1, {(void *)&PAN_BYTE_SETS}, 12, /*d_VISTIPOSTUFA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_DWORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 1, {(void *)&StatSec.AccConsumo}, 13, /*NUMMSG,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_WORD, 0, {(unsigned long)0}, {(unsigned long)0}, /*{(unsigned long)0},*/ 1, {(void *)&StatSec.NumNoAcc}, 14, /*NUMMSG,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)(UCOL_MAX-1)}, /*{(unsigned long)0},*/ 1, {(void *)&StatSec.UserColorID}, 15, /*NUMMSG,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	// parametri particolari
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&INPBMAP25_LOW}, 100, /*d_PRECARICAPELLET,*/ sOnOff_tab, 0x0019, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },	// impostabile tramite Comando Codice Debug
	//{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&INPBMAP25_LOW}, 104, /*d_PULIZIA,*/ sOnOff_tab, 0x0019, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },	// impostabile tramite Comando Codice Debug
	{ TYPE_BOOL, 5, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 104, /*d_PULIZIA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },	// impostabile tramite Comando Codice Debug
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&INPBMAP25_HIGH}, 105, /*d_ATTIVA_POMPA,*/ sOnOff_tab, 0x0019, MBTAB_INPUT_REG, /*LOGIN_FREE*/ },	// impostabile tramite Comando Codice Debug
	{ TYPE_BOOL, 13, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 106, /*d_TEST_SEQUENZA_TEMP,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	{ TYPE_BOOL, 18, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 107, /*d_TEST_CALIB_FOTORES_ON,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	{ TYPE_BOOL, 19, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 108, /*d_TEST_CALIB_FOTORES_OFF,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	{ TYPE_BOOL, 17, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 109, /*sINTERPOLAZIONE_MENU,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	{ TYPE_BOOL, 17, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 110, /*sINTERPOLAZIONE_MENU,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	{ TYPE_BOOL, 17, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 111, /*sINTERPOLAZIONE_MENU,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	{ TYPE_BOOL, 17, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 112, /*sINTERPOLAZIONE_MENU,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	{ TYPE_BOOL, 17, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 113, /*sINTERPOLAZIONE_MENU,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	{ TYPE_BOOL, 17, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 114, /*sINTERPOLAZIONE_MENU,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	{ TYPE_BOOL, 17, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 115, /*sINTERPOLAZIONE_MENU,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	{ TYPE_BOOL, 17, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 116, /*sINTERPOLAZIONE_MENU,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },
	{ TYPE_BOOL, 17, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&SystemFlags}, 117, /*sINTERPOLAZIONE_MENU,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_SERVICE*/ },

	// parametro Sleep
	{ TYPE_BYTE, 0, {(unsigned long)SLEEP_MIN_SET_VALUE}, {(unsigned long)(SLEEP_MAX_SET_VALUE+1)}, /*{(unsigned long)0},*/ 1, {(void *)NULL}, 200, /*AVVIO3SLEEP,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },

	// parametri Crono
	{ TYPE_BYTE_MASK, 0x80, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.CronoMode.BYTE}, 300, /*ABILITAZIONE,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x7F, {(unsigned long)1}, {(unsigned long)NUMCRONOPROFILE}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.CronoMode.BYTE}, 301, /*CARICAPROFILO,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	// prg1 settimanale
	{ TYPE_BYTE_MASK, 0x80, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].Enab.BYTE}, 310, /*PRGABILITA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)95}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].OraStart}, 311, /*PRGSTART,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)1}, {(unsigned long)96}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].OraStop}, 312, /*PRGSTOP,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)MINSETTEMP}, {(unsigned long)MAXSETTEMP}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].TempAria}, 313, /*PRGTEMPARIA,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)H2O_TMIN}, {(unsigned long)H2O_TMAX}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].TempH2O}, 314, /*PRGTEMPH2O,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)POTMINLEV}, {(unsigned long)POTMAXLEV}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].Fire}, 315, /*PRGPOTENZA,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].Enab.BYTE}, 320, /*sDOMENICA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].Enab.BYTE}, 321, /*sLUNEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].Enab.BYTE}, 322, /*sMARTEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].Enab.BYTE}, 323, /*sMERCOLEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].Enab.BYTE}, 324, /*sGIOVEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].Enab.BYTE}, 325, /*sVENERDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[0].Enab.BYTE}, 326, /*sSABATO,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
#ifdef menu_CRONO	
	// prg2 settimanale
	{ TYPE_BYTE_MASK, 0x80, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].Enab.BYTE}, 330, /*PRGABILITA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)95}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].OraStart}, 331, /*PRGSTART,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)1}, {(unsigned long)96}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].OraStop}, 332, /*PRGSTOP,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)MINSETTEMP}, {(unsigned long)MAXSETTEMP}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].TempAria}, 333, /*PRGTEMPARIA,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)H2O_TMIN}, {(unsigned long)H2O_TMAX}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].TempH2O}, 334, /*PRGTEMPH2O,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)POTMINLEV}, {(unsigned long)POTMAXLEV}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].Fire}, 335, /*PRGPOTENZA,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].Enab.BYTE}, 340, /*sDOMENICA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].Enab.BYTE}, 341, /*sLUNEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].Enab.BYTE}, 342, /*sMARTEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].Enab.BYTE}, 343, /*sMERCOLEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].Enab.BYTE}, 344, /*sGIOVEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].Enab.BYTE}, 345, /*sVENERDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[1].Enab.BYTE}, 346, /*sSABATO,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	// prg3 settimanale
	{ TYPE_BYTE_MASK, 0x80, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].Enab.BYTE}, 350, /*PRGABILITA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)95}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].OraStart}, 351, /*PRGSTART,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)1}, {(unsigned long)96}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].OraStop}, 352, /*PRGSTOP,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)MINSETTEMP}, {(unsigned long)MAXSETTEMP}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].TempAria}, 353, /*PRGTEMPARIA,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)H2O_TMIN}, {(unsigned long)H2O_TMAX}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].TempH2O}, 354, /*PRGTEMPH2O,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)POTMINLEV}, {(unsigned long)POTMAXLEV}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].Fire}, 355, /*PRGPOTENZA,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].Enab.BYTE}, 360, /*sDOMENICA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].Enab.BYTE}, 361, /*sLUNEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].Enab.BYTE}, 362, /*sMARTEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].Enab.BYTE}, 363, /*sMERCOLEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].Enab.BYTE}, 364, /*sGIOVEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].Enab.BYTE}, 365, /*sVENERDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[2].Enab.BYTE}, 366, /*sSABATO,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	// prg4 settimanale
	{ TYPE_BYTE_MASK, 0x80, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].Enab.BYTE}, 370, /*PRGABILITA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)95}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].OraStart}, 371, /*PRGSTART,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)1}, {(unsigned long)96}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].OraStop}, 372, /*PRGSTOP,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)MINSETTEMP}, {(unsigned long)MAXSETTEMP}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].TempAria}, 373, /*PRGTEMPARIA,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)H2O_TMIN}, {(unsigned long)H2O_TMAX}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].TempH2O}, 374, /*PRGTEMPH2O,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)POTMINLEV}, {(unsigned long)POTMAXLEV}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].Fire}, 375, /*PRGPOTENZA,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].Enab.BYTE}, 380, /*sDOMENICA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].Enab.BYTE}, 381, /*sLUNEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].Enab.BYTE}, 382, /*sMARTEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].Enab.BYTE}, 383, /*sMERCOLEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].Enab.BYTE}, 384, /*sGIOVEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].Enab.BYTE}, 385, /*sVENERDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[3].Enab.BYTE}, 386, /*sSABATO,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	// prg5 settimanale
	{ TYPE_BYTE_MASK, 0x80, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].Enab.BYTE}, 390, /*PRGABILITA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)95}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].OraStart}, 391, /*PRGSTART,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)1}, {(unsigned long)96}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].OraStop}, 392, /*PRGSTOP,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)MINSETTEMP}, {(unsigned long)MAXSETTEMP}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].TempAria}, 393, /*PRGTEMPARIA,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)H2O_TMIN}, {(unsigned long)H2O_TMAX}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].TempH2O}, 394, /*PRGTEMPH2O,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)POTMINLEV}, {(unsigned long)POTMAXLEV}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].Fire}, 395, /*PRGPOTENZA,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].Enab.BYTE}, 400, /*sDOMENICA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].Enab.BYTE}, 401, /*sLUNEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].Enab.BYTE}, 402, /*sMARTEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].Enab.BYTE}, 403, /*sMERCOLEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].Enab.BYTE}, 404, /*sGIOVEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].Enab.BYTE}, 405, /*sVENERDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[4].Enab.BYTE}, 406, /*sSABATO,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	// prg6 settimanale
	{ TYPE_BYTE_MASK, 0x80, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].Enab.BYTE}, 410, /*PRGABILITA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)0}, {(unsigned long)95}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].OraStart}, 411, /*PRGSTART,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)1}, {(unsigned long)96}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].OraStop}, 412, /*PRGSTOP,*/ NULL, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)MINSETTEMP}, {(unsigned long)MAXSETTEMP}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].TempAria}, 413, /*PRGTEMPARIA,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)H2O_TMIN}, {(unsigned long)H2O_TMAX}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].TempH2O}, 414, /*PRGTEMPH2O,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE, 0, {(unsigned long)POTMINLEV}, {(unsigned long)POTMAXLEV}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].Fire}, 415, /*PRGPOTENZA,*/ NULL, NO_INDVAR, MBTAB_HOLD_REG, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x01, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].Enab.BYTE}, 420, /*sDOMENICA,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x02, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].Enab.BYTE}, 421, /*sLUNEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x04, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].Enab.BYTE}, 422, /*sMARTEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x08, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].Enab.BYTE}, 423, /*sMERCOLEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x10, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].Enab.BYTE}, 424, /*sGIOVEDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x20, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].Enab.BYTE}, 425, /*sVENERDI,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
	{ TYPE_BYTE_MASK, 0x40, {(unsigned long)0}, {(unsigned long)1}, /*{(unsigned long)0},*/ 1, {(void *)&Crono.Crono_ProgWeek[5].Enab.BYTE}, 426, /*sSABATO,*/ sOnOff_tab, NO_INDVAR, NUM_MBTAB, /*LOGIN_FREE*/ },
#endif	// menu_CRONO


};




/*!
** \fn unsigned char userDelLeadingZero( unsigned char *buf, unsigned char size )
** \brief Replace the leading '0' with ' ' for a numeric value in a string buffer
** \param buf The string buffer of the numeric value
** \param size The string buffer size
** \return The first not-replaced character index in the string buffer
**/
unsigned char userDelLeadingZero( unsigned char *buf, unsigned char size )
{
	unsigned char RetVal = 0;
	unsigned char i;

	if( size >= 2 )
	{
		for( i=0; i<size-1; i++ )
		{
			if( buf[i] == '0' )
			{
				if( i )
				{
					if( (buf[i-1] == cBLANK) && (buf[i+1] != '.') )
						buf[i] = cBLANK;	// sostituisce lo zero e continua
					else
						break;	// termina
				}
				else
				{
					if( buf[i+1] != '.' )
						buf[i] = cBLANK;	// sostituisce lo zero e continua
					else
						break;	// termina
				}
			}
			else if( buf[i] != cBLANK )
				break;	// termina

			RetVal++;	// indice del carattere successivo
		}
	}

	return RetVal;
}

/*!
** \fn void userDelLeadingEndingSpace( unsigned char *buf, unsigned char size )
** \brief Copy the string buffer in the 'str1' string without leading and ending
** 		space characters
** \param buf The string buffer
** \param size The string buffer size
** \return None
**/
void userDelLeadingEndingSpace( unsigned char *buf, unsigned char size )
{
	unsigned char i;

	memset( str1, cBLANK, size );
	if( memcmp( buf, str1, size ) )
	{
		// spazi iniziali
		for( i=0; i<size-1; i++ )
		{
			if( buf[i] != cBLANK )
				break;
		}
		size -= i;
		memcpy( str1, &buf[i], size );
		// spazi finali
		for( i=0; i<size-1; i++ )
		{
			if( str1[size-1-i] != cBLANK )
				break;
		}
		size -= i;
		str1[size] = '\0';
	}
	else
	{
		// buffer di soli spazi
		str1[0] = '\0';
	}

}

/*!
** \fn unsigned short SearchParam( void )
** \brief Ricerca indice parametro da descrizione sintetica 'sDescPar'
** \return L'indice del parametro trovato in tabella, 0xFFFF altrimenti.
**/
unsigned short SearchParam( void )
{
	unsigned short RetVal = 0xFFFF;
	const PARTAB_STRUCT *ps;
	unsigned char i;
	unsigned short iGrPar;
	unsigned short mid;
	unsigned short left;
	unsigned short right;

	// ricerca gruppo
	for( i=0; i<NUM_GROUP_PAR; i++ )
	{
		if( sDescPar[0] == Group_tab[i].cId )
			break;
	}
	if( i < NUM_GROUP_PAR )
	{
		iGrPar = atoi( &sDescPar[1] );	// codice parametro nel gruppo
		// ricerca binaria parametro nel gruppo
		left = Group_tab[i].iParStart;
		right = Group_tab[i].iParEnd;
		i = 0;
		while( left <= right )
		{
			mid = (left + right) / 2;
			ps = &Param_tab[mid];
			if( ps->iGrPar == iGrPar )
			{
				i = 1;
				break;
			}
			if( ps->iGrPar < iGrPar )
				left = mid + 1;
			else
				right = mid - 1;
		}
		if( i )
		{
			// determino l'indice del parametro trovato
			RetVal = mid;
		}
	}

	return RetVal;
}

/*!
	\fn void userGetParCode( unsigned short iPar )
	\brief Visualizza il codice del parametro nella stringa 'str2'
	\param iPar Indice parametro
	\return None
*/
void userGetParCode( unsigned short iPar )
{
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];
	const PAR_GROUP *pGroup;
	short i;

	str2[0] = '\0';
	// ricerca gruppo
	for( i=0; i<NUM_GROUP_PAR; i++ )
	{
		pGroup = &Group_tab[i];
		if( (iPar >= pGroup->iParStart) && (iPar <= pGroup->iParEnd) )
		{
			// gruppo
			str2[0] = pGroup->cId;

			// indice
			i = pParTab->iGrPar;
			str2[1] = Hex2Ascii( i/100 );
			i %= 100;
			str2[2] = Hex2Ascii( i/10 );
			str2[3] = Hex2Ascii( i%10 );
			str2[4] = '\0';

			break;
		}
	}
}

/*!
** \fn void userGetPar( unsigned short iPar )
** \brief Get the parameter value into 'userParValue' variable
** \param iPar Parameter index
** \return None
**/
void userGetPar( unsigned short iPar )
{
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];
	unsigned char b, msk;
	CRONO_PROG_WEEK *prg = &Crono.Crono_ProgWeek[iCronoPrg];

	switch (pParTab->Type)
	{
		case TYPE_BOOL:
			userParValue.b[0] = GetBitBMap( (unsigned char *)pParTab->upPar.pPar, pParTab->Pos_Len);
		break;

		case TYPE_BYTE:
			if( iPar == P_SLEEP )
			{
				/*
				if( Sleep.Ore == SLEEP_OFF )
				{
					userParValue.b[0] = SLEEP_OFF_VALUE;	// sleep off
				}
				else
				{
					userParValue.b[0] = SLEEP_MIN_SET_VALUE;	// sleep attivo sull'ora corrente arrotondata alla decina di minuti superiore
				}
				*/
				userGetOraSleep();
			}
			else if( iPar == P_CRONO_P1_SETTIM_START )
			{
				userParValue.b[0] = prg->OraStart;
			}
			else if( iPar == P_CRONO_P1_SETTIM_STOP )
			{
				userParValue.b[0] = prg->OraStop;
			}
			else if( iPar == P_CRONO_P1_SETTIM_TEMP_ARIA )
			{
				userParValue.b[0] = prg->TempAria;
			}
			else if( iPar == P_CRONO_P1_SETTIM_TEMP_H2O )
			{
				userParValue.b[0] = prg->TempH2O;
			}
			else if( iPar == P_CRONO_P1_SETTIM_POTENZA )
			{
				userParValue.b[0] = prg->Fire;
			}
			else
			{
				userParValue.b[0] = *(unsigned char *)pParTab->upPar.pPar;
			}
		break;

		case TYPE_BYTE_MASK:
			if( (iPar == P_CRONO_P1_SETTIM)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_DOM)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_LUN)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_MAR)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_MER)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_GIO)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_VEN)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_SAB)
				)
			{
				b = prg->Enab.BYTE;
			}
			else
			{
				b = *(unsigned char *)pParTab->upPar.pPar;
			}
			msk = pParTab->Pos_Len;
			b &= msk;
			while( !(msk & 0x01) )
			{
				msk >>= 1;
				b >>= 1;
			}
			userParValue.b[0] = b;
		break;

		case TYPE_WORD:
			userParValue.w[0] = *(unsigned short *)pParTab->upPar.pPar;
		break;

		case TYPE_INTEGER:
			userParValue.i[0] = *(short *)pParTab->upPar.pPar;
		break;

		case TYPE_DWORD:
			userParValue.d = *(unsigned long *)pParTab->upPar.pPar;
		break;
	}
}

/*!
** \fn void userVisPar( unsigned short iPar )
** \brief Get the parameter value from the 'userParValue' variable 
** 		into the 'str2' string
** \param iPar Parameter index
** \return None
**/
void userVisPar( unsigned short iPar )
{
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];
	unsigned char b = 0;
	unsigned short w = 0;
	short i = 0;
	short k = 0;
	unsigned long d = 0;

	switch( pParTab->Type )
	{
		case TYPE_BOOL:
		case TYPE_BYTE_MASK:
		case TYPE_BYTE:
			// rilevo il valore correntemente impostato
			b = userParValue.b[0];
			w = b;
		break;

		case TYPE_WORD:
			// rilevo il valore correntemente impostato
			if( (iPar == P_ORESAT) && (userSubState == 1) )
			{
				// Ore Service in modifica: solo azzeramento
				userParValue.w[0] = 0;
			}
			w = userParValue.w[0];
		break;

		case TYPE_INTEGER:
			// rilevo il valore correntemente impostato
			i = userParValue.i[0];
		break;

		case TYPE_DWORD:
			// rilevo il valore correntemente impostato
			d = userParValue.d;
		break;
	}

	// Parametri numerici con formati particolari

	if( iPar == P_WIFI_IP )
	{
		// indirizzo IP
		b = Ip[0];
		str2[0] = Hex2Ascii( b/100 );
		b %= 100;
		str2[1] = Hex2Ascii( b/10 );
		str2[2] = Hex2Ascii( b%10 );
		str2[3] = '.';
		b = Ip[1];
		str2[4] = Hex2Ascii( b/100 );
		b %= 100;
		str2[5] = Hex2Ascii( b/10 );
		str2[6] = Hex2Ascii( b%10 );
		str2[7] = '.';
		b = Ip[2];
		str2[8] = Hex2Ascii( b/100 );
		b %= 100;
		str2[9] = Hex2Ascii( b/10 );
		str2[10] = Hex2Ascii( b%10 );
		str2[11] = '.';
		b = Ip[3];
		str2[12] = Hex2Ascii( b/100 );
		b %= 100;
		str2[13] = Hex2Ascii( b/10 );
		str2[14] = Hex2Ascii( b%10 );
		str2[15] = '\0';
	}
	else if( iPar == P_LINGUA )
	{
		// sigla internazionale
		strncpy( str2, (char *)vector_get( &SigleStati, w ), MSGDESCLEN );
		str2[MSGDESCLEN] = '\0';
	}
	else if( iPar == P_STATO_ATTUA )
	{
		// stato attuazioni
		GetMsg( 0, str2, iLanguage, sATTUASTAT00+StatoAttuazioni );
	}
	else if( iPar == P_SERVICE )
	{
		// converto il valore in stringa per la visualizzazione scorrevole
		memset( str1, 0, sizeof(str1) );
		b = 0;
		for( k=0; k<SERVNUM_SIZE; k++ )
		{
			if( ServNumber[k].BIT.CifraHigh == 0 )
				break;
			else
				str1[b++] = Hex2Ascii( ServNumber[k].BIT.CifraHigh - 1 );
			if( ServNumber[k].BIT.CifraLow == 0 )
				break;
			else
				str1[b++] = Hex2Ascii( ServNumber[k].BIT.CifraLow - 1 );
		}
		strcpy( str2, str1 );
	}
	else if( iPar == P_SLEEP )
	{
		if( userSubState == 1 )
		{
			// modifica
			if( userParValue.b[0] == SLEEP_OFF_VALUE )
			{
				GetMsg( 0, str2, iLanguage, sOFF );
			}
			else
			{
				userScriviOraSleep();
			}
		}
		else
		{
			// selezione
			if( Sleep.Ore == SLEEP_OFF )
			{
				GetMsg( 0, str2, iLanguage, sOFF );
			}
			else
			{
				str2[0] = Hex2Ascii( Sleep.Ore/10 );
				str2[1] = Hex2Ascii( Sleep.Ore%10 );
				str2[2] = ':';
				str2[3] = Hex2Ascii( Sleep.Minuti/10 );
				str2[4] = Hex2Ascii( Sleep.Minuti%10 );
				str2[5] = '\0';
			}
		}
	}
	else if( iPar == P_CRONO_PROFILO )
	{
		// stringa vuota, se profilo crono non impostato
		if( b == 0 )
		{
			str2[0] = '\0';
		}
		else
		{
			str2[0] = Hex2Ascii( b/10 );
			str2[1] = Hex2Ascii( b%10 );
			i = userDelLeadingZero( (unsigned char *)str2, 2 );
			strncpy( str1, &str2[i], 2-i );
			str1[2-i] = '\0';
			strcpy( str2, str1 );
		}
	}
	else if(
				(iPar == P_CRONO_P1_SETTIM_START) || (iPar == P_CRONO_P1_SETTIM_STOP)
	#ifdef menu_CRONO
				|| (iPar == P_CRONO_P2_SETTIM_START) || (iPar == P_CRONO_P2_SETTIM_STOP)
				|| (iPar == P_CRONO_P3_SETTIM_START) || (iPar == P_CRONO_P3_SETTIM_STOP)
				|| (iPar == P_CRONO_P4_SETTIM_START) || (iPar == P_CRONO_P4_SETTIM_STOP)
				|| (iPar == P_CRONO_P5_SETTIM_START) || (iPar == P_CRONO_P5_SETTIM_STOP)
				|| (iPar == P_CRONO_P6_SETTIM_START) || (iPar == P_CRONO_P6_SETTIM_STOP)
	#endif
			 )
	{
		if( w <= 96 )
		{
			// converto da quarti d'ora in hh:mm
			b = (BYTE)(w/4);
			str2[0] = Hex2Ascii( b/10 );
			str2[1] = Hex2Ascii( b%10 );
			str2[2] = ':';
			b = (BYTE)(w%4);
			b *= 15;
			str2[3] = Hex2Ascii( b/10 );
			str2[4] = Hex2Ascii( b%10 );
			i = userDelLeadingZero( (unsigned char *)str2, 2 );
			strncpy( str1, &str2[i], 5-i );
			str1[5-i] = '\0';
			strcpy( str2, str1 );
		}
		else
		{
			// stringa descrittiva: off.
			GetMsg( 0, str2, iLanguage, sOFF );
		}
	}
	else if( iPar == P_TIPOSTUFA )
	{
		strncpy( str1, pChTipoDesc[w], LENGTH_TYPEDESC );
		str1[LENGTH_TYPEDESC] = '\0';
		strcpy( str2, str1 );
	}
	else if(
				(iPar == P_SETP_SANITARI) ||
				(iPar == P_TEMPERATURAFUMI) ||
				(iPar == P_TEMPERATURE ) ||
				(iPar == P_SET_TRISC) ||
				(iPar == P_SET_TSANIT) ||
				(iPar == P_DELTATEMP) ||
				(iPar == P_TSETONPOMPA) ||
				(iPar == P_TSETOFFPOMPA) ||
				(iPar == P_TEMPAUX) ||
				(iPar == P_DELTA_SANITARI) ||
				(iPar == P_ISTERESI_TEMP_ACQUA) ||
				(iPar == P_DELTA_TACC_CALDO) ||
				(iPar == P_GRAD_PREALM_FUMI) ||
				(iPar == P_TFUMI_ON) ||
				(iPar == P_TEMP_SOGLIA_FUMI_LEGNA) ||
				(iPar == P_TFUMI_OFF) ||
				(iPar == P_TEMP_PREALM_FUMI) ||
				(iPar == P_TEMP_ALLARME_FUMI) ||
				(iPar == P_TFUMI_FAN_ON) ||
				(iPar == P_TFUMI_FAN_MIN) ||
				(iPar == P_DELTATEMP_NO_FIAMMA) ||
				(iPar == P_CRONO_P1_SETTIM_TEMP_ARIA) || (iPar == P_CRONO_P1_SETTIM_TEMP_H2O)
	#ifdef menu_CRONO
				|| (iPar == P_CRONO_P2_SETTIM_TEMP_ARIA) || (iPar == P_CRONO_P2_SETTIM_TEMP_H2O)
				|| (iPar == P_CRONO_P3_SETTIM_TEMP_ARIA) || (iPar == P_CRONO_P3_SETTIM_TEMP_H2O)
				|| (iPar == P_CRONO_P4_SETTIM_TEMP_ARIA) || (iPar == P_CRONO_P4_SETTIM_TEMP_H2O)
				|| (iPar == P_CRONO_P5_SETTIM_TEMP_ARIA) || (iPar == P_CRONO_P5_SETTIM_TEMP_H2O)
				|| (iPar == P_CRONO_P6_SETTIM_TEMP_ARIA) || (iPar == P_CRONO_P6_SETTIM_TEMP_H2O)
	#endif
			)
	{
		// il valore e' espresso in �C
		// formato di visualizzazione: ####�C / ####�F
		if( TEMPFAHRENHEIT )
		{
			// Converto la temperatura in Fahrenheit
			d = (DWORD)w * 18;	// in decimi di grado
			// per i parametri in temperatura differenziale non si deve sommare l'offset
			if(
				(iPar != P_DELTATEMP) &&
				(iPar != P_DELTA_SANITARI) &&
				(iPar != P_ISTERESI_TEMP_ACQUA) &&
				(iPar != P_DELTA_TACC_CALDO) &&
				(iPar != P_GRAD_PREALM_FUMI) &&
				(iPar != P_DELTATEMP_NO_FIAMMA)
				)
			{
				d += 320;
			}
			d += 5;	// arrotondamento
			d /= 10;	// in gradi
			if( d > 9999 )	// 4 cifre
				d = 9999;
			w = (WORD)d;
		}
		b = (BYTE)(w/1000);
		str2[0] = Hex2Ascii( b );
		w %= 1000;
		b = (BYTE)(w/100);
		str2[1] = Hex2Ascii( b );
		w %= 100;
		b = (BYTE)(w/10);
		str2[2] = Hex2Ascii( b );
		b = (BYTE)(w%10);
		str2[3] = Hex2Ascii( b );
		// valore in gradi
		str2[4] = cDEG;
		if( TEMPFAHRENHEIT )
			str2[5] = 'F';
		else
			str2[5] = 'C';
		i = userDelLeadingZero( (unsigned char *)str2, 4 );
		strncpy( str1, &str2[i], 6-i );
		str1[6-i] = '\0';
		strcpy( str2, str1 );
	}
	else if(
				(iPar == P_TEMP_AMB_PAN) ||
				(iPar == P_TEMPERATURAAMB) ||
				(iPar == P_TEMPERATURAH2O) ||
				(iPar == P_TEMP_P0) ||
				(iPar == P_TEMP_P1) ||
				(iPar == P_TEMP_P2) ||
				(iPar == P_TEMP_P3) ||
				(iPar == P_TEMP_P4)
			)
	{
		// formato di visualizzazione: +###.#�C / +####�F
		if( iPar != P_TEMP_AMB_PAN )
		{
			// il valore e' espresso in step 0.5�C
			i *= 5;	// converto in 0.1�C
		}
		// valore assoluto
		if( i >= 0 )
		{
			w = i;
			i = 1;
		}
		else
		{
			w = (-i);
			i = 0;
		}
		if( TEMPFAHRENHEIT )
		{
			// Converto la temperatura in Fahrenheit
			d = (DWORD)w * 18;	// in centesimi di grado
			d += 3200;
			// arrotondamento
			if( i )
				d += 50;
			else
				d -= 50;
			d /= 100;	// in gradi
			if( d > 9999 )	// 4 cifre
				d = 9999;
			w = (WORD)d;
			b = (BYTE)(w/1000);
			str2[0] = Hex2Ascii( b );
			w %= 1000;
			b = (BYTE)(w/100);
			str2[1] = Hex2Ascii( b );
			w %= 100;
			b = (BYTE)(w/10);
			str2[2] = Hex2Ascii( b );
			b = (BYTE)(w%10);
			str2[3] = Hex2Ascii( b );
			// valore in gradi
			str2[4] = cDEG;
			str2[5] = 'F';
			k = userDelLeadingZero( (unsigned char *)str2, 3 );
			strncpy( str1, &str2[k], 6-k );
			str1[6-k] = '\0';
		}
		else
		{
			b = (BYTE)(w/1000);
			str2[0] = Hex2Ascii( b );
			w %= 1000;
			b = (BYTE)(w/100);
			str2[1] = Hex2Ascii( b );
			w %= 100;
			b = (BYTE)(w/10);
			str2[2] = Hex2Ascii( b );
			str2[3] = '.';
			b = (BYTE)(w%10);
			str2[4] = Hex2Ascii( b );
			// valore in gradi
			str2[5] = cDEG;
				str2[6] = 'C';
			k = userDelLeadingZero( (unsigned char *)str2, 3 );
			strncpy( str1, &str2[k], 7-k );
			str1[7-k] = '\0';
		}
		// valore con segno
		if( i )
			str2[0] = cBLANK;
		else
			str2[0] = '-';
		str2[1] = '\0';
		strcat( str2, str1 );
	}
	else if( iPar == P_CODICE_SCHEDA )
	{
		str2[0] = Hex2Ascii( gBySchedaCod/10 );
		str2[1] = Hex2Ascii( gBySchedaCod%10 );
		str2[2] = Hex2Ascii( gBySchedaVers/10 );
		str2[3] = Hex2Ascii( gBySchedaVers%10 );
		str2[4] = Hex2Ascii( gBySchedaRev/10 );
		str2[5] = Hex2Ascii( gBySchedaRev%10 );
		str2[6] = '_';
		str2[7] = Hex2Ascii( gBySchedaBuild/10 );
		str2[8] = Hex2Ascii( gBySchedaBuild%10 );
		str2[9] = '\0';
	}
	else if( iPar == P_CODICE_SICUREZZA)
	{
		str2[0] = Hex2Ascii( gBySicurezzaCod/10 );
		str2[1] = Hex2Ascii( gBySicurezzaCod%10 );
		str2[2] = Hex2Ascii( gBySicurezzaVers/10 );
		str2[3] = Hex2Ascii( gBySicurezzaVers%10 );
		str2[4] = Hex2Ascii( gBySicurezzaRev/10 );
		str2[5] = Hex2Ascii( gBySicurezzaRev%10 );
		str2[6] = '_';
		str2[7] = Hex2Ascii( gBySicurezzaBuild/10 );
		str2[8] = Hex2Ascii( gBySicurezzaBuild%10 );
		str2[9] = '\0';
	}
	else if( iPar == P_CODICE_PANNELLO)
	{
		/* str2[0] = Hex2Ascii(codSW/10);
		str2[1] = Hex2Ascii(codSW%10);
		str2[2] = Hex2Ascii(verSW/10);
		str2[3] = Hex2Ascii(verSW%10);
		str2[4] = Hex2Ascii(revSW/10);
		str2[5] = Hex2Ascii(revSW%10);
		str2[6] = '_';
		str2[7] = Hex2Ascii( buildSW/10 );
		str2[8] = Hex2Ascii( buildSW%10 );
		str2[9] = '\0'; */
		strcpy( str2, sFW_VERSIONE );
	}
	else if( iPar == P_CODICE_DRIVER)
	{
		str2[0] = Hex2Ascii(DriverCod/10);
		str2[1] = Hex2Ascii(DriverCod%10);
		str2[2] = Hex2Ascii(DriverVers/10);
		str2[3] = Hex2Ascii(DriverVers%10);
		str2[4] = Hex2Ascii(DriverRev/10);
		str2[5] = Hex2Ascii(DriverRev%10);
		str2[6] = '_';
		str2[7] = Hex2Ascii( DriverBuild/10 );
		str2[8] = Hex2Ascii( DriverBuild%10 );
		str2[9] = '\0';
	}
	else if( iPar == P_CODICE_PARAM)
	{
		str2[0] = Hex2Ascii( (ParamCode / 0x1000) & 0x000F );
		str2[1] = Hex2Ascii( (ParamCode / 0x100) & 0x000F );
		str2[2] = Hex2Ascii( (ParamCode / 0x10) & 0x000F );
		str2[3] = Hex2Ascii( ParamCode & 0x000F );
		str2[4] = Hex2Ascii( (ParamRel / 0x1000) & 0x000F );
		str2[5] = Hex2Ascii( (ParamRel / 0x100) & 0x000F );
		str2[6] = Hex2Ascii( (ParamRel / 0x10) & 0x000F );
		str2[7] = Hex2Ascii( ParamRel & 0x000F );
		str2[8] = '\0';
	}
	else if( (iPar == P_PRESSIONEIDRO) || (iPar == P_MAXPRESSH2O) )
	{
		// il valore e` espresso in 0.1 bar
		// formato di visualizzazione: #.# Bar
		str2[0] = Hex2Ascii( b/10 );
		str2[1] = '.';
		str2[2] = Hex2Ascii( b%10 );
		str2[3] = cBLANK;
		str2[4] = 'B';
		str2[5] = 'a';
		str2[6] = 'r';
		str2[7] = '\0';
	}
	else if( iPar == P_K_SONDA )
	{
		// il valore e` espresso in 0.5
		// formato di visualizzazione: #.#
		w *= 5;	// converto in 0.1
		b = (BYTE)(w/10);
		str2[0] = Hex2Ascii( b );
		str2[1] = '.';
		b = (BYTE)(w%10);
		str2[2] = Hex2Ascii( b );
		str2[3] = '\0';
	}
	else if( (iPar == P_OSSIGENO) || (iPar == P_OSSIGENO_RESIDUO) )
	{
		// il valore e` espresso in 0.5 %
		// formato di visualizzazione: ###.# %
		w *= 5;	// converto in 0.1 %
		b = (BYTE)(w/1000);
		str2[0] = Hex2Ascii( b );
		w %= 1000;
		b = (BYTE)(w/100);
		str2[1] = Hex2Ascii( b );
		w %= 100;
		b = (BYTE)(w/10);
		str2[2] = Hex2Ascii( b );
		str2[3] = '.';
		b = (BYTE)(w%10);
		str2[4] = Hex2Ascii( b );
		str2[5] = cBLANK;
		str2[6] = '%';
		k = userDelLeadingZero( (unsigned char *)str2, 3 );
		strncpy( str1, &str2[k], 7-k );
		str1[7-k] = '\0';
		strcpy( str2, str1 );
	}
	else if( iPar == P_CONSUMO )
	{
		// il valore e` espresso in 0.05 kg/h
		// formato di visualizzazione: ####.# kg/h
		w /= 2;	// converto in 0.1 kg/h
		b = (BYTE)(w/10000);
		str2[0] = Hex2Ascii( b );
		w %= 10000;
		b = (BYTE)(w/1000);
		str2[1] = Hex2Ascii( b );
		w %= 1000;
		b = (BYTE)(w/100);
		str2[2] = Hex2Ascii( b );
		w %= 100;
		b = (BYTE)(w/10);
		str2[3] = Hex2Ascii( b );
		str2[4] = '.';
		b = (BYTE)(w%10);
		str2[5] = Hex2Ascii( b );
		str2[6] = cBLANK;
		str2[7] = 'k';
		str2[8] = 'g';
		str2[9] = '/';
		str2[10] = 'h';
		k = userDelLeadingZero( (unsigned char *)str2, 4 );
		strncpy( str1, &str2[k], 11-k );
		str1[11-k] = '\0';
		strcpy( str2, str1 );
	}
	else if( iPar == P_ACC_CONSUMO )
	{
		// il valore e` espresso in 0.1 g
		// formato di visualizzazione: ######.# kg
		d = (d + 500)/1000;	// arrotondo a 0.1 kg
		b = (BYTE)(d/1000000);
		str2[0] = Hex2Ascii( b );
		d %= 1000000;
		b = (BYTE)(d/100000);
		str2[1] = Hex2Ascii( b );
		d %= 100000;
		b = (BYTE)(d/10000);
		str2[2] = Hex2Ascii( b );
		d %= 10000;
		b = (BYTE)(d/1000);
		str2[3] = Hex2Ascii( b );
		d %= 1000;
		b = (BYTE)(d/100);
		str2[4] = Hex2Ascii( b );
		d %= 100;
		b = (BYTE)(d/10);
		str2[5] = Hex2Ascii( b );
		str2[6] = '.';
		b = (BYTE)(d%10);
		str2[7] = Hex2Ascii( b );
		str2[8] = cBLANK;
		str2[9] = 'k';
		str2[10] = 'g';
		k = userDelLeadingZero( (unsigned char *)str2, 6 );
		strncpy( str1, &str2[k], 11-k );
		str1[11-k] = '\0';
		strcpy( str2, str1 );
	}
	else if(
				(iPar == P_PORTATA_SET) ||
				(iPar == P_PORTATAARIAMISURATA) ||
				(iPar == P_SPE_PORTATA) ||
				(iPar == P_SPEA_PORTATA) ||
				(iPar == P_SPEB_PORTATA) ||
				(iPar == P_RAFFA_PORTATA) ||
				(iPar == P_PULIZIA_PORTATA) ||
				(iPar == P_SPEC_PORTATA) ||
				(iPar == P_RAFFB_PORTATA) ||
				(iPar == P_PULIZIAB_PORTATA) ||
				(iPar == P_PREACC1_PORTATA) ||
				(iPar == P_PREACC2_PORTATA) ||
				(iPar == P_PREACCCALDO_PORTATA) ||
				(iPar == P_ACCA_PORTATA) ||
				(iPar == P_ACCB_PORTATA) ||
				(iPar == P_FIREONA_PORTATA) ||
				(iPar == P_FIREONB_PORTATA) ||
				(iPar == P_ACCC_PORTATA) ||
				(iPar == P_POT1_PORTATA) ||
				(iPar == P_POT2_PORTATA) ||
				(iPar == P_POT3_PORTATA) ||
				(iPar == P_POT4_PORTATA) ||
				(iPar == P_POT5_PORTATA) ||
				(iPar == P_POT6_PORTATA) ||
				(iPar == P_POT7_PORTATA) ||
				(iPar == P_POT8_PORTATA) ||
				(iPar == P_PORTATA_CRITICA) ||
				(iPar == P_DELTA_PORTATA)
			)
	{
		// formato di visualizzazione: ### lpm
		b = (BYTE)(w/100);
		str2[0] = Hex2Ascii( b );
		w %= 100;
		b = (BYTE)(w/10);
		str2[1] = Hex2Ascii( b );
		b = (BYTE)(w%10);
		str2[2] = Hex2Ascii( b );
		str2[3] = cBLANK;
		str2[4] = 'l';
		str2[5] = 'p';
		str2[6] = 'm';
		i = userDelLeadingZero( (unsigned char *)str2, 3 );
		strncpy( str1, &str2[i], 7-i );
		str1[7-i] = '\0';
		strcpy( str2, str1 );
	}
	else if(
				(iPar == P_TSHUTDOWN_ECO) ||
				(iPar == P_TSWITCHON_ECO) ||
				(iPar == P_SCUOT_PERIODO)
			)
	{
		// formato di visualizzazione: ##### min
		b = (BYTE)(w/10000);
		str2[0] = Hex2Ascii( b );
		w %= 10000;
		b = (BYTE)(w/1000);
		str2[1] = Hex2Ascii( b );
		w %= 1000;
		b = (BYTE)(w/100);
		str2[2] = Hex2Ascii( b );
		w %= 100;
		b = (BYTE)(w/10);
		str2[3] = Hex2Ascii( b );
		b = (BYTE)(w%10);
		str2[4] = Hex2Ascii( b );
		str2[5] = cBLANK;
		str2[6] = 'm';
		str2[7] = 'i';
		str2[8] = 'n';
		i = userDelLeadingZero( (unsigned char *)str2, 5 );
		strncpy( str1, &str2[i], 9-i );
		str1[9-i] = '\0';
		strcpy( str2, str1 );
	}
	else if(
				(iPar == P_GIRI_SET) ||
				(iPar == P_GIRI2_SET) ||
				(iPar == P_VENTILATOREFUMI) ||
				(iPar == P_GIRI2) ||
				(iPar == P_SPE_GIRI_COCLEA) ||
				(iPar == P_SPE_GIRI) ||
				(iPar == P_SPE_GIRI2) ||
				(iPar == P_SPEA_GIRI_COCLEA) ||
				(iPar == P_SPEA_GIRI) ||
				(iPar == P_SPEA_GIRI2) ||
				(iPar == P_SPEB_GIRI_COCLEA) ||
				(iPar == P_SPEB_GIRI) ||
				(iPar == P_SPEB_GIRI2) ||
				(iPar == P_RAFFA_GIRI_COCLEA) ||
				(iPar == P_RAFFA_GIRI) ||
				(iPar == P_RAFFA_GIRI2) ||
				(iPar == P_PULIZIA_GIRI_COCLEA) ||
				(iPar == P_PULIZIA_GIRI) ||
				(iPar == P_PULIZIA_GIRI2) ||
				(iPar == P_SPEC_GIRI_COCLEA) ||
				(iPar == P_SPEC_GIRI) ||
				(iPar == P_SPEC_GIRI2) ||
				(iPar == P_RAFFB_GIRI_COCLEA) ||
				(iPar == P_RAFFB_GIRI) ||
				(iPar == P_RAFFB_GIRI2) ||
				(iPar == P_PULIZIAB_GIRI_COCLEA) ||
				(iPar == P_PULIZIAB_GIRI) ||
				(iPar == P_PULIZIAB_GIRI2) ||
				(iPar == P_PREACC1_GIRI_COCLEA) ||
				(iPar == P_PREACC1_GIRI) ||
				(iPar == P_PREACC1_GIRI2) ||
				(iPar == P_PREACC2_GIRI_COCLEA) ||
				(iPar == P_PREACC2_GIRI) ||
				(iPar == P_PREACC2_GIRI2) ||
				(iPar == P_PREACCCALDO_GIRI_COCLEA) ||
				(iPar == P_PREACCCALDO_GIRI) ||
				(iPar == P_PREACCCALDO_GIRI2) ||
				(iPar == P_ACCA_GIRI_COCLEA) ||
				(iPar == P_ACCA_GIRI) ||
				(iPar == P_ACCA_GIRI2) ||
				(iPar == P_ACCB_GIRI_COCLEA) ||
				(iPar == P_ACCB_GIRI) ||
				(iPar == P_ACCB_GIRI2) ||
				(iPar == P_FIREONA_GIRI_COCLEA) ||
				(iPar == P_FIREONA_GIRI) ||
				(iPar == P_FIREONA_GIRI2) ||
				(iPar == P_FIREONB_GIRI_COCLEA) ||
				(iPar == P_FIREONB_GIRI) ||
				(iPar == P_FIREONB_GIRI2) ||
				(iPar == P_ACCC_GIRI_COCLEA) ||
				(iPar == P_ACCC_GIRI) ||
				(iPar == P_ACCC_GIRI2) ||
				(iPar == P_POT1_GIRI_COCLEA) ||
				(iPar == P_POT1_GIRI) ||
				(iPar == P_POT1_GIRI2) ||
				(iPar == P_POT2_GIRI_COCLEA) ||
				(iPar == P_POT2_GIRI) ||
				(iPar == P_POT2_GIRI2) ||
				(iPar == P_POT3_GIRI_COCLEA) ||
				(iPar == P_POT3_GIRI) ||
				(iPar == P_POT3_GIRI2) ||
				(iPar == P_POT4_GIRI_COCLEA) ||
				(iPar == P_POT4_GIRI) ||
				(iPar == P_POT4_GIRI2) ||
				(iPar == P_POT5_GIRI_COCLEA) ||
				(iPar == P_POT5_GIRI) ||
				(iPar == P_POT5_GIRI2) ||
				(iPar == P_POT6_GIRI_COCLEA) ||
				(iPar == P_POT6_GIRI) ||
				(iPar == P_POT6_GIRI2) ||
				(iPar == P_POT7_GIRI_COCLEA) ||
				(iPar == P_POT7_GIRI) ||
				(iPar == P_POT7_GIRI2) ||
				(iPar == P_POT8_GIRI_COCLEA) ||
				(iPar == P_POT8_GIRI) ||
				(iPar == P_POT8_GIRI2)
			)
	{
		// formato di visualizzazione: #### rpm
		b = (BYTE)(w/1000);
		str2[0] = Hex2Ascii( b );
		w %= 1000;
		b = (BYTE)(w/100);
		str2[1] = Hex2Ascii( b );
		w %= 100;
		b = (BYTE)(w/10);
		str2[2] = Hex2Ascii( b );
		b = (BYTE)(w%10);
		str2[3] = Hex2Ascii( b );
		str2[4] = cBLANK;
		str2[5] = 'r';
		str2[6] = 'p';
		str2[7] = 'm';
		i = userDelLeadingZero( (unsigned char *)str2, 4 );
		strncpy( str1, &str2[i], 8-i );
		str1[8-i] = '\0';
		strcpy( str2, str1 );
	}
	else if(
				(iPar == P_PERIODO_COCLEA) ||
				(iPar == P_PERIODO_FAN1) ||
				(iPar == P_TEMPOCOCLEAON_SET) ||
				(iPar == P_TEMPOCOCLEAOFF_SET) ||
				(iPar == P_TEMPOFAN1ON_SET) ||
				(iPar == P_TEMPOFAN1OFF_SET) ||
				(iPar == P_SPE_TON_COCLEA) ||
				(iPar == P_SPE_TOFF_COCLEA) ||
				(iPar == P_SPE_TON_FAN1) ||
				(iPar == P_SPE_TOFF_FAN1) ||
				(iPar == P_SPEA_TON_COCLEA) ||
				(iPar == P_SPEA_TOFF_COCLEA) ||
				(iPar == P_SPEA_TON_FAN1) ||
				(iPar == P_SPEA_TOFF_FAN1) ||
				(iPar == P_SPEB_TON_COCLEA) ||
				(iPar == P_SPEB_TOFF_COCLEA) ||
				(iPar == P_SPEB_TON_FAN1) ||
				(iPar == P_SPEB_TOFF_FAN1) ||
				(iPar == P_RAFFA_TON_COCLEA) ||
				(iPar == P_RAFFA_TOFF_COCLEA) ||
				(iPar == P_RAFFA_TON_FAN1) ||
				(iPar == P_RAFFA_TOFF_FAN1) ||
				(iPar == P_PULIZIA_TON_COCLEA) ||
				(iPar == P_PULIZIA_TOFF_COCLEA) ||
				(iPar == P_PULIZIA_TON_FAN1) ||
				(iPar == P_PULIZIA_TOFF_FAN1) ||
				(iPar == P_SPEC_TON_COCLEA) ||
				(iPar == P_SPEC_TOFF_COCLEA) ||
				(iPar == P_SPEC_TON_FAN1) ||
				(iPar == P_SPEC_TOFF_FAN1) ||
				(iPar == P_RAFFB_TON_COCLEA) ||
				(iPar == P_RAFFB_TOFF_COCLEA) ||
				(iPar == P_RAFFB_TON_FAN1) ||
				(iPar == P_RAFFB_TOFF_FAN1) ||
				(iPar == P_PULIZIAB_TON_COCLEA) ||
				(iPar == P_PULIZIAB_TOFF_COCLEA) ||
				(iPar == P_PULIZIAB_TON_FAN1) ||
				(iPar == P_PULIZIAB_TOFF_FAN1) ||
				(iPar == P_PREACC1_TON_COCLEA) ||
				(iPar == P_PREACC1_TOFF_COCLEA) ||
				(iPar == P_PREACC1_TON_FAN1) ||
				(iPar == P_PREACC1_TOFF_FAN1) ||
				(iPar == P_PREACC2_TON_COCLEA) ||
				(iPar == P_PREACC2_TOFF_COCLEA) ||
				(iPar == P_PREACC2_TON_FAN1) ||
				(iPar == P_PREACC2_TOFF_FAN1) ||
				(iPar == P_PREACCCALDO_TON_COCLEA) ||
				(iPar == P_PREACCCALDO_TOFF_COCLEA) ||
				(iPar == P_PREACCCALDO_TON_FAN1) ||
				(iPar == P_PREACCCALDO_TOFF_FAN1) ||
				(iPar == P_ACCA_TON_COCLEA) ||
				(iPar == P_ACCA_TOFF_COCLEA) ||
				(iPar == P_ACCA_TON_FAN1) ||
				(iPar == P_ACCA_TOFF_FAN1) ||
				(iPar == P_ACCB_TON_COCLEA) ||
				(iPar == P_ACCB_TOFF_COCLEA) ||
				(iPar == P_ACCB_TON_FAN1) ||
				(iPar == P_ACCB_TOFF_FAN1) ||
				(iPar == P_FIREONA_TON_COCLEA) ||
				(iPar == P_FIREONA_TOFF_COCLEA) ||
				(iPar == P_FIREONA_TON_FAN1) ||
				(iPar == P_FIREONA_TOFF_FAN1) ||
				(iPar == P_FIREONB_TON_COCLEA) ||
				(iPar == P_FIREONB_TOFF_COCLEA) ||
				(iPar == P_FIREONB_TON_FAN1) ||
				(iPar == P_FIREONB_TOFF_FAN1) ||
				(iPar == P_ACCC_TON_COCLEA) ||
				(iPar == P_ACCC_TOFF_COCLEA) ||
				(iPar == P_ACCC_TON_FAN1) ||
				(iPar == P_ACCC_TOFF_FAN1) ||
				(iPar == P_POT1_TON_COCLEA) ||
				(iPar == P_POT1_TOFF_COCLEA) ||
				(iPar == P_POT1_TON_FAN1) ||
				(iPar == P_POT1_TOFF_FAN1) ||
				(iPar == P_POT2_TON_COCLEA) ||
				(iPar == P_POT2_TOFF_COCLEA) ||
				(iPar == P_POT2_TON_FAN1) ||
				(iPar == P_POT2_TOFF_FAN1) ||
				(iPar == P_POT3_TON_COCLEA) ||
				(iPar == P_POT3_TOFF_COCLEA) ||
				(iPar == P_POT3_TON_FAN1) ||
				(iPar == P_POT3_TOFF_FAN1) ||
				(iPar == P_POT4_TON_COCLEA) ||
				(iPar == P_POT4_TOFF_COCLEA) ||
				(iPar == P_POT4_TON_FAN1) ||
				(iPar == P_POT4_TOFF_FAN1) ||
				(iPar == P_POT5_TON_COCLEA) ||
				(iPar == P_POT5_TOFF_COCLEA) ||
				(iPar == P_POT5_TON_FAN1) ||
				(iPar == P_POT5_TOFF_FAN1) ||
				(iPar == P_POT6_TON_COCLEA) ||
				(iPar == P_POT6_TOFF_COCLEA) ||
				(iPar == P_POT6_TON_FAN1) ||
				(iPar == P_POT6_TOFF_FAN1) ||
				(iPar == P_POT7_TON_COCLEA) ||
				(iPar == P_POT7_TOFF_COCLEA) ||
				(iPar == P_POT7_TON_FAN1) ||
				(iPar == P_POT7_TOFF_FAN1) ||
				(iPar == P_POT8_TON_COCLEA) ||
				(iPar == P_POT8_TOFF_COCLEA) ||
				(iPar == P_POT8_TON_FAN1) ||
				(iPar == P_POT8_TOFF_FAN1)
			)
	{
		// il valore e' espresso in step di 50ms
		// formato di visualizzazione: ####.# s
		w /= 2;	// converto in 0.1s
		b = (BYTE)(w/10000);
		str2[0] = Hex2Ascii( b );
		w %= 10000;
		b = (BYTE)(w/1000);
		str2[1] = Hex2Ascii( b );
		w %= 1000;
		b = (BYTE)(w/100);
		str2[2] = Hex2Ascii( b );
		w %= 100;
		b = (BYTE)(w/10);
		str2[3] = Hex2Ascii( b );
		str2[4] = '.';
		b = (BYTE)(w%10);
		str2[5] = Hex2Ascii( b );
		str2[6] = cBLANK;
		str2[7] = 's';
		i = userDelLeadingZero( (unsigned char *)str2, 4 );
		strncpy( str1, &str2[i], 8-i );
		str1[8-i] = '\0';
		strcpy( str2, str1 );
	}
	else if(
				(iPar == P_MAX_TIME_PREALM_FUMI) ||
				(iPar == P_TIME_PREALM_SENS_PELLET) ||
				(iPar == P_TEMPO_PREALM_ARIA_COMBUST) ||
				(iPar == P_TEMPO_ALLARME_PORTA) ||
				(iPar == P_TEMPO_PREALM_PORTA) ||
				(iPar == P_TRIPRI_BLACKOUT) ||
				(iPar == P_TPREACC) ||
				(iPar == P_TPREACC2) ||
				(iPar == P_TPRELOAD) ||
				(iPar == P_TEMPO_RAFFA) ||
				(iPar == P_MAX_TWARMUP) ||
				(iPar == P_MAX_TFIREON_A) ||
				(iPar == P_TIME_SPEA) ||
				(iPar == P_DELAY_INC_ESP) ||
				(iPar == P_DELAY_DEC_ESP) ||
				(iPar == P_DELAY_INC_STEP_POTENZA) ||
				(iPar == P_DELAY_DEC_STEP_POTENZA) ||
				(iPar == P_TPB) ||
				(iPar == P_TWAIT_PB) ||
				(iPar == P_TACC_B) ||
				(iPar == P_TACC_C) ||
				(iPar == P_TFON_B) ||
				(iPar == P_TSPE_B) ||
				(iPar == P_TSPE_C) ||
				(iPar == P_TRAFF_B) ||
				(iPar == P_TPB_B) ||
				(iPar == P_SCUOT_DURATA) ||
				(iPar == P_BACKLIGHT) ||
				(iPar == P_DURATA_BLACKOUT)
			)
	{
		// il valore e' espresso in sec
		// formato di visualizzazione: ##### s
		if( iPar == P_BACKLIGHT )
		{
			// parametro locale in step di 10s
			w *= 10;
		}
		// valore su 5 cifre
		if( (iPar == P_BACKLIGHT) && (w == 0) )
		{
			GetMsg( 0, str1, iLanguage, sON );
		}
		else
		{
			// valore in secondi
			b = (BYTE)(w/10000);
			str2[0] = Hex2Ascii( b );
			w %= 10000;
			b = (BYTE)(w/1000);
			str2[1] = Hex2Ascii( b );
			w %= 1000;
			b = (BYTE)(w/100);
			str2[2] = Hex2Ascii( b );
			w %= 100;
			b = (BYTE)(w/10);
			str2[3] = Hex2Ascii( b );
			b = (BYTE)(w%10);
			str2[4] = Hex2Ascii( b );
			str2[5] = cBLANK;
			str2[6] = 's';
			i = userDelLeadingZero( (unsigned char *)str2, 5 );
			strncpy( str1, &str2[i], 7-i );
			str1[7-i] = '\0';
		}
		strcpy( str2, str1 );
	}
	else if(
				//(iPar == P_ORESAT_LIMITE) ||
				(iPar == P_ORESAT) ||
				(iPar == P_OREFUNZIONAMENTO)
			)
	{
		// il valore e' espresso in h
		// formato di visualizzazione: ######## h
		if( iPar != P_OREFUNZIONAMENTO )
			d = w;
		if( d > 99999999L )	// 8 cifre
			d = 99999999L;
		b = (BYTE)(d/10000000L);
		str2[0] = Hex2Ascii( b );
		d %= 10000000L;
		b = (BYTE)(d/1000000L);
		str2[1] = Hex2Ascii( b );
		d %= 1000000L;
		b = (BYTE)(d/100000L);
		str2[2] = Hex2Ascii( b );
		d %= 100000L;
		b = (BYTE)(d/10000);
		str2[3] = Hex2Ascii( b );
		w = d % 10000;
		b = (BYTE)(w/1000);
		str2[4] = Hex2Ascii( b );
		w %= 1000;
		b = (BYTE)(w/100);
		str2[5] = Hex2Ascii( b );
		w %= 100;
		b = (BYTE)(w/10);
		str2[6] = Hex2Ascii( b );
		b = (BYTE)(w%10);
		str2[7] = Hex2Ascii( b );
		str2[8] = cBLANK;
		str2[9] = 'h';
		i = userDelLeadingZero( (unsigned char *)str2, 8 );
		strncpy( str1, &str2[i], 10-i );
		str1[10-i] = '\0';
		strcpy( str2, str1 );
	}
	else
	{
		if( pParTab->iStrVal == NULL )
		{
			// Parametri numerici senza stringa: anno, mese, data, ore, minuti, tipo stufa, ...
			// formato di visualizzazione: +#####
			if( pParTab->Type == TYPE_INTEGER )
			{
				// valore assoluto
				if( i >= 0 )
				{
					w = i;
					i = 1;
				}
				else
				{
					w = (-i);
					i = 0;
				}
			}
			b = (BYTE)(w/10000);
			str2[0] = Hex2Ascii( b );
			w %= 10000;
			b = (BYTE)(w/1000);
			str2[1] = Hex2Ascii( b );
			w %= 1000;
			b = (BYTE)(w/100);
			str2[2] = Hex2Ascii( b );
			w %= 100;
			b = (BYTE)(w/10);
			str2[3] = Hex2Ascii( b );
			b = (BYTE)(w%10);
			str2[4] = Hex2Ascii( b );
			k = userDelLeadingZero( (unsigned char *)str2, 5 );
			strncpy( str1, &str2[k], 5-k );
			str1[5-k] = '\0';
			// eventuale valore con segno
			if( !i && (pParTab->Type == TYPE_INTEGER) )
			{
				str2[0] = '-';
				str2[1] = '\0';
				strcat( str2, str1 );
			}
			else
			{
				strcpy( str2, str1 );
			}
		}
		else
		{
			// Parametri con stringa descrittiva: on/off, giorni, lingua, ...
			if( pParTab->Type == TYPE_INTEGER )
			{
				if( i >= 0 )
					w = i;
				else
					w = 0;
			}
			GetMsg( 0, str2, iLanguage, pParTab->iStrVal[w] );
		}
	}
}

/*!
** \fn void userVisEPar( unsigned short iPar )
** \brief Get the parameter long string value into the 'strPar' string
** \param iPar Parameter index
** \return None
**/
void userVisEPar( unsigned short iPar )
{
	memset( strPar, 0, TEXT_MAX_LEN+1 );
	
	if( iPar == P_WIFI_SSID )
	{
		strncpy( strPar, Ssid, SSID_SIZE );
		strPar[SSID_SIZE] = '\0';
		maxLen = SSID_SIZE;
	}
	else if( iPar == P_WIFI_KEYW )
	{
		strncpy( strPar, CryptoKey, KEYW_SIZE );
		strPar[KEYW_SIZE] = '\0';
		maxLen = KEYW_SIZE;
	}
	else if( iPar == P_WPS_PIN )
	{
		strncpy( strPar, WPSPin, WPS_PIN_SIZE );
		strPar[WPS_PIN_SIZE] = '\0';
		maxLen = WPS_PIN_SIZE;
	}
	else if( iPar == P_WIFI_PORT )
	{
		userGetPar( P_WIFI_PORT );
		userVisPar( P_WIFI_PORT );
		strcpy( strPar, str2 );
		strPar[TCPPORT_SIZE] = '\0';
		maxLen = TCPPORT_SIZE;
	}
	else
	{
		maxLen = TEXT_MAX_LEN;
	}

}

/*!
** \fn void userIncPar( unsigned short iPar, unsigned char Ricirc )
** \brief Increment the parameter value into the 'userParValue' variable
** \param iPar Parameter index
** \param Ricirc 0: without roll-off, 1: with roll-off
** \return None
**/
void userIncPar( unsigned short iPar, unsigned char Ricirc )
{
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];
	BYTE_UNION vMax;
	BYTE_UNION vMin;
	unsigned char step;

	userGetMinMaxStep( iPar, &vMin, &vMax, &step );

	switch( iPar )
	{
		case P_LINGUA:
			vMax.b[0] = NumLanguage - 1;
		break;

		case P_CRONO_P1_SETTIM_STOP:
	#ifdef menu_CRONO
		case P_CRONO_P2_SETTIM_STOP:
		case P_CRONO_P3_SETTIM_STOP:
		case P_CRONO_P4_SETTIM_STOP:
		case P_CRONO_P5_SETTIM_STOP:
		case P_CRONO_P6_SETTIM_STOP:
	#endif
		case P_CRONO_P1_SETTIM_START:
	#ifdef menu_CRONO
		case P_CRONO_P2_SETTIM_START:
		case P_CRONO_P3_SETTIM_START:
		case P_CRONO_P4_SETTIM_START:
		case P_CRONO_P5_SETTIM_START:
		case P_CRONO_P6_SETTIM_START:
	#endif
			// no ricircolo
			Ricirc = 0;
		break;

		case P_SLEEP:
			if ( (userParValue.b[0] < SLEEP_MAX_SET_VALUE) || (userParValue.b[0] == SLEEP_OFF_VALUE) )
			{
				if (userParValue.b[0] == SLEEP_OFF_VALUE)
					userParValue.b[0] = SLEEP_MIN_SET_VALUE;
				else
					userParValue.b[0]++;
			}
		return;
	}

	switch( pParTab->Type )
	{
		case TYPE_BOOL:
		case TYPE_BYTE_MASK:
		case TYPE_BYTE:
			if( (short)userParValue.b[0] < (short)vMax.b[0]-step )
				userParValue.b[0] += step;
			else if( userParValue.b[0] < vMax.b[0] )
				userParValue.b[0] = vMax.b[0];
			else if( Ricirc )
				userParValue.b[0] = vMin.b[0];
			else
				userParValue.b[0] = vMax.b[0];
		break;

		case TYPE_WORD:
			if( (long)userParValue.w[0] < (long)vMax.w[0]-step )
				userParValue.w[0] += step;
			else if( userParValue.w[0] < vMax.w[0] )
				userParValue.w[0] = vMax.w[0];
			else if( Ricirc )
				userParValue.w[0] = vMin.w[0];
			else
				userParValue.w[0] = vMax.w[0];
		break;

		case TYPE_INTEGER:
			if( (long)userParValue.i[0] < (long)vMax.i[0]-step )
				userParValue.i[0] += step;
			else if( userParValue.i[0] < vMax.i[0] )
				userParValue.i[0] = vMax.i[0];
			else if( Ricirc )
				userParValue.i[0] = vMin.i[0];
			else
				userParValue.i[0] = vMax.i[0];
		break;
	}
}

/*!
** \fn void userDecPar( unsigned short iPar, unsigned char Ricirc )
** \brief Decrement the parameter value into the 'userParValue' variable
** \param iPar Parameter index
** \param Ricirc 0: without roll-off, 1: with roll-off
** \return None
**/
void userDecPar( unsigned short iPar, unsigned char Ricirc )
{
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];
	BYTE_UNION vMax;
	BYTE_UNION vMin;
	unsigned char step;

	userGetMinMaxStep( iPar, &vMin, &vMax, &step );

	switch( iPar )
	{
		case P_LINGUA:
			vMax.b[0] = NumLanguage - 1;
		break;

		case P_CRONO_P1_SETTIM_STOP:
	#ifdef menu_CRONO
		case P_CRONO_P2_SETTIM_STOP:
		case P_CRONO_P3_SETTIM_STOP:
		case P_CRONO_P4_SETTIM_STOP:
		case P_CRONO_P5_SETTIM_STOP:
		case P_CRONO_P6_SETTIM_STOP:
	#endif
			/* In modifica il limite inferiore dell'ora di stop e` uguale
				all'ora di start del programma crono indicato da 'iCronoPrg' */
			vMin.b[0] = Crono.Crono_ProgWeek[iCronoPrg].OraStart;
		case P_CRONO_P1_SETTIM_START:
	#ifdef menu_CRONO
		case P_CRONO_P2_SETTIM_START:
		case P_CRONO_P3_SETTIM_START:
		case P_CRONO_P4_SETTIM_START:
		case P_CRONO_P5_SETTIM_START:
		case P_CRONO_P6_SETTIM_START:
	#endif
			// no ricircolo
			Ricirc = 0;
		break;

		case P_SLEEP:
			if( (userParValue.b[0] >= SLEEP_MIN_SET_VALUE) && (userParValue.b[0] != SLEEP_OFF_VALUE) )
			{
				if( userParValue.b[0] == SLEEP_MIN_SET_VALUE )
					userParValue.b[0] = SLEEP_OFF_VALUE;
				else
					userParValue.b[0]--;
			}
		return;
	}

	switch( pParTab->Type )
	{
		case TYPE_BOOL:
		case TYPE_BYTE_MASK:
		case TYPE_BYTE:
			if( (unsigned short)userParValue.b[0] > (unsigned short)vMin.b[0]+step )
				userParValue.b[0] -= step;
			else if( userParValue.b[0] > vMin.b[0] )
				userParValue.b[0] = vMin.b[0];
			else if( Ricirc )
				userParValue.b[0] = vMax.b[0];
			else
				userParValue.b[0] = vMin.b[0];
		break;

		case TYPE_WORD:
			if( (unsigned long)userParValue.w[0] > (unsigned long)vMin.w[0]+step )
				userParValue.w[0] -= step;
			else if( userParValue.w[0] > vMin.w[0] )
				userParValue.w[0] = vMin.w[0];
			else if( Ricirc )
				userParValue.w[0] = vMax.w[0];
			else
				userParValue.w[0] = vMin.w[0];
		break;

		case TYPE_INTEGER:
			if( (long)userParValue.i[0] > (long)vMin.i[0]+step )
				userParValue.i[0] -= step;
			else if( userParValue.i[0] > vMin.i[0] )
				userParValue.i[0] = vMin.i[0];
			else if( Ricirc )
				userParValue.i[0] = vMax.i[0];
			else
				userParValue.i[0] = vMin.i[0];
		break;
	}
}

/*!
** \fn void userSetPar( unsigned short iPar )
** \brief Set the parameter value by the 'userParValue' variable
** \param iPar Parameter index
** \return None
**/
void userSetPar( unsigned short iPar )
{
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];
	PKT_STRUCT sPkt;	// warning size: MAX_PACKET_SIZE+2
	unsigned char b, msk;
	const MBDATA *pt;
	TBitWord IndVar;
	CRONO_PROG_WEEK *prg = &Crono.Crono_ProgWeek[iCronoPrg];

	switch( pParTab->Type )
	{
		case TYPE_BOOL:
			if( userParValue.b[0] )
			{
				SetBitBMap( (unsigned char *)pParTab->upPar.pPar, pParTab->Pos_Len );
			}
			else
			{
				ResBitBMap( (unsigned char *)pParTab->upPar.pPar, pParTab->Pos_Len );
			}
		break;

		case TYPE_BYTE_MASK:
			b = userParValue.b[0];
			msk = pParTab->Pos_Len;
			while( !(msk & 0x01) )
			{
				msk >>= 1;
				b <<= 1;
			}
			if( (iPar == P_CRONO_P1_SETTIM)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_DOM)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_LUN)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_MAR)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_MER)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_GIO)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_VEN)
				|| (iPar == P_CRONO_P1_SETTIM_WEEK_SAB)
				)
			{
				prg->Enab.BYTE &= (~pParTab->Pos_Len);
				prg->Enab.BYTE |= b;
			}
			else
			{
				*(unsigned char *)pParTab->upPar.pPar &= (~pParTab->Pos_Len);
				*(unsigned char *)pParTab->upPar.pPar |= b;
			}
		break;

		case TYPE_BYTE:
			if( iPar == P_WIFI_IP )
			{
				memcpy( Ip, strPar, IP_SIZE );
			}
			else if( iPar == P_RFPANEL )
			{
				// RFID
				tmpRFid = userParValue.b[0];
				if( OT_Stat == OT_STAT_LINKED )
				{
					// registra il nuovo RFID solo se confermato dalla scheda
					sPkt.pktLen = 0;
					sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
					sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
					sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
					sPkt.pkt[sPkt.pktLen++] = HIBYTE( ADDR_RFID );	// addr H
					sPkt.pkt[sPkt.pktLen++] = LOBYTE( ADDR_RFID );	// addr L
					sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
					sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
					sPkt.pkt[sPkt.pktLen++] = 2;		// Nbyte
					sPkt.pkt[sPkt.pktLen++] = 0x00;		// RFid H
					sPkt.pkt[sPkt.pktLen++] = tmpRFid;		// RFid L
					OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
				}
				else
				{
					// registra il nuovo RFID
					ExecSetRFid();
				}
			}
			else if( iPar == P_CRONO_P1_SETTIM_START )
			{
				prg->OraStart = userParValue.b[0];
			}
			else if( iPar == P_CRONO_P1_SETTIM_STOP )
			{
				prg->OraStop = userParValue.b[0];
			}
			else if( iPar == P_CRONO_P1_SETTIM_TEMP_ARIA )
			{
				prg->TempAria = userParValue.b[0];
			}
			else if( iPar == P_CRONO_P1_SETTIM_TEMP_H2O )
			{
				prg->TempH2O = userParValue.b[0];
			}
			else if( iPar == P_CRONO_P1_SETTIM_POTENZA )
			{
				prg->Fire = userParValue.b[0];
			}
			else
			{
				*(unsigned char *)pParTab->upPar.pPar = userParValue.b[0];
			}
		break;

		case TYPE_WORD:
			if( iPar == P_WIFI_PORT )
			{
				strPar[TCPPORT_SIZE] = '\0';
				userParValue.w[0] = atoi( strPar );
			}
			*(unsigned short *)pParTab->upPar.pPar = userParValue.w[0];
		break;

		case TYPE_INTEGER:
			*(short *)pParTab->upPar.pPar = userParValue.i[0];
		break;

		case TYPE_CHAR:
			if( iPar == P_WIFI_SSID )
			{
				memset( Ssid, 0, sizeof(Ssid) );
				strPar[SSID_SIZE] = '\0';
				strcpy( Ssid, strPar );
			}
			else if( iPar == P_WIFI_KEYW )
			{
				memset( CryptoKey, 0, sizeof(CryptoKey) );
				strPar[KEYW_SIZE] = '\0';
				strcpy( CryptoKey, strPar );
			}
			else if( iPar == P_WPS_PIN )
			{
				memset( WPSPin, 0, sizeof(WPSPin) );
				strPar[WPS_PIN_SIZE] = '\0';
				strcpy( WPSPin, strPar );
			}
			else
			{
				*(unsigned char *)pParTab->upPar.pPar = userParValue.b[0];
			}
		break;
	}

	if( iPar == P_TIPOSTUFA )
	{
		// impostazione tipo stufa
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_TYPE_SET;	// cmd
		sPkt.pkt[sPkt.pktLen++] = userParValue.b[0];	// tipo stufa
		sPkt.pkt[sPkt.pktLen++] = 1;	// param (tipo in EEPROM)
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
	}
	else if( iPar == P_ATTIVA_COCLEA )
	{
		// attivazione/disattivazione coclea
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
		sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_TEST_COCLEA;
		sPkt.pkt[sPkt.pktLen++] = userParValue.b[0] ? 1 : 0;
		sPkt.pkt[sPkt.pktLen++] = 0;
		sPkt.pkt[sPkt.pktLen++] = 0;
		sPkt.pkt[sPkt.pktLen++] = 0;
		sPkt.pkt[sPkt.pktLen++] = 0;
		sPkt.pkt[sPkt.pktLen++] = 0;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
	}
	else if( iPar == P_ATTIVA_ESP )
	{
		// attivazione/disattivazione espulsore
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
		sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_TEST_ESPULSORE;
		sPkt.pkt[sPkt.pktLen++] = userParValue.b[0] ? 1 : 0;
		sPkt.pkt[sPkt.pktLen++] = 0;
		sPkt.pkt[sPkt.pktLen++] = 0;
		sPkt.pkt[sPkt.pktLen++] = 0;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
	}
	else if( iPar == P_ATTIVA_POMPA )
	{
		// attivazione/disattivazione pompa
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
		sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_TEST_POMPA;
		sPkt.pkt[sPkt.pktLen++] = userParValue.b[0] ? 1 : 0;
		sPkt.pkt[sPkt.pktLen++] = 0;
		sPkt.pkt[sPkt.pktLen++] = 0;
		sPkt.pkt[sPkt.pktLen++] = 0;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
	}
	else if( iPar == P_TEST )
	{
		// attivazione/disattivazione collaudo manuale
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
		sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_EN_TEST;
		sPkt.pkt[sPkt.pktLen++] = userParValue.b[0] ? 1 : 0;
		sPkt.pkt[sPkt.pktLen++] = 1;	// manuale
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
	}
	else if( iPar == P_TEST_AUTO )
	{
		// attivazione/disattivazione collaudo automatico
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
		sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_EN_TEST;
		sPkt.pkt[sPkt.pktLen++] = userParValue.b[0] ? 1 : 0;
		sPkt.pkt[sPkt.pktLen++] = 0;	// automatico
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
	}
	else if( iPar == P_TEST_CALIB_FOTORES_ON )
	{
		// attivazione/disattivazione calibrazione fotoresistenza ON
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
		sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_CALIB_FOTORES_ON;
		sPkt.pkt[sPkt.pktLen++] = userParValue.b[0] ? 1 : 0;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
	}
	else if( iPar == P_TEST_CALIB_FOTORES_OFF )
	{
		// attivazione/disattivazione calibrazione fotoresistenza OFF
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
		sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_CALIB_FOTORES_OFF;
		sPkt.pkt[sPkt.pktLen++] = userParValue.b[0] ? 1 : 0;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
	}
	else if( iPar == P_WIFI_IP )
	{
		// IP
		IndVar.WORD = pParTab->IndVar;
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
		sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.H;	// addr H
		sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.L;	// addr L
		sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
		sPkt.pkt[sPkt.pktLen++] = IP_SIZE/2;		// Nword L
		sPkt.pkt[sPkt.pktLen++] = IP_SIZE;		// Nbyte
		memcpy( &sPkt.pkt[sPkt.pktLen], Ip, IP_SIZE );
		sPkt.pktLen += IP_SIZE;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
	}
	else if( iPar == P_WIFI_SSID )
	{
		// SSID
		/*
			Per la massima dimensione del pacchetto (MAX_PACKET_SIZE)
			occorrono 2 richieste di 10+6 registri.
		*/
		IndVar.WORD = pParTab->IndVar;
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
		sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.H;	// addr H
		sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.L;	// addr L
		sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
		sPkt.pkt[sPkt.pktLen++] = 0x0A;		// Nword L
		sPkt.pkt[sPkt.pktLen++] = 20;		// Nbyte
		memcpy( &sPkt.pkt[sPkt.pktLen], Ssid, 20 );
		sPkt.pktLen += 20;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
		sPkt.pktLen -= 20;
		IndVar.WORD += 0x000A;
		sPkt.pkt[3] = IndVar.BYTE.H;	// addr H
		sPkt.pkt[4] = IndVar.BYTE.L;	// addr L
		sPkt.pkt[6] = 0x06;		// Nword L
		sPkt.pkt[7] = 12;		// Nbyte
		memcpy( &sPkt.pkt[sPkt.pktLen], &Ssid[20], 12 );
		sPkt.pktLen += 12;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
	}
	else if( iPar == P_WIFI_KEYW )
	{
		// KEYWORD
		/*
			Per la massima dimensione del pacchetto (MAX_PACKET_SIZE)
			occorrono 4 richieste di 10+10+10+2 registri.
		*/
		IndVar.WORD = pParTab->IndVar;
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
		sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.H;	// addr H
		sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.L;	// addr L
		sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
		sPkt.pkt[sPkt.pktLen++] = 0x0A;		// Nword L
		sPkt.pkt[sPkt.pktLen++] = 20;		// Nbyte
		memcpy( &sPkt.pkt[sPkt.pktLen], CryptoKey, 20 );
		sPkt.pktLen += 20;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
		sPkt.pktLen -= 20;
		IndVar.WORD += 0x000A;
		sPkt.pkt[3] = IndVar.BYTE.H;	// addr H
		sPkt.pkt[4] = IndVar.BYTE.L;	// addr L
		sPkt.pkt[6] = 0x0A;		// Nword L
		sPkt.pkt[7] = 20;		// Nbyte
		memcpy( &sPkt.pkt[sPkt.pktLen], &CryptoKey[20], 20 );
		sPkt.pktLen += 20;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
		sPkt.pktLen += 20;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
		sPkt.pktLen -= 20;
		IndVar.WORD += 0x000A;
		sPkt.pkt[3] = IndVar.BYTE.H;	// addr H
		sPkt.pkt[4] = IndVar.BYTE.L;	// addr L
		sPkt.pkt[6] = 0x0A;		// Nword L
		sPkt.pkt[7] = 20;		// Nbyte
		memcpy( &sPkt.pkt[sPkt.pktLen], &CryptoKey[40], 20 );
		sPkt.pktLen += 20;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
		sPkt.pktLen -= 20;
		IndVar.WORD += 0x000A;
		sPkt.pkt[3] = IndVar.BYTE.H;	// addr H
		sPkt.pkt[4] = IndVar.BYTE.L;	// addr L
		sPkt.pkt[6] = 0x02;		// Nword L
		sPkt.pkt[7] = 4;		// Nbyte
		memcpy( &sPkt.pkt[sPkt.pktLen], &CryptoKey[60], 4 );
		sPkt.pktLen += 4;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
	}
	else if( iPar == P_WPS_PIN )
	{
		// WPS PIN
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
		sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_NETWORK;
		sPkt.pkt[sPkt.pktLen++] = 1;	// abilita WPS
		memcpy( &sPkt.pkt[sPkt.pktLen], WPSPin, WPS_PIN_SIZE );
		sPkt.pktLen += WPS_PIN_SIZE;
		sPkt.pkt[sPkt.pktLen++] = 0;
		OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
	}
	else if(
				(iPar == P_FAN1_INTERPOL) ||
				(iPar == P_FAN2_INTERPOL) ||
				(iPar == P_FAN3_INTERPOL) ||
				(iPar == P_TON_COCLEA_INTERPOL) ||
				(iPar == P_TON_FAN1_INTERPOL) ||
				(iPar == P_RPM1_INTERPOL) ||
				(iPar == P_RPM2_INTERPOL) ||
				(iPar == P_LPM_INTERPOL) ||
				(iPar == P_ESP2_INTERPOL)
			)
	{
		// comando di interpolazione
		if( userParValue.b[0] )
		{
			INTERPOL_CMD = 0;	// reset flag comando
			sPkt.pktLen = 0;
			sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
			sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
			sPkt.pkt[sPkt.pktLen++] = MB_FUNC_CMD_DEBUG;	// cmd
			sPkt.pkt[sPkt.pktLen++] = CMDDEBUG_INTERP;
			if( iPar == P_FAN1_INTERPOL )
				sPkt.pkt[sPkt.pktLen++] = 1;
			else if( iPar == P_FAN2_INTERPOL )
				sPkt.pkt[sPkt.pktLen++] = 2;
			else if( iPar == P_FAN3_INTERPOL )
				sPkt.pkt[sPkt.pktLen++] = 3;
			else if( iPar == P_TON_COCLEA_INTERPOL )
				sPkt.pkt[sPkt.pktLen++] = 4;
			else if( iPar == P_TON_FAN1_INTERPOL )
				sPkt.pkt[sPkt.pktLen++] = 5;
			else if( iPar == P_RPM1_INTERPOL )
				sPkt.pkt[sPkt.pktLen++] = 6;
			else if( iPar == P_RPM2_INTERPOL )
				sPkt.pkt[sPkt.pktLen++] = 7;
			else if( iPar == P_LPM_INTERPOL )
				sPkt.pkt[sPkt.pktLen++] = 8;
			else //if( iPar == P_ESP2_INTERPOL )
				sPkt.pkt[sPkt.pktLen++] = 9;
			OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
		}
	}
	else if( pParTab->IndVar != NO_INDVAR )
	{
		// parametro della scheda
		IndVar.WORD = pParTab->IndVar;
		switch( pParTab->MBTab )
		{
			case MBTAB_COIL:
				sPkt.pktLen = 0;
				sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
				sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
				sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_SINGLE_COIL;	// cmd
				sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.H;	// addr H
				sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.L;	// addr L
				sPkt.pkt[sPkt.pktLen++] = userParValue.b[0] ? 1 : 0;		// bit value
				OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
			break;

			case MBTAB_HOLD_REG:
			case MBTAB_HOLD2_REG:
			case MBTAB_HOLD3_REG:
				sPkt.pktLen = 0;
				sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
				sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
				sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
				sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.H;	// addr H
				sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.L;	// addr L
				sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
				sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
				sPkt.pkt[sPkt.pktLen++] = 2;		// Nbyte
				if( pParTab->MBTab == MBTAB_HOLD_REG )
				{
					pt = &HoldReg_tab[IndVar.WORD];
				}
				else if( pParTab->MBTab == MBTAB_HOLD2_REG )
				{
					pt = &HoldReg2_tab[IndVar.WORD-THOLREG2_FIRST_ADDR];
				}
				else
				{
					pt = &HoldReg3_tab[IndVar.WORD-THOLREG3_FIRST_ADDR];
				}
				switch (pParTab->Type)
				{
					case TYPE_BYTE:
						sPkt.pkt[sPkt.pktLen++] = 0x00;		// byte H
						sPkt.pkt[sPkt.pktLen++] = userParValue.b[0];		// byte L
					break;

					case TYPE_BYTE_MASK:
						if( (pt->Tipo == MBTYPE_BYTE) ||
							(pt->Tipo == MBTYPE_BYTE_MASK) )
						{
							sPkt.pkt[sPkt.pktLen++] = 0x00;		// byte H
							sPkt.pkt[sPkt.pktLen++] = userParValue.b[0];		// byte L
						}
						else if( pt->Tipo == MBTYPE_WORD )
						{
							// 16-bit bitmap
							sPkt.pkt[sPkt.pktLen++] = HIBYTE( *pt->pw );		// byte H
							sPkt.pkt[sPkt.pktLen++] = LOBYTE( *pt->pw );		// byte L
						}
						else
						{
							sPkt.pktLen = 0;	// non invia niente
						}
					break;

					case TYPE_WORD:
					case TYPE_INTEGER:
						sPkt.pkt[sPkt.pktLen++] = userParValue.b[1];		// byte H
						sPkt.pkt[sPkt.pktLen++] = userParValue.b[0];		// byte L
					break;

					default:
						sPkt.pktLen = 0;	// non invia niente
					break;
				}
				OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 1 );
			break;
		}
	}
}

/*!
** \fn void userEscPar( unsigned short iPar )
** \brief Quit the parameter's setting
** \param iPar Parameter index
** \return None
**/
void userEscPar( unsigned short iPar )
{
}

/*!
** \fn short userIsMinPar( unsigned short iPar )
** \brief Check if the parameter value into the 'userParValue' variable is at minimum 
** \param iPar Parameter index
** \return None
**/
short userIsMinPar( unsigned short iPar )
{
	short RetVal = 0;
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];
	BYTE_UNION vMax;
	BYTE_UNION vMin;
	unsigned char step;

	userGetMinMaxStep( iPar, &vMin, &vMax, &step );

	switch( iPar )
	{
		case P_CRONO_P1_SETTIM_STOP:
	#ifdef menu_CRONO
		case P_CRONO_P2_SETTIM_STOP:
		case P_CRONO_P3_SETTIM_STOP:
		case P_CRONO_P4_SETTIM_STOP:
		case P_CRONO_P5_SETTIM_STOP:
		case P_CRONO_P6_SETTIM_STOP:
	#endif
			/* In modifica il limite inferiore dell'ora di stop e` uguale
				all'ora di start del programma crono indicato da 'iCronoPrg' */
			vMin.b[0] = Crono.Crono_ProgWeek[iCronoPrg].OraStart;
		break;
	}

	switch( pParTab->Type )
	{
		case TYPE_BOOL:
		case TYPE_BYTE_MASK:
		case TYPE_BYTE:
			if( userParValue.b[0] <= vMin.b[0] )
				RetVal = 1;
		break;

		case TYPE_WORD:
			if( userParValue.w[0] <= vMin.w[0] )
				RetVal = 1;
		break;

		case TYPE_INTEGER:
			if( userParValue.i[0] <= vMin.i[0] )
				RetVal = 1;
		break;
	}

	return RetVal;
}

/*!
** \fn short userIsMaxPar( unsigned short iPar )
** \brief Check if the parameter value into the 'userParValue' variable is at maximum 
** \param iPar Parameter index
** \return None
**/
short userIsMaxPar( unsigned short iPar )
{
	short RetVal = 0;
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];
	BYTE_UNION vMax;
	BYTE_UNION vMin;
	unsigned char step;

	userGetMinMaxStep( iPar, &vMin, &vMax, &step );

	switch( iPar )
	{
		case P_LINGUA:
			vMax.b[0] = NumLanguage - 1;
		break;
	}

	switch( pParTab->Type )
	{
		case TYPE_BOOL:
		case TYPE_BYTE_MASK:
		case TYPE_BYTE:
			if( userParValue.b[0] >= vMax.b[0] )
				RetVal = 1;
		break;

		case TYPE_WORD:
			if( userParValue.w[0] >= vMax.w[0] )
				RetVal = 1;
		break;

		case TYPE_INTEGER:
			if( userParValue.i[0] >= vMax.i[0] )
				RetVal = 1;
		break;
	}

	return RetVal;
}

/*!
	\fn short userReqPar( unsigned short iPar )
   \brief Richiede il valore e i limiti del parametro indicato da 'iPar'.
	\param iPar Indice parametro
	\return 1: OK, 0: FAILED
*/
short userReqPar( unsigned short iPar )
{
	short RetVal = 2;
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[10];		//!< packet buffer
	} sPkt;
	TBitWord IndVar;

	if( pParTab->IndVar != NO_INDVAR )
	{
		// parametro della scheda
		IndVar.WORD = pParTab->IndVar;
		switch( pParTab->MBTab )
		{
			case MBTAB_COIL:
				sPkt.pktLen = 0;
				sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
				sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
				sPkt.pkt[sPkt.pktLen++] = MB_FUNC_READ_COILS;	// cmd
				sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.H;	// addr H
				sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.L;	// addr L
				sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nbit H
				sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nbit L
				if( OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 ) )
					RetVal = 0;
			break;

			case MBTAB_INPUT_REG:
				sPkt.pktLen = 0;
				sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
				sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
				sPkt.pkt[sPkt.pktLen++] = MB_FUNC_READ_INPUT_REGISTER;	// cmd
				sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.H;	// addr H
				sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.L;	// addr L
				sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
				switch (pParTab->Type)
				{
					case TYPE_BOOL:
					case TYPE_BYTE:
					case TYPE_BYTE_MASK:
					case TYPE_WORD:
					case TYPE_INTEGER:
						if( iPar == P_CODICE_SCHEDA )
						{
							// codice comfort
							sPkt.pkt[sPkt.pktLen++] = 0x04;		// Nword L
						}
						else if( iPar == P_CODICE_SICUREZZA )
						{
							// codice boot/driver
							sPkt.pkt[sPkt.pktLen++] = 0x04;		// Nword L
						}
						else if( iPar == P_CODICE_DRIVER )
						{
							// codice boot/driver
							sPkt.pkt[sPkt.pktLen++] = 0x04;		// Nword L
						}
						else if( iPar == P_CODICE_PARAM )
						{
							// codice parametri
							sPkt.pkt[sPkt.pktLen++] = 0x02;		// Nword L
						}
						else if( iPar == P_SERVICE )
						{
							// numero assistenza
							sPkt.pkt[sPkt.pktLen++] = 0x04;		// Nword L
						}
						else
						{
							sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
						}
					break;

					case TYPE_DWORD:
					case TYPE_LONG:
					case TYPE_FLOAT:
						sPkt.pkt[sPkt.pktLen++] = 0x02;		// Nword L
					break;

					case TYPE_CHAR:
					break;
				}
				if( OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 ) )
					RetVal = 0;
			break;

			case MBTAB_HOLD_REG:
			case MBTAB_HOLD2_REG:
			case MBTAB_HOLD3_REG:
				// richiesta valore
				sPkt.pktLen = 0;
				sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
				sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
				sPkt.pkt[sPkt.pktLen++] = MB_FUNC_READ_HOLDING_REGISTER;	// cmd
				sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.H;	// addr H
				sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.L;	// addr L
				sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
				switch (pParTab->Type)
				{
					case TYPE_BOOL:
					case TYPE_BYTE:
					case TYPE_BYTE_MASK:
					case TYPE_WORD:
					case TYPE_INTEGER:
						if( iPar == P_WIFI_IP )
						{
							// indirizzo IP
							sPkt.pkt[sPkt.pktLen++] = 0x08;		// Nword L
						}
						else
						{
							sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
						}
					break;

					case TYPE_DWORD:
					case TYPE_LONG:
					case TYPE_FLOAT:
						sPkt.pkt[sPkt.pktLen++] = 0x02;		// Nword L
					break;

					case TYPE_CHAR:
						if( iPar == P_WIFI_SSID )
						{
							sPkt.pkt[sPkt.pktLen++] = SSID_SIZE/2;		// Nword L
						}
						else if( iPar == P_WIFI_KEYW )
						{
							/*
								Per la nuova massima dimensione del pacchetto (MAX_PACKET_SIZE)
								non occorrono piu' 2 richieste di 16 registri ciascuna.
							*/
							sPkt.pkt[sPkt.pktLen++] = KEYW_SIZE/2;		// Nword L
						}
						else if( iPar == P_WPS_PIN )
						{
							sPkt.pkt[sPkt.pktLen++] = WPS_PIN_SIZE/2;		// Nword L
						}
						else
						{
							sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
						}
					break;
				}
				if( OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 ) )
					RetVal = 1;

				// eventuale richiesta limiti/step
				if( !userIsFlagVar( iPar ) &&
					(
						(pParTab->MBTab == MBTAB_HOLD2_REG) ||
						((pParTab->MBTab == MBTAB_HOLD_REG) && !GetBitBMap( OT_LimitStep_Requested, IndVar.WORD ))
					)
					)
				{
					sPkt.pktLen = 0;
					sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
					sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
					sPkt.pkt[sPkt.pktLen++] = MB_FUNC_READ_LIMITS;	// cmd
					sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.H;	// addr H
					sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.L;	// addr L
					sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
					switch (pParTab->Type)
					{
						case TYPE_BOOL:
						case TYPE_BYTE:
						case TYPE_BYTE_MASK:
						case TYPE_WORD:
						case TYPE_INTEGER:
							sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
						break;

						case TYPE_DWORD:
						case TYPE_LONG:
						case TYPE_FLOAT:
							sPkt.pkt[sPkt.pktLen++] = 0x02;		// Nword L
						break;

						case TYPE_CHAR:
						break;
					}
					if( OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 ) )
						RetVal = 0;
				}
				else
				{
					RetVal = 0;
				}
			break;
		}
	}
	else if(
				(iPar == P_CRONO_P1_SETTIM_TEMP_ARIA)
	#ifdef menu_CRONO
				|| (iPar == P_CRONO_P2_SETTIM_TEMP_ARIA)
				|| (iPar == P_CRONO_P3_SETTIM_TEMP_ARIA)
				|| (iPar == P_CRONO_P4_SETTIM_TEMP_ARIA)
				|| (iPar == P_CRONO_P5_SETTIM_TEMP_ARIA)
				|| (iPar == P_CRONO_P6_SETTIM_TEMP_ARIA)
	#endif
			)
	{
		// richiesta limiti/step per setpoint temperatura ambiente crono
		IndVar.WORD = ADDR_TEMP_PAN;
		if( !GetBitBMap( OT_LimitStep_Requested, IndVar.WORD ) )
		{
			sPkt.pktLen = 0;
			sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
			sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
			sPkt.pkt[sPkt.pktLen++] = MB_FUNC_READ_LIMITS;	// cmd
			sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.H;	// addr H
			sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.L;	// addr L
			sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
			sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
			if( OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 ) )
				RetVal = 0;
		}
		else
		{
			RetVal = 0;
		}
	}
	else if(
				(iPar == P_CRONO_P1_SETTIM_TEMP_H2O)
	#ifdef menu_CRONO
				|| (iPar == P_CRONO_P2_SETTIM_TEMP_H2O)
				|| (iPar == P_CRONO_P3_SETTIM_TEMP_H2O)
				|| (iPar == P_CRONO_P4_SETTIM_TEMP_H2O)
				|| (iPar == P_CRONO_P5_SETTIM_TEMP_H2O)
				|| (iPar == P_CRONO_P6_SETTIM_TEMP_H2O)
	#endif
			)
	{
		// richiesta limiti/step per setpoint temperatura acqua riscaldamento crono
		IndVar.WORD = ADDR_H2O_TSET;
		if( !GetBitBMap( OT_LimitStep_Requested, IndVar.WORD ) )
		{
			sPkt.pktLen = 0;
			sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
			sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
			sPkt.pkt[sPkt.pktLen++] = MB_FUNC_READ_LIMITS;	// cmd
			sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.H;	// addr H
			sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.L;	// addr L
			sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
			sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
			if( OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 ) )
				RetVal = 0;
		}
		else
		{
			RetVal = 0;
		}
	}
	else if(
				(iPar == P_CRONO_P1_SETTIM_POTENZA)
	#ifdef menu_CRONO
				|| (iPar == P_CRONO_P2_SETTIM_POTENZA)
				|| (iPar == P_CRONO_P3_SETTIM_POTENZA)
				|| (iPar == P_CRONO_P4_SETTIM_POTENZA)
				|| (iPar == P_CRONO_P5_SETTIM_POTENZA)
				|| (iPar == P_CRONO_P6_SETTIM_POTENZA)
	#endif
			)
	{
		// richiesta limiti/step per livello di potenza crono
		IndVar.WORD = ADDR_POWER_LEVEL;	// limiti variabili
		sPkt.pktLen = 0;
		sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
		sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
		sPkt.pkt[sPkt.pktLen++] = MB_FUNC_READ_LIMITS;	// cmd
		sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.H;	// addr H
		sPkt.pkt[sPkt.pktLen++] = IndVar.BYTE.L;	// addr L
		sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
		sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
		if( OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 ) )
			RetVal = 0;
	}
	else
	{
		RetVal = 0;
	}

	// tutto ok?
	if( RetVal )
		RetVal = 0;
	else
		RetVal = 1;

	return RetVal;
}

/*!
	\fn short userIsReadOnlyVar( unsigned short iPar )
	\brief Determina se il parametro indicato da 'iPar' e` read-only o meno.
	\param iPar Indice parametro
	\return 1: read-only, 0: altrimenti
*/
short userIsReadOnlyVar( unsigned short iPar )
{
	unsigned short RetVal = 0;
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];

	if( iPar != P_TIPOSTUFA )	// eccezione: tipo stufa e` modificabile
	{
		if( (pParTab->Step == 0) || (pParTab->MBTab == MBTAB_INPUT_REG) )
		{
			RetVal = 1;
		}
	}

	return RetVal;
}

/*! 
	\fn short userIsFlagVar( unsigned short iPar )
	\brief Determina se il parametro indicato da 'iPar' e` un flag o meno.
	\param iPar Indice parametro
	\return 1: flag, 0: altrimenti
*/
short userIsFlagVar( unsigned short iPar )
{
	unsigned short RetVal = 0;
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];

	if( (pParTab->uMin.d == 0) && (pParTab->uMax.d == 1) &&
		((pParTab->Type == TYPE_BYTE_MASK) || (pParTab->Type == TYPE_BOOL)) )
	{
		RetVal = 1;
	}

	return RetVal;
}

/*!
	\fn void userGetMinMaxStep( unsigned short iPar, BYTE_UNION *vMin,
										BYTE_UNION *vMax, unsigned char *step )
	\brief Ritorna i limiti e il passo di incremento/decremento per il parametro
			 indicato da 'iPar'.
	\param iPar Indice parametro
	\param vMin Puntatore al valore minimo
	\param vMax Puntatore al valore massimo
	\param step Puntatore al passo di incremento/decremento
*/
void userGetMinMaxStep( unsigned short iPar, BYTE_UNION *vMin,
								BYTE_UNION *vMax, unsigned char *step )
{
	const PARTAB_STRUCT *pParTab = &Param_tab[iPar];
	LIMITS_STRUCT *pLim;

	if( iPar == P_TIPOSTUFA )
	{
		/*
			Il Tipo Stufa appartiene agli Input Registers,
			ma e` modificabile. Si considerano i limiti locali.
		*/
		*vMin = pParTab->uMin.Min;
		*vMax = pParTab->uMax.Max;
		*step = pParTab->Step;
	}
	else if( pParTab->MBTab == MBTAB_COIL )
	{
		// flag -> limiti locali
		*vMin = pParTab->uMin.Min;
		*vMax = pParTab->uMax.Max;
		*step = pParTab->Step;
	}
	else if( pParTab->IndVar != NO_INDVAR )
	{
		// parametro della scheda
		if( pParTab->MBTab == MBTAB_HOLD3_REG )
		{
			pLim = (LIMITS_STRUCT *)&Limits3_tab[pParTab->IndVar-THOLREG3_FIRST_ADDR];
		}
		else if( pParTab->MBTab == MBTAB_HOLD2_REG )
		{
			pLim = (LIMITS_STRUCT *)&Limits2_tab[pParTab->IndVar-THOLREG2_FIRST_ADDR];
		}
		else
		{
			pLim = (LIMITS_STRUCT *)&Limits_tab[pParTab->IndVar];
		}

		if( (pLim->Min == 0) && (pLim->Max == 0xFFFF) &&
			(pParTab->uMin.d == 0) && (pParTab->uMax.d == 1) && (pParTab->Type == TYPE_BYTE_MASK) )
		{
			// flag di una bitmap -> limiti locali
			*vMin = pParTab->uMin.Min;
			*vMax = pParTab->uMax.Max;
			*step = pParTab->Step;
		}
		else
		{
			vMin->w[0] = pLim->Min;	// ATTENZIONE: range 0-65535
			vMax->w[0] = pLim->Max;	// ATTENZIONE: range 0-65535
			*step = pLim->Step;
		}
	}
	else if(
				(iPar == P_CRONO_P1_SETTIM_TEMP_ARIA)
	#ifdef menu_CRONO
				|| (iPar == P_CRONO_P2_SETTIM_TEMP_ARIA)
				|| (iPar == P_CRONO_P3_SETTIM_TEMP_ARIA)
				|| (iPar == P_CRONO_P4_SETTIM_TEMP_ARIA)
				|| (iPar == P_CRONO_P5_SETTIM_TEMP_ARIA)
				|| (iPar == P_CRONO_P6_SETTIM_TEMP_ARIA)
	#endif
			)
	{
		// setpoint temperatura ambiente crono
		pLim = (LIMITS_STRUCT *)&Limits_tab[ADDR_TEMP_PAN];
		vMin->w[0] = pLim->Min;	// ATTENZIONE: range 0-65535
		vMax->w[0] = pLim->Max;	// ATTENZIONE: range 0-65535
		*step = pLim->Step;
	}
	else if(
				(iPar == P_CRONO_P1_SETTIM_TEMP_H2O)
	#ifdef menu_CRONO
				|| (iPar == P_CRONO_P2_SETTIM_TEMP_H2O)
				|| (iPar == P_CRONO_P3_SETTIM_TEMP_H2O)
				|| (iPar == P_CRONO_P4_SETTIM_TEMP_H2O)
				|| (iPar == P_CRONO_P5_SETTIM_TEMP_H2O)
				|| (iPar == P_CRONO_P6_SETTIM_TEMP_H2O)
	#endif
			)
	{
		// setpoint temperatura acqua riscaldamento crono
		pLim = (LIMITS_STRUCT *)&Limits_tab[ADDR_H2O_TSET];
		vMin->w[0] = pLim->Min;	// ATTENZIONE: range 0-65535
		vMax->w[0] = pLim->Max;	// ATTENZIONE: range 0-65535
		*step = pLim->Step;
	}
	else if(
				(iPar == P_CRONO_P1_SETTIM_POTENZA)
	#ifdef menu_CRONO
				|| (iPar == P_CRONO_P2_SETTIM_POTENZA)
				|| (iPar == P_CRONO_P3_SETTIM_POTENZA)
				|| (iPar == P_CRONO_P4_SETTIM_POTENZA)
				|| (iPar == P_CRONO_P5_SETTIM_POTENZA)
				|| (iPar == P_CRONO_P6_SETTIM_POTENZA)
	#endif
			)
	{
		// livello di potenza crono
		pLim = (LIMITS_STRUCT *)&Limits2_tab[ADDR_POWER_LEVEL-THOLREG2_FIRST_ADDR];
		vMin->w[0] = pLim->Min;	// ATTENZIONE: range 0-65535
		vMax->w[0] = pLim->Max;	// ATTENZIONE: range 0-65535
		*step = pLim->Step;
	}
	else if( iPar == P_LINGUA )
	{
		// lingua
		vMin->d = 0;
		vMax->d = NumLanguage - 1;
		*step = 1;
	}
	else
	{
		// parametro locale
		*vMin = pParTab->uMin.Min;
		*vMax = pParTab->uMax.Max;
		*step = pParTab->Step;
	}
}
								
/*!
** \fn void userScriviOraSleep( void )
** \brief Get the Sleep time from the 'userParValue' variable into the 'str2' string
** \return None
**/
void userScriviOraSleep( void )
{
	unsigned char ore,minuti;
	unsigned char bbApp;

	ore = DateTime.tm_hour;
	minuti = DateTime.tm_min;
	bbApp = minuti % 10;
	if( bbApp < 5 )
	{
		// arrotondo alla decade superiore
		minuti = minuti/10;
		minuti = 10*(minuti + 1);
		if( minuti >= 60 )
		{
			minuti = 0;
			if( ++ore >= 24 )
				ore = 0;
		}
	}
	else
	{
		// arrotondo alla 2a decade superiore
		minuti = minuti/10;
		minuti = minuti + 1;
		if( minuti >= 6 )
		{
			minuti = 0;
			if( ++ore >= 24 )
				ore = 0;
		}
		minuti = 10*(minuti + 1);
		if( minuti >= 60 )
		{
			minuti = 0;
			if( ++ore >= 24 )
				ore = 0;
		}
	}

	if( userParValue.b[0] > 0 )
	{
		// sommo i multipli delle decine di minuti
		minuti += ((10*userParValue.b[0]) % 60);
		if( minuti >= 60 )
		{
			minuti %= 60;
			if( ++ore >= 24 )
				ore = 0;
		}
		ore += (userParValue.b[0] / 6);
		ore %= 24;
	}

	str2[0] = Hex2Ascii( ore/10 );
	str2[1] = Hex2Ascii( ore%10 );
	str2[2] = ':';
	str2[3] = Hex2Ascii( minuti/10 );
	str2[4] = Hex2Ascii( minuti%10 );
	str2[5] = '\0';
}

/*!
** \fn void userGetOraSleep( void )
** \brief Get the current Sleep time into the 'userParValue' variable
** \return None
**/
void userGetOraSleep( void )
{
	unsigned char ore,minuti;

	if( Sleep.Ore == SLEEP_OFF )
	{
		userParValue.b[0] = SLEEP_OFF_VALUE;	// sleep off
	}
	else
	{
		userParValue.b[0] = SLEEP_MIN_SET_VALUE;
		userScriviOraSleep();
		ore = 10*Ascii2Hex( str2[0] ) + Ascii2Hex( str2[1] );
		minuti = 10*Ascii2Hex( str2[3] ) + Ascii2Hex( str2[4] );
		if( ore > Sleep.Ore )
		{
			userParValue.b[0] = SLEEP_OFF_VALUE;	// sleep off
		}
		else if( (ore == Sleep.Ore) && (minuti > Sleep.Minuti) )
		{
			userParValue.b[0] = SLEEP_OFF_VALUE;	// sleep off
		}
		else
		{
			while( !((ore == Sleep.Ore) && (minuti == Sleep.Minuti)) )
			{
				if( ++userParValue.b[0] > SLEEP_MAX_SET_VALUE )
				{
					userParValue.b[0] = SLEEP_MAX_SET_VALUE;
					break;
				}
				userScriviOraSleep();
				ore = 10*Ascii2Hex( str2[0] ) + Ascii2Hex( str2[1] );
				minuti = 10*Ascii2Hex( str2[3] ) + Ascii2Hex( str2[4] );
			}
		}
	}
}

/*! 
	\fn unsigned char UpdateSleepTime( void )
	\brief Esegue il set della struttura "Sleep" in base al valore impostato su
			"userParValue.b[0]"
	\return Il codice di stato corrente
*/
unsigned char UpdateSleepTime( void )
{
	// eseguo l'effettivo 'userSetPar()'
	if( userParValue.b[0] == SLEEP_OFF_VALUE )
	{
		Sleep.Ore = SLEEP_OFF;
		Sleep.Minuti = 0;
	}
	else
	{
		// il nuovo valore Sleep e` visualizzato nella stringa 'str2': hh:mm
		Sleep.Ore = 10*Ascii2Hex( str2[0] ) + Ascii2Hex( str2[1] );
		Sleep.Minuti = 10*Ascii2Hex( str2[3] ) + Ascii2Hex( str2[4] );
	}

	return userCurrentState;
}

/*!
** \fn void userSetNetworkFlags( void )
** \brief Set the Network flags bitmap
** \return None
**/
void userSetNetworkFlags( void )
{
	struct
	{
		unsigned short pktLen;		//!< packet length
		unsigned char pkt[16];		//!< packet buffer
	} sPkt;
	
	sPkt.pktLen = 0;
	sPkt.pkt[sPkt.pktLen++] = RFid;	// radio ID
	sPkt.pkt[sPkt.pktLen++] = MB_ADDR;	// addr
	sPkt.pkt[sPkt.pktLen++] = MB_FUNC_WRITE_MULTIPLE_REGISTERS;	// cmd
	sPkt.pkt[sPkt.pktLen++] = 0x20;	// addr H
	sPkt.pkt[sPkt.pktLen++] = 0x02;	// addr L
	sPkt.pkt[sPkt.pktLen++] = 0x00;		// Nword H
	sPkt.pkt[sPkt.pktLen++] = 0x01;		// Nword L
	sPkt.pkt[sPkt.pktLen++] = 2;		// Nbyte
	sPkt.pkt[sPkt.pktLen++] = HOLDBMAP8194_HIGH;
	sPkt.pkt[sPkt.pktLen++] = HOLDBMAP8194_LOW;
	OT_Send( (PKT_STRUCT *)&sPkt, SEND_MODE_ONE_SHOT, 0 );
}



/*****************************************************************************
 * EOF
 *****************************************************************************/
