/*!
** \file userParamCode.c
** \author Alessandro Vagniluca
** \date 19/01/2016
** \brief User Interface USER_STATE_PARAM_CODE state handler
** 
** \version 1.0
**/

#include "user.h"
#include "User_Parametri.h"
#include "userMenu.h"
#include "userAvvio.h"
#include "userParamCode.h"



/*!
** \enum TouchTags
** \brief The used touch tags values
**/
enum TouchTags
{
	TAG_VERT_SCROLL = 0,
};

// stringa titolo (di max DISPLAY_MAX_COL caratteri!!!)
static char sTitle[DISPLAY_MAX_COL+1];
// indice parametro trovato
static unsigned short iPar;


/*!
** \fn unsigned char userVisReqParCod( void )
** \brief Starts the USER_STATE_PARAM_CODE User Interface state handling
** \return The next User Interface state 
**/
unsigned char userVisReqParCod( void )
{
	strncpy( sTitle, GetMsg( 1, NULL, iLanguage, sRICERCA_CODICE_PARAM ), DISPLAY_MAX_COL );
	sTitle[DISPLAY_MAX_COL] = '\0';
	strcpy( sDescPar, sDescParInit );
	
	return USER_STATE_PARAM_CODE;
}

/*!
** \fn unsigned char userParamCode( void )
** \brief Handler function for the USER_STATE_PARAM_CODE User Interface state
** \return The next User Interface state 
**/
unsigned char userParamCode( void )
{
	unsigned char RetVal = USER_STATE_PARAM_CODE;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	unsigned char group = 0;
	unsigned char hundreds = 0;
	unsigned char tens = 0;
	unsigned char units = 0;
	unsigned char group2 = 0;
	unsigned char hundreds2 = 0;
	unsigned char tens2 = 0;
	unsigned char units2 = 0;
	unsigned long key_cnt = 0;
	unsigned char loop = 1;
	char *s3 = NULL;
	char *s4 = NULL;
	// format: "C         0      0      0"	
	const char sFmt[] = "%c         %c      %c      %c";
	short i = 0;
	unsigned char scrollON[4] = { 0 };	//!< scrolling status
	unsigned short preYscroll[4] = { 0 };	//!< previous Y value for vertical scrolling
	short Dy = 0;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );
	
	if( (s3 = (char *)malloc(MAX_LEN_STRINGA+1)) == NULL )
	{
		return RetVal;
	}
	else if( (s4 = (char *)malloc(MAX_LEN_STRINGA+1)) == NULL )
	{
		free( s3 );
		return RetVal;
	}

	// inizializza valori per scroll DOWN (decremento)
	group2 = group;
	if( group2 == 0 )
	{
		group2 = NUM_GROUP_PAR - 1;
	}
	else
	{
		group2--;
	}
	hundreds2 = hundreds;
	if( hundreds2 == 0 )
	{
		hundreds2 = 9;
	}
	else
	{
		hundreds2--;
	}
	tens2 = tens;
	if( tens2 == 0 )
	{
		tens2 = 9;
	}
	else
	{
		tens2--;
	}
	units2 = units;
	if( units2 == 0 )
	{
		units2 = 9;
	}
	else
	{
		units2--;
	}
	mysprintf( s3, sFmt,
				Group_tab[group2].cId,
				Hex2Ascii(hundreds2),
				Hex2Ascii(tens2),
				Hex2Ascii(units2)
				);
	
	// inizializza valori per scroll UP (incremento)
	group2 = group;
	if( ++group2 >= NUM_GROUP_PAR )
	{
		group2 = 0;
	}
	hundreds2 = hundreds;
	if( ++hundreds2 > 9 )
	{
		hundreds2 = 0;
	}
	tens2 = tens;
	if( ++tens2 > 9 )
	{
		tens2 = 0;
	}
	units2 = units;
	if( ++units2 > 9 )
	{
		units2 = 0;
	}
	mysprintf( s4, sFmt,
				Group_tab[group2].cId,
				Hex2Ascii(hundreds2),
				Hex2Ascii(tens2),
				Hex2Ascii(units2)
				);

	
	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
		}

		if( !((tagval == TAG_VSCROLL_BASE + 0) && (scrollON[0] == 1))
			&& !((tagval == TAG_VSCROLL_BASE + 1) && (scrollON[1] == 1))
			&& !((tagval == TAG_VSCROLL_BASE + 2) && (scrollON[2] == 1))
			&& !((tagval == TAG_VSCROLL_BASE + 3) && (scrollON[3] == 1))
			)
		{
			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			
			// draw QUIT bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// QUIT bitmap tag
				TagRegister( TAG_QUIT, 0, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height), p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
			}
			
			// draw OK bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-p_bmhdrOK->bmhdr.Width)*16, (FT_DispHeight-p_bmhdrOK->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// OK bitmap tag
				TagRegister( TAG_OK, (FT_DispWidth-p_bmhdrOK->bmhdr.Width), (FT_DispHeight-p_bmhdrOK->bmhdr.Height), p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height );
			}
			
			// char/digit width = 25, space width = 10
			
			// draw the centered code
			sDescPar[0] = Group_tab[group].cId;
			sDescPar[1] = Hex2Ascii(hundreds);
			sDescPar[2] = Hex2Ascii(tens);
			sDescPar[3] = Hex2Ascii(units);
			sDescPar[4] = '\0';
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColor_tab[UserColorId].addon) );	// Text Color
			// format: "C    0   0   0"
			mysprintf( str2, "%c    %c   %c   %c",
							sDescPar[0], sDescPar[1], sDescPar[2], sDescPar[3] );
			Ft_Gpu_CoCmd_Text( pHost, (FT_DispWidth/2), 160, FONT_MAX_DEF, OPT_CENTER, str2 );

			// draw the scroll DOWN parameter value
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, (FT_DispWidth/2), 160-25-5-14, FONT_TITLE_DEF, OPT_CENTER, s3 );

			// draw the scroll UP parameter value
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, (FT_DispWidth/2), 160+25+5+14, FONT_TITLE_DEF, OPT_CENTER, s4 );

			if( tagval == TOUCH_TAG_INIT )
			{
				// format: "C    0   0   0"; max string width: 36+4*10+24+3*10+24+3*10+24 = 208
				// group scroll tag
				TagRegister( TAG_VSCROLL_BASE+0, (FT_DispWidth/2-104+5), 31, 40, FT_DispHeight-30 );
				// hundreds scroll tag
				TagRegister( TAG_VSCROLL_BASE+1, (FT_DispWidth/2-104+70), 31, 40, FT_DispHeight-30 );
				// tens scroll tag
				TagRegister( TAG_VSCROLL_BASE+2, (FT_DispWidth/2-104+120), 31, 40, FT_DispHeight-30 );
				// units scroll tag
				TagRegister( TAG_VSCROLL_BASE+3, (FT_DispWidth/2-104+170), 31, 40, FT_DispHeight-30 );
			}
			
			// waiting pop-up window for the setting result
			if( Flag.popup  )
			{
				ShowWaitSetResultPopup();
			}
				
			// title
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sTitle, fontTitle) );

			Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
			Ft_Gpu_CoCmd_Swap( pHost );
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
		}
				
	#ifdef SCREENSHOT
		if( IsGenScreeshot() )
		{
			// screenshot
			DoScreenshot( SSHOT_ParCode );
		}
	#endif	// SCREENSHOT

		do
		{
			Wdog();
			if( Flag.popup	)
			{
				// wait for a setting result
				Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
			
				// get the current setting result state
				i = OT_GetResult();
				if( (i == OT_RESULT_OK) || (i == OT_RESULT_FAILED) )
				{
					// received setting result
					WaitSetResult.result = i;
					WaitSetResult.Time = 2000/USER_TIME_TICK;	// 2s for viewing result
				}
				
				// check for pop-up window timeout
				if( WaitSetResult.Time )
				{
					WaitSetResult.Time--;
				}
				if( WaitSetResult.Time == 0 )
				{
					// disable pop-up window
					Flag.popup = 0;
				}
				
				// flush the touch queue
				FlushTouchQueue();
				
				// simulate a touch timeout event
				if( userCounterTime <= 1 )
				{
					userCounterTime = USER_TIMEOUT_SHOW_60;
				}
				event.status = osEventTimeout;
			}
			else
			{
				// wait for a touch event
				event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

				// process the touch event
				if( event.status == osEventMessage )
				{
					pt = (TAG_STRUCT *)event.value.v;
				}
			}
		} while( event.status == osOK );
			
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			Flag.popup = 0;
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			scrollON[0] = 0;
			scrollON[1] = 0;
			scrollON[2] = 0;
			scrollON[3] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
			Flag.popup = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			scrollON[0] = 0;
			scrollON[1] = 0;
			scrollON[2] = 0;
			scrollON[3] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					RetVal = RetMenuTec();
					loop = 0;	// exit
				}
			}
		}
		else if( (pt->tag == TAG_QUIT) && !pt->status )
		{
			// exit touch
			Flag.popup = 0;
			
			// flush the touch queue
			FlushTouchQueue();
			
			RetVal = RetMenuTec();
			loop = 0;	// exit
		}
		else if( (pt->tag == TAG_OK) && !pt->status )
		{
			// exit touch
			Flag.popup = 0;
			
			// flush the touch queue
			FlushTouchQueue();
			
			// ricerca parametro
			if( (iPar = SearchParam()) == 0xFFFF )
			{
				// non trovato -> passo a richiesta codice parametro
				RetVal = userVisReqParCod();
			}
			else
			{
				userReqPar( iPar );	// recupera valore e limiti parametro
				Ft_Gpu_Hal_Sleep(500);	// attesa per eecupero valore e limiti parametro
				userSubState = 0;	// parameter view
				if(
					(iPar == P_WIFI_SSID)
					|| (iPar == P_WIFI_KEYW)
					// || (iPar == P_WIFI_PORT)
					// || (iPar == P_WPS_PIN)*/
					)
				{
					// modifica valore stringa lunga
					userVisEPar( iPar );
					if( WinSetString( 
											sDescPar,
											strPar,
											maxLen
											) )
					{
						//userSetPar( iPar );
						switch( iPar )
						{
							case P_WIFI_SSID:
								i = d_SSID;
							break;
							case P_WIFI_KEYW:
								i = d_KEYWORD;
							break;
							/*case P_WIFI_PORT:
								i = d_TCP_PORT;
							break;
							case P_WPS_PIN:
								i = d_WPS_PIN;
							break;*/
						}
						WinSetParWaitResult( 
													iPar,
													sDescPar,
													(char *)GetMsg( 2, NULL, iLanguage, i )
													);
					}
					else
					{
						userEscPar( iPar );
					}
				}
				else
				{
					// passa a visualizzazione parametro
					userGetPar( iPar );
					RetVal = USER_STATE_PARAM_VIS;
				}
			}
			loop = 0;	// exit
		}
		else if( tagval == TAG_VSCROLL_BASE+0 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset hundreds vertical scroll
			scrollON[1] = 0;
			// reset tens vertical scroll
			scrollON[2] = 0;
			// reset units vertical scroll
			scrollON[3] = 0;
			// group vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[0] )
				{
				default:
				case 0:
					preYscroll[0] = pt->y;
					scrollON[0] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[0];
						if( Dy > 5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[0] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP group value
							group2 = group;
							hundreds2 = hundreds;
							if( ++hundreds2 > 9 )
							{
								hundreds2 = 0;
							}
							tens2 = tens;
							if( ++tens2 > 9 )
							{
								tens2 = 0;
							}
							units2 = units;
							if( ++units2 > 9 )
							{
								units2 = 0;
							}
							mysprintf( s4, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
							// down group
							if( group == 0 )
							{
								group = NUM_GROUP_PAR - 1;
							}
							else
							{
								group--;
							}
							// update the scroll DOWN group value
							if( group2 == 0 )
							{
								group2 = NUM_GROUP_PAR - 1;
							}
							else
							{
								group2--;
							}
							hundreds2 = hundreds;
							if( hundreds2 == 0 )
							{
								hundreds2 = 9;
							}
							else
							{
								hundreds2--;
							}
							tens2 = tens;
							if( tens2 == 0 )
							{
								tens2 = 9;
							}
							else
							{
								tens2--;
							}
							units2 = units;
							if( units2 == 0 )
							{
								units2 = 9;
							}
							else
							{
								units2--;
							}
							mysprintf( s3, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
						}
						else if( Dy < -5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[0] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN group value
							group2 = group;
							hundreds2 = hundreds;
							if( hundreds2 == 0 )
							{
								hundreds2 = 9;
							}
							else
							{
								hundreds2--;
							}
							tens2 = tens;
							if( tens2 == 0 )
							{
								tens2 = 9;
							}
							else
							{
								tens2--;
							}
							units2 = units;
							if( units2 == 0 )
							{
								units2 = 9;
							}
							else
							{
								units2--;
							}
							mysprintf( s3, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
							// up group
							if( ++group >= NUM_GROUP_PAR )
							{
								group = 0;
							}
							// update the scroll UP group value
							group2 = group;
							if( ++group2 >= NUM_GROUP_PAR )
							{
								group2 = 0;
							}
							hundreds2 = hundreds;
							if( ++hundreds2 > 9 )
							{
								hundreds2 = 0;
							}
							tens2 = tens;
							if( ++tens2 > 9 )
							{
								tens2 = 0;
							}
							units2 = units;
							if( ++units2 > 9 )
							{
								units2 = 0;
							}
							mysprintf( s4, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[0] = 0;
			}
		}
		else if( tagval == TAG_VSCROLL_BASE+1 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset groups vertical scroll
			scrollON[0] = 0;
			// reset tens vertical scroll
			scrollON[2] = 0;
			// reset units vertical scroll
			scrollON[3] = 0;
			// hundreds vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[1] )
				{
				default:
				case 0:
					preYscroll[1] = pt->y;
					scrollON[1] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[1];
						if( Dy > 5 )
						{
							scrollON[1] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[1] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP hundreds value
							group2 = group;
							if( ++group2 >= NUM_GROUP_PAR )
							{
								group2 = 0;
							}
							hundreds2 = hundreds;
							tens2 = tens;
							if( ++tens2 > 9 )
							{
								tens2 = 0;
							}
							units2 = units;
							if( ++units2 > 9 )
							{
								units2 = 0;
							}
							mysprintf( s4, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
							// down hundreds
							if( hundreds == 0 )
							{
								hundreds = 9;
							}
							else
							{
								hundreds--;
							}
							// update the scroll DOWN hundreds value
							group2 = group;
							if( group2 == 0 )
							{
								group2 = NUM_GROUP_PAR - 1;
							}
							else
							{
								group2--;
							}
							hundreds2 = hundreds;
							if( hundreds2 == 0 )
							{
								hundreds2 = 9;
							}
							else
							{
								hundreds2--;
							}
							tens2 = tens;
							if( tens2 == 0 )
							{
								tens2 = 9;
							}
							else
							{
								tens2--;
							}
							units2 = units;
							if( units2 == 0 )
							{
								units2 = 9;
							}
							else
							{
								units2--;
							}
							mysprintf( s3, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
						}
						else if( Dy < -5 )
						{
							scrollON[1] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[1] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN hundreds value
							group2 = group;
							if( group2 == 0 )
							{
								group2 = NUM_GROUP_PAR - 1;
							}
							else
							{
								group2--;
							}
							hundreds2 = hundreds;
							tens2 = tens;
							if( tens2 == 0 )
							{
								tens2 = 9;
							}
							else
							{
								tens2--;
							}
							units2 = units;
							if( units2 == 0 )
							{
								units2 = 9;
							}
							else
							{
								units2--;
							}
							mysprintf( s3, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
							// up hundreds
							if( ++hundreds > 9 )
							{
								hundreds = 0;
							}
							// update the scroll UP hundreds value
							group2 = group;
							if( ++group2 >= NUM_GROUP_PAR )
							{
								group2 = 0;
							}
							hundreds2 = hundreds;
							if( ++hundreds2 > 9 )
							{
								hundreds2 = 0;
							}
							tens2 = tens;
							if( ++tens2 > 9 )
							{
								tens2 = 0;
							}
							units2 = units;
							if( ++units2 > 9 )
							{
								units2 = 0;
							}
							mysprintf( s4, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[1] = 0;
			}
		}
		else if( tagval == TAG_VSCROLL_BASE+2 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset groups vertical scroll
			scrollON[0] = 0;
			// reset hundreds vertical scroll
			scrollON[1] = 0;
			// reset units vertical scroll
			scrollON[3] = 0;
			// tens vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[2] )
				{
				default:
				case 0:
					preYscroll[2] = pt->y;
					scrollON[2] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[2];
						if( Dy > 5 )
						{
							scrollON[2] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[2] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP tens value
							group2 = group;
							if( ++group2 >= NUM_GROUP_PAR )
							{
								group2 = 0;
							}
							hundreds2 = hundreds;
							if( ++hundreds2 > 9 )
							{
								hundreds2 = 0;
							}
							tens2 = tens;
							units2 = units;
							if( ++units2 > 9 )
							{
								units2 = 0;
							}
							mysprintf( s4, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
							// down tens
							if( tens == 0 )
							{
								tens = 9;
							}
							else
							{
								tens--;
							}
							// update the scroll DOWN tens value
							group2 = group;
							if( group2 == 0 )
							{
								group2 = NUM_GROUP_PAR - 1;
							}
							else
							{
								group2--;
							}
							hundreds2 = hundreds;
							if( hundreds2 == 0 )
							{
								hundreds2 = 9;
							}
							else
							{
								hundreds2--;
							}
							tens2 = tens;
							if( tens2 == 0 )
							{
								tens2 = 9;
							}
							else
							{
								tens2--;
							}
							units2 = units;
							if( units2 == 0 )
							{
								units2 = 9;
							}
							else
							{
								units2--;
							}
							mysprintf( s3, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
						}
						else if( Dy < -5 )
						{
							scrollON[2] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[2] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN tens value
							group2 = group;
							if( group2 == 0 )
							{
								group2 = NUM_GROUP_PAR - 1;
							}
							else
							{
								group2--;
							}
							hundreds2 = hundreds;
							if( hundreds2 == 0 )
							{
								hundreds2 = 9;
							}
							else
							{
								hundreds2--;
							}
							tens2 = tens;
							units2 = units;
							if( units2 == 0 )
							{
								units2 = 9;
							}
							else
							{
								units2--;
							}
							mysprintf( s3, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
							// up tens
							if( ++tens > 9 )
							{
								tens = 0;
							}
							// update the scroll UP tens value
							group2 = group;
							if( ++group2 >= NUM_GROUP_PAR )
							{
								group2 = 0;
							}
							hundreds2 = hundreds;
							if( ++hundreds2 > 9 )
							{
								hundreds2 = 0;
							}
							tens2 = tens;
							if( ++tens2 > 9 )
							{
								tens2 = 0;
							}
							units2 = units;
							if( ++units2 > 9 )
							{
								units2 = 0;
							}
							mysprintf( s4, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[2] = 0;
			}
		}
		else if( tagval == TAG_VSCROLL_BASE+3 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// reset groups vertical scroll
			scrollON[0] = 0;
			// reset hundreds vertical scroll
			scrollON[1] = 0;
			// reset tens vertical scroll
			scrollON[2] = 0;
			// units vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[3] )
				{
				default:
				case 0:
					preYscroll[3] = pt->y;
					scrollON[3] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[3];
						if( Dy > 5 )
						{
							scrollON[3] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[3] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll UP units value
							group2 = group;
							if( ++group2 >= NUM_GROUP_PAR )
							{
								group2 = 0;
							}
							hundreds2 = hundreds;
							if( ++hundreds2 > 9 )
							{
								hundreds2 = 0;
							}
							tens2 = tens;
							if( ++tens2 > 9 )
							{
								tens2 = 0;
							}
							units2 = units;
							mysprintf( s4, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
							// down units
							if( units == 0 )
							{
								units = 9;
							}
							else
							{
								units--;
							}
							// update the scroll DOWN units value
							group2 = group;
							if( group2 == 0 )
							{
								group2 = NUM_GROUP_PAR - 1;
							}
							else
							{
								group2--;
							}
							hundreds2 = hundreds;
							if( hundreds2 == 0 )
							{
								hundreds2 = 9;
							}
							else
							{
								hundreds2--;
							}
							tens2 = tens;
							if( tens2 == 0 )
							{
								tens2 = 9;
							}
							else
							{
								tens2--;
							}
							units2 = units;
							if( units2 == 0 )
							{
								units2 = 9;
							}
							else
							{
								units2--;
							}
							mysprintf( s3, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
						}
						else if( Dy < -5 )
						{
							scrollON[3] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[3] = pt->y;
							
							userSubState = 1;	// parameter modify
							// update the scroll DOWN units value
							group2 = group;
							if( group2 == 0 )
							{
								group2 = NUM_GROUP_PAR - 1;
							}
							else
							{
								group2--;
							}
							hundreds2 = hundreds;
							if( hundreds2 == 0 )
							{
								hundreds2 = 9;
							}
							else
							{
								hundreds2--;
							}
							tens2 = tens;
							if( tens2 == 0 )
							{
								tens2 = 9;
							}
							else
							{
								tens2--;
							}
							units2 = units;
							mysprintf( s3, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
							// up units
							if( ++units > 9 )
							{
								units = 0;
							}
							// update the scroll UP units value
							group2 = group;
							if( ++group2 >= NUM_GROUP_PAR )
							{
								group2 = 0;
							}
							hundreds2 = hundreds;
							if( ++hundreds2 > 9 )
							{
								hundreds2 = 0;
							}
							tens2 = tens;
							if( ++tens2 > 9 )
							{
								tens2 = 0;
							}
							units2 = units;
							if( ++units2 > 9 )
							{
								units2 = 0;
							}
							mysprintf( s4, sFmt,
										Group_tab[group2].cId,
										Hex2Ascii(hundreds2),
										Hex2Ascii(tens2),
										Hex2Ascii(units2)
										);
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[3] = 0;
			}
		}
		else if( (scrollON[0] != 2)
				&& (scrollON[1] != 2)
				&& (scrollON[2] != 2)
				&& (scrollON[3] != 2)
				)
		{
			Flag.popup = 0;
			tagval = Read_Keypad( pt );	// read the keys
			if( tagval )
			{
				// touched
				userCounterTime = USER_TIMEOUT_SHOW_60;
				if( Flag.Key_Detect )
				{
					// first touch
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
				/*else if( ++key_cnt >= VALID_TOUCHED_KEY )
				{
					// pressed item
					key_cnt = VALID_TOUCHED_KEY;
				}*/
				/* else if( ++key_cnt >= 100 )
				{
					// pressed item
					if( !(key_cnt % 5) )
					{
						// process the pressed item on continuous touch
						switch( tagval )
						{
						}
					}
				} */
			}
			else if( pt->tag )
			{
				// untouched
				if( key_cnt >= VALID_TOUCHED_KEY )
				{
					// flush the touch queue
					FlushTouchQueue();

					// process the pressed item on touch release
					switch( pt->tag )
					{
					}
				}
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}
		else
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}
	
	}

	if( s4 != NULL )
	{
		free( s4 );
	}
	if( s3 != NULL )
	{
		free( s3 );
	}
	
	return RetVal;
}

/*!
** \fn unsigned char userParamVis( void )
** \brief Handler function for the USER_STATE_PARAM_VIS User Interface state
** \return The next User Interface state 
**/
unsigned char userParamVis( void )
{
	unsigned char RetVal = USER_STATE_PARAM_VIS;
	osEvent event;
	TAG_STRUCT *pt = NULL;
	ft_uint16_t tagval = TOUCH_TAG_INIT;
	BM_HEADER * p_bmhdrQUIT;
	BM_HEADER * p_bmhdrOK;
	unsigned long key_cnt = 0;
	unsigned char loop = 1;
	short i;
	//const PARTAB_STRUCT *pParTab = &Param_tab[iPar];
	unsigned char scrollON[1] = { 0 };	//!< scrolling status
	unsigned short preYscroll[1] = { 0 };	//!< previous Y value for vertical scrolling
	short Dy = 0;
	
	// get bitmap QUIT into Graphic RAM
	p_bmhdrQUIT = res_bm_GetHeader( BITMAP_QUIT );
	// get bitmap OK into Graphic RAM
	p_bmhdrOK = res_bm_GetHeader( BITMAP_OK );

	if(
		!userIsReadOnlyVar( iPar )  	// read-only?
		// &&	(LivAccesso >= pParTab->login)			// accesso?
		)
	{
		// inizializza valore per scroll DOWN (decremento)
		userDecPar( iPar, 1 );
		userVisPar( iPar );
		strcpy( str3, str2 );
		userIncPar( iPar, 1 );
		
		// inizializza valore per scroll UP (incremento)
		userIncPar( iPar, 1 );
		userVisPar( iPar );
		strcpy( str4, str2 );
		userDecPar( iPar, 1 );
	}

	
	while( loop )
	{
		if( tagval == TOUCH_TAG_INIT )
		{
			// it's the first time: tag registration
			CTP_TagDeregisterAll();
			userCounterTime = USER_TIMEOUT_SHOW_60;
		}

		if( !((tagval == TAG_VSCROLL_BASE + 0) && (scrollON[0] == 1) && (userSubState == 1)) )
		{
			Ft_Gpu_CoCmd_Dlstart(pHost);
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR_COLOR_RGB(0,0,0));	// Set the default clear color to black
			Ft_App_WrCoCmd_Buffer(pHost,CLEAR(1,1,1));	// Clear the screen
			Ft_App_WrCoCmd_Buffer(pHost,COLOR_RGB(0xFF,0xFF,0xFF));	// WHITE
			
			// draw QUIT bitmap
			// specify the starting address of the bitmap in graphics RAM
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrQUIT->GramAddr));
			// specify the bitmap format, linestride and height
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrQUIT->bmhdr.Format, p_bmhdrQUIT->bmhdr.Stride, p_bmhdrQUIT->bmhdr.Height));
			// set filtering, wrapping and on-screen size
			Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height));
			// start drawing bitmap
			Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
			Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( 0*16, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height)*16 ) );
			if( tagval == TOUCH_TAG_INIT )
			{
				// QUIT bitmap tag
				TagRegister( TAG_QUIT, 0, (FT_DispHeight-p_bmhdrQUIT->bmhdr.Height), p_bmhdrQUIT->bmhdr.Width, p_bmhdrQUIT->bmhdr.Height );
			}
			
			if(
				!userIsReadOnlyVar( iPar )  	// read-only?
				// &&	(LivAccesso >= pParTab->login)			// accesso?
				)
			{
				// draw OK bitmap
				// specify the starting address of the bitmap in graphics RAM
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SOURCE(p_bmhdrOK->GramAddr));
				// specify the bitmap format, linestride and height
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_LAYOUT(p_bmhdrOK->bmhdr.Format, p_bmhdrOK->bmhdr.Stride, p_bmhdrOK->bmhdr.Height));
				// set filtering, wrapping and on-screen size
				Ft_App_WrCoCmd_Buffer(pHost,BITMAP_SIZE(BILINEAR, BORDER, BORDER, p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height));
				// start drawing bitmap
				Ft_App_WrCoCmd_Buffer(pHost,BEGIN(BITMAPS));
				Ft_App_WrCoCmd_Buffer(pHost, VERTEX2F( (FT_DispWidth-p_bmhdrOK->bmhdr.Width)*16, (FT_DispHeight-p_bmhdrOK->bmhdr.Height)*16 ) );
				if( tagval == TOUCH_TAG_INIT )
				{
					// OK bitmap tag
					TagRegister( TAG_OK, (FT_DispWidth-p_bmhdrOK->bmhdr.Width), (FT_DispHeight-p_bmhdrOK->bmhdr.Height), p_bmhdrOK->bmhdr.Width, p_bmhdrOK->bmhdr.Height );
				}
			}

			/* description *
			if( sDesc != NULL )
			{
				if( (i = GetFont4StringWidth( (char *)sDesc, fontTitle, FT_DispWidth )) > 0 )
				{
					Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 50, i, OPT_CENTER, String2Font(sDesc, i) );
				}
			}	**/
			
			// draw the centered parameter value
			userVisPar( iPar );
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColor_tab[UserColorId].addon) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 160, fontMax, OPT_CENTER, String2Font(str2, fontMax) );
			
			if( userSubState == 1 )	// parameter modify?
			{
				// draw the scroll DOWN parameter value
				Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
				Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 160-25-5-14, fontTitle, OPT_CENTER, String2Font(str3, fontTitle) );

				// draw the scroll UP parameter value
				Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
				Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 160+25+5+14, fontTitle, OPT_CENTER, String2Font(str4, fontTitle) );

				if( tagval == TOUCH_TAG_INIT )
				{
					// digit scroll tag
					TagRegister( TAG_VSCROLL_BASE+0, (FT_DispWidth/2-20), 31, 40, FT_DispHeight-30 );
				}
			}
			
			// waiting pop-up window for the setting result
			if( Flag.popup  )
			{
				ShowWaitSetResultPopup();
			}
					
			// title
			Ft_App_WrCoCmd_Buffer( pHost, Get_UserColorHex(UserColorId) );
			Ft_App_WrCoCmd_Buffer( pHost, LINE_WIDTH( 1*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, BEGIN(RECTS) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( 0*16, 0*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, VERTEX2F( (FT_DispWidth-1)*16, (30)*16 ) );
			Ft_App_WrCoCmd_Buffer( pHost, COLOR_RGB( 255, 255, 255 ) );	// Text Color
			Ft_Gpu_CoCmd_Text( pHost, FT_DispWidth/2, 30/2, fontTitle, OPT_CENTER, String2Font(sDescPar, fontTitle) );

			Ft_App_WrCoCmd_Buffer( pHost, DISPLAY() );
			Ft_Gpu_CoCmd_Swap( pHost );
			Ft_App_Flush_Co_Buffer( pHost );
			Ft_Gpu_Hal_WaitCmdfifo_empty( pHost );
		}

		do
		{
			Wdog();
			if( Flag.popup	)
			{
				// wait for a setting result
				Ft_Gpu_Hal_Sleep(USER_TIME_TICK);
			
				// get the current setting result state
				i = OT_GetResult();
				if( (i == OT_RESULT_OK) || (i == OT_RESULT_FAILED) )
				{
					// received setting result
					WaitSetResult.result = i;
					WaitSetResult.Time = 2000/USER_TIME_TICK;	// 2s for viewing result
				}
				
				// check for pop-up window timeout
				if( WaitSetResult.Time )
				{
					WaitSetResult.Time--;
				}
				if( WaitSetResult.Time == 0 )
				{
					// disable pop-up window
					Flag.popup = 0;
				}
				
				// flush the touch queue
				FlushTouchQueue();
				
				// simulate a touch timeout event
				if( userCounterTime <= 1 )
				{
					userCounterTime = USER_TIMEOUT_SHOW_60;
				}
				event.status = osEventTimeout;
			}
			else
			{
				// wait for a touch event
				event = osMessageGet( TouchQueueHandle, USER_TIME_TICK );

				// process the touch event
				if( event.status == osEventMessage )
				{
					pt = (TAG_STRUCT *)event.value.v;
				}
			}
		} while( event.status == osOK );
			
		// event processing
		if( Check4StatusChange() != USER_MAX_STATES )
		{
			Flag.popup = 0;
			loop = 0;	// exit
		}
		else if( LoadFonts() > 0 )
		{
			// fonts updated -> restart window
			tagval = TOUCH_TAG_INIT;
			scrollON[0] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;
			Flag.popup = 0;
		}
		else if( event.status == osEventTimeout )
		{
			// tag reset
			tagval = 0;
			scrollON[0] = 0;
			Flag.Key_Detect = 0;
			key_cnt = 0;

			// controllo cambio di stato a timer
			if( userCounterTime )
			{
				if( --userCounterTime == 0 )
				{
					if( userSubState == 1 )
					{
						// modifica
						// ???
					}
					RetVal = userVisAvvio();
					loop = 0;	// exit
				}
				else
				{
					if( userSubState == 1 )
					{
						// modifica
						// ???
					}
					else
					{
						// visualizzazione
						if( !(userCounterTime % USER_TIMEOUT_SHOW_1) )
						{
							// recupera valore e limiti parametro
							userReqPar( iPar );
							userGetPar( iPar );
						}
					}
				}
			}
		}
		else if( (pt->tag == TAG_QUIT) && !pt->status )
		{
			// exit touch
			Flag.popup = 0;
			
			// flush the touch queue
			FlushTouchQueue();
			
			if( userSubState == 0 )
			{
				// quit parameter view
				RetVal = userVisReqParCod();
			}
			else
			{
				// quit parameter modify
				userSubState = 0;
				userReqPar( iPar );
				userGetPar( iPar );
			}
			loop = 0;	// exit
		}
		else if( (pt->tag == TAG_OK) && !pt->status )
		{
			// SET touch
			Flag.popup = 0;
			
			// flush the touch queue
			FlushTouchQueue();

			if( userSubState == 1 )
			{
				// SET
				//userSetPar( iPar );
				WinSetParWaitResult( iPar,
									sDescPar,
									NULL );
				// exit parameter modify
				userSubState = 0;
				userReqPar( iPar );
				userGetPar( iPar );
			}
			else if(
					!userIsReadOnlyVar( iPar )  	// read-only?
					// &&	(LivAccesso >= pParTab->login)			// accesso?
					)
			{
				// ingresso in modifica
				userCounterTime = USER_TIMEOUT_SHOW_60;
				userSubState = 1;	// parameter modify
				userGetPar( iPar );
				if( iPar == P_ORESAT )
				{
					// ore service solo azzerabili
					userParValue.w[0] = 0;
				}
			}
			loop = 0;	// exit
		}
		else if( tagval == TAG_VSCROLL_BASE+0 )
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			// digit vertical scroll
			if( pt->status )
			{
				// touched
				switch( scrollON[0] )
				{
				default:
				case 0:
					preYscroll[0] = pt->y;
					scrollON[0] = 1;
					break;

				case 1:
				case 2:
					if( key_cnt < 5 )
					{
						Dy = pt->y - preYscroll[0];
						if( Dy > 5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll up
							preYscroll[0] = pt->y;
							
							if( userSubState == 0 )
							{
								userSubState = 1;	// parameter modify
								//userGetPar( iPar ); userParValue.b[0] gia' inizializzato
							}
							// update the scroll UP parameter value
							strcpy( str4, str2 );
							// down 
							userDecPar( iPar, 1 );
							// update the scroll DOWN parameter value
							userDecPar( iPar, 1 );
							userVisPar( iPar );
							strcpy( str3, str2 );
							userIncPar( iPar, 1 );
						}
						else if( Dy < -5 )
						{
							scrollON[0] = 2;
							Flag.Key_Detect = 0;
							key_cnt = 0;
							// scroll down
							preYscroll[0] = pt->y;
							
							if( userSubState == 0 )
							{
								userSubState = 1;	// parameter modify
								//userGetPar( iPar ); userParValue.b[0] gia' inizializzato
							}
							// update the scroll DOWN parameter value
							strcpy( str3, str2 );
							// up 
							userIncPar( iPar, 1 );
							// update the scroll UP parameter value
							userIncPar( iPar, 1 );
							userVisPar( iPar );
							strcpy( str4, str2 );
							userDecPar( iPar, 1 );
						}
					}
					break;
				}
			}
			else
			{
				// untouched
				scrollON[0] = 0;
			}
		}
		else if( scrollON[0] != 2 )
		{
			Flag.popup = 0;
			tagval = Read_Keypad( pt );	// read the keys
			if( tagval )
			{
				// touched
				userCounterTime = USER_TIMEOUT_SHOW_60;
				if( Flag.Key_Detect )
				{
					// first touch
					Flag.Key_Detect = 0;
					key_cnt = 0;
				}
				/*else if( ++key_cnt >= VALID_TOUCHED_KEY )
				{
					// pressed item
					key_cnt = VALID_TOUCHED_KEY;
				}*/
				/* else if( ++key_cnt >= 100 )
				{
					// pressed item
					if( !(key_cnt % 5) )
					{
						// process the pressed item on continuous touch
						switch( tagval )
						{
						}
					}
				} */
			}
			else if( pt->tag )
			{
				// untouched
				if( key_cnt >= VALID_TOUCHED_KEY )
				{
					// flush the touch queue
					FlushTouchQueue();

					// process the pressed item on touch release
					switch( pt->tag )
					{
					}
				}
				Flag.Key_Detect = 0;
				key_cnt = 0;
			}
		}
		else
		{
			userCounterTime = USER_TIMEOUT_SHOW_20;
			Flag.Key_Detect = 0;
			key_cnt = 0;
		}

	}
	
	return RetVal;
}

