/*!
** \file printf-stdarg.h
** \author Alessandro Vagniluca
** \date 04/06/2015
** \brief Header for printf-stdarg.c module
** 
** \version 1.0
**/

#ifndef __PRINTF_STDARG_H
#define __PRINTF_STDARG_H

#ifdef __cplusplus
 extern "C" {
#endif

#define LOGGING		// enable debug/logging output
#ifdef LOGGING
#define LOGGING_UART6	// enable debug/logging output on UART6
#endif	// LOGGING

int myprintf(const char *format, ...);
int mysprintf(char *out, const char *format, ...);


#ifdef __cplusplus
}
#endif

#endif	// __PRINTF_STDARG_H

