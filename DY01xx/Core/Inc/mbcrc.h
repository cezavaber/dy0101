/*!
	\file mbrcr.h
	\brief Include file del modulo mbrcr.c

	Algoritmo di calcolo CRC16 per MODBUS-RTU.
*/

#ifndef _MB_CRC_H
#define _MB_CRC_H

/*!
	\fn unsigned short CalcCrcBuff( unsigned char *ptr, unsigned short len )
	\brief Calcola il CRC su un buffer
	\param ptr Puntatore al buffer
	\param len Dimensione buffer
	\return Il valore del CRC calcolato sul buffer
*/
unsigned short CalcCrcBuff( unsigned char *ptr, unsigned short len );

#endif
