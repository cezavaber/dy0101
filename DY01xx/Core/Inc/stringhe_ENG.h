/*!
** \file stringhe_ENG.h
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief STRINGHE_ENG.C header file
** 
** \version 1.0
**/

#ifndef _STRINGHE_ENG_H_
#define _STRINGHE_ENG_H_

#include "languages.h"

//!< font group for the ENG strings set
#define FONT_GROUP_ENG		(FONT_GROUP_EUROPE)

//!< string set descriptor for each font group
extern const char DescMsg_ENG[NUM_FONT_GROUP][MSGDESCLEN];

//!< ENG strings set
extern const PCSTR pStr_ENG[NUMMSG];



#endif	// _STRINGHE_ENG_H_

