/*!
** \file crono.h
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief CRONO.C header file
** 
** \version 1.0
**/

#ifndef __CRONO_H
#define __CRONO_H

//#define DEBUG_SLEEP	// abilita il debug impostazione SLEEP

#define PROG_OFF		255
 
// Struttura Crono programma settimanale:
typedef union
{
	unsigned char BYTE;
	struct
	{
		unsigned char EnabSunday		:1;
		unsigned char EnabMonday		:1;
		unsigned char EnabTuesday		:1;
		unsigned char EnabWednesday	:1;
		unsigned char EnabThursday		:1;
		unsigned char EnabFriday		:1;
		unsigned char EnabSaturday		:1;
		unsigned char WeekEnab			:1;
	} BIT;
} CRONO_WEEK_ENAB;
typedef struct
{
	unsigned char OraStart;		// 0-95 quarti d'ora (00.00 - 23.45), 255=off
	unsigned char OraStop;		// 1-96 quarti d'ora (00.15 - 24.00), 255=off
	unsigned char TempAria;		// setpoint temperatura aria
	unsigned char TempH2O;		// setpoint temperatura acqua
	unsigned char Fire;			// livello di potenza
	CRONO_WEEK_ENAB Enab;		// enable bitmap
} CRONO_PROG_WEEK;
 
#define NPROGWEEK		6

// profili crono utente preimpostati
typedef struct
{
	unsigned char OraStart;		// 0-95 quarti d'ora (00.00 - 23.45), 255=off
	unsigned char OraStop;		// 1-96 quarti d'ora (00.15 - 24.00), 255=off
	CRONO_WEEK_ENAB Enab;		// enable bitmap
} CRONO_PROFILE_PROG_WEEK;
typedef struct
{
	CRONO_PROFILE_PROG_WEEK CronoProfile_ProgWeek[NPROGWEEK];	// programmi settimanali
} CRONOPROFILE;
#define NUMCRONOPROFILE			10
extern const CRONOPROFILE CronoProfile[NUMCRONOPROFILE];

enum cromod
{
	CRONO_OFF = 0,
	CRONO_ON = 1,
	NUMCRONOMODE
};
typedef union
{
	unsigned char BYTE;
	struct
	{
		unsigned char iProfile		:7;	// indice profilo preimpostato corrente
		unsigned char Enab			:1;
	} BIT;
} CRONO_MODE;
typedef struct
{
	CRONO_MODE CronoMode;			// modo funzione CRONO
	CRONO_PROG_WEEK Crono_ProgWeek[NPROGWEEK];	// programmi settimanali
} CRONOSTRUCT;

#define SLEEP_OFF			255	// valore ora di sleep, se sleep off
typedef struct
{
	unsigned char Ore;
	unsigned char Minuti;
} SLEEPSTRUCT;
extern SLEEPSTRUCT Sleep;
#define SLEEP_MIN_SET_VALUE		0		// min sleep time: 00h 00'
#define SLEEP_MAX_SET_VALUE		142	// max sleep time (in decine di minuti): 23h 40'
#define SLEEP_OFF_VALUE				255



/***********************************************************************/
/*              PROTOTIPO FUNZIONI                                     */
/***********************************************************************/
void InitCrono( void );
void GestCronoSleep( void );
void AllineaStatoCrono( void );
void AttuaStatoCrono( void );
BYTE IsCronoProfile( void );
void LoadCronoProfile( BYTE iProfile );


#endif //__CRONO_H

