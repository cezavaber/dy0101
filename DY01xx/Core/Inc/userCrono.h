/*!
** \file userCrono.h
** \author Alessandro Vagniluca
** \date 02/03/2016
** \brief USERCRONO.C header file
** 
** \version 1.0
**/

#ifndef _USER_CRONO_H_
#define _USER_CRONO_H_



/*!
** \fn void userPostSetCronoPar( void )
** \brief Execute the Chrono post-setting
** \return None
**/
void userPostSetCronoPar( void );

/*!
** \fn unsigned char userVisCrono( void )
** \brief Starts the USER_STATE_CRONO User Interface state handling
** \return The next User Interface state 
**/
unsigned char userVisCrono( void );

/*!
** \fn unsigned char userCrono( void )
** \brief Handler function for the USER_STATE_CRONO User Interface state
** \return The next User Interface state 
**/
unsigned char userCrono( void );

/*!
** \fn void userToggleCronoEnable( void )
** \brief Toggle enable/disable Chrono
** \return None
**/
void userToggleCronoEnable( void );


#endif	// _USER_CRONO_H_
