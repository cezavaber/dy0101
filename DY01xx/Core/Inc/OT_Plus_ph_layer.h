/*!
	\file OT_Plus_ph_layer.h
	\brief Include file del modulo OT_Plus_ph_layer.c
*/

#ifndef _OT_PLUS_PH_LAYER_H
#define _OT_PLUS_PH_LAYER_H


#include "OT_Plus_tabelle.h"


/*!
	\enum PWR_MODE
	\brief Power mode
*/
enum PWR_MODE
{
	PWR_MODE_LOW,		//!< low power
	PWR_MODE_HIGH,		//!< high power
	NUM_PWR_MODE
};
/*!
	\var PwrMode
	\brief Indice power mode
*/
extern unsigned char PwrMode;

/*!
	\var rfID
	\brief Identificativo radio corrente
*/
extern unsigned char rfID;

/*!
	\def MAX_PACKET_SIZE
	\brief The maximum packet size (without CRC16).
*/
#define MAX_PACKET_SIZE				28

/*!
	\typedef void (*OTRxPkt)( unsigned char *pkt, unsigned short pktSize );
	\brief Puntatore a funzione call-back per ricezione pacchetto.
	\param pkt pacchetto ricevuto
	\param pktSize dimensione pacchetto
*/
typedef void (*OTRxPkt)( unsigned char *pkt, unsigned short pktSize );

/*!
	\enum RF_Channels
	\brief Enumerazione canali RF
*/
enum RF_Channels
{
	RF_CHAN_868_2_MHZ = 0x75,
	RF_CHAN_868_4_MHZ = 0x76,
	RF_CHAN_868_6_MHZ = 0x77,
	RF_CHAN_868_8_MHZ = 0x78,
	RF_CHAN_869_0_MHZ = 0x79,
	RF_CHAN_869_2_MHZ = 0x7A,
	RF_CHAN_869_4_MHZ = 0x7B,
	RF_CHAN_869_6_MHZ = 0x7C,
	RF_CHAN_869_8_MHZ = 0x7D,
	RF_CHAN_870_0_MHZ = 0x7E,
};
#define NUM_RF_CHAN		10

/*!
	\enum RF_Tx_Power
	\brief Enumerazione livelli di potenza TX
*/
enum RF_Tx_Power
{
	RF_TXPOWER_01_DBM = 0x00,
	RF_TXPOWER_02_DBM = 0x01,
	RF_TXPOWER_05_DBM = 0x02,
	RF_TXPOWER_08_DBM = 0x03,
	RF_TXPOWER_11_DBM = 0x04,
	RF_TXPOWER_14_DBM = 0x05,
	RF_TXPOWER_17_DBM = 0x06,
	RF_TXPOWER_20_DBM = 0x07,
	
};
#define NUM_RF_TXPOWER		8

/*!
	\struct RCQ2_868_struct
	\brief The RCQ2-868 config tag structure
*/
struct __packed RCQ2_868_struct
{
	unsigned long DestAddr;						//!< destination address (Big Endian)
	unsigned long RCQ2Addr;						//!< source address (Big Endian)
	unsigned char RFChan;						//!< RF channel
	unsigned char TXPower;						//!< RF TX power
	unsigned char TxDataPacketSize;				//!< Tx Data Packet Size (max 30)
	unsigned char RxDataPacketSize;				//!< Rx Data Packet Size (max 30)
};
/*!
	\typedef RCQ2_868_STRUCT
	\brief Typedef definition for RCQ2_868_struct
*/
typedef struct __packed RCQ2_868_struct RCQ2_868_STRUCT;


/*!
	\fn short OT_PH_Init( OTRxPkt Rx_CallBack )
   \brief Inizializzazione physical layer OT/+
	\param Rx_CallBack funzione call-back di ricezione
	\return 1: OK, 0: FAILED
*/
short OT_PH_Init( OTRxPkt Rx_CallBack );

/*!
	\fn short OT_PH_Send( unsigned char *pkt, unsigned short pktSize,
								unsigned char enCRC )
   \brief Invio di un pacchetto
	\param pkt pacchetto da inviare
	\param pktSize dimensione pacchetto
	\param enCRC flag di abilitazione aggiunta CRC16 al pacchetto
	\return 1: OK, 0: FAILED
*/
short OT_PH_Send( unsigned char *pkt, unsigned short pktSize, unsigned char enCRC );

/*!
	\fn void OT_ResetRX( void )
   \brief Reset della ricezione
*/
void OT_ResetRX( void );

/*!
	\fn void OT_ResetRXTX( void )
   \brief Reset della comunicazione
*/
void OT_ResetRXTX( void );

/*!
	\fn void OT_RxTimeout( void )
   \brief Gestione timeout ricezione carattere
*/
void OT_RxTimeout( void );

/*!
	\fn short OT_PH_GetRCQ2Cfg( RCQ2_868_STRUCT *pCfg )
   \brief Rileva la configurazione del radio-modem RCQ2-868
	\param pCfg La struttura di configurazione rilevata
	\return 1: OK, 0: RUNNING, -1: FAILED
*/
short OT_PH_GetRCQ2Cfg( RCQ2_868_STRUCT *pCfg );

/*!
	\fn short OT_PH_SetRCQ2Cfg( RCQ2_868_STRUCT *pCfg )
   \brief Imposta la configurazione del radio-modem RCQ2-868
	\param pCfg La struttura di configurazione da impostare
	\return 1: OK, 0: RUNNING, -1: FAILED
*/
short OT_PH_SetRCQ2Cfg( RCQ2_868_STRUCT *pCfg );

/*!
	\fn void OT_PH_SetLowPower( void )
   \brief Radio-modem RCQ2-868 in low-power
*/
void OT_PH_SetLowPower( void );

/*!
	\fn void OT_PH_SetFullPower( void )
   \brief Radio-modem RCQ2-868 in full-power
*/
void OT_PH_SetFullPower( void );




#endif	// _OT_PLUS_PH_LAYER_H
