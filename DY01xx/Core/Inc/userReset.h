/*!
** \file userReset.h
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief USERRESET.C header file
** 
** \version 1.0
**/

#ifndef _USER_RESET_H_
#define _USER_RESET_H_



/*!
** \fn unsigned char userVisReset( void )
** \brief Starts the USER_STATE_RESET User Interface state handling
** \return The next User Interface state 
**/
unsigned char userVisReset( void );

/*!
** \fn unsigned char userReset( void )
** \brief Handler function for the USER_STATE_RESET User Interface state
** \return The next User Interface state 
**/
unsigned char userReset( void );


#endif	// _USER_RESET_H_
