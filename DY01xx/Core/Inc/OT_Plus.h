/*!
	\file OT_Plus.h
	\brief Include file del modulo OT_Plus.c
*/

#ifndef _OT_PLUS_H
#define _OT_PLUS_H



#include "OT_Plus_data_layer.h"


#define MB_ADDRESS_BROADCAST    ( 0 )   /*! Modbus broadcast address. */
#define MB_ADDRESS_MIN          ( 1 )   /*! Smallest possible slave address. */
#define MB_ADDRESS_MAX          ( 247 ) /*! Biggest possible slave address. */

#define MB_ADDR		1		// indirizzo slave


/*!
	\def MB_FUNC_XXX
	\brief MODBUS and USER function codes.
*/
#define MB_FUNC_NONE                          (  0 )
#define MB_FUNC_READ_COILS                    (  1 )
//#define MB_FUNC_READ_DISCRETE_INPUTS          (  2 )
#define MB_FUNC_WRITE_SINGLE_COIL             (  5 )
#define MB_FUNC_WRITE_MULTIPLE_COILS          ( 15 )
#define MB_FUNC_READ_HOLDING_REGISTER         (  3 )
#define MB_FUNC_READ_INPUT_REGISTER           (  4 )
//#define MB_FUNC_WRITE_REGISTER                (  6 )
#define MB_FUNC_WRITE_MULTIPLE_REGISTERS      ( 16 )
//#define MB_FUNC_READWRITE_MULTIPLE_REGISTERS  ( 23 )
//#define MB_FUNC_DIAG_READ_EXCEPTION           (  7 )
//#define MB_FUNC_DIAG_DIAGNOSTIC               (  8 )
//#define MB_FUNC_DIAG_GET_COM_EVENT_CNT        ( 11 )
//#define MB_FUNC_DIAG_GET_COM_EVENT_LOG        ( 12 )
//#define MB_FUNC_OTHER_REPORT_SLAVEID          ( 17 )
#define MB_FUNC_ERROR                         ( 128 )

#define MB_FUNC_READ_LIMITS						(0x41)
#define MB_FUNC_UNLOCK								(0x42)
#define MB_FUNC_CMD_DEBUG							(0x43)
#define MB_FUNC_SUONO_RC							(0x44)
#define MB_FUNC_TYPE_SET							(0x45)

/*!
	\enum eSuonoRC
	\brief Modalita` riproduzione suono RC
*/
enum eSuonoRC
{
	BUZ_NONE,
	BUZ_WAIT1SHORT,	// n.a.
	BUZ_1SHORT,
	BUZ_WAIT1LONG,		// n.a.
	BUZ_1LONG,
	BUZ_2SHORT,
	BUZ_1SHORT_1LONG,
	BUZ_MAX
};

/*!
	\enum eMBException
	\brief MODBUS exception codes
*/
enum eMBException
{
	MB_EX_NONE = 0x00,
	MB_EX_ILLEGAL_FUNCTION = 0x01,
	MB_EX_ILLEGAL_DATA_ADDRESS = 0x02,
	MB_EX_ILLEGAL_DATA_VALUE = 0x03,
	MB_EX_SLAVE_DEVICE_FAILURE = 0x04,
	MB_EX_ACKNOWLEDGE = 0x05,
	MB_EX_SLAVE_BUSY = 0x06,
	MB_EX_MEMORY_PARITY_ERROR = 0x08,
	MB_EX_GATEWAY_PATH_FAILED = 0x0A,
	MB_EX_GATEWAY_TGT_FAILED = 0x0B
};


/*!
	\struct OTPkt_struct
	\brief The general OT packet tag structure
*/
struct OTPkt_struct
{
	unsigned short pktLen;		//!< packet length
	unsigned char pkt[1];		//!< packet buffer (variable length)
};
/*!
	\typedef OTPKT_STRUCT
	\brief Typedef definition for OTPkt_struct
*/
typedef struct OTPkt_struct OTPKT_STRUCT;

typedef const OTPKT_STRUCT	CREQ;
typedef const CREQ*	PCREQ;
typedef const PCREQ*	PPCREQ;


/*!
	\var reqXXX
	\brief Richieste Fisse in formato OTPKT_STRUCT corrispondente a PKT_STRUCT
*/
typedef struct
{
	unsigned short pktLen;		//!< packet length
	unsigned char pkt[7];		//!< packet buffer
} REQ_STATO;
typedef REQ_STATO		REQ_LIMITS;
extern const REQ_STATO reqVerFW;
extern const REQ_STATO reqLinguaDef;
extern const REQ_STATO reqTipoStufa;
extern const REQ_STATO reqCoils;
extern const REQ_STATO reqInputStato_Misure_1;
extern const REQ_STATO reqInputStato_Misure_2;
extern const REQ_STATO reqPassword;
extern const REQ_STATO reqLogo;
extern const REQ_STATO reqTipoDesc_1_2;
extern const REQ_STATO reqTipoDesc_3_4;
extern const REQ_STATO reqTipoDesc_5_6;
extern const REQ_STATO reqTipoDesc_7_8;
extern const REQ_STATO reqEcoBmap_NFan;
extern const REQ_STATO reqTermAmbBmap;
extern const REQ_STATO reqAccumBmap;
extern const REQ_STATO reqFireFan;
extern const REQ_LIMITS reqLimitsFireFan;
extern const REQ_STATO reqSoundAlarmBmap;
extern const REQ_STATO reqWiFiBmap;
extern const REQ_STATO reqMaxPower;
extern const REQ_STATO reqReleTermBmap;
extern const REQ_STATO reqTempPx;
extern const REQ_STATO reqConfigIdro;
extern const REQ_STATO reqISOCode;
extern const REQ_STATO reqIPaddr;



/*!
	\enum SEND_REQ_SET
	\brief Gruppi di richieste a invio ciclico
*/
enum SEND_REQ_SET
{
	SEND_REQ_SET_STATUS,
	//
	NUM_SEND_REQ_SET
};


/*!
	\enum ot_stat_enum
	\brief Codici stato gestore comunicazione
*/
enum ot_stat_enum
{
	OT_STAT_UNLINKED,
	//OT_STAT_UNLINKED_STBY,
	OT_STAT_UNLINKED_RADIO_GET_CONFIG,
	//OT_STAT_UNLINKED_STBY_RADIO_GET_CONFIG,
	OT_STAT_UNLINKED_RADIO_SET_CONFIG,
	//OT_STAT_UNLINKED_STBY_RADIO_SET_CONFIG,
	OT_STAT_UNLINKED_READ_VERFW,
	OT_STAT_UNLINKED_WRITE_VERFW,
	OT_STAT_LINKED_WRITE_RFID,
	OT_STAT_LINKED,
	OT_STAT_UNLINKED_STBY_WAIT,
	NUM_OT_STAT
};
/*!
	\typedef OT_STAT
	\brief Typedef definition for ot_stat_enum
*/
typedef enum ot_stat_enum	OT_STAT;
/*!
	\var OT_Stat
	\brief Stato gestore comunicazione
*/
extern OT_STAT OT_Stat;
extern OT_STAT OT_NextStat;	// stato gestore in uscita dalla pausa standby

/*!
	\var OT_LimitStep_Requested
	\brief Bitmap di flag limiti/step richiesti per i Holding Registers
*/
#define LIMSTEP_REQ_BMAP_SIZE		(THOLREG_NREG/8+1)
extern unsigned char OT_LimitStep_Requested[LIMSTEP_REQ_BMAP_SIZE];

extern unsigned char iLimReq;	//!< indice richiesta Limiti/Step


/*! \enum idCmdDebug
    \brief codici comandi di debug
*/
enum idCmdDebug
{
	CMDDEBUG_NO_COMANDO,
	CMDDEBUG_RIPRISTINO_VELOCE,
	CMDDEBUG_BYPASS_ACC,
	CMDDEBUG_CALIB_TC,
	CMDDEBUG_EN_TEST,
	CMDDEBUG_TEST_COCLEA,
	CMDDEBUG_TEST_ESPULSORE,
	CMDDEBUG_TEST_FAN1,
	CMDDEBUG_TEST_FAN2,
	CMDDEBUG_TEST_POMPA,
	CMDDEBUG_TEST_CAND,
	CMDDEBUG_CALIB_FOTORES_ON,
	CMDDEBUG_CALIB_FOTORES_OFF,
	CMDDEBUG_13,
	CMDDEBUG_14,
	CMDDEBUG_15,
	CMDDEBUG_TEST_FAN3,
	CMDDEBUG_READ_EE,
	CMDDEBUG_INTERP,
	CMDDEBUG_NETWORK,
	NIDCMDDEBUG
};

/*! \struct strBLOCCO_83
    \brief Struttura del Blocco 83
*/
struct strBLOCCO_83
{
	unsigned char CodeDebug;			//!< codice comando di debug
	unsigned char b[7];
};
/*! \typedef BLOCCO_83
    \brief Typedef per strBLOCCO_83
*/
typedef struct strBLOCCO_83 BLOCCO_83;


/*!
	\enum ot_result_enum
	\brief Codici di esito richiesta
*/
enum ot_result_enum
{
	OT_RESULT_IDLE,
	OT_RESULT_RUN,
	OT_RESULT_OK,
	OT_RESULT_FAILED,
	NUM_OT_RESULT
};
/*!
	\typedef OT_RESULT
	\brief Typedef definition for ot_result_enum
*/
typedef enum ot_result_enum	OT_RESULT;
/*!
	\var OT_Result
	\brief Esito richiesta
*/
extern OT_RESULT OT_Result;

extern short PreTempAmbPAN;	//!< valore precedente temperatura ambiente in 0.1�C


/*!
	\fn short OT_Init( void )
   \brief Inizializzazione gestore comunicazione OT/+
	\return 1: OK, 0: FAILED
*/
short OT_Init( void );

/*!
	\fn void OT_Handler( void )
	\brief Gestore comunicazione OT/+
*/
void OT_Handler( void );

/*!
	\fn short OT_Send_SetReq( unsigned short iSetReq )
   \brief Predispone l'invio ciclico del gruppo di richieste fisse indicato.
	\param iSetReq indice del gruppo di richieste fisse
	\return 1: OK, 0: FAILED
*/
short OT_Send_SetReq( unsigned short iSetReq );

/*!
	\fn short OT_Send( PKT_STRUCT *sPkt, unsigned char mode, unsigned char esito )
   \brief Invio di un pacchetto
	\param pkt struttura pacchetto da inviare
	\param mode modo di invio (SEND_MODE_ONE_SHOT o SEND_MODE_CYCLE)
	\param esito 1: richiede l'esito dell'invio (solo per SEND_MODE_ONE_SHOT)
	\return 1: OK, 0: FAILED
	\note I pacchetti con invio unico sono prioritari rispetto a quelli
			con invio ciclico.
			La struttura del pacchetto con invio unico viene ricopiata dalla
			funzione nella propria coda e puo` quindi essere allocata in modo
			dinamico.
			La struttura del pacchetto con invio ciclico viene registrata dalla
			funzione come puntatore nella propria coda e deve quindi essere
			allocata in modo statico.
*/
short OT_Send( PKT_STRUCT *sPkt, unsigned char mode, unsigned char esito );

/*!
	\fn void OT_ReqSound( unsigned char SoundCode )
   \brief Invio comando di generazione suono
	\param SoundCode Codice suono
*/
void OT_ReqSound( unsigned char SoundCode );

/*!
	\fn OT_RESULT OT_GetResult( void )
	\brief Ritorna l'esito dell'invio registrato
	\return Il codice di esito
*/
OT_RESULT OT_GetResult( void );

/*!
	\fn void OT_RestartRequests( void )
   \brief Riavvia la sequenza delle richieste come alla connessione
*/
void OT_RestartRequests( void );

/*!
	\fn short SendVerFW( void )
   \brief Invio versione FW
	\return 1: OK, 0: FAILED
*/
short SendVerFW( void );

/*!
	\fn void OT_SetETime( unsigned long ETime )
   \brief Invio comando di impostazione Epoch Time
	\param ETime Valore Epoch Time
*/
void OT_SetETime( unsigned long ETime );

/*!
	\fn void OT_ExitStandby( void )
	\brief Uscita dallo stato di standby
*/
void OT_ExitStandby( void );



#endif	// _OT_PLUS_H
