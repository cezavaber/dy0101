/*!
** \file fonts.h
** \author Alessandro Vagniluca
** \date 15/10/2015
** \brief FONTS.C header file
** 
** \version 1.0
**/

#ifndef _FONTS_H_
#define _FONTS_H_

#include "FT_Gpu.h"
#include "FT_Gpu_Hal.h"
#include "FT_Hal_Utils.h"
#include "FT_CoPro_Cmds.h"

// font allocation in the data-flash
#define FONT_HEADER_ARRAY_ADDR		(0x00320000)	//!< font header array start address
#define FONT_RAW_DATA_ADDR				(0x00322000)	//!< fonts raw data start address
#define FONT_MAX_ADDR					(0x003BFFFF)	//!< fonts raw data end address
#define FONT_FIRST_64K_SECT			50		//!< the first 64KB sector for fonts
#define FONT_NUM_64K_SECT				10		//!< 64KB sectors number for fonts

// font allocation in the Graphic RAM
#define FONT_GRAM_BASE_ADDR			(RAM_G)


/*!
** \typedef Font_header_t
** \brief The font metric blocks structure.
**/
typedef struct __packed Font_header
{
	ft_uint16_t Font_Group;			//!< font group index
	FT_Gpu_Fonts_t FontTable;		//!< font metric block
	ft_uint32_t Font_RawData;		//!< font bitmap raw data address in the data-flash
	ft_uint32_t Font_RawDataSize;	//!< font bitmap raw data size
	ft_uint32_t FontTableAddr;		//!< font metric block GRAM address
} Font_header_t;

/*!
** Metric block and address of Bitmap raw data.
** 
** The metric block is totally 148 bytes and its format can be found in programmer guide.
** Please note the 4 bytes of metric block from offset 144 specifies the address of bitmap
** raw data of characters in FT800 RAM_G(in Little Endian). We call it 'AddressInMetricBlk' below.
** 
** AddressInMetricBlk is determined in different way at '-u' and '-a' argument case:
** '-u':  AddressInMetricBlk = FT800_address + GRAMG + 148 - stride * Height * 1
** '-a':  AddressInMetricBlk = FT800_address + GRAMG + 148 - stride * Height * 32
**/

/*!
** \enum FONT_TAB_INDEX
** \brief The fonts' enumeration
**/
enum FONT_TAB_INDEX
{
	FONT_ARIAL_L1_12,	//!< Arial L1 maxWidth=12
	FONT_ARIAL_L1_16,	//!< Arial L1 maxWidth=16
	FONT_ARIAL_L1_18,	//!< Arial L1 maxWidth=18
	FONT_ARIAL_L1_22,	//!< Arial L1 maxWidth=22
	FONT_ARIAL_L1_28,	//!< Arial L1 maxWidth=28
	FONT_ARIAL_L1_36,	//!< Arial L1 maxWidth=36
	NFONT_PER_GROUP	//!< number of fonts in a group
};

/*!
** \enum FONT_GROUP
** \brief The fonts' groups
**/
enum FONT_GROUP
{
	FONT_GROUP_EUROPE,		//!< latin europe
	FONT_GROUP_CYRILLIC,	//!< cyrillic
	FONT_GROUP_2,	//!< 
	FONT_GROUP_3,	//!< 
	
	NUM_FONT_GROUP
};

extern unsigned char NumFontGroup;	//!< number of fonts' groups



/*!
** \var Font_RawData_Header
** \brief The font metric blocks.
**/
extern Font_header_t Font_RawData_Header;

/*!
** \var RAMFontTableAddr
** \brief The RAM font metric blocks GRAM address.
**/
#define NUM_RAM_FONT			15
extern ft_uint32_t RAMFontTableAddr[NUM_RAM_FONT];


#endif	// _FONTS_H_
