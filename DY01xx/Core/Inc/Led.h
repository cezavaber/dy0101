/*!
**  \file Led.h
**  \author Alessandro Vagniluca
**  \date 24/07/2015
**  \brief Header file for LED.C source module
**  
**  \version 1.0
**/

#ifndef _LED_H
#define _LED_H

#include "main.h"

/*!
** \enum LedStat_enum
** \brief Led states' enumeration
**/
enum LedStat_enum
{
	LEDSTAT_OFF,
	LEDSTAT_ON,
	LEDSTAT_LOW_BLINK,
	LEDSTAT_BLINK,
	LEDSTAT_HIGH_BLINK,
	LEDSTAT_VERY_HIGH_BLINK,
	LEDSTAT_1_IMPULSE_SHORT,
	LEDSTAT_1_IMPULSE,
	LEDSTAT_1_IMPULSE_LONG,
	LEDSTAT_MANUAL,
	NLEDSTAT
};

/*!
** \enum LedIndex_enum
** \brief Led indexes' enumeration
**/
enum LedIndex_enum
{
	BUZ,
	NLED
};

/*!
** \enum LedCmd_enum
** \brief Led commands' enumeration
**/
enum LedCmd_enum
{
	LED_OFF = 0,
	LED_ON = 1,
	NCMDLED
};
#define BUZ_OFF		(LED_OFF)
#define BUZ_ON		(LED_ON)

/*!
** \def BSP_DLx_OnOff( LedStat )
** \brief Macro to set DLx led on or off
**/
#define BSP_BUZ_OnOff( LedStat )	(HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, LedStat ? GPIO_PIN_SET : GPIO_PIN_RESET))
/*!
** \def BSP_DLx_Toggle
** \brief Macro to toggle DLx led
**/
#define BSP_BUZ_Toggle	(HAL_GPIO_TogglePin(BUZZER_GPIO_Port, BUZZER_Pin))



/*! 
** \fn void led_Init( void )
** \brief Init led handler
** \return None
**/
void led_Init( void );

/*!
** \fn void led_Handler(void)
** \brief Led handler
** \return None
**/
void led_Handler( void );

/*!
** \fn short led_Set( unsigned char iLed, unsigned char mode )
** \brief Set the led state
** \param iLed led index
** \param mode led mode
** \return 1: OK, 0: FAILED
**/
short led_Set( unsigned char iLed, unsigned char mode );




#endif	// _LED_H
