/*!
	\file OT_Plus_tabelle.h
	\brief Include file del modulo OT_Plus_tabelle.c
*/

#ifndef _OT_PLUS_TABELLE_H
#define _OT_PLUS_TABELLE_H


#include "DatiStufa.h"


// definizione range indirizzi tabella Coils
#define TCOIL_FIRST_ADDR		0x0000
#define TCOIL_END_ADDR			(0x0001+1)
#define TCOIL_NREG				(TCOIL_END_ADDR-TCOIL_FIRST_ADDR)

// definizione range indirizzi tabella Input Registers
#define TINPREG_FIRST_ADDR		0x0000
#define TINPREG_END_ADDR		(0x0073+1)
#define TINPREG_NREG				(TINPREG_END_ADDR-TINPREG_FIRST_ADDR)

// definizione range indirizzi tabella Holding Registers
#define THOLREG_FIRST_ADDR		0x0000
#define THOLREG_END_ADDR		(0x0148+1)
#define THOLREG_NREG				(THOLREG_END_ADDR-THOLREG_FIRST_ADDR)

// definizione range indirizzi tabella Holding Registers 2
#define THOLREG2_FIRST_ADDR	0x0400
#define THOLREG2_END_ADDR		(0x0443+1)
#define THOLREG2_NREG			(THOLREG2_END_ADDR-THOLREG2_FIRST_ADDR)

// definizione range indirizzi tabella Holding Registers 3
#define THOLREG3_FIRST_ADDR	0x2000
#define THOLREG3_END_ADDR		(0x2081+1)
#define THOLREG3_NREG			(THOLREG3_END_ADDR-THOLREG3_FIRST_ADDR)

/*!
	\enum Type_MBVal_enum
	\brief Tipi di dati MB
*/
enum Type_MBVal_enum
{
	MBTYPE_BOOL,
	MBTYPE_CHAR,
	MBTYPE_BYTE,
	MBTYPE_WORD,
	MBTYPE_BITMAP,
	MBTYPE_BYTE_MASK,
	MBTYPE_INT,
	MBTYPE_DWORD,
	MBTYPE_LONG,
	MBTYPE_FLOAT,
	NMBTYPE
};
/*!
	\typedef TYPE_MBVAL
	\brief Typedef definition for Type_MBVal_enum
*
typedef enum Type_MBVal_enum	TYPE_MBVAL;	*/
/*!
	\struct MBData_struct
	\brief Struttura per dato in tabella MB
*/
struct MBData_struct
{
	unsigned char Tipo;		//!< tipo valore (TYPE_MBVAL)
	unsigned short Pos;		//!< posizione nella bitmap (valido solo per il tipo MBTYPE_BITMAP)
									//!< maschera nel campo di bit (valido solo per il tipo MBTYPE_BYTE_MASK)
									//!< posizione nell'array del successivo elemento (valido solo per gli array di tipo CHAR e BYTE)
									//!< 0: word bassa, 1: word alta (valido solo per i tipi DWORD, LONG e FLOAT)
	unsigned short *pw;		//!< puntatore a valore (bitmap, campo di bit, byte, word, ...)
};
/*!
	\typedef MBDATA
	\brief Typedef definition for MBData_struct
*/
typedef struct MBData_struct	MBDATA;

/*!
	\var Coil_tab
	\brief Tabella MB dei coils
*/
extern const MBDATA Coil_tab[TCOIL_NREG];

/*!
	\var InpReg_tab
	\brief Tabella MB degli Input Registers
*/
extern const MBDATA InpReg_tab[TINPREG_NREG];

/*!
	\var HoldReg_tab
	\brief Tabella MB degli Holding Registers
*/
extern const MBDATA HoldReg_tab[THOLREG_NREG];

/*!
	\struct Limits_struct
	\brief The Limits tag structure
*/
struct Limits_struct
{
	unsigned short Min;	//!< min
	unsigned short Max;	//!< max
	unsigned char Step;	//!< inc/dec step	
};
/*!
	\typedef LIMITS_STRUCT
	\brief Typedef definition for Limits_struct
*/
typedef struct Limits_struct LIMITS_STRUCT;
/*!
	\var Limits_tab
	\brief Tabella dei limiti per gli Holding Registers
*/
extern LIMITS_STRUCT Limits_tab[THOLREG_NREG];

/*!
	\struct LimitsReq_struct
	\brief Struttura per richieste Limiti/Step
*/
struct LimitsReq_struct
{
	unsigned short HoldRegAddr;
	unsigned char nHoldAddr;
};
/*!
	\typedef LIMITSREQ_STRUCT
	\brief Typedef definition for LimitsReq_struct
*/
typedef struct LimitsReq_struct LIMITSREQ_STRUCT;
/*!
	\var LimitsReq_tab
	\brief Tabella delle richieste dei limiti per gli Holding Registers
	\note Richiesta limiti di max 4 registri
*/
#define LIMITSREQ_NUM		69
extern const LIMITSREQ_STRUCT LimitsReq_tab[LIMITSREQ_NUM];


// definizione indirizzi di Input Registers particolari
#define ADDR_TIPO_STUFA			(0x006C)
// definizione indirizzi di Holding Registers particolari
#define ADDR_TEMP_PAN			(0x0008)
#define ADDR_H2O_TSET			(0x0009)
#define ADDR_POWER_LEVEL		(0x0430)
#define ADDR_RFID					(0x2000)
#define ADDR_WIFI_SSID			(0x2024)
#define ADDR_WIFI_KEYW			(0x2034)
#define ADDR_WIFI_IP				(0x2004)
#define ADDR_WPS_PIN				(0x2054)


/*!
	\var HoldReg2_tab
	\brief Tabella MB degli Holding Registers 2
*/
extern const MBDATA HoldReg2_tab[THOLREG2_NREG];
/*!
	\var Limits2_tab
	\brief Tabella dei limiti variabili per gli Holding Registers 2
*/
extern LIMITS_STRUCT Limits2_tab[THOLREG2_NREG];

/*!
	\var HoldReg3_tab
	\brief Tabella MB degli Holding Registers 3
*/
extern const MBDATA HoldReg3_tab[THOLREG3_NREG];
/*!
	\var Limits3_tab
	\brief Tabella dei limiti variabili per gli Holding Registers 3
*/
extern const LIMITS_STRUCT Limits3_tab[THOLREG3_NREG];



#endif	// _OT_PLUS_TABELLE_H
