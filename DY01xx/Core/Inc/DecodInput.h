/*!
	\file DecodInput.h
	\brief Include file del modulo DecodInput.c
*/

#ifndef _DECODINPUT_H
#define _DECODINPUT_H




/*! \var DigInput
    \brief Bitmap ingressi digitali
*/
extern TBitByte DigInput;
#define DIGIN						DigInput.BYTE
#define DIGIN_PULS					DigInput.BIT.D0	// =1 pulsante premuto
#define DIGIN_ALIM					DigInput.BIT.D1	// =1 alimentazione esterna presente



/*!
** \var VBatt_tab
** \brief Tabella dei parametri batteria
**/
typedef struct
{
	unsigned short VBattNom;	//!< livello tensione batteria nominale (mV)
	unsigned short VBattOff;	//!< livello tensione batteria scarica (mV)
	unsigned short VBattLow;	//!< basso livello tensione batteria (mV)
	unsigned short VBattFull;	//!< max livello tensione batteria carica (mV)
	unsigned short VBattADNom;	//!< valore A/D accumulato per tensione batteria nominale
} BATT_STRUCT;
extern BATT_STRUCT VBatt_tab;

extern short TempAmb;	//!< temperatura ambiente (0.01 degC)
extern unsigned short VBatt;	//!< tensione batteria (mV)
extern unsigned char BattUp;	//!< stato di ricarica batteria
extern unsigned short PercVBatt;	//!< percentuale di carica batteria (0.1%)

// variabili per stato di ricarica batteria
extern unsigned char CntSenseBat;
extern unsigned char CntChargeBat;


#define TEMP_AMB_NOM		(1800)	// temperatura ambiente nominale (18.00°C)


/*
	La tensione viene ridotta da un partitore di un fattore 1/2 e poi
	acquisita dall'ADC 12-bit con VREF = 3V.

	Tensione (mV)	= 1000 * vADC * (VREF/4095) * (2) =
							= (vADC * 3000 * 2) / (4095) =
							= (vADC * 6000) / 4095
*/
#define LITIO_VBATT_NOM			(3700)	// tensione batteria nominale (mV)
#define LITIO_VBATT_ANIN_NOM	(2525)	//(20202)	// valore A/D accumulato per tensione batteria nominale
// batteria al litio 3.7 V ricaricabile
#define LITIO_VBATT_OFF		(3500)	// batteria scarica in scarica: 3.5 V
#define LITIO_VBATT_OFF_ALIM	(3550)	// batteria scarica in carica: 3.55 V
#define LITIO_VBATT_LOW		(3410)	// soglia di forzatura low-power: 3.41 V
#define LITIO_VBATT_FULL	(4100)	// batteria completamente carica: 4.1 V

#define BATTERY_CHARGE				0	// stato batteria: in carica
#define BATTERY_DISCHARGE			1	// stato batteria: in scarica
#define BATTERY_CHARGE_ERROR		2	// stato batteria: in errore di ricarica

/*
	La tensione VREFINT (1.21 V) viene acquisita dall'ADC 12-bit con VREF = 3V.

	VREFINT (mV)	= 1000 * vADC * (VREF/4095) =
							= (vADC * 3000) / 4095
	
	Il valore VREFINT_CAL acquisito in produzione dall'ADC con VREF = 3,3V e'
	disponibile alla locazione 0x1FFF7A2A, per cui:
	
	VREFINT (mV)	= 1000 * VREFINT_CAL * (VREF/4095) =
							= (VREFINT_CAL * 3300) / 4095
	
*/
#define VREFINT_CAL			(*(unsigned short *)0x1FFF7A2A)
//#define VREFINT_NOM			(1210)	// tensione VREFINT nominale (mV)
#define VREFINT_NOM(x)			(((x) * 3300UL) / 4095UL)	// tensione VREFINT nominale (mV)
#define VREFINT_ANIN_NOM	(1652)	//(13213)	// valore A/D accumulato per tensione VREFINT


/*
	Sensore di temperatura interno.
	
	Macro e funzioni del file stm32f4xx_ll_adc.h
*/
/* Definitions of environment analog values */
  /* Value of analog reference voltage (Vref+), connected to analog voltage   */
  /* supply Vdda (unit: mV).                                                  */
#define VDDA_APPLI                       ((uint32_t)3000)	// 3.0V
// Temperature sensor characteristics from data-sheet
#define INTERNAL_TEMPSENSOR_AVGSLOPE   ((int32_t) 2500)        /* Internal temperature sensor, parameter Avg_Slope (unit: uV/DegCelsius). Refer to device datasheet for min/typ/max values. */
#define INTERNAL_TEMPSENSOR_V25        ((int32_t)  760)        /* Internal temperature sensor, parameter V25 (unit: mV). Refer to device datasheet for min/typ/max values. */
#define INTERNAL_TEMPSENSOR_V25_TEMP   ((int32_t)   25)
#define INTERNAL_TEMPSENSOR_V25_VREF   ((int32_t) 3300)
/**
  * @brief  Helper macro to calculate the temperature (unit: degree Celsius)
  *         from ADC conversion data of internal temperature sensor.
  * @note   Computation is using temperature sensor typical values
  *         (refer to device datasheet).
  * @note   Calculation formula:
  *           Temperature = (TS_TYP_CALx_VOLT(uV) - TS_ADC_DATA * Conversion_uV)
  *                         / Avg_Slope + CALx_TEMP
  *           with TS_ADC_DATA      = temperature sensor raw data measured by ADC
  *                                   (unit: digital value)
  *                Avg_Slope        = temperature sensor slope
  *                                   (unit: uV/Degree Celsius)
  *                TS_TYP_CALx_VOLT = temperature sensor digital value at
  *                                   temperature CALx_TEMP (unit: mV)
  *         Caution: Calculation relevancy under reserve the temperature sensor
  *                  of the current device has characteristics in line with
  *                  datasheet typical values.
  *                  If temperature sensor calibration values are available on
  *                  on this device (presence of macro __LL_ADC_CALC_TEMPERATURE()),
  *                  temperature calculation will be more accurate using
  *                  helper macro @ref __LL_ADC_CALC_TEMPERATURE().
  * @note   As calculation input, the analog reference voltage (Vref+) must be
  *         defined as it impacts the ADC LSB equivalent voltage.
  * @note   Analog reference voltage (Vref+) must be either known from
  *         user board environment or can be calculated using ADC measurement
  *         and ADC helper macro @ref __LL_ADC_CALC_VREFANALOG_VOLTAGE().
  * @note   ADC measurement data must correspond to a resolution of 12bits
  *         (full scale digital value 4095). If not the case, the data must be
  *         preliminarily rescaled to an equivalent resolution of 12 bits.
  * @param  __TEMPSENSOR_TYP_AVGSLOPE__   Device datasheet data Temperature sensor slope typical value (unit uV/DegCelsius).
  *                                       On STM32F4, refer to device datasheet parameter "Avg_Slope".
  * @param  __TEMPSENSOR_TYP_CALX_V__     Device datasheet data Temperature sensor voltage typical value (at temperature and Vref+ defined in parameters below) (unit mV).
  *                                       On STM32F4, refer to device datasheet parameter "V25".
  * @param  __TEMPSENSOR_CALX_TEMP__      Device datasheet data Temperature at which temperature sensor voltage (see parameter above) is corresponding (unit mV)
  * @param  __VREFANALOG_VOLTAGE__        Analog voltage reference (Vref+) voltage (unit mV)
  * @param  __TEMPSENSOR_ADC_DATA__       ADC conversion data of internal temperature sensor (unit digital value).
  * @param  __ADC_RESOLUTION__            ADC resolution at which internal temperature sensor voltage has been measured.
  *         This parameter can be one of the following values:
  *         @arg @ref LL_ADC_RESOLUTION_12B
  *         @arg @ref LL_ADC_RESOLUTION_10B
  *         @arg @ref LL_ADC_RESOLUTION_8B
  *         @arg @ref LL_ADC_RESOLUTION_6B
  * @retval Temperature (unit: degree Celsius)
  */
#define __LL_ADC_CALC_TEMPERATURE_TYP_PARAMS(__TEMPSENSOR_TYP_AVGSLOPE__,\
                                             __TEMPSENSOR_TYP_CALX_V__,\
                                             __TEMPSENSOR_CALX_TEMP__,\
                                             __VREFANALOG_VOLTAGE__,\
                                             __TEMPSENSOR_ADC_DATA__,\
                                             __ADC_RESOLUTION__)               \
  ((( (                                                                        \
       (int32_t)(((__TEMPSENSOR_TYP_CALX_V__))                                 \
                 * 1000)                                                       \
       -                                                                       \
       (int32_t)((((__TEMPSENSOR_ADC_DATA__) * (__VREFANALOG_VOLTAGE__))       \
                  / __LL_ADC_DIGITAL_SCALE(__ADC_RESOLUTION__))                \
                 * 1000)                                                       \
      )                                                                        \
    ) / (__TEMPSENSOR_TYP_AVGSLOPE__)                                          \
   ) + (__TEMPSENSOR_CALX_TEMP__)                                              \
  )

// Temperature sensor calibration values
#define TEMPSENSOR_CAL1_ADDR               ((uint16_t*) (0x1FFF7A2CU)) /* Internal temperature sensor, address of parameter TS_CAL1: On STM32F4, temperature sensor ADC raw data acquired at temperature  30 DegC (tolerance: +-5 DegC), Vref+ = 3.3 V (tolerance: +-10 mV). */
#define TEMPSENSOR_CAL2_ADDR               ((uint16_t*) (0x1FFF7A2EU)) /* Internal temperature sensor, address of parameter TS_CAL2: On STM32F4, temperature sensor ADC raw data acquired at temperature 110 DegC (tolerance: +-5 DegC), Vref+ = 3.3 V (tolerance: +-10 mV). */
#define TEMPSENSOR_CAL1_TEMP               (( int32_t)   3000)           /* Internal temperature sensor, temperature at which temperature sensor has been calibrated in production for data into TEMPSENSOR_CAL1_ADDR (tolerance: +-5 DegC) (unit: 0.01DegC). */
#define TEMPSENSOR_CAL2_TEMP               (( int32_t)  11000)           /* Internal temperature sensor, temperature at which temperature sensor has been calibrated in production for data into TEMPSENSOR_CAL2_ADDR (tolerance: +-5 DegC) (unit: 0.01DegC). */
#define TEMPSENSOR_CAL_VREFANALOG          ( 3300U)                    /* Analog voltage reference (Vref+) voltage with which temperature sensor has been calibrated in production (+-10 mV) (unit: mV). */
/** @defgroup ADC_LL_EC_RESOLUTION  ADC instance - Resolution
  * @{
  */
#define LL_ADC_RESOLUTION_12B              0x00000000U                         /*!< ADC resolution 12 bits */
#define LL_ADC_RESOLUTION_10B              (                ADC_CR1_RES_0)     /*!< ADC resolution 10 bits */
#define LL_ADC_RESOLUTION_8B               (ADC_CR1_RES_1                )     /*!< ADC resolution  8 bits */
#define LL_ADC_RESOLUTION_6B               (ADC_CR1_RES_1 | ADC_CR1_RES_0)     /*!< ADC resolution  6 bits */
/**
  * @}
  */
/* ADC registers bits positions */
#define ADC_CR1_RES_BITOFFSET_POS          (24U) /* Value equivalent to POSITION_VAL(ADC_CR1_RES) */
#define ADC_TR_HT_BITOFFSET_POS            (16U) /* Value equivalent to POSITION_VAL(ADC_TR_HT) */
/**
  * @brief  Helper macro to convert the ADC conversion data from
  *         a resolution to another resolution.
  * @param  __DATA__ ADC conversion data to be converted 
  * @param  __ADC_RESOLUTION_CURRENT__ Resolution of to the data to be converted
  *         This parameter can be one of the following values:
  *         @arg @ref LL_ADC_RESOLUTION_12B
  *         @arg @ref LL_ADC_RESOLUTION_10B
  *         @arg @ref LL_ADC_RESOLUTION_8B
  *         @arg @ref LL_ADC_RESOLUTION_6B
  * @param  __ADC_RESOLUTION_TARGET__ Resolution of the data after conversion
  *         This parameter can be one of the following values:
  *         @arg @ref LL_ADC_RESOLUTION_12B
  *         @arg @ref LL_ADC_RESOLUTION_10B
  *         @arg @ref LL_ADC_RESOLUTION_8B
  *         @arg @ref LL_ADC_RESOLUTION_6B
  * @retval ADC conversion data to the requested resolution
  */
#define __LL_ADC_CONVERT_DATA_RESOLUTION(__DATA__, __ADC_RESOLUTION_CURRENT__, __ADC_RESOLUTION_TARGET__) \
  (((__DATA__)                                                                 \
    << ((__ADC_RESOLUTION_CURRENT__) >> (ADC_CR1_RES_BITOFFSET_POS - 1U)))     \
   >> ((__ADC_RESOLUTION_TARGET__) >> (ADC_CR1_RES_BITOFFSET_POS - 1U))        \
  )
/**
  * @brief  Helper macro to calculate the temperature (unit: degree Celsius)
  *         from ADC conversion data of internal temperature sensor.
  * @note   Computation is using temperature sensor calibration values
  *         stored in system memory for each device during production.
  * @note   Calculation formula:
  *           Temperature = ((TS_ADC_DATA - TS_CAL1)
  *                           * (TS_CAL2_TEMP - TS_CAL1_TEMP))
  *                         / (TS_CAL2 - TS_CAL1) + TS_CAL1_TEMP
  *           with TS_ADC_DATA = temperature sensor raw data measured by ADC
  *                Avg_Slope = (TS_CAL2 - TS_CAL1)
  *                            / (TS_CAL2_TEMP - TS_CAL1_TEMP)
  *                TS_CAL1   = equivalent TS_ADC_DATA at temperature
  *                            TEMP_DEGC_CAL1 (calibrated in factory)
  *                TS_CAL2   = equivalent TS_ADC_DATA at temperature
  *                            TEMP_DEGC_CAL2 (calibrated in factory)
  *         Caution: Calculation relevancy under reserve that calibration
  *                  parameters are correct (address and data).
  *                  To calculate temperature using temperature sensor
  *                  datasheet typical values (generic values less, therefore
  *                  less accurate than calibrated values),
  *                  use helper macro @ref __LL_ADC_CALC_TEMPERATURE_TYP_PARAMS().
  * @note   As calculation input, the analog reference voltage (Vref+) must be
  *         defined as it impacts the ADC LSB equivalent voltage.
  * @note   Analog reference voltage (Vref+) must be either known from
  *         user board environment or can be calculated using ADC measurement
  *         and ADC helper macro @ref __LL_ADC_CALC_VREFANALOG_VOLTAGE().
  * @note   On this STM32 serie, calibration data of temperature sensor
  *         corresponds to a resolution of 12 bits,
  *         this is the recommended ADC resolution to convert voltage of
  *         temperature sensor.
  *         Otherwise, this macro performs the processing to scale
  *         ADC conversion data to 12 bits.
  * @param  __VREFANALOG_VOLTAGE__  Analog reference voltage (unit mV)
  * @param  __TEMPSENSOR_ADC_DATA__ ADC conversion data of internal
  *                                 temperature sensor (unit: digital value).
  * @param  __ADC_RESOLUTION__      ADC resolution at which internal temperature
  *                                 sensor voltage has been measured.
  *         This parameter can be one of the following values:
  *         @arg @ref LL_ADC_RESOLUTION_12B
  *         @arg @ref LL_ADC_RESOLUTION_10B
  *         @arg @ref LL_ADC_RESOLUTION_8B
  *         @arg @ref LL_ADC_RESOLUTION_6B
  * @retval Temperature (unit: degree Celsius)
  */
#define __LL_ADC_CALC_TEMPERATURE(__VREFANALOG_VOLTAGE__,\
                                  __TEMPSENSOR_ADC_DATA__,\
                                  __ADC_RESOLUTION__)                              \
  (((( ((int32_t)((__LL_ADC_CONVERT_DATA_RESOLUTION((__TEMPSENSOR_ADC_DATA__),     \
                                                    (__ADC_RESOLUTION__),          \
                                                    LL_ADC_RESOLUTION_12B)         \
                   * (__VREFANALOG_VOLTAGE__))                                     \
                  / TEMPSENSOR_CAL_VREFANALOG)                                     \
        - (int32_t) *TEMPSENSOR_CAL1_ADDR)                                         \
     ) * (int32_t)(TEMPSENSOR_CAL2_TEMP - TEMPSENSOR_CAL1_TEMP)                    \
    ) / (int32_t)((int32_t)*TEMPSENSOR_CAL2_ADDR - (int32_t)*TEMPSENSOR_CAL1_ADDR) \
   ) + TEMPSENSOR_CAL1_TEMP                                                        \
  )




/*! \fn void inpInit( void )
    \brief Inizializza gestore ingressi
*/
void inpInit( void );

/*! \fn void inpHandler( void )
    \brief Gestore ingressi
*/
void inpHandler( void );

/*! \fn void inpMonitor( void )
    \brief Monitor degli ingressi acquisiti
*/
void inpMonitor( void );

/*!
 * \fn void DecodSenseCharge( void )
 * \brief Rileva lo stato di ricarica della batteria
 * \note La funzione presuppone di essere chiamata ogni 50ms
 */
void DecodSenseCharge(void);

/*!
** \fn short ExecTAConv( void )
** \brief Esegue una singola acquisizione TA con aggiornamento TempAmb
** \return 1: OK, 0: FAILED
** 
** \details Si considera RTOS sospeso e I2C mutex non definito
**
short ExecTAConv( void );

/*!
 * \fn void INT1_Acc_irq( void )
 * \brief Gestione INT1_ACC interrupt
 * \return None
 */
void INT1_Acc_irq( void );

/*!
 * \fn void INT2_Acc_irq( void )
 * \brief Gestione INT1_ACC interrupt
 * \return None
 */
void INT2_Acc_irq( void );

/*!
 * \fn void inpRestart( void )
 * \brief Riavvia il gestore ingressi
 * \return None
 */
void inpRestart( void );

/*!
 * \fn void inpSuspend( void )
 * \brief Sospende il gestore ingressi
 * \return None
 */
void inpSuspend( void );



#endif	// _DECODINPUT_H
