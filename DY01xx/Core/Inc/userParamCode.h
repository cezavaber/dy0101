/*!
** \file userParamCode.h
** \author Alessandro Vagniluca
** \date 19/01/2016
** \brief USERPARAMCODE.C header file
** 
** \version 1.0
**/

#ifndef _USER_PARAM_CODE_H_
#define _USER_PARAM_CODE_H_



/*!
** \fn unsigned char userVisReqParCod( void )
** \brief Starts the USER_STATE_PARAM_CODE User Interface state handling
** \return The next User Interface state 
**/
unsigned char userVisReqParCod( void );

/*!
** \fn unsigned char userParamCode( void )
** \brief Handler function for the USER_STATE_PARAM_CODE User Interface state
** \return The next User Interface state 
**/
unsigned char userParamCode( void );

/*!
** \fn unsigned char userParamVis( void )
** \brief Handler function for the USER_STATE_PARAM_VIS User Interface state
** \return The next User Interface state 
**/
unsigned char userParamVis( void );


#endif	// _USER_PARAM_CODE_H_
