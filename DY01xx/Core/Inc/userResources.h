/*!
** \file userResources.h
** \author Alessandro Vagniluca
** \date 07/12/2015
** \brief USERRESOURCES.C header file
** 
** \version 1.0
**/

#ifndef _USER_RESOURCES_H_
#define _USER_RESOURCES_H_



/*!
** \fn unsigned char userVisResources( void )
** \brief Starts the USER_STATE_RESOURCES User Interface state handling
** \return The next User Interface state 
**/
unsigned char userVisResources( void );

/*!
** \fn unsigned char userResources( void )
** \brief Handler function for the USER_STATE_RESOURCES User Interface state
** \return The next User Interface state 
**/
unsigned char userResources( void );

/*!
** \fn unsigned char userBattery( void )
** \brief Handler function for the USER_STATE_BATTERY User Interface state
** \return The next User Interface state 
**/
unsigned char userBattery( void );

/*!
** \fn unsigned char userVisBattery( void )
** \brief Starts the USER_STATE_BATTERY User Interface state handling
** \return The next User Interface state
**/
unsigned char userVisBattery( void );



#endif	// _USER_RESOURCES_H_
