/*!
**  \file Utils.h
**  \author Alessandro Vagniluca
**  \date 22/06/2015
**  \brief Header file for UTILS.C source module
**  
**  \version 1.0
**/

#ifndef _UTILS_H
#define _UTILS_H

#ifdef __cplusplus
 extern "C" {
#endif


#include "typedefine.h"


/* maschere per 32 bit */
#define BIT0           0x00000001
#define BIT1           0x00000002
#define BIT2           0x00000004
#define BIT3           0x00000008
#define BIT4           0x00000010
#define BIT5           0x00000020
#define BIT6           0x00000040
#define BIT7           0x00000080
#define BIT8           0x00000100
#define BIT9           0x00000200
#define BIT10          0x00000400
#define BIT11          0x00000800
#define BIT12          0x00001000
#define BIT13          0x00002000
#define BIT14          0x00004000
#define BIT15          0x00008000
#define BIT16          0x00010000
#define BIT17          0x00020000
#define BIT18          0x00040000
#define BIT19          0x00080000
#define BIT20          0x00100000
#define BIT21          0x00200000
#define BIT22          0x00400000
#define BIT23          0x00800000
#define BIT24          0x01000000
#define BIT25          0x02000000
#define BIT26          0x04000000
#define BIT27          0x08000000
#define BIT28          0x10000000
#define BIT29          0x20000000
#define BIT30          0x40000000
#define BIT31          0x80000000


#define STR_MAXLEN	270	//150	// divisibile per 3 per debug buffer in hex
extern char str[STR_MAXLEN+1];
extern char str1[STR_MAXLEN+1];
extern char str2[STR_MAXLEN+1];


/*! \fn unsigned char Ascii2Hex( char car )
    \brief Converte in HEX un carattere ASCII 0,1,..,9,A,..,F
	 \param car - carattere da convertire
	 \return Il valore del carattere convertito
*/
unsigned char Ascii2Hex( char car );

/*! \fn char Hex2Ascii( unsigned char byte )
    \brief Converte in ASCII un byte HEX con valore 0,1,..,9,A,..,F
	 \param byte - byte da convertire
	 \return Il carattere corrispondente al byte da convertire
*/
char Hex2Ascii( unsigned char byte );

// funzioni di conversione numero intero -> stringa
char *itoa(int value, char *string, int radix);
char *ultoa(unsigned long value, char *string, int radix);
char *ltoa(long value, char *string, int radix);

// funzioni di controllo bitmap
// si intende: bit 0-7 nel primo byte, bit 8-15 nel secondo byte, ...
void SetBitBMap( unsigned char *bitmap, unsigned short ibit );
void ResBitBMap( unsigned char *bitmap, unsigned short ibit );
void NotBitBMap( unsigned char *bitmap, unsigned short ibit );
unsigned char GetBitBMap( unsigned char *bitmap, unsigned short ibit );

// funzioni di conversione Little Endian (Intel) - Big Endian (Motorola)
unsigned short WordLittle2BigEndian( unsigned short w );
unsigned long DWordLittle2BigEndian( unsigned long d );

// visualizza un buffer in forma esadecimale
char *VisHexMsg( unsigned char *pb, unsigned short len );

/*! \fn unsigned char CalcChecksum( unsigned char *pb, unsigned char len )
    \brief Calcolo checksum su un buffer
    \param pb - buffer
    \param len - lunghezza buffer
    \return Il valore calcolato del checksum
*/
unsigned char CalcChecksum( unsigned char *pb, unsigned char len );

#ifdef __cplusplus
}
#endif

#endif	// _UTILS_H
