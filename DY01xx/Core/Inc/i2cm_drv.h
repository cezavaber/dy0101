/***************************************************************************
* File      : I2CM_DRV.H
* Scopo     : File header per il modulo I2CM_DRV.C
***************************************************************************/

#ifndef _I2CM_DRV_H
#define _I2CM_DRV_H

// stato/esito transazione I2C
enum esit
{
	I2C_RUN,
	I2C_RUN_RESTART,
	I2C_OK,
	I2C_FAIL,
	NESITO
};


short i2cInit( void );
short i2cWriteBuffer( unsigned char *pBufWr, unsigned short sizeWr,
						unsigned char *pData, unsigned short sizeData );
short i2cWriteReadBuffer( unsigned char *pBufWr, unsigned short sizeWr,
						unsigned char *pBufRd, unsigned short sizeRd );



#endif

