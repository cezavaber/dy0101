/*!
**  \file rtc.h
**  \author Alessandro Vagniluca
**  \date 15/10/2015
**  \brief RTC.C header file
**  
**  \version 1.0
**/

#ifndef _RTC_H_
#define _RTC_H_


// struttura data-ora
typedef struct
{
	unsigned char tm_sec;   /* Seconds (0-59) */
	unsigned char tm_min;   /* Minutes (0-59) */
	unsigned char tm_hour;  /* Hour (0-23) */
	unsigned char tm_wday;  /* Day of week (0-6, 0=Sunday) */
	unsigned char tm_mday;  /* Day of month (1-31) */
	unsigned char tm_mon;   /* Month (1-12) */
	unsigned short tm_year;  /* Year */
} TM;
extern TM DateTime;


/*!
** Limiti di tempo impostabili
** 
** RTC di STM32F4 supporta solo 2 cifre per l'anno (max 99 anni).
** Volendo arrivare all'ultimo anno coperto dall'Epoch Time (2106),
** il minimo anno impostabile su RTC corrisponde al 2007.
**/
#define MIN_YEAR		1970	// base Epoch Time: gioved� 01/01/1970 00.00.00
#define MIN_YEAR_RTC	2007	// base RTC: lunedi 01/01/2007 00.00.00
#define MAX_YEAR		2106	// max 136 anni in 4294967296 secondi
// giorno della settimana del 01/01/MIN_YEAR
#define FIRST_WDAY		4	// il 01/01/1970 era giovedi
#define FIRST_WDAY_RTC	1	// il 01/01/2007 era lunedi
// numero di secondi al 01/01/MIN_YEAR 00.00.00 (Epoch timestamp)
#define NUM_SEC_START		0	// Epoch timestamp al 01/01/1970 00.00.00
#define NUM_SEC_START_RTC	1167609600	// Epoch timestamp al 01/01/2007 00.00.00

extern unsigned long SecCounter;




/* prototipi */
short rtcInit( void );
short rtcCheckDateTime( TM *pTm );
unsigned long rtcRd( TM *pTm );
short rtcWr( TM *pTm );
unsigned long DateTime2Sec( TM *pTm );
void Sec2DateTime( unsigned long Nsec, TM *pTm );
void rtcStrMakeDate( char *s, TM *pTm );
void rtcStrMakeTime( char *s, TM *pTm );
void ScriviData( char *s );
void ScriviOra( char *s );

unsigned char rtcGetWeekDay( unsigned char day, unsigned char month, unsigned short year );

#endif	// _RTC_H_


