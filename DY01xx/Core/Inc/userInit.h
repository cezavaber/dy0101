/*!
** \file userInit.h
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief USERINIT.C header file
** 
** \version 1.0
**/

#ifndef _USER_INIT_H_
#define _USER_INIT_H_



/*!
** \fn unsigned char userVisInit( void )
** \brief Starts the USER_STATE_INIT User Interface state handling
** \return The next User Interface state 
**/
unsigned char userVisInit( void );

/*!
** \fn unsigned char userInit( void )
** \brief Handler function for the USER_STATE_INIT User Interface state
** \return The next User Interface state 
**/
unsigned char userInit( void );

/*!
** \fn void InitPassword( void )
** \brief Password initialization
** \return None
**/
void InitPassword( void );

/*!
** \fn void InitLogo( void )
** \brief Logo initialization
** \return None
**/
void InitLogo( void );

/*!
** \fn void InitLogo( void )
** \brief Tipo Stufa description strings initialization
** \return None
**/
void InitTipoDesc (void);

/*!
** \fn short userCollectParam( void )
** \brief Check for the reception of all the initial required parameters
** \return 0: recetion is running, 1: reception completed
**/
short userCollectParam( void );


#endif	// _USER_INIT_H_
