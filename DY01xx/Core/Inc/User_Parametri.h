/*!
** \file User_Parametri.h
** \author Alessandro Vagniluca
** \date 24/11/2015
** \brief USER_PARAMETRI.C header file
** 
** \version 1.0
**/

#ifndef _USER_PARAMETRI_H_
#define _USER_PARAMETRI_H_

#include "main.h"
#include "OT_Plus.h"
#include "nvram.h"
#include "rtc.h"


// enumerazione dei tipi di parametro
enum TipoParametro
{
	TYPE_BOOL,			// flag appartenente ad una bitmap di 16 bit (TBitWord)
	TYPE_BYTE_MASK,
	TYPE_BYTE,
	TYPE_WORD,
	TYPE_INTEGER,
	TYPE_DWORD,
	TYPE_LONG,
	TYPE_FLOAT,
	TYPE_CHAR,
	NUMPARAMTYPE
};

// enumerazione dei livelli di accesso
enum
{
	LOGIN_FREE,				// livello di accesso libero
	LOGIN_SERVICE,			// livello di accesso assistenza
	LOGIN_MANUFACTURER,	// livello di accesso costruttore
	NUMLOGIN
};

// tipi tabella MODBUS
enum
{
	MBTAB_COIL,
	MBTAB_INPUT_REG,
	MBTAB_HOLD_REG,
	MBTAB_HOLD2_REG,
	MBTAB_HOLD3_REG,
	NUM_MBTAB
};


// enumerazione dei parametri (ordinati per gruppo e codice di gruppo crescenti)
enum Parametri
{
	// Gruppo C
	P_TIPOSTUFA,			// 0
	P_TIPOMOTORE,			// 1
	P_MULTIFAN,			// 2
	P_MAXPOWER,			// 3
	P_PESATA_PELLET,		// 4
	P_TIPOMOTORE_2,			// 5
	P_IDRO_CFG,				// 7
	P_NUM_ACC,				// 8
	P_PERIODO_COCLEA,			// 10
	P_TIPO_FRENATA_COCLEA,	// 11
	P_PERIODO_FAN1,			// 30
	P_TIPO_FRENATA_FAN1,		// 31
	P_FAN1_MIN_SET,			// 35
	P_FAN2_MIN_SET,			// 36
	P_FAN3_MIN_SET,			// 37
	P_LINGUA_DEFAULT,		// 100
	//P_ORESAT_LIMITE,	// 101
	P_SERVICE,				// 104
	P_WIFI_SSID,			// 200
	P_WIFI_KEYW,			// 201
	P_WIFI_PORT,			// 202
	P_WIFI_IP,				// 203
	P_WPS_PIN,				// 207
	P_LOCAL_ISO3166_1_CODE,	// 208
	P_OT_DELAY,				// 209
	P_TEST,				// 999

	// Gruppo D
	P_CODICE_PANNELLO,			// 1
	P_CODICE_SCHEDA,			// 2
	P_CODICE_SICUREZZA,			// 3
	P_OREFUNZIONAMENTO,			// 4
	P_ORESAT,			// 5
	P_STATO_LOGICO,			// 6
	P_STATO_ATTUA,			// 7
	P_STATO_USER,			// 9
	P_CODICE_DRIVER,			// 10
	P_CODICE_PARAM,			// 11
	P_ALARM_CODE,			// 12
	P_MODEM_CMD,			// 30
	P_ONOFF,			// 31
	P_SPE_IDRO,			// 33
	P_SPE_ECO,			// 34
	P_TEMPOCOCLEAON_SET,			// 150
	P_TEMPOCOCLEAOFF_SET,			// 151
	P_FAN1_SET,			// 152
	P_FAN2_SET,			// 153
	P_PORTATA_SET,			// 154
	P_GIRI_SET,			// 155
	P_ATTUA_ESP,			// 156
	P_TEMPOFAN1ON_SET,			// 161
	P_TEMPOFAN1OFF_SET,			// 162
	P_GIRI2_SET,			// 163
	P_FAN3_SET,			// 164
	P_ALARM_SOUND,			// 400
	P_SERVICEREQ,			// 402
	P_GUASTO_SENS_PORTATA,	// 409
	P_SOVRAPRESSACQUA,	// 410
	P_GUASTOPRESSOSTACQUA,	// 411
	P_GUASTOSONDATEMPARIA,	// 412
	P_GUASTOSONDATEMPACQUA,	// 413
	P_FINE_PELLET,				// 415
	P_TERMOSTATO,			// 501
	P_PORTATAARIAMISURATA,			// 505
	P_VENTILATOREFUMI,			// 506
	P_TEMPERATURAFUMI,			// 507
	P_TEMPERATURAAMB,			// 508
	P_TEMPERATURAH2O,			// 509
	P_FOTORESISTENZA,			// 510
	P_PRESSIONEIDRO,			// 511
	P_PREALL_PORTA,			// 514
	P_GIRI2,				// 518
	P_OSSIGENO_RESIDUO,	// 519
	P_CONSUMO,				// 520
	P_TEMP_AMB_PAN,	// 700
	P_STATO_PRIMA_BLACOUT,	// 750
	P_DURATA_BLACKOUT,	// 751

	// Gruppo E
	P_TEMPERATURE,			// 1
	P_DELTATEMP,			// 2
	P_TFUMI_ON,			// 150
	P_TEMP_SOGLIA_FUMI_LEGNA,			// 151
	P_TFUMI_OFF,			// 152
	P_DELTA_TACC_CALDO,			// 153
	P_TEMP_PREALM_FUMI,			// 154
	P_GRAD_PREALM_FUMI,			// 155
	P_TEMP_ALLARME_FUMI,			// 156
	P_FOTORES_THRES_ON,			// 160
	P_FOTORES_THRES_OFF,			// 161
	P_SET_TRISC,			// 200
	P_TSETONPOMPA,			// 201
	P_TSETOFFPOMPA,			// 202
	P_TEMPAUX,			// 203
	P_SET_TSANIT,			// 204
	P_DELTA_SANITARI,			// 205
	P_ISTERESI_TEMP_ACQUA,			// 206
	P_TFUMI_FAN_ON,			// 207
	P_TFUMI_FAN_MIN,			// 208
	P_SETP_SANITARI,		// 209
	P_MAX_TIME_PREALM_FUMI,			// 300
	P_TIME_PREALM_SENS_PELLET,			// 301
	P_DELTATEMP_NO_FIAMMA,			// 302
	P_TSWITCHON_ECO,			// 303
	P_TSHUTDOWN_ECO,			// 304
	P_TEMPO_ALLARME_PORTA,			// 305
	P_TEMPO_PREALM_PORTA,			// 306
	P_TEMPO_PREALM_ARIA_COMBUST,			// 307
	P_TRIPRI_BLACKOUT,			// 308
	P_TPREACC,			// 309
	P_TPREACC2,			// 310
	P_TPRELOAD,			// 311
	P_TEMPO_RAFFA,			// 312
	P_MAX_TWARMUP,			// 313
	P_MAX_TFIREON_A,			// 314
	P_TIME_SPEA,			// 315
	P_DELAY_INC_ESP,			// 316
	P_DELAY_DEC_ESP,			// 317
	P_DELAY_INC_STEP_POTENZA,			// 318
	P_DELAY_DEC_STEP_POTENZA,			// 319
	P_TPB,			// 320
	P_TWAIT_PB,			// 321
	P_SCUOT_PERIODO,			// 322
	P_SCUOT_DURATA,			// 323
	P_TACC_B,			// 324
	P_TACC_C,			// 325
	P_TFON_B,			// 326
	P_TSPE_B,			// 327
	P_TSPE_C,			// 328
	P_TRAFF_B,			// 329
	P_TPB_B,				// 332
	P_PREACC1_NUM_EXEC,	// 333
	P_PREACC2_NUM_EXEC,	// 334
	P_OSSIGENO,			// 340
	P_REL0,				// 446
	P_REL1,				// 447
	P_REL2,				// 448
	P_REL3,				// 449
	P_REL4,				// 450
	P_TERM1,				// 462
	P_TERM2,				// 463
	P_TEMP_P0,			// 478
	P_TEMP_P1,			// 479
	P_TEMP_P2,			// 480
	P_TEMP_P3,			// 481
	P_TEMP_P4,			// 482
	P_POWER,			// 600
	P_FAN,			// 601
	P_FAN_2,			// 602
	P_GAIN_RISC,			// 603
	P_GAIN_SANIT,			// 604
	P_MAXPRESSH2O,			// 605
	P_SCUOT_NCICLI,			// 606
	P_MINPOWER_PB,			// 607
	P_FAN_3,			// 608
	P_K_SONDA,		// 610
	P_PORTATA_CRITICA,			// 613
	P_DELTA_PORTATA,			// 614
	P_RICETTA_ARIA_T,			// 615
	P_RICETTA_ARIA_P,			// 616
	P_RICETTA_PELLET_T,			// 617
	P_RICETTA_PELLET_P,			// 618

	// Gruppo F
	P_LOGIC_ONOFF,			// 0
	P_MODE,			// 1
	P_IBRIDA,			// 2
	P_TERM_AMB,			// 3
	P_ECO,			// 4
	P_RIPRISTINO_BLACKOUT,			// 100
	P_INIB_ACC_CALDO,			// 101
	P_ACC_TC,			// 102
	P_ACC_FR,			// 103
	P_EN_LEGNA,			// 104
	P_LIMIT_SERVICE,	// 106
	P_RAMPA_CAND,			// 200
	P_FRENATACOCLEA,			// 220
	P_EN_PERIODO_COCLEA,		// 221
	P_EN_COCLEA_HALL,			// 222
	P_PULIT_COCLEA2,			// 240
	P_EN_FANTEMPFUMI,			// 241
	P_EN_PERIODO_FAN1,		// 242
	P_FRENATA_FAN1_COCLEA2,			// 251
	P_EN_FAN2_HALL,			// 252
	P_ESP2_ENABLE,			// 260
	P_SENSOREPORTATAARIA,			// 280
	P_INIB_ALM_ARIACOBUST_INSUFF,			// 281
	P_SENSOREHALL,			// 320
	P_LIM_GIRI_MINIMI,			// 321
	P_SCUOTITORE,			// 322
	P_SONDA_LAMBDA,			// 323
	P_SENSOREPELLET,			// 400
	P_INIB_ALM_PREALM_FUMI,			// 500
	P_IDRO,			// 700
	P_INDIPENDENTE,			// 701
	P_POMPA_MODULANTE,			// 702
	P_INIB_SENSING_H2O,			// 703
	P_SPEGNIMENTO_IDRO,			// 704
	P_ACCUMULO,			// 705
	P_FLUSS_DIGIT,			// 706
	P_FPRESSIDRO,			// 707
	P_EN_WIFI,			// 800

	// Gruppo H
	P_SPE_COCLEA,				// 0
	P_SPE_CAND,					// 1
	P_SPE_FAN1,					// 2
	P_SPE_FAN2,					// 3
	P_SPE_ESP,					// 4
	P_SPE_FAN3,					// 5
	P_SPE_AUXA,					// 6
	P_SPE_TON_COCLEA,			// 10
	P_SPE_TOFF_COCLEA,			// 11
	P_SPE_GIRI_COCLEA,			// 12
	P_SPE_PORTATA,			// 20
	P_SPE_GIRI,			// 21
	P_SPE_TON_FAN1,		// 30
	P_SPE_TOFF_FAN1,		// 31
	P_SPE_ATTUA_FAN1,		// 32
	P_SPE_GIRI2,			// 40
	P_SPE_ATTUA_FAN2,		// 41
	P_SPE_ATTUA_ESP2,		// 42
	P_SPE_ATTUA_FAN3,		// 52
	P_SPE_POMPA,			// 58
	P_SPE_3VIE,				// 59
	P_SPE_PULIZMECC,		// 60
	P_SPE_DIRPULIZMECC,	// 61
	P_SPE_AUX1,				// 62
	P_SPE_AUX2,				// 63
	P_SPEA_COCLEA,				// 100
	P_SPEA_CAND,					// 101
	P_SPEA_FAN1,					// 102
	P_SPEA_FAN2,					// 103
	P_SPEA_ESP,					// 104
	P_SPEA_FAN3,					// 105
	P_SPEA_AUXA,					// 106
	P_SPEA_TON_COCLEA,			// 110
	P_SPEA_TOFF_COCLEA,			// 111
	P_SPEA_GIRI_COCLEA,			// 112
	P_SPEA_PORTATA,			// 120
	P_SPEA_GIRI,			// 121
	P_SPEA_TON_FAN1,		// 130
	P_SPEA_TOFF_FAN1,		// 131
	P_SPEA_ATTUA_FAN1,	// 132
	P_SPEA_GIRI2,			// 140
	P_SPEA_ATTUA_FAN2,		// 141
	P_SPEA_ATTUA_ESP2,		// 142
	P_SPEA_ATTUA_FAN3,		// 152
	P_SPEA_POMPA,			// 158
	P_SPEA_3VIE,				// 159
	P_SPEA_PULIZMECC,		// 160
	P_SPEA_DIRPULIZMECC,	// 161
	P_SPEA_AUX1,				// 162
	P_SPEA_AUX2,				// 163
	P_SPEB_COCLEA,				// 200
	P_SPEB_CAND,					// 201
	P_SPEB_FAN1,					// 202
	P_SPEB_FAN2,					// 203
	P_SPEB_ESP,					// 204
	P_SPEB_FAN3,					// 205
	P_SPEB_AUXA,					// 206
	P_SPEB_TON_COCLEA,			// 210
	P_SPEB_TOFF_COCLEA,			// 211
	P_SPEB_GIRI_COCLEA,			// 212
	P_SPEB_PORTATA,			// 220
	P_SPEB_GIRI,			// 221
	P_SPEB_TON_FAN1,		// 230
	P_SPEB_TOFF_FAN1,		// 231
	P_SPEB_ATTUA_FAN1,	// 232
	P_SPEB_GIRI2,			// 240
	P_SPEB_ATTUA_FAN2,		// 241
	P_SPEB_ATTUA_ESP2,		// 242
	P_SPEB_ATTUA_FAN3,		// 252
	P_SPEB_POMPA,			// 258
	P_SPEB_3VIE,				// 259
	P_SPEB_PULIZMECC,		// 260
	P_SPEB_DIRPULIZMECC,	// 261
	P_SPEB_AUX1,				// 262
	P_SPEB_AUX2,				// 263
	P_RAFFA_COCLEA,				// 300
	P_RAFFA_CAND,					// 301
	P_RAFFA_FAN1,					// 302
	P_RAFFA_FAN2,					// 303
	P_RAFFA_ESP,					// 304
	P_RAFFA_FAN3,					// 305
	P_RAFFA_AUXA,					// 306
	P_RAFFA_TON_COCLEA,			// 310
	P_RAFFA_TOFF_COCLEA,			// 311
	P_RAFFA_GIRI_COCLEA,			// 312
	P_RAFFA_PORTATA,			// 320
	P_RAFFA_GIRI,			// 321
	P_RAFFA_TON_FAN1,		// 330
	P_RAFFA_TOFF_FAN1,		// 331
	P_RAFFA_ATTUA_FAN1,	// 332
	P_RAFFA_GIRI2,			// 340
	P_RAFFA_ATTUA_FAN2,		// 341
	P_RAFFA_ATTUA_ESP2,		// 342
	P_RAFFA_ATTUA_FAN3,		// 352
	P_RAFFA_POMPA,			// 358
	P_RAFFA_3VIE,				// 359
	P_RAFFA_PULIZMECC,		// 360
	P_RAFFA_DIRPULIZMECC,	// 361
	P_RAFFA_AUX1,				// 362
	P_RAFFA_AUX2,				// 363
	P_PULIZIA_COCLEA,				// 400
	P_PULIZIA_CAND,					// 401
	P_PULIZIA_FAN1,					// 402
	P_PULIZIA_FAN2,					// 403
	P_PULIZIA_ESP,					// 404
	P_PULIZIA_FAN3,					// 405
	P_PULIZIA_AUXA,					// 406
	P_PULIZIA_TON_COCLEA,			// 410
	P_PULIZIA_TOFF_COCLEA,			// 411
	P_PULIZIA_GIRI_COCLEA,			// 412
	P_PULIZIA_PORTATA,			// 420
	P_PULIZIA_GIRI,			// 421
	P_PULIZIA_TON_FAN1,		// 430
	P_PULIZIA_TOFF_FAN1,		// 431
	P_PULIZIA_ATTUA_FAN1,	// 432
	P_PULIZIA_GIRI2,			// 440
	P_PULIZIA_ATTUA_FAN2,		// 441
	P_PULIZIA_ATTUA_ESP2,		// 442
	P_PULIZIA_ATTUA_FAN3,		// 452
	P_PULIZIA_POMPA,			// 458
	P_PULIZIA_3VIE,				// 459
	P_PULIZIA_PULIZMECC,		// 460
	P_PULIZIA_DIRPULIZMECC,	// 461
	P_PULIZIA_AUX1,				// 462
	P_PULIZIA_AUX2,				// 463
	P_SPEC_COCLEA,				// 500
	P_SPEC_CAND,					// 501
	P_SPEC_FAN1,					// 502
	P_SPEC_FAN2,					// 503
	P_SPEC_ESP,					// 504
	P_SPEC_FAN3,					// 505
	P_SPEC_AUXA,					// 506
	P_SPEC_TON_COCLEA,			// 510
	P_SPEC_TOFF_COCLEA,			// 511
	P_SPEC_GIRI_COCLEA,			// 512
	P_SPEC_PORTATA,			// 520
	P_SPEC_GIRI,			// 521
	P_SPEC_TON_FAN1,		// 530
	P_SPEC_TOFF_FAN1,		// 531
	P_SPEC_ATTUA_FAN1,	// 532
	P_SPEC_GIRI2,			// 540
	P_SPEC_ATTUA_FAN2,		// 541
	P_SPEC_ATTUA_ESP2,		// 542
	P_SPEC_ATTUA_FAN3,		// 552
	P_SPEC_POMPA,			// 558
	P_SPEC_3VIE,				// 559
	P_SPEC_PULIZMECC,		// 560
	P_SPEC_DIRPULIZMECC,	// 561
	P_SPEC_AUX1,				// 562
	P_SPEC_AUX2,				// 563
	P_RAFFB_COCLEA,				// 600
	P_RAFFB_CAND,					// 601
	P_RAFFB_FAN1,					// 602
	P_RAFFB_FAN2,					// 603
	P_RAFFB_ESP,					// 604
	P_RAFFB_FAN3,					// 605
	P_RAFFB_AUXA,					// 606
	P_RAFFB_TON_COCLEA,			// 610
	P_RAFFB_TOFF_COCLEA,			// 611
	P_RAFFB_GIRI_COCLEA,			// 612
	P_RAFFB_PORTATA,			// 620
	P_RAFFB_GIRI,			// 621
	P_RAFFB_TON_FAN1,		// 630
	P_RAFFB_TOFF_FAN1,		// 631
	P_RAFFB_ATTUA_FAN1,	// 632
	P_RAFFB_GIRI2,			// 640
	P_RAFFB_ATTUA_FAN2,		// 641
	P_RAFFB_ATTUA_ESP2,		// 642
	P_RAFFB_ATTUA_FAN3,		// 652
	P_RAFFB_POMPA,			// 658
	P_RAFFB_3VIE,				// 659
	P_RAFFB_PULIZMECC,		// 660
	P_RAFFB_DIRPULIZMECC,	// 661
	P_RAFFB_AUX1,				// 662
	P_RAFFB_AUX2,				// 663
	P_PULIZIAB_COCLEA,				// 700
	P_PULIZIAB_CAND,					// 701
	P_PULIZIAB_FAN1,					// 702
	P_PULIZIAB_FAN2,					// 703
	P_PULIZIAB_ESP,					// 704
	P_PULIZIAB_FAN3,					// 705
	P_PULIZIAB_AUXA,					// 706
	P_PULIZIAB_TON_COCLEA,			// 710
	P_PULIZIAB_TOFF_COCLEA,			// 711
	P_PULIZIAB_GIRI_COCLEA,			// 712
	P_PULIZIAB_PORTATA,			// 720
	P_PULIZIAB_GIRI,			// 721
	P_PULIZIAB_TON_FAN1,		// 730
	P_PULIZIAB_TOFF_FAN1,		// 731
	P_PULIZIAB_ATTUA_FAN1,	// 732
	P_PULIZIAB_GIRI2,			// 740
	P_PULIZIAB_ATTUA_FAN2,		// 741
	P_PULIZIAB_ATTUA_ESP2,		// 742
	P_PULIZIAB_ATTUA_FAN3,		// 752
	P_PULIZIAB_POMPA,			// 758
	P_PULIZIAB_3VIE,				// 759
	P_PULIZIAB_PULIZMECC,		// 760
	P_PULIZIAB_DIRPULIZMECC,	// 761
	P_PULIZIAB_AUX1,				// 762
	P_PULIZIAB_AUX2,				// 763

	// Gruppo L
	P_PREACC1_COCLEA,					// 100
	P_PREACC1_CAND,					// 101
	P_PREACC1_FAN1,					// 102
	P_PREACC1_FAN2,					// 103
	P_PREACC1_ESP,						// 104
	P_PREACC1_FAN3,					// 105
	P_PREACC1_AUXA,					// 106
	P_PREACC1_TON_COCLEA,			// 110
	P_PREACC1_TOFF_COCLEA,			// 111
	P_PREACC1_GIRI_COCLEA,			// 112
	P_PREACC1_PORTATA,			// 120
	P_PREACC1_GIRI,			// 121
	P_PREACC1_TON_FAN1,		// 130
	P_PREACC1_TOFF_FAN1,		// 131
	P_PREACC1_ATTUA_FAN1,	// 132
	P_PREACC1_GIRI2,			// 140
	P_PREACC1_ATTUA_FAN2,	// 141
	P_PREACC1_ATTUA_ESP2,		// 142
	P_PREACC1_ATTUA_FAN3,	// 152
	P_PREACC1_POMPA,			// 158
	P_PREACC1_3VIE,			// 159
	P_PREACC1_PULIZMECC,		// 160
	P_PREACC1_DIRPULIZMECC,	// 161
	P_PREACC1_AUX1,			// 162
	P_PREACC1_AUX2,			// 163
	P_PREACC2_COCLEA,					// 200
	P_PREACC2_CAND,					// 201
	P_PREACC2_FAN1,					// 202
	P_PREACC2_FAN2,					// 203
	P_PREACC2_ESP,						// 204
	P_PREACC2_FAN3,					// 205
	P_PREACC2_AUXA,					// 206
	P_PREACC2_TON_COCLEA,			// 210
	P_PREACC2_TOFF_COCLEA,			// 211
	P_PREACC2_GIRI_COCLEA,			// 212
	P_PREACC2_PORTATA,			// 220
	P_PREACC2_GIRI,			// 221
	P_PREACC2_TON_FAN1,		// 230
	P_PREACC2_TOFF_FAN1,		// 231
	P_PREACC2_ATTUA_FAN1,	// 232
	P_PREACC2_GIRI2,			// 240
	P_PREACC2_ATTUA_FAN2,	// 241
	P_PREACC2_ATTUA_ESP2,		// 242
	P_PREACC2_ATTUA_FAN3,	// 252
	P_PREACC2_POMPA,			// 258
	P_PREACC2_3VIE,			// 259
	P_PREACC2_PULIZMECC,		// 260
	P_PREACC2_DIRPULIZMECC,	// 261
	P_PREACC2_AUX1,			// 262
	P_PREACC2_AUX2,			// 263
	P_PREACCCALDO_COCLEA,					// 300
	P_PREACCCALDO_CAND,					// 301
	P_PREACCCALDO_FAN1,					// 302
	P_PREACCCALDO_FAN2,					// 303
	P_PREACCCALDO_ESP,						// 304
	P_PREACCCALDO_FAN3,					// 305
	P_PREACCCALDO_AUXA,					// 306
	P_PREACCCALDO_TON_COCLEA,			// 310
	P_PREACCCALDO_TOFF_COCLEA,			// 311
	P_PREACCCALDO_GIRI_COCLEA,			// 312
	P_PREACCCALDO_PORTATA,			// 320
	P_PREACCCALDO_GIRI,			// 321
	P_PREACCCALDO_TON_FAN1,		// 330
	P_PREACCCALDO_TOFF_FAN1,		// 331
	P_PREACCCALDO_ATTUA_FAN1,	// 332
	P_PREACCCALDO_GIRI2,			// 340
	P_PREACCCALDO_ATTUA_FAN2,	// 341
	P_PREACCCALDO_ATTUA_ESP2,		// 342
	P_PREACCCALDO_ATTUA_FAN3,	// 352
	P_PREACCCALDO_POMPA,			// 358
	P_PREACCCALDO_3VIE,			// 359
	P_PREACCCALDO_PULIZMECC,		// 360
	P_PREACCCALDO_DIRPULIZMECC,	// 361
	P_PREACCCALDO_AUX1,			// 362
	P_PREACCCALDO_AUX2,			// 363
	P_ACCA_COCLEA,					// 400
	P_ACCA_CAND,					// 401
	P_ACCA_FAN1,					// 402
	P_ACCA_FAN2,					// 403
	P_ACCA_ESP,						// 404
	P_ACCA_FAN3,					// 405
	P_ACCA_AUXA,					// 406
	P_ACCA_TON_COCLEA,			// 410
	P_ACCA_TOFF_COCLEA,			// 411
	P_ACCA_GIRI_COCLEA,			// 412
	P_ACCA_PORTATA,			// 420
	P_ACCA_GIRI,			// 421
	P_ACCA_TON_FAN1,		// 430
	P_ACCA_TOFF_FAN1,		// 431
	P_ACCA_ATTUA_FAN1,	// 432
	P_ACCA_GIRI2,			// 440
	P_ACCA_ATTUA_FAN2,	// 441
	P_ACCA_ATTUA_ESP2,		// 442
	P_ACCA_ATTUA_FAN3,	// 452
	P_ACCA_POMPA,			// 458
	P_ACCA_3VIE,			// 459
	P_ACCA_PULIZMECC,		// 460
	P_ACCA_DIRPULIZMECC,	// 461
	P_ACCA_AUX1,			// 462
	P_ACCA_AUX2,			// 463
	P_ACCB_COCLEA,					// 500
	P_ACCB_CAND,					// 501
	P_ACCB_FAN1,					// 502
	P_ACCB_FAN2,					// 503
	P_ACCB_ESP,						// 504
	P_ACCB_FAN3,					// 505
	P_ACCB_AUXA,					// 506
	P_ACCB_TON_COCLEA,			// 510
	P_ACCB_TOFF_COCLEA,			// 511
	P_ACCB_GIRI_COCLEA,			// 512
	P_ACCB_PORTATA,			// 520
	P_ACCB_GIRI,			// 521
	P_ACCB_TON_FAN1,		// 530
	P_ACCB_TOFF_FAN1,		// 531
	P_ACCB_ATTUA_FAN1,	// 532
	P_ACCB_GIRI2,			// 540
	P_ACCB_ATTUA_FAN2,	// 541
	P_ACCB_ATTUA_ESP2,		// 542
	P_ACCB_ATTUA_FAN3,	// 552
	P_ACCB_POMPA,			// 558
	P_ACCB_3VIE,			// 559
	P_ACCB_PULIZMECC,		// 560
	P_ACCB_DIRPULIZMECC,	// 561
	P_ACCB_AUX1,			// 562
	P_ACCB_AUX2,			// 563
	P_FIREONA_COCLEA,					// 600
	P_FIREONA_CAND,					// 601
	P_FIREONA_FAN1,					// 602
	P_FIREONA_FAN2,					// 603
	P_FIREONA_ESP,						// 604
	P_FIREONA_FAN3,					// 605
	P_FIREONA_AUXA,					// 606
	P_FIREONA_TON_COCLEA,			// 610
	P_FIREONA_TOFF_COCLEA,			// 611
	P_FIREONA_GIRI_COCLEA,			// 612
	P_FIREONA_PORTATA,			// 620
	P_FIREONA_GIRI,			// 621
	P_FIREONA_TON_FAN1,		// 630
	P_FIREONA_TOFF_FAN1,		// 631
	P_FIREONA_ATTUA_FAN1,	// 632
	P_FIREONA_GIRI2,			// 640
	P_FIREONA_ATTUA_FAN2,	// 641
	P_FIREONA_ATTUA_ESP2,		// 642
	P_FIREONA_ATTUA_FAN3,	// 652
	P_FIREONA_POMPA,			// 658
	P_FIREONA_3VIE,			// 659
	P_FIREONA_PULIZMECC,		// 660
	P_FIREONA_DIRPULIZMECC,	// 661
	P_FIREONA_AUX1,			// 662
	P_FIREONA_AUX2,			// 663
	P_FIREONB_COCLEA,					// 700
	P_FIREONB_CAND,					// 701
	P_FIREONB_FAN1,					// 702
	P_FIREONB_FAN2,					// 703
	P_FIREONB_ESP,						// 704
	P_FIREONB_FAN3,					// 705
	P_FIREONB_AUXA,					// 706
	P_FIREONB_TON_COCLEA,			// 710
	P_FIREONB_TOFF_COCLEA,			// 711
	P_FIREONB_GIRI_COCLEA,			// 712
	P_FIREONB_PORTATA,			// 720
	P_FIREONB_GIRI,			// 721
	P_FIREONB_TON_FAN1,		// 730
	P_FIREONB_TOFF_FAN1,		// 731
	P_FIREONB_ATTUA_FAN1,	// 732
	P_FIREONB_GIRI2,			// 740
	P_FIREONB_ATTUA_FAN2,	// 741
	P_FIREONB_ATTUA_ESP2,		// 742
	P_FIREONB_ATTUA_FAN3,	// 752
	P_FIREONB_POMPA,			// 758
	P_FIREONB_3VIE,			// 759
	P_FIREONB_PULIZMECC,		// 760
	P_FIREONB_DIRPULIZMECC,	// 761
	P_FIREONB_AUX1,			// 762
	P_FIREONB_AUX2,			// 763
	P_ACCC_COCLEA,					// 800
	P_ACCC_CAND,					// 801
	P_ACCC_FAN1,					// 802
	P_ACCC_FAN2,					// 803
	P_ACCC_ESP,						// 804
	P_ACCC_FAN3,					// 805
	P_ACCC_AUXA,					// 806
	P_ACCC_TON_COCLEA,			// 810
	P_ACCC_TOFF_COCLEA,			// 811
	P_ACCC_GIRI_COCLEA,			// 812
	P_ACCC_PORTATA,			// 820
	P_ACCC_GIRI,			// 821
	P_ACCC_TON_FAN1,		// 830
	P_ACCC_TOFF_FAN1,		// 831
	P_ACCC_ATTUA_FAN1,	// 832
	P_ACCC_GIRI2,			// 840
	P_ACCC_ATTUA_FAN2,	// 841
	P_ACCC_ATTUA_ESP2,		// 842
	P_ACCC_ATTUA_FAN3,	// 852
	P_ACCC_POMPA,			// 858
	P_ACCC_3VIE,			// 859
	P_ACCC_PULIZMECC,		// 860
	P_ACCC_DIRPULIZMECC,	// 861
	P_ACCC_AUX1,			// 862
	P_ACCC_AUX2,			// 863

	// Gruppo P
	P_POT1_COCLEA,					// 100
	P_POT1_CAND,					// 101
	P_POT1_FAN1,					// 102
	P_POT1_FAN2,					// 103
	P_POT1_ESP,						// 104
	P_POT1_FAN3,					// 105
	P_POT1_AUXA,					// 106
	P_POT1_TON_COCLEA,			// 110
	P_POT1_TOFF_COCLEA,			// 111
	P_POT1_GIRI_COCLEA,			// 112
	P_POT1_PORTATA,			// 120
	P_POT1_GIRI,			// 121
	P_POT1_TON_FAN1,		// 130
	P_POT1_TOFF_FAN1,		// 131
	P_POT1_ATTUA_FAN1,	// 132
	P_POT1_GIRI2,			// 140
	P_POT1_ATTUA_FAN2,	// 141
	P_POT1_ATTUA_ESP2,		// 142
	P_POT1_ATTUA_FAN3,	// 152
	P_POT1_POMPA,			// 158
	P_POT1_3VIE,			// 159
	P_POT1_PULIZMECC,		// 160
	P_POT1_DIRPULIZMECC,	// 161
	P_POT1_AUX1,			// 162
	P_POT1_AUX2,			// 163
	P_POT2_COCLEA,					// 200
	P_POT2_CAND,					// 201
	P_POT2_FAN1,					// 202
	P_POT2_FAN2,					// 203
	P_POT2_ESP,						// 204
	P_POT2_FAN3,					// 205
	P_POT2_AUXA,					// 206
	P_POT2_TON_COCLEA,			// 210
	P_POT2_TOFF_COCLEA,			// 211
	P_POT2_GIRI_COCLEA,			// 212
	P_POT2_PORTATA,			// 220
	P_POT2_GIRI,			// 221
	P_POT2_TON_FAN1,		// 230
	P_POT2_TOFF_FAN1,		// 231
	P_POT2_ATTUA_FAN1,	// 232
	P_POT2_GIRI2,			// 240
	P_POT2_ATTUA_FAN2,	// 241
	P_POT2_ATTUA_ESP2,		// 242
	P_POT2_ATTUA_FAN3,	// 252
	P_POT2_POMPA,			// 258
	P_POT2_3VIE,			// 259
	P_POT2_PULIZMECC,		// 260
	P_POT2_DIRPULIZMECC,	// 261
	P_POT2_AUX1,			// 262
	P_POT2_AUX2,			// 263
	P_POT3_COCLEA,					// 300
	P_POT3_CAND,					// 301
	P_POT3_FAN1,					// 302
	P_POT3_FAN2,					// 303
	P_POT3_ESP,						// 304
	P_POT3_FAN3,					// 305
	P_POT3_AUXA,					// 306
	P_POT3_TON_COCLEA,			// 310
	P_POT3_TOFF_COCLEA,			// 311
	P_POT3_GIRI_COCLEA,			// 312
	P_POT3_PORTATA,			// 320
	P_POT3_GIRI,			// 321
	P_POT3_TON_FAN1,		// 330
	P_POT3_TOFF_FAN1,		// 331
	P_POT3_ATTUA_FAN1,	// 332
	P_POT3_GIRI2,			// 340
	P_POT3_ATTUA_FAN2,	// 341
	P_POT3_ATTUA_ESP2,		// 342
	P_POT3_ATTUA_FAN3,	// 352
	P_POT3_POMPA,			// 358
	P_POT3_3VIE,			// 359
	P_POT3_PULIZMECC,		// 360
	P_POT3_DIRPULIZMECC,	// 361
	P_POT3_AUX1,			// 362
	P_POT3_AUX2,			// 363
	P_POT4_COCLEA,					// 400
	P_POT4_CAND,					// 401
	P_POT4_FAN1,					// 402
	P_POT4_FAN2,					// 403
	P_POT4_ESP,						// 404
	P_POT4_FAN3,					// 405
	P_POT4_AUXA,					// 406
	P_POT4_TON_COCLEA,			// 410
	P_POT4_TOFF_COCLEA,			// 411
	P_POT4_GIRI_COCLEA,			// 412
	P_POT4_PORTATA,			// 420
	P_POT4_GIRI,			// 421
	P_POT4_TON_FAN1,		// 430
	P_POT4_TOFF_FAN1,		// 431
	P_POT4_ATTUA_FAN1,	// 432
	P_POT4_GIRI2,			// 440
	P_POT4_ATTUA_FAN2,	// 441
	P_POT4_ATTUA_ESP2,		// 442
	P_POT4_ATTUA_FAN3,	// 452
	P_POT4_POMPA,			// 458
	P_POT4_3VIE,			// 459
	P_POT4_PULIZMECC,		// 460
	P_POT4_DIRPULIZMECC,	// 461
	P_POT4_AUX1,			// 462
	P_POT4_AUX2,			// 463
	P_POT5_COCLEA,					// 500
	P_POT5_CAND,					// 501
	P_POT5_FAN1,					// 502
	P_POT5_FAN2,					// 503
	P_POT5_ESP,						// 504
	P_POT5_FAN3,					// 505
	P_POT5_AUXA,					// 506
	P_POT5_TON_COCLEA,			// 510
	P_POT5_TOFF_COCLEA,			// 511
	P_POT5_GIRI_COCLEA,			// 512
	P_POT5_PORTATA,			// 520
	P_POT5_GIRI,			// 521
	P_POT5_TON_FAN1,		// 530
	P_POT5_TOFF_FAN1,		// 531
	P_POT5_ATTUA_FAN1,	// 532
	P_POT5_GIRI2,			// 540
	P_POT5_ATTUA_FAN2,	// 541
	P_POT5_ATTUA_ESP2,		// 542
	P_POT5_ATTUA_FAN3,	// 552
	P_POT5_POMPA,			// 558
	P_POT5_3VIE,			// 559
	P_POT5_PULIZMECC,		// 560
	P_POT5_DIRPULIZMECC,	// 561
	P_POT5_AUX1,			// 562
	P_POT5_AUX2,			// 563
	P_POT6_COCLEA,					// 600
	P_POT6_CAND,					// 601
	P_POT6_FAN1,					// 602
	P_POT6_FAN2,					// 603
	P_POT6_ESP,						// 604
	P_POT6_FAN3,					// 605
	P_POT6_AUXA,					// 606
	P_POT6_TON_COCLEA,			// 610
	P_POT6_TOFF_COCLEA,			// 611
	P_POT6_GIRI_COCLEA,			// 612
	P_POT6_PORTATA,			// 620
	P_POT6_GIRI,			// 621
	P_POT6_TON_FAN1,		// 630
	P_POT6_TOFF_FAN1,		// 631
	P_POT6_ATTUA_FAN1,	// 632
	P_POT6_GIRI2,			// 640
	P_POT6_ATTUA_FAN2,	// 641
	P_POT6_ATTUA_ESP2,		// 642
	P_POT6_ATTUA_FAN3,	// 652
	P_POT6_POMPA,			// 658
	P_POT6_3VIE,			// 659
	P_POT6_PULIZMECC,		// 660
	P_POT6_DIRPULIZMECC,	// 661
	P_POT6_AUX1,			// 662
	P_POT6_AUX2,			// 663
	P_POT7_COCLEA,					// 700
	P_POT7_CAND,					// 701
	P_POT7_FAN1,					// 702
	P_POT7_FAN2,					// 703
	P_POT7_ESP,						// 704
	P_POT7_FAN3,					// 705
	P_POT7_AUXA,					// 706
	P_POT7_TON_COCLEA,			// 710
	P_POT7_TOFF_COCLEA,			// 711
	P_POT7_GIRI_COCLEA,			// 712
	P_POT7_PORTATA,			// 720
	P_POT7_GIRI,			// 721
	P_POT7_TON_FAN1,		// 730
	P_POT7_TOFF_FAN1,		// 731
	P_POT7_ATTUA_FAN1,	// 732
	P_POT7_GIRI2,			// 740
	P_POT7_ATTUA_FAN2,	// 741
	P_POT7_ATTUA_ESP2,		// 742
	P_POT7_ATTUA_FAN3,	// 752
	P_POT7_POMPA,			// 758
	P_POT7_3VIE,			// 759
	P_POT7_PULIZMECC,		// 760
	P_POT7_DIRPULIZMECC,	// 761
	P_POT7_AUX1,			// 762
	P_POT7_AUX2,			// 763
	P_POT8_COCLEA,					// 800
	P_POT8_CAND,					// 801
	P_POT8_FAN1,					// 802
	P_POT8_FAN2,					// 803
	P_POT8_ESP,						// 804
	P_POT8_FAN3,					// 805
	P_POT8_AUXA,					// 806
	P_POT8_TON_COCLEA,			// 810
	P_POT8_TOFF_COCLEA,			// 811
	P_POT8_GIRI_COCLEA,			// 812
	P_POT8_PORTATA,			// 820
	P_POT8_GIRI,			// 821
	P_POT8_TON_FAN1,		// 830
	P_POT8_TOFF_FAN1,		// 831
	P_POT8_ATTUA_FAN1,	// 832
	P_POT8_GIRI2,			// 840
	P_POT8_ATTUA_FAN2,	// 841
	P_POT8_ATTUA_ESP2,		// 842
	P_POT8_ATTUA_FAN3,	// 852
	P_POT8_POMPA,			// 858
	P_POT8_3VIE,			// 859
	P_POT8_PULIZMECC,		// 860
	P_POT8_DIRPULIZMECC,	// 861
	P_POT8_AUX1,			// 862
	P_POT8_AUX2,			// 863


	// Gruppo Z (parametri locali)
	P_LINGUA,			// 0
	P_YEAR,				// 1
	P_MONTH,				// 2
	P_DAY,				// 3
	P_WEEKDAY,			// 4
	P_HOUR,				// 5
	P_MINUTE,			// 6
	P_BACKLIGHT,		// 7
	//P_TONE,			// 8
	P_SHOWTEMP,			// 9
	P_RFPANEL,			// 10
	P_VIS_SANITARI,	// 11
	P_VIS_TIPOSTUFA,	// 12
	P_ACC_CONSUMO,		// 13
	P_NUM_NO_ACC,		// 14
	P_USER_COLOR_ID,		// 15
	// parametri particolari
	P_ATTIVA_COCLEA,			// 100
	P_ATTIVA_ESP,			// 104
	P_ATTIVA_POMPA,		// 105
	P_TEST_AUTO,		// 106
	P_TEST_CALIB_FOTORES_ON,		// 107
	P_TEST_CALIB_FOTORES_OFF,		// 108
	P_FAN1_INTERPOL,		// 109
	P_FAN2_INTERPOL,		// 110
	P_FAN3_INTERPOL,		// 111
	P_TON_COCLEA_INTERPOL,		// 112
	P_TON_FAN1_INTERPOL,		// 113
	P_RPM1_INTERPOL,		// 114
	P_RPM2_INTERPOL,		// 115
	P_LPM_INTERPOL,		// 116
	P_ESP2_INTERPOL,		// 117

	// parametro Sleep
	P_SLEEP,			// 200

	// parametri Crono
	P_CRONO,									// 300
	P_CRONO_PROFILO,						// 301
	// prg1 settimanale
	P_CRONO_P1_SETTIM,				// 310
	P_CRONO_P1_SETTIM_START,
	P_CRONO_P1_SETTIM_STOP,
	P_CRONO_P1_SETTIM_TEMP_ARIA,
	P_CRONO_P1_SETTIM_TEMP_H2O,
	P_CRONO_P1_SETTIM_POTENZA,
	P_CRONO_P1_SETTIM_WEEK_DOM,		// 320
	P_CRONO_P1_SETTIM_WEEK_LUN,
	P_CRONO_P1_SETTIM_WEEK_MAR,
	P_CRONO_P1_SETTIM_WEEK_MER,
	P_CRONO_P1_SETTIM_WEEK_GIO,
	P_CRONO_P1_SETTIM_WEEK_VEN,
	P_CRONO_P1_SETTIM_WEEK_SAB,
#ifdef menu_CRONO
	// prg2 settimanale
	P_CRONO_P2_SETTIM,				// 330
	P_CRONO_P2_SETTIM_START,
	P_CRONO_P2_SETTIM_STOP,
	P_CRONO_P2_SETTIM_TEMP_ARIA,
	P_CRONO_P2_SETTIM_TEMP_H2O,
	P_CRONO_P2_SETTIM_POTENZA,
	P_CRONO_P2_SETTIM_WEEK_DOM,		// 340
	P_CRONO_P2_SETTIM_WEEK_LUN,
	P_CRONO_P2_SETTIM_WEEK_MAR,
	P_CRONO_P2_SETTIM_WEEK_MER,
	P_CRONO_P2_SETTIM_WEEK_GIO,
	P_CRONO_P2_SETTIM_WEEK_VEN,
	P_CRONO_P2_SETTIM_WEEK_SAB,
	// prg3 settimanale
	P_CRONO_P3_SETTIM,				// 350
	P_CRONO_P3_SETTIM_START,
	P_CRONO_P3_SETTIM_STOP,
	P_CRONO_P3_SETTIM_TEMP_ARIA,
	P_CRONO_P3_SETTIM_TEMP_H2O,
	P_CRONO_P3_SETTIM_POTENZA,
	P_CRONO_P3_SETTIM_WEEK_DOM,		// 360
	P_CRONO_P3_SETTIM_WEEK_LUN,
	P_CRONO_P3_SETTIM_WEEK_MAR,
	P_CRONO_P3_SETTIM_WEEK_MER,
	P_CRONO_P3_SETTIM_WEEK_GIO,
	P_CRONO_P3_SETTIM_WEEK_VEN,
	P_CRONO_P3_SETTIM_WEEK_SAB,
	// prg4 settimanale
	P_CRONO_P4_SETTIM,				// 370
	P_CRONO_P4_SETTIM_START,
	P_CRONO_P4_SETTIM_STOP,
	P_CRONO_P4_SETTIM_TEMP_ARIA,
	P_CRONO_P4_SETTIM_TEMP_H2O,
	P_CRONO_P4_SETTIM_POTENZA,
	P_CRONO_P4_SETTIM_WEEK_DOM,		// 380
	P_CRONO_P4_SETTIM_WEEK_LUN,
	P_CRONO_P4_SETTIM_WEEK_MAR,
	P_CRONO_P4_SETTIM_WEEK_MER,
	P_CRONO_P4_SETTIM_WEEK_GIO,
	P_CRONO_P4_SETTIM_WEEK_VEN,
	P_CRONO_P4_SETTIM_WEEK_SAB,
	// prg5 settimanale
	P_CRONO_P5_SETTIM,				// 390
	P_CRONO_P5_SETTIM_START,
	P_CRONO_P5_SETTIM_STOP,
	P_CRONO_P5_SETTIM_TEMP_ARIA,
	P_CRONO_P5_SETTIM_TEMP_H2O,
	P_CRONO_P5_SETTIM_POTENZA,
	P_CRONO_P5_SETTIM_WEEK_DOM,		// 400
	P_CRONO_P5_SETTIM_WEEK_LUN,
	P_CRONO_P5_SETTIM_WEEK_MAR,
	P_CRONO_P5_SETTIM_WEEK_MER,
	P_CRONO_P5_SETTIM_WEEK_GIO,
	P_CRONO_P5_SETTIM_WEEK_VEN,
	P_CRONO_P5_SETTIM_WEEK_SAB,
	// prg6 settimanale
	P_CRONO_P6_SETTIM,				// 410
	P_CRONO_P6_SETTIM_START,
	P_CRONO_P6_SETTIM_STOP,
	P_CRONO_P6_SETTIM_TEMP_ARIA,
	P_CRONO_P6_SETTIM_TEMP_H2O,
	P_CRONO_P6_SETTIM_POTENZA,
	P_CRONO_P6_SETTIM_WEEK_DOM,		// 420
	P_CRONO_P6_SETTIM_WEEK_LUN,
	P_CRONO_P6_SETTIM_WEEK_MAR,
	P_CRONO_P6_SETTIM_WEEK_MER,
	P_CRONO_P6_SETTIM_WEEK_GIO,
	P_CRONO_P6_SETTIM_WEEK_VEN,
	P_CRONO_P6_SETTIM_WEEK_SAB,		// 426
#endif	// menu_CRONO

	NUMPAR
};

// tabella dei gruppi di parametri
#define NUM_GROUP_PAR			7
typedef struct
{
	char cId;						// carattere identificativo
	unsigned short iParStart;	// indice primo parametro
	unsigned short iParEnd;		// indice ultimo parametro
} PAR_GROUP;
extern const PAR_GROUP Group_tab[NUM_GROUP_PAR+1];

// tabella degli indici alle stringhe correlate ai valori di alcuni parametri
extern const WORD sWeekDay_tab[7];
extern const WORD sOnOff_tab[2];
extern const WORD ShowTemp_StrInd_tab[NSHOWTEMP];

// Tabella dei riferimenti ai parametri
typedef struct
{
	unsigned char Type;	// tipo
	unsigned char Pos_Len;	// posizione nella bitmap (valido solo per il tipo BOOL),
							// maschera per TYPE_BYTE_MASK,
							// lunghezza della stringa (valido solo per il tipo CHAR)
	union
	{
		unsigned long d;
		BYTE_UNION Min;		// minimo
	} uMin;
	union
	{
		unsigned long d;
		BYTE_UNION Max;		// massimo
	} uMax;
	/* union
	{
		unsigned long d;
		BYTE_UNION Def;		// default
	} uDef; */
	unsigned char Step;		// inc/dec step
	union
	{
		void *p;
		BYTE_UNION *pPar;		// puntatore al parametro o alla bitmap
	} upPar;
	unsigned short iGrPar;	// indice parametro nel gruppo
	// unsigned short iStrDesc;		// indice stringa di descrizione o NUMMSG
	const unsigned short *iStrVal;		// array degli indici delle stringhe correlate ai possibili valori
	unsigned short IndVar;		// indirizzo in tabella MODBUS
	unsigned char MBTab;		// tipo tabella MODBUS
	// unsigned char login;	// livello di accesso
} PARTAB_STRUCT;
extern const PARTAB_STRUCT Param_tab[NUMPAR];




/*!
** \fn unsigned char userDelLeadingZero( unsigned char *buf, unsigned char size )
** \brief Replace the leading '0' with ' ' for a numeric value in a string buffer
** \param buf The string buffer of the numeric value
** \param size The string buffer size
** \return The first not-replaced character index in the string buffer
**/
unsigned char userDelLeadingZero( unsigned char *buf, unsigned char size );

/*!
** \fn void userDelLeadingEndingSpace( unsigned char *buf, unsigned char size )
** \brief Copy the string buffer in the 'str1' string without leading and ending
** 		space characters
** \param buf The string buffer
** \param size The string buffer size
** \return None
**/
void userDelLeadingEndingSpace( unsigned char *buf, unsigned char size );

/*!
** \fn unsigned short SearchParam( void )
** \brief Ricerca indice parametro da descrizione sintetica 'sDescPar'
** \return L'indice del parametro trovato in tabella, 0xFFFF altrimenti.
**/
unsigned short SearchParam( void );

/*!
	\fn void userGetParCode( unsigned short iPar )
	\brief Visualizza il codice del parametro nella stringa 'str2'
	\param iPar Indice parametro
	\return None
*/
void userGetParCode( unsigned short iPar );

/*!
** \fn void userGetPar( unsigned short iPar )
** \brief Get the parameter value into the 'userParValue' variable
** \param iPar Parameter index
** \return None
**/
void userGetPar( unsigned short iPar );

/*!
** \fn void userVisPar( unsigned short iPar )
** \brief Get the parameter value from the 'userParValue' variable 
** 		into the 'str2' string
** \param iPar Parameter index
** \return None
**/
void userVisPar( unsigned short iPar );

/*!
** \fn void userVisEPar( unsigned short iPar )
** \brief Get the parameter long string value into the 'strPar' string
** \param iPar Parameter index
** \return None
**/
void userVisEPar( unsigned short iPar );

/*!
** \fn void userIncPar( unsigned short iPar, unsigned char Ricirc )
** \brief Increment the parameter value into the 'userParValue' variable
** \param iPar Parameter index
** \param Ricirc 0: without roll-off, 1: with roll-off
** \return None
**/
void userIncPar( unsigned short iPar, unsigned char Ricirc );

/*!
** \fn void userDecPar( unsigned short iPar, unsigned char Ricirc )
** \brief Decrement the parameter value into the 'userParValue' variable
** \param iPar Parameter index
** \param Ricirc 0: without roll-off, 1: with roll-off
** \return None
**/
void userDecPar( unsigned short iPar, unsigned char Ricirc );

/*!
** \fn void userSetPar( unsigned short iPar )
** \brief Set the parameter value by the 'userParValue' variable
** \param iPar Parameter index
** \return None
**/
void userSetPar( unsigned short iPar );

/*!
** \fn void userEscPar( unsigned short iPar )
** \brief Quit the parameter's setting
** \param iPar Parameter index
** \return None
**/
void userEscPar( unsigned short iPar );

/*!
** \fn short userIsMinPar( unsigned short iPar )
** \brief Check if the parameter value into the 'userParValue' variable is at minimum 
** \param iPar Parameter index
** \return 1: at minimum, 0: otherwise
**/
short userIsMinPar( unsigned short iPar );

/*!
** \fn short userIsMaxPar( unsigned short iPar )
** \brief Check if the parameter value into the 'userParValue' variable is at maximum 
** \param iPar Parameter index
** \return 1: at minimum, 0: otherwise
**/
short userIsMaxPar( unsigned short iPar );

/*!
	\fn short userReqPar( unsigned short iPar )
   \brief Richiede il valore e i limiti del parametro indicato da 'iPar'.
	\param iPar Indice parametro
	\return 1: OK, 0: FAILED
*/
short userReqPar( unsigned short iPar );

/*!
	\fn short userIsReadOnlyVar( unsigned short iPar )
	\brief Determina se il parametro indicato da 'iPar' e` read-only o meno.
	\param iPar Indice parametro
	\return 1: read-only, 0: altrimenti
*/
short userIsReadOnlyVar( unsigned short iPar );

/*! 
	\fn short userIsFlagVar( unsigned short iPar )
	\brief Determina se il parametro indicato da 'iPar' e` un flag o meno.
	\param iPar Indice parametro
	\return 1: flag, 0: altrimenti
*/
short userIsFlagVar( unsigned short iPar );

/*!
	\fn void userGetMinMaxStep( unsigned short iPar, BYTE_UNION *vMin,
										BYTE_UNION *vMax, unsigned char *step )
	\brief Ritorna i limiti e il passo di incremento/decremento per il parametro
			 indicato da 'iPar'.
	\param iPar Indice parametro
	\param vMin Puntatore al valore minimo
	\param vMax Puntatore al valore massimo
	\param step Puntatore al passo di incremento/decremento
	\return None
*/
void userGetMinMaxStep( unsigned short iPar, BYTE_UNION *vMin,
								BYTE_UNION *vMax, unsigned char *step );
								
/*!
** \fn void userScriviOraSleep( void )
** \brief Get the Sleep time from the 'userParValue' variable into the 'str2' string
** \return None
**/
void userScriviOraSleep( void );

/*!
** \fn void userGetOraSleep( void )
** \brief Get the current Sleep time into the 'userParValue' variable
** \return None
**/
void userGetOraSleep( void );

/*! 
	\fn unsigned char UpdateSleepTime( void )
	\brief Esegue il set della struttura "Sleep" in base al valore impostato su
			"userParValue.b[0]"
	\return None
*/
unsigned char UpdateSleepTime( void );

/*!
** \fn void userSetNetworkFlags( void )
** \brief Set the Network flags bitmap
** \return None
**/
void userSetNetworkFlags( void );




#endif	// _USER_PARAMETRI_H_
