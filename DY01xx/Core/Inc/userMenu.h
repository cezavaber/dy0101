/*!
** \file userMenu.h
** \author Alessandro Vagniluca
** \date 26/11/2015
** \brief USERMENU.C header file
** 
** \version 1.0
**/

#ifndef _USER_MENU_H_
#define _USER_MENU_H_



#define SPECIALF_TYPE	-2			// definizione voce di tipo special function
#define SUBMENU_M_TYPE	-3			// definizione voce di tipo submenu menu
#define SUBMENU_P_TYPE	-4			// definizione voce di tipo submenu param
#define FUNCTION_ATTIVA_COCLEA_TYPE	(-(P_ATTIVA_COCLEA))	// definizione voce ATTIVA_COCLEA (funzione attivabile/disattivabile)
#define FUNCTION_ATTIVA_ESP_TYPE	(-(P_ATTIVA_ESP))	// definizione voce ATTIVA_ESP (funzione attivabile/disattivabile)
#define FUNCTION_ATTIVA_POMPA_TYPE	(-(P_ATTIVA_POMPA))	// definizione voce ATTIVA_POMPA (funzione attivabile/disattivabile)
#define FUNCTION_TEST_TYPE	(-(P_TEST))	// definizione voce TEST (funzione attivabile/disattivabile)
#define FUNCTION_TEST_AUTO_TYPE	(-(P_TEST_AUTO))	// definizione voce TEST_AUTO (funzione attivabile/disattivabile)
#define FUNCTION_TEST_CALIB_FOTORES_ON_TYPE	(-(P_TEST_CALIB_FOTORES_ON))	// definizione voce TEST_CALIB_FOTORES_ON (funzione attivabile/disattivabile)
#define FUNCTION_TEST_CALIB_FOTORES_OFF_TYPE	(-(P_TEST_CALIB_FOTORES_OFF))	// definizione voce TEST_CALIB_FOTORES_OFF (funzione attivabile/disattivabile)
#define SPECIAL_WIFI_SSID_TYPE	(-(P_WIFI_SSID))	// definizione voce WIFI_SSID (valore stringa lunga)
#define SPECIAL_WIFI_KEYW_TYPE	(-(P_WIFI_KEYW))	// definizione voce WIFI_KEYW (valore stringa lunga)
#define SPECIAL_WIFI_PORT_TYPE	(-(P_WIFI_PORT))	// definizione voce WIFI_PORT (valore stringa lunga)
#define SPECIAL_WPS_PIN_TYPE	(-(P_WPS_PIN))	// definizione voce WPS_PIN (valore stringa lunga)
#define SPECIAL_DATE_TYPE	(-(P_YEAR))	// definizione voce DATE (data)
#define SPECIAL_TIME_TYPE	(-(P_HOUR))	// definizione voce TIME (ora)


// --------------------------------------------------------------------------------
// definizione struttura dati record tabella gestione voce per menu di tipo EXTENDED
typedef struct ExtRec_Structure
{
   short  code;						// codice parametro o tipo voce (submenu o special)
	short (*IsToView)(void);		// ptr a funzione di check visualizzazione parametro
   const void *VoceExtIndex;	// ptr a struttura submenu (se code == SUBMENU_TYPE)
	// userStateHandler PreFunc;		// ptr a funzione esecutiva all'ingresso in modifica (se code != SUBMENU_TYPE)
	userStateHandler ExecFunc;		// ptr a funzione esecutiva al SET (se code != SUBMENU_TYPE)
	userStateHandler EscModFunc;	// ptr a funzione in caso di tasto ESC in modifica
	userStateHandler EscFunc;		// ptr a funzione in caso di tasto ESC da selezione (non da modifica)
	unsigned short wDispId;			// id stringa di visualizzazione
	unsigned char LivAccesso;		// livello di accesso
} EXTREC_STRUCT;


// variabili per gestione menu`
#define NUM_MAX_ITEM		25		// max 25 voci (5 pagine di 5 voci ciascuna)
// livelli di menu
enum livmenu
{
	LIVMNENU_0,
	LIVMNENU_1,
	LIVMNENU_2,
	LIVMNENU_3,
	LIVMNENU_4,
	LIVMNENU_5,
	NLIVMNENU
};
extern unsigned char iLivMenu;	//!< livello corrente di menu`
extern unsigned char iVoceMenu[NLIVMNENU];	//!< indice item di menu`
#define iMenu1Item	iVoceMenu[0]	// indice item di men� di livello 1
#define iMenu2Item	iVoceMenu[1]	// indice item di men� di livello 2
#define iMenu3Item	iVoceMenu[2]	// indice item di men� di livello 3
#define iMenu4Item	iVoceMenu[3]	// indice item di men� di livello 4
#define iMenu5Item	iVoceMenu[4]	// indice item di men� di livello 5
#define iMenu6Item	iVoceMenu[5]	// indice item di men� di livello 6
extern unsigned char iPageMenu[NLIVMNENU];	//!< indice pagina di menu`
#define iMenu1Pag		iPageMenu[0]	// indice pagina di men� di livello 1
#define iMenu2Pag		iPageMenu[1]	// indice pagina di men� di livello 2
#define iMenu3Pag		iPageMenu[2]	// indice pagina di men� di livello 3
#define iMenu4Pag		iPageMenu[3]	// indice pagina di men� di livello 4
#define iMenu5Pag		iPageMenu[4]	// indice pagina di men� di livello 5
#define iMenu6Pag		iPageMenu[5]	// indice pagina di men� di livello 6
extern char strTab[NUM_MAX_ITEM+4][DISPLAY_MAX_COL+1];	//!< stringhe voci di menu`
#define strPar			strTab[0]	// stringa per valore parametro
#define str1			strTab[NUM_MAX_ITEM]
#define str2			strTab[NUM_MAX_ITEM+1]
#define str3			strTab[NUM_MAX_ITEM+2]
#define str4			strTab[NUM_MAX_ITEM+3]
extern unsigned char maxLen;	//!< max lunghezza stringa lunga valore parametro
#define TEXT_MAX_LEN		125	//!< max lunghezza stringa editabile

extern unsigned char IndexVisItem[NUM_MAX_ITEM];	// indici in tabella EXTREC_STRUCT delle voci visualizzate

#define NDIGIT		4
extern char sDescPar[NDIGIT+1];	// codice descrizione parametro corrente

extern unsigned char LivAccesso;	//!< livello di accesso al menu`

extern BM_HEADER * m_bmhdrQUIT;	//!< QUIT bitmap header pointer
extern BM_HEADER * m_bmhdrCHECKED;	//!< CHECKED bitmap header pointer
extern BM_HEADER * m_bmhdrUNCHECKED;	//!< UNCHECKED bitmap  header pointer

/*!
** \fn short WinSetString( char *sTitle, char *String, unsigned short MaxLen )
** \brief Set string window handler
** \param sTitle The window's title string
** \param String The string to be set
** \param MaxLen The string maximum lenght
** \return 1: SET, 0: QUIT
**/
short WinSetString( char *sTitle, char *String, unsigned short MaxLen );

/*!
**  \fn short WinSetParam( unsigned short iPar, char *sDesc, char *sTitle )
**  \brief Set parameter value window handler
**  \param iPar The parameter index
**  \param sDesc The parameter description string
**  \param sTitle The window's title string
**  \return 1: SET, 0: QUIT
**/
short WinSetParam( unsigned short iPar, char *sDesc, char *sTitle );

/*!
**  \fn short WinSetTime( char *sTitle )
**  \brief Set time window handler
**  \param sTitle The window's title string
**  \return 1: SET, 0: QUIT
**/
short WinSetTime( char *sTitle );

/*!
**  \fn short WinSetDate( char *sTitle )
**  \brief Set date window handler
**  \param sTitle The window's title string
**  \return 1: SET, 0: QUIT
**/
short WinSetDate( char *sTitle );

/*!
** \fn short WinPassword( char *title, unsigned char *Char, unsigned char *Number )
** \brief PASSWORD window handler
** \param sTitle The window's title string
** \param Char The pointer to the password character part 
** \param Number The pointer to the password numerical part 
** \return 1: OK, 0: quit
**/
short WinPassword( char *sTitle, unsigned char *Char, unsigned char *Number );

/*!
** \fn short WinConfirm( char *sTitle, char *msg )
** \brief CONFIRM window handler
** \param sTitle The window's title string
** \param msg The description string of what to confirm
** \return 1: OK, 0: quit
**/
short WinConfirm( char *sTitle, char *msg );

/*!
** \fn short WinSetParWaitResult( unsigned short iPar, char *sTitle, char *sDesc )
** \brief Send the setting command for the parameter and wait the setting result
** \param iPar The parameter index
** \param sTitle The window's title string
** \param sDesc The parameter description string
** \return 1: SUCCESSFUL, 0: FAILED, -1: TIMEOUT
**/
short WinSetParWaitResult( unsigned short iPar, char *sTitle, char *sDesc );

/*!
** \fn short WinSetDisplay( unsigned short iPar, char *sDesc, char *sTitle )
**  \brief Set display (user color ID and LCD backlight time) window handler
**  \param iPar The parameter index (P_BACKLIGHT)
**  \param sDesc The parameter description string
**  \param sTitle The window's title string
**  \return 1: SET, 0: QUIT
**/
short WinSetDisplay( unsigned short iPar, char *sDesc, char *sTitle );

/*!
** \fn unsigned char userExitMenu( void )
** \brief Return to Main Window from Menu
** \return The next User Interface state 
**/
unsigned char userExitMenu( void );

/*! 
	\fn short CheckMenuAccess( const EXTREC_STRUCT *ExtRec, unsigned char nItem )
	\brief Verifica l'accessibilita` di un menu`
	\param ExtRec Tabella dei dati di gestione degli elementi
	\param nItem Numero degli elementi nella tabella
	\return 1: menu` accessibile, 0: menu` non accessibile
*/
short CheckMenuAccess( const EXTREC_STRUCT *ExtRec, unsigned char nItem );

/*! 
	\fn short userIsWarning( void )
	\brief Verifica la presenza di anomalie da visualizzare
	\return 0: nessuna anomalia, 1: una o piu' anomalie presenti
*/
short userIsWarning( void );

/*! 
	\fn unsigned char EnterMenuInfo( void )
	\brief Ingresso nel MENU INFO
	\return Il codice dello stato di transizione
*/
unsigned char EnterMenuInfo( void );

/*! 
	\fn unsigned char RetMenuInfo( void )
	\brief Ritorno al MENU INFO
	\return Il codice dello stato di transizione
*/
unsigned char RetMenuInfo( void );

/*! 
	\fn unsigned char EnterMenuNetwork( void )
	\brief Ingresso nel MENU NETWORK
	\return Il codice dello stato di transizione
*/
unsigned char EnterMenuNetwork( void );

/*! 
	\fn unsigned char EnterMenuWarn( void )
	\brief Ingresso nel MENU ANOMALIE
	\return Il codice dello stato di transizione
*/
unsigned char EnterMenuWarn( void );

/*! 
	\fn unsigned char EnterMainMenu( void )
	\brief Ingresso nel MAIN MENU
	\return Il codice dello stato di transizione
*/
unsigned char EnterMainMenu( void );

/*!
** \fn unsigned char userMenu( void )
** \brief Handler function for the USER_STATE_MENU and USER_STATE_INFO
** 		User Interface states
** \return The next User Interface state 
**/
unsigned char userMenu( void );

/*! 
	\fn unsigned char RetMenuTec( void )
	\brief Ritorno al MENU TECNICO
	\return Il codice dello stato di transizione
*/
unsigned char RetMenuTec( void );

/*! 
	\fn unsigned char EnterLocalSettings( void )
	\brief Ingresso nel LOCAL SETTINGS MENU
	\return Il codice dello stato di transizione
*/
unsigned char EnterLocalSettings( void );

/*! 
	\fn unsigned char EnterMenuCronoProg( void )
	\brief Ingresso nel MENU CRONO PROGRAMMA SETTIMANALE
	\return Il codice dello stato di transizione
*/
unsigned char EnterMenuCronoProg( void );

/*!
** \fn unsigned char ExitMenuCronoProg( void )
** \brief Return to Crono Window from Crono Prog Window
** \return The next User Interface state
**/
unsigned char ExitMenuCronoProg( void );



#endif	// _USER_MENU_H_
