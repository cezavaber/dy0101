; 
; \file stringhe_RUS.c
; \brief RUS strings set
; 


; week days
Bockpeceh�e; sDomenica_RUS
Soheger�hnk; sLunedi_RUS
Btophnk; sMartedi_RUS
Cpega; sMercoledi_RUS
Yetbepf; sGiovedi_RUS
S�thnwa; sVenerdi_RUS
Cuddota; sSabato_RUS

B�k; sOff_RUS
Bkr; sOn_RUS
Abto; sAuto_RUS
�C; sDegC_RUS
�F; sDegF_RUS
H2O; sH2O_RUS

; stati della stufa
LAJNFAHNE; Stufa_Accensione_RUS
BKR; Stufa_Accesa_RUS
B�KR�YEHNE; Stufa_Spegnimento_RUS
B�K; Stufa_MCZ_RUS
ARAPM; Stufa_Allarme_RUS
B�KR�YEHNE SOCRE DEL LREKTPNYECTBA; Stufa_SpeRete_RUS
LAJNFAHNE SOCRE DEL LREKTPNYECTBA; Stufa_AccRete_RUS
CDPOC; Stufa_Reset_RUS
KOHTPOR�; Stufa_Collaudo_RUS
 ; Stufa_NonDef_RUS

; allarme
ARAPM; Alarm_RUS

; descrizione allarme
 ; AlarmSource00_RUS
Heugaya laj�fahn�; AlarmSource01_RUS
B�kr�yehne sramehn; AlarmSource02_RUS
Sepefpebahne tahk serret; AlarmSource03_RUS
Temsepatupa g�m. Falob ypelmepha�; AlarmSource04_RUS
Arapm Pere gabrehn�; AlarmSource05_RUS
Arapm bolgux fopehn�; AlarmSource06_RUS
Gbepwa otkp�ta; AlarmSource07_RUS
Sobpejgeh g�mococ; AlarmSource08_RUS
Sobpejgeh Lohg g�ma; AlarmSource09_RUS
Sobpejgeh Bocsramehnter�; AlarmSource10_RUS
Sobpejgeh �hek; AlarmSource11_RUS
 ; AlarmSource12_RUS
Sobpejgeh �rektpohha� kaptoyka; AlarmSource13_RUS
 ; AlarmSource14_RUS
Arapm upobh� serret; AlarmSource15_RUS
Gabrehne bog� la spegeramn sor�; AlarmSource16_RUS
Gbepwa otkp�ta tahk serret; AlarmSource17_RUS
Sepefpebahne tahk bog�; AlarmSource18_RUS

; ripristino allarme
 ; AlarmRecovery00_RUS
Oynctnt� Bctp�x. N sospoduqte choba; AlarmRecovery01_RUS
Sosorh�t� tahk serret; AlarmRecovery02_RUS
Spobep�t� dukret nhctpuhwnn; AlarmRecovery03_RUS
Spobep�t� dukret nhctpuhwnn; AlarmRecovery04_RUS
Spobepnt� hespoxognmoct�; AlarmRecovery05_RUS
Spobepnt� ynctka bctp�x./bxog bolgux/g�moxog; AlarmRecovery06_RUS
Spobepnt� lakp�tne gbepwa; AlarmRecovery07_RUS
Solbohnte somo��; AlarmRecovery08_RUS
Solbohnte somo��; AlarmRecovery09_RUS
Solbohnte somo��; AlarmRecovery10_RUS
Solbohnte somo��; AlarmRecovery11_RUS
Solbohnte somo��; AlarmRecovery12_RUS
Solbohnte somo��; AlarmRecovery13_RUS
Solbohnte somo��; AlarmRecovery14_RUS
Spobep�t� upobh� serret; AlarmRecovery15_RUS
Bocctahobnt� spabnr�hoe gabrehne uctahobke; AlarmRecovery16_RUS
Spobep�t� lakp�tne gbepwa serret; AlarmRecovery17_RUS
Spobep�t� dukret nhctpuhwnn; AlarmRecovery18_RUS

; sleep
Abtob�kr�yehne; Menu_Sleep_RUS

; avvio
Yac�; Settings_Time_RUS
Gata; Menu_DataOra_RUS	
Mo�hoct�; Potenza_RUS		
Temsepatupa bolguxa; Temperatura_RUS
Temsepatupa bog�; Menu_Idro_RUS
Behtnr�top; Fan_RUS

; network
Cet�; Menu_Network_RUS
;Wi-Fi; WiFi_RUS
SSID; Ssid_RUS
Keyword; Keyword_RUS
;TCP Port; TCPPort_RUS
;WPS PIN; WPSPin_RUS

; anomalie
Ahomarnn; Menu_Anomalie_RUS
Texhnyecka� somo��; RichiestaService_RUS
Lohg tems. bolguxa sobpejgeh; GuastoSondaTempAria_RUS
Lohg tems. bog� sobpejgeh; GuastoSondaTempAcqua_RUS
Pere gabr. bog� sobpejgeho; GuastoPressostAcqua_RUS
Gabr. bog� bhe gnasaloha; SovraPressAcqua_RUS
Gatynk pacxoga bolguxa sobpejgeh; GuastoSensorePortata_RUS
Serret� lakahynba�tc�; PelletEsaurito_RUS
Gbepwa otkp�ta; PortaAperta_RUS

; info
Uhvopmawn�; Menu_Infos_RUS
Kog kapt�; CodiceScheda_RUS
Kog delosachoctn; CodiceSicurezza_RUS
Kog gncsre�; CodicePannello_RUS
Kog Sapametp�; CodiceParametri_RUS
Bpem� padot�; OreFunzionamento_RUS
Hujho texh. Somo��; OreSat_RUS
Texhnyecka� Somo��; Service_RUS
Odopot� g�mococa 1; VentilatoreFumi_RUS
Pacxog bolguxa nlmepeh; PortataAriaMisurata_RUS
Octatoyhofo Kncropoga; OssigenoResiduo_RUS
Sotpedrehne Serret�; Consumo_RUS
Temsepatupa g�m. falob; TemperaturaFumi_RUS
Votocospotnbrehne; FotoResistenza_RUS
Bpem� zheka 1; TempoCoclea_RUS
Od. zheka 1; GiriCoclea_RUS
Aktnbawn� beht.1; MotoreScambiatore_RUS
Aktnbawn� beht.2; MotoreScambiatore2_RUS
Gabrehne bog�; IdroPress_RUS
bocsramehehnq Homep; NumAccensioni_RUS
NP agpec; IPAddress_RUS
Nctopn� tpebof; StoricoAllarmi_RUS

; menu principale
Frabhoe meh�; MainMenu_RUS
Hactpoqkn; Menu_Impostazioni_RUS
Texhnyeckoe meh�; Menu_Tecnico_RUS

; settings
�l�k; Lingua_RUS
Eko; EcoMode_RUS
Gncsre�; Settings_Backlight_RUS
�C / �F; Settings_ShowTemp_RUS
Lafpulka zheka; Precarica_Pellet_RUS
Ynctka; Attiva_Pulizia_RUS
Aktnb. hacoca; Attiva_Pompa_RUS
Pagno NG.; RFPanel_RUS

; ricette
Pefurnpobat� fopehn�; Ricette_RUS
Bolgux; OffsetEspulsore_RUS
Serret�; RicettaPelletON_RUS
Kncropog; Ossigeno_RUS

; temp H2O
Ctabnt� otosrehne; TempH2O_SetRisc_RUS
Ctabnt� fop�ya� boga; TempH2O_SetSan_RUS

; menu nascosto
Sapor�; HidPassword_RUS

;012345678901234567890 menu tecnico - livello 1					
Kohvnfupawn�; ConfigSystem_RUS
Kohtpor�; Controllo_RUS
Fngpo; MenuIdro_RUS
Lajnfahne/b�kr�yehne; AttuaTransitorie_RUS
Mo�hoct�; AttuaPotenza_RUS
Uspabr. abap. cnfh.; GestAllarmi_RUS
Kohtpor�; Collaudo_RUS
Sonck so kogu; RicercaCodParam_RUS

;012345678901234567890 menu tecnico - livello 2							
Hactpoqkn; MenuParamGen_RUS
Del Lrektpnyectba; Blackout_RUS
Zhek; MenuCoclea_RUS
Oynctnter�/Bctp�x.; MenuScuotitore_RUS
Beht. some�ehn�; FanAmbiente_RUS
Behtnr�top 1; Fan1_RUS
Behtnr�top 2; Fan2_RUS
Behtnr�top 3; Fan3_RUS

Vuhkwn� Eko; MenuEco_RUS
Bpem� padot�; TempiFunz_RUS

Hactpoqkn; ParamTransitori_RUS
Spegb. lajnfahne 1; Preacc1_RUS
Spegb. lajnfahne 2; Preacc2_RUS
Spegb. lajnf. fop.; PreaccCaldo_RUS
Lajnfahne A; AccA_RUS
Lajnfahne D; AccB_RUS
Sramehn BKR; FireON_RUS
B�kr�yehne A; SpeA_RUS
B�kr�yehne D; SpeB_RUS
Oxrajgehne; Raff_RUS

Sapametp� mo�hoctn; ParamPotenza_RUS
Wnkr ynctkn; MenuPB_RUS
Lagepjka g�mococa; RitardoAttuaEsp_RUS
Lagepj. zafa mo�h.; RitardoStepPot_RUS
Mo�hoct� 1; Pot1_RUS
Mo�hoct� 2; Pot2_RUS
Mo�hoct� 3; Pot3_RUS
Mo�hoct� 4; Pot4_RUS
Mo�hoct� 5; Pot5_RUS
Mo�hoct� 6; Pot6_RUS
Ynctka; Pulizia_RUS
Nhtepsor�wn�; Interpolazione_RUS

Ab. cnf. g�m. falob; AllarmeFumi_RUS
Gatynk serret; SensPellet_RUS
;Otcutctbne sramehn; AssenzaFiamma_RUS
Pacxog bolguxa; SensAria_RUS

Komahg�; Comandi_RUS
Hafpulkn; Carichi_RUS
Ocu�ectbrehne; Attuazioni_RUS

; parametri generali
Tns seyn; TipoStufa_RUS
Bocctahobrehne sepbohayar�hofo Tns seyn; TipoStufaDef_RUS
Tns gbnfater� 1; MotoreEspulsore_RUS
Tns gbnfater� 2; MotoreEspulsore2_RUS
Yncro beht.; Multifan_RUS
Bkr�yehne bog�; IdroMode_RUS
Socmotpet� FBC; VisSanitari_RUS
Socmotpet� Tns seyn; VisTipoStufa_RUS
Kohtpor� bolgux; SensorePortataAria_RUS
Kohtpor� odopotob; SensoreHall_RUS
Kohtpor� r�mdga; ControlloLambda_RUS
Ofpahnynter� mnh. odopotob; LimGiriMinimi_RUS
Tepmosapa; AccTermocoppia_RUS
Votocospotnbrehne; AccFotoResistenza_RUS
Laspe�ehne lajnf. D; InibAccB_RUS
;Rampa Candeletta; RampaCandeletta_RUS
Tepmoctat; Termostato_Ambiente_RUS
Pejnm sramehn/tems.; GestioneIbrida_RUS
;Uspabrehne gpoba/serret�; GestioneLegna_RUS
Mexahnyecka� ynctka; FScuotitore_RUS
Zhek 2 / Oynctnter�; Coclea2_RUS
Sogkr�yehne g�mococa 2; Esp2Enable_RUS
Kohtpor� odopotob g�mococa 2; SensoreHall_2_RUS
Gatynk upobh� serret; SensorePellet_RUS

; parametri blackout
Sogkr�yehne bocctah.; BlackoutRipristino_RUS
Spogorj. bocctah.; BlackoutDurata_RUS

; parametri coclea
Topmoj. zheka 1; FrenataCoclea_RUS
Tns topmoj. zheka 1; TipoFrenataCoclea_RUS
Topmoj. zheka 2; FrenataCoclea_2_RUS
Tns topmoj. zheka 2; TipoFrenataCoclea_2_RUS
Kohtpor� od. zheka 1; CtrlGiriCoclea_RUS
Sepnog zhek 1; PeriodoCoclea_RUS
Sepnog zhek 2; PeriodoCoclea2_RUS
Sogkr�yehne Sepnog zhek 1; AbilPeriodoCoclea_RUS
Sogkr�yehne Sepnog zhek 2; AbilPeriodoCoclea2_RUS

; parametri scuotitore
Spogorjnt. soruwnkra; ScuotDurata_RUS
Yncro wnkrob; ScuotNCicli_RUS
;Nhtepbar wnkrob; ScuotPeriodo_RUS

; parametri fan ambiente
Kohtpor� tems. g�m. falob; FanCtrlTempFumi_RUS
Tems. Bkr�y. Beht.; FanTempFumiON_RUS
Tems. Beht. mnh.; FanTempFumiOFF_RUS

; parametri fan
Upobeh� 1; FanAttuaL1_RUS
Upobeh� 2; FanAttuaL2_RUS
Upobeh� 3; FanAttuaL3_RUS
Upobeh� 4; FanAttuaL4_RUS
Upobeh� 5; FanAttuaL5_RUS

; parametri ricette

; parametri eco
Ekoctos; EcoStop_RUS
Ojng. Bkr; EcoAttesaAccensione_RUS
Ojng. B�k; Settings_TShutdown_Eco_RUS
Ger�ta tems.; EcoDeltaTemperatura_RUS

; parametri tempi funzionamento

; parametri idro
Fngpo helabncnm.; IdroIndipendente_RUS
B�kr�yehne Fngpo; SpegnimentoIdro_RUS
;Accumulo; FAccumulo_RUS
Laspe�ehne Sensing hacoca; InibSensing_RUS
Mogurnpu��nq hacoc; PompaModulante_RUS
Pere gabrehn� bog�; FPressIdro_RUS
;Flussostato Secondario; FlussostatoDigitale_RUS
Ucnrehne otosrehn�; GainRisc_RUS
Fnctepnlnc tems. bog�; IsteresiTempAcqua_RUS
Ger�ta Temse. fop�yeq bog�; DeltaSanitari_RUS
Ucnr. fop�yeq bog�; GainSanit_RUS
Tems. aktnbawnn LASACHOQ 2; TempAux_RUS
Makc. gabrehne bog�; MaxPressH2O_RUS
Tems. Bkr hacoc; TSETON_Pompa_RUS
Tems. B�k hacoc; TSETOFF_Pompa_RUS

; parametri transitori
Spogorj. spegb. lajnf. 1; AccTempoPreacc_RUS
Spogorj. spegb. lajnf. 2; AccTempoPreLoad_RUS
Spogorj. spegb. lajnf. fop�y.; AccTempoPreacc2_RUS
Tems. g�m. falob Bkr; AccTempOnFumi_RUS
Tems. g�m. falob B�k; AccTempOffFumi_RUS
Speger g�m. gpoba; AccTempSogliaFumi_RUS
Ojngahne gr� lajnf. fop�y.; AccTempoInc_RUS
Ger�ta tems. fop�y.; AccDeltaTempCaldo_RUS
Makc. spogorj. lajnfahn�; AccMaxTimeWarmUp_RUS
Spogorj. sramehn Bkr; AccMaxTimeFireOn_RUS
Spogorj. b�kr�yehn�; AccMaxTimeSpe_RUS

; parametri di attuazione
Bp. Bkr zhek 1; AttuaTempoOnCoclea1_RUS
Bp. B�k zhek 1; AttuaTempoOffCoclea1_RUS
Pacxog bolguxa; AttuaPortata_RUS
Odopot� g�mococa 1; AttuaGiriEsp_RUS
Odopot� g�mococa 2; AttuaGiriEsp2_RUS
Bp. Bkr zhek 2; AttuaTempoOnCoclea2_RUS
Bp. B�k zhek 2; AttuaTempoOffCoclea2_RUS
G�mococ 2; AttuaEspulsore2_RUS

; parametri di potenza
Upobhn mo�hoctn; LivPotenzaMax_RUS

; parametri pulizia braciere
Nhtepbar; AttesaPB_RUS
Spogorjnter�hoct�; DurataPB_RUS
Speger; MinPowerPB_RUS

; parametri ritardo espulsore
Lagepjka ubernyehn�; RitardoEspInc_RUS
Lagepjka umeh�zehn�; RitardoEspDec_RUS

; parametri ritardo step potenza
Lagepjka ubernyehn�; RitardoPotInc_RUS
Lagepjka umeh�zehn�; RitardoPotDec_RUS

; parametri allarme fumi
Laspe�ehne spegusp. cnfh.; AlmFumi_InibPreAlm_RUS
Bp. g�m. falob spegusp. cnfh.; AlmFumi_TempPreAlm_RUS
Makc. spogorj. spegusp. cnfh.; AlmFumi_DurataPreAlm_RUS
Fnctepnlnc spegusp. cnfh.; AlmFumi_GradientePreAlm_RUS
Bp. g�m. falob abap. cnfh.; AlmFumi_TempAlm_RUS

; parametri sensore pellet
Spogorjnt. spegusp. cnfh.; SensPellet_DurataPreAlm_RUS

; parametri assenza fiamma
Ger�ta g�m. falob B�k; NoFiamma_DeltaTemp_RUS

; parametri sensore portata aria
Speger otkp. gbepw�; Aria_PortataCritica_RUS
Spogorj. otkp. gbepw�; Aria_TempoAlmPorta_RUS
Spogorj. spegusp. cnfh. gbepw�; Aria_TempoPreAlmPorta_RUS
Laspe�ehne abap. cnfh. bolguxa fopehn�; Aria_InibAlmAriaCombust_RUS
Spogorj. spegusp. cnfh. bolguxa fopehn�; Aria_TempoPreAlmAriaCombust_RUS
Ger�ta pacxoga bolguxa fopehn�; Aria_DeltaPortAriaCombust_RUS

; comandi
Kohtpor� puyhoq; Test_StartStop_RUS
Kohtpor� abtomatnyecknq; Test_Sequenza_RUS
Daqsac; Test_Bypass_RUS
Karndpobka tepmosap�; Test_CalibTC_RUS
Karndpobka Votocospotnbrehne Bkr; Test_CalibFotoresOn_RUS
Karndpobka Votocospotnbrehne B�k; Test_CalibFotoresOff_RUS
Spogorj. Zafa Kohtpor�; Test_DurataStep_RUS

; carichi
Zhek 1; Test_Coclea_RUS
Bocsramehnter�; Test_Candeletta_RUS
Behtnr�top 1; Test_Fan1_RUS
Behtnr�top 2; Test_Fan2_RUS
Behtnr�top 3; Test_Fan3_RUS
G�mococ 1; Test_Espulsore_RUS
Hacoc; Test_Pompa_RUS
Tpexxogoboq behtnr�; Test_3Vie_RUS
Lasachoq 1; Test_Aux1_RUS
Lasachoq 2; Test_Aux2_RUS
Lasachoq A; Test_AuxA_RUS

; attuazioni
Ocu�ectbrehne beht. 1; AttuaFan1_RUS
Ocu�ectbrehne beht. 2; AttuaFan2_RUS
Ocu�ectbrehne beht. 3; AttuaFan3_RUS

; richieste di conferma
Sogtbepjgehne?; ReqConf_RUS
Kopotkoe lam�kahne tepmnhara, n aktnbnpobat�; TC_ReqConf_RUS

; Crono
Xpoho; Menu_Crono_RUS
Sogkr�yehne; Abilitazione_RUS
Lafpulka spovnr�; CaricaProfilo_RUS
Odhurehne; Azzera_RUS

; prog. crono settimanale
Spofpamma; PrgSettimanale_RUS
Sogkr�yehne; PrgAbilita_RUS
Ctapt; PrgStart_RUS
Ctos; PrgStop_RUS
Temsepatupa bolguxa; PrgTempAria_RUS
Temsepatupa bog�; PrgTempH2O_RUS
Sram�; PrgPotenza_RUS

;01 sigle fan
B1; sF1_RUS,
B2; sF2_RUS,

;0123 stati di attuazione
 OFF; sAttuaStat00_RUS,
PSU1; sAttuaStat01_RUS,
PSU2; sAttuaStat02_RUS,
WPSU; sAttuaStat03_RUS,
SU A; sAttuaStat04_RUS,
SU B; sAttuaStat05_RUS,
SU C; sAttuaStat06_RUS,
FONA; sAttuaStat07_RUS,
FONB; sAttuaStat08_RUS,
SD A; sAttuaStat09_RUS,
SD B; sAttuaStat10_RUS,
SD C; sAttuaStat11_RUS,
CD A; sAttuaStat12_RUS,
CD B; sAttuaStat13_RUS,
BCLA; sAttuaStat14_RUS,
BCLB; sAttuaStat15_RUS,
PL 1; sAttuaStat16_RUS,
PL 2; sAttuaStat17_RUS,
PL 3; sAttuaStat18_RUS,
PL 4; sAttuaStat19_RUS,
PL 5; sAttuaStat20_RUS,
PL 6; sAttuaStat21_RUS,
PL 7; sAttuaStat22_RUS,
PL 8; sAttuaStat23_RUS,
PL 9; sAttuaStat24_RUS,
PLSA; sAttuaStat25_RUS,


