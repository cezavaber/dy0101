; 
; \file stringhe_SRB.c
; \brief SRB strings set
; 


; week days
Hege�a; sDomenica_SRB
Sohege�ak; sLunedi_SRB
Utopak; sMartedi_SRB
Cpega; sMercoledi_SRB
Yetbptak; sGiovedi_SRB
Setak; sVenerdi_SRB
Cudota; sSabato_SRB

Ovv; sOff_SRB
Oh ; sOn_SRB
Auto; sAuto_SRB
�C; sDegC_SRB
�F; sDegF_SRB
H2O; sH2O_SRB

; stati della stufa
SA�E�E; Stufa_Accensione_SRB
OH; Stufa_Accesa_SRB
FAZE�E; Stufa_Spegnimento_SRB
OVV; Stufa_MCZ_SRB
ARAPMA; Stufa_Allarme_SRB
FAZE�E SOCRE HECTAHAK CTPU�E; Stufa_SpeRete_SRB
SA�E�E SOCRE HECTAHAK CTPU�E; Stufa_AccRete_SRB
PECETOBATN; Stufa_Reset_SRB
SPOBEPA; Stufa_Collaudo_SRB
 ; Stufa_NonDef_SRB

; allarme
ARAPMA; Alarm_SRB

; descrizione allarme
 ; AlarmSource00_SRB
Sa�e�e heucsero; AlarmSource01_SRB
Srameh ce facn; AlarmSource02_SRB
Spefpeba�e pelepboapa sereta; AlarmSource03_SRB
Spebernka tems.gnmhnx facoba; AlarmSource04_SRB
Arapm specoctata; AlarmSource05_SRB
Arapm balguxa la cafopeb; AlarmSource06_SRB
Bpata otbopeha; AlarmSource07_SRB
Kbap ekctpaktopa gnmh.facoba; AlarmSource08_SRB
Kbap cohge gnmh.facoba; AlarmSource09_SRB
Kbap cbe�nwe; AlarmSource10_SRB
Kbap motopa suja; AlarmSource11_SRB
 ; AlarmSource12_SRB
Kbap erektpohcke sroye; AlarmSource13_SRB
 ; AlarmSource14_SRB
Arapm hnbo sereta; AlarmSource15_SRB
Spntncak boge bah fpahnwa; AlarmSource16_SRB
Bpata ha pelepb.sereta otbopeha; AlarmSource17_SRB
Spefpe�ah pelepboap boge; AlarmSource18_SRB

; ripristino allarme
 ; AlarmRecovery00_SRB
Oynctnte rojnzte n sokuza�.sohobo; AlarmRecovery01_SRB
Hasuhnte pelepboap sereta; AlarmRecovery02_SRB
Spobep.spnpuyh.c usutctbnma; AlarmRecovery03_SRB
Spobep.spnpuyh.c usutctbnma; AlarmRecovery04_SRB
Otkrohntn ebehtuarho lasuze�e; AlarmRecovery05_SRB
Spob. ga rn cu rojnzte/Ural balguxa/Gnmob.web ynctn; AlarmRecovery06_SRB
Spobep.ga rn cu bpata latbop.; AlarmRecovery07_SRB
Solobnte acnctehwn�u; AlarmRecovery08_SRB
Solobnte acnctehwn�u; AlarmRecovery09_SRB
Solobnte acnctehwn�u; AlarmRecovery10_SRB
Solobnte acnctehwn�u; AlarmRecovery11_SRB
Solobnte acnctehwn�u; AlarmRecovery12_SRB
Solobnte acnctehwn�u; AlarmRecovery13_SRB
Solobnte acnctehwn�u; AlarmRecovery14_SRB
Spobepnte hnbo sereta; AlarmRecovery15_SRB
Ucsoctabnte spabnrah spntncak upe�a�a; AlarmRecovery16_SRB
Spobep.ga rn cu bpata sereta latbop.; AlarmRecovery17_SRB
Spobep.spnpuyh.c usutctbnma; AlarmRecovery18_SRB

; sleep
Crns; Menu_Sleep_SRB

; avvio
Cat; Settings_Time_SRB
Gatum; Menu_DataOra_SRB	
Chafa; Potenza_SRB		
Temsepatupa; Temperatura_SRB
Temsepatupa boge; Menu_Idro_SRB
Behtnr.; Fan_SRB

; network
Mepja; Menu_Network_SRB
;Wi-Fi; WiFi_SRB
SSID; Ssid_SRB
Keyword; Keyword_SRB
;TCP Port; TCPPort_SRB
;WPS PIN; WPSPin_SRB

; anomalie
Kbapobn; Menu_Anomalie_SRB
Pag; RichiestaService_SRB
Cohga tems.balguxa u kbapu; GuastoSondaTempAria_SRB
Cohga tems.boge u kbapu; GuastoSondaTempAcqua_SRB
Specoctat boge u kbapu; GuastoPressostAcqua_SRB
Spntncak boge bah fpahnwa; SovraPressAcqua_SRB
Kbap cehlopa spotoka balguxa; GuastoSensorePortata_SRB
Seretn spn kpa�u; PelletEsaurito_SRB
Bpata otbopeha; PortaAperta_SRB

; info
Nhvo; Menu_Infos_SRB
Znvpa sroye; CodiceScheda_SRB
Cnfuphocha znvpa; CodiceSicurezza_SRB
Znvpa ekpaha; CodicePannello_SRB
Znvpa sapametapa; CodiceParametri_SRB
Catn paga; OreFunzionamento_SRB
Catn paga; OreSat_SRB
Acnctehwn�a; Service_SRB
Odpta�n ekctpaktopa 1; VentilatoreFumi_SRB
Nlmepehn spotok balguxa; PortataAriaMisurata_SRB
Speoctarn knceohnk; OssigenoResiduo_SRB
Sotpoz�a sereta; Consumo_SRB
Temsep. gnmhnx facoba; TemperaturaFumi_SRB
Votootsophnk; FotoResistenza_SRB
Bpeme suja 11; TempoCoclea_SRB
Odpta�n suja 1; GiriCoclea_SRB
Uk�uynb. behtnr.1; MotoreScambiatore_SRB
Uk�uynb. behtnr.2; MotoreScambiatore2_SRB
Spntncak boge; IdroPress_SRB
Dpo� sa�e�a; NumAccensioni_SRB
NS Agpeca; IPAddress_SRB
Arapm Xnctopn; StoricoAllarmi_SRB

; menu principale
Frabhn mehn; MainMenu_SRB
Soctabke; Menu_Impostazioni_SRB
Texhnykn mehn; Menu_Tecnico_SRB

; settings
�elnk; Lingua_SRB
Eko; EcoMode_SRB
Ekpaha; Settings_Backlight_SRB
�C / �F; Settings_ShowTemp_SRB
Utobap suja; Precarica_Pellet_SRB
Ynz�e�e; Attiva_Pulizia_SRB
Aktnbnp.sumsu; Attiva_Pompa_SRB
Pagno NG; RFPanel_SRB

; ricette
Pewestn cafopeba�a; Ricette_SRB
Balgux; OffsetEspulsore_SRB
Seretn; RicettaPelletON_SRB
Knceohnk; Ossigeno_SRB

; temp H2O
Soctabka fpe�a�a; TempH2O_SetRisc_SRB
Soctabka cahnt.boge; TempH2O_SetSan_SRB

; menu nascosto
Rolnhka; HidPassword_SRB

;012345678901234567890 menu tecnico - livello 1					
Kohvnfupawn�a; ConfigSystem_SRB
Kohtpora; Controllo_SRB
Xngpo; MenuIdro_SRB
Sa�e�e/Faze�e; AttuaTransitorie_SRB
Chafa; AttuaPotenza_SRB
Uspab�a�e arapmnma; GestAllarmi_SRB
Spobepa; Collaudo_SRB
Spetpafa spema znvpn; RicercaCodParam_SRB

;012345678901234567890 menu tecnico - livello 2							
Soctabke; MenuParamGen_SRB
Hectahak ctpu�e; Blackout_SRB
Suj; MenuCoclea_SRB
Ynctay/Spotpecnbay; MenuScuotitore_SRB
Beht.spoctopn�e; FanAmbiente_SRB
Beht. 1; Fan1_SRB
Beht. 2; Fan2_SRB
Beht. 3; Fan3_SRB

Vuhkwn�a Eko; MenuEco_SRB
Bpeme paga; TempiFunz_SRB

Soctabke; ParamTransitori_SRB
Spegsa�e�e 1; Preacc1_SRB
Spegsa�e�e 2; Preacc2_SRB
Spegsa�. ha tosro; PreaccCaldo_SRB
Sa�e�e A; AccA_SRB
Sa�e�e B; AccB_SRB
Batpa OH; FireON_SRB
Faze�e A; SpeA_SRB
Faze�e B; SpeB_SRB
Xra�e�e; Raff_SRB

Sapametpn chafe; ParamPotenza_SRB
Wnkruc ynz�e�a; MenuPB_SRB
Kaz�e�e ekctpakt.; RitardoAttuaEsp_SRB
Kaz�e�e kopaka chafe; RitardoStepPot_SRB
Chafa 1; Pot1_SRB
Chafa 2; Pot2_SRB
Chafa 3; Pot3_SRB
Chafa 4; Pot4_SRB
Chafa 5; Pot5_SRB
Chafa 6; Pot6_SRB
Ynz�e�e; Pulizia_SRB
Nhtepsorawn�a; Interpolazione_SRB

Arapm.gnmh.facoba; AllarmeFumi_SRB
Cehlop sereta; SensPellet_SRB
;Assenza Fiamma; AssenzaFiamma_SRB
Spotok balguxa; SensAria_SRB

Komahge; Comandi_SRB
Ostepe�.; Carichi_SRB
Uk�uynba�a; Attuazioni_SRB

; parametri generali
Tns se�n; TipoStufa_SRB
Ucsoctab�.soyethnx bpegh.se�n; TipoStufaDef_SRB
Tns motopa 1; MotoreEspulsore_SRB
Tns motopa 2; MotoreEspulsore2_SRB
Dpo� behtnr.; Multifan_SRB
Ocsocod�aba�e Xngpo; IdroMode_SRB
Spnkal.cahnt.boge; VisSanitari_SRB
Spnkal.tnsa se�n; VisTipoStufa_SRB
Kohtpora spotoka; SensorePortataAria_SRB
Kohtpora odpta�a; SensoreHall_SRB
Kohtpora Ramdga; ControlloLambda_SRB
Fpahnyhnk mnh. odpta�a; LimGiriMinimi_SRB
Tepmosap; AccTermocoppia_SRB
Votootsophnk; AccFotoResistenza_SRB
Cspeyab.sa�e�a D; InibAccB_SRB
;Rampa Candeletta; RampaCandeletta_SRB
Tepmoctat; Termostato_Ambiente_SRB
Pejnm Batpa/tems.; GestioneIbrida_SRB
;Gestione Legna/Pellet; GestioneLegna_SRB
Mexahnyko ynz�e�e; FScuotitore_SRB
Suj 2 / Ynctay; Coclea2_SRB
Ocsocod�. ekctpaktopa 2; Esp2Enable_SRB
Kohtpora odpta�a ekctpaktopa 2; SensoreHall_2_SRB
Cehlop hnboa sereta; SensorePellet_SRB

; parametri blackout
Ocsocod�aba�e peceta; BlackoutRipristino_SRB
Tpa�a�e peceta; BlackoutDurata_SRB

; parametri coclea
Koye�e suja 1; FrenataCoclea_SRB
Tns koye�a suja 1; TipoFrenataCoclea_SRB
Koye�e suja 2; FrenataCoclea_2_SRB
Tns koye�a suja 2; TipoFrenataCoclea_2_SRB
Kohtpora odpta�a suja 1; CtrlGiriCoclea_SRB
Sepnog suja 1; PeriodoCoclea_SRB
Sepnog suja 2; PeriodoCoclea2_SRB
Ocsocod�. sepnoga suja 1; AbilPeriodoCoclea_SRB
Ocsocod�. sepnoga suja 2; AbilPeriodoCoclea2_SRB

; parametri scuotitore
Tpa�a�e soruwnkruca; ScuotDurata_SRB
Dpo� wnkruca; ScuotNCicli_SRB
;Nhtepbar wnkruca; ScuotPeriodo_SRB

; parametri fan ambiente
Kohtpora c Tems.gnmh.facoba; FanCtrlTempFumi_SRB
Tems. Oh behtnr.; FanTempFumiON_SRB
Mnh. tems. behtnr.; FanTempFumiOFF_SRB

; parametri fan
Hnbo 1; FanAttuaL1_SRB
Hnbo 2; FanAttuaL2_SRB
Hnbo 3; FanAttuaL3_SRB
Hnbo 4; FanAttuaL4_SRB
Hnbo 5; FanAttuaL5_SRB

; parametri ricette

; parametri eco
Ekoctos; EcoStop_SRB
Yeka�e Oh; EcoAttesaAccensione_SRB
Yeka�e Ovv; Settings_TShutdown_Eco_SRB
Gerta tems.; EcoDeltaTemperatura_SRB

; parametri tempi funzionamento

; parametri idro
Xngpo helabnctah; IdroIndipendente_SRB
Faze�e xngpo; SpegnimentoIdro_SRB
;Accumulo; FAccumulo_SRB
Cspeyaba�e Cehcnhf sumse; InibSensing_SRB
Mogurawnoha sumsa; PompaModulante_SRB
Specoctat boge; FPressIdro_SRB
;Flussostato Secondario; FlussostatoDigitale_SRB
So�aya�e tosr. Fpe�a�e; GainRisc_SRB
Xnctepela tems.boge; IsteresiTempAcqua_SRB
Gerta cahnt.boge; DeltaSanitari_SRB
So�aya�e tosr. Cahnt.boga; GainSanit_SRB
Tems.aktnb.som.kpuf. 2; TempAux_SRB
Makc. spntncak boge; MaxPressH2O_SRB
Tems. Oh sumsa; TSETON_Pompa_SRB
Tems. Ovv sumsa; TSETOFF_Pompa_SRB

; parametri transitori
Tpa�a�e spegsa�. 1; AccTempoPreacc_SRB
Tpa�a�e spegsa�. 2; AccTempoPreLoad_SRB
Tpa�a�e spegsa�. ha tosro; AccTempoPreacc2_SRB
Tems.gnmh.facoba Oh; AccTempOnFumi_SRB
Tems.gnmh.facoba Ovv; AccTempOffFumi_SRB
Spaf gpba; AccTempSogliaFumi_SRB
Yeka�e la sa�. ha tosro; AccTempoInc_SRB
Gerta tems. Tosro; AccDeltaTempCaldo_SRB
Makc.tpa�a�e sa�e�a; AccMaxTimeWarmUp_SRB
Tpa�a�e Batpa OH; AccMaxTimeFireOn_SRB
Tpa�a�e faze�a; AccMaxTimeSpe_SRB

; parametri di attuazione
T. Oh Suj 1; AttuaTempoOnCoclea1_SRB
T. Ovv Suj 1; AttuaTempoOffCoclea1_SRB
Spotok balguxa; AttuaPortata_SRB
Odpta�n ekctpaktopa 1; AttuaGiriEsp_SRB
Odpta�n ekctpaktopa 2; AttuaGiriEsp2_SRB
T. Oh Suj 2; AttuaTempoOnCoclea2_SRB
T. Ovv Suj 2; AttuaTempoOffCoclea2_SRB
Ekctpaktop 2; AttuaEspulsore2_SRB

; parametri di potenza
Hnbon chafe; LivPotenzaMax_SRB

; parametri pulizia braciere
Nhtepbar; AttesaPB_SRB
Tpa�a�e; DurataPB_SRB
Spaf; MinPowerPB_SRB

; parametri ritardo espulsore
Kaz�e�e Sobe�a�e; RitardoEspInc_SRB
Kaz�e�e Cma�e�e; RitardoEspDec_SRB

; parametri ritardo step potenza
Kaz�e�e Sobe�a�e; RitardoPotInc_SRB
Kaz�e�e Cma�e�e; RitardoPotDec_SRB

; parametri allarme fumi
Cspeyab. speg-arapma; AlmFumi_InibPreAlm_SRB
T.gnmh.facoba speg-arapm; AlmFumi_TempPreAlm_SRB
Makc.tpa�a�e speg-arapma; AlmFumi_DurataPreAlm_SRB
Xnctepela speg-arapm; AlmFumi_GradientePreAlm_SRB
T.gnmh.facoba arapm; AlmFumi_TempAlm_SRB

; parametri sensore pellet
Tpa�a�e speg-arapma; SensPellet_DurataPreAlm_SRB

; parametri assenza fiamma
Gerta gnmh.facoba Ovv; NoFiamma_DeltaTemp_SRB

; parametri sensore portata aria
Spaf bpata otbopeha; Aria_PortataCritica_SRB
Tpa�a�e bpata otbopeha; Aria_TempoAlmPorta_SRB
Tpa�a�e speg-arapma Bpata; Aria_TempoPreAlmPorta_SRB
Cspeyab.arapma Balgux la cafopeb.; Aria_InibAlmAriaCombust_SRB
Tpa�a�e speg-arapma Balgux la cafopeb.; Aria_TempoPreAlmAriaCombust_SRB
Gerta spotoka balguxa la cafopeb.; Aria_DeltaPortAriaCombust_SRB

; comandi
Puyha spobepa; Test_StartStop_SRB
Automat.spobepa; Test_Sequenza_SRB
Da�sac; Test_Bypass_SRB
Dajgape�e tepmosapa; Test_CalibTC_SRB
Dajgape�e votootsophnka Oh; Test_CalibFotoresOn_SRB
Dajgape�e votootsophnka Ovv; Test_CalibFotoresOff_SRB
Tpa�a�e kopaka spobepe; Test_DurataStep_SRB

; carichi
Suj 1; Test_Coclea_SRB
Cbe�nwa; Test_Candeletta_SRB
Beht. 1; Test_Fan1_SRB
Beht. 2; Test_Fan2_SRB
Beht. 3; Test_Fan3_SRB
Ekctpaktop 1; Test_Espulsore_SRB
Sumsa; Test_Pompa_SRB
3-sutn behtnr; Test_3Vie_SRB
Som.kpuf. 1; Test_Aux1_SRB
Som.kpuf. 2; Test_Aux2_SRB
Som.kpuf. A; Test_AuxA_SRB

; attuazioni
Uk�uynb. behtnr. 1; AttuaFan1_SRB
Uk�uynb. behtnr. 2; AttuaFan2_SRB
Uk�uynb. behtnr. 3; AttuaFan3_SRB

; richieste di conferma
Sotbpgntn?; ReqConf_SRB
Kpatko cso�ntn tepmnh.drok �E sa latnm ocsocodntn; TC_ReqConf_SRB

; Crono
Xpoho; Menu_Crono_SRB
Ocsocod�aba�e; Abilitazione_SRB
Uynta� spovnr; CaricaProfilo_SRB
Ahurnpa�e; Azzera_SRB

; prog. crono settimanale
Spofpam; PrgSettimanale_SRB
Ocsocod�aba�e; PrgAbilita_SRB
Ctapt; PrgStart_SRB
Ctos; PrgStop_SRB
Temsepatupa balguxa; PrgTempAria_SRB
Temsepatupa boge; PrgTempH2O_SRB
Batpa; PrgPotenza_SRB

;01 sigle fan
B1; sF1_SRB,
B2; sF2_SRB,

;0123 stati di attuazione
 OFF; sAttuaStat00_SRB,
PSU1; sAttuaStat01_SRB,
PSU2; sAttuaStat02_SRB,
WPSU; sAttuaStat03_SRB,
SU A; sAttuaStat04_SRB,
SU B; sAttuaStat05_SRB,
SU C; sAttuaStat06_SRB,
FONA; sAttuaStat07_SRB,
FONB; sAttuaStat08_SRB,
SD A; sAttuaStat09_SRB,
SD B; sAttuaStat10_SRB,
SD C; sAttuaStat11_SRB,
CD A; sAttuaStat12_SRB,
CD B; sAttuaStat13_SRB,
BCLA; sAttuaStat14_SRB,
BCLB; sAttuaStat15_SRB,
PL 1; sAttuaStat16_SRB,
PL 2; sAttuaStat17_SRB,
PL 3; sAttuaStat18_SRB,
PL 4; sAttuaStat19_SRB,
PL 5; sAttuaStat20_SRB,
PL 6; sAttuaStat21_SRB,
PL 7; sAttuaStat22_SRB,
PL 8; sAttuaStat23_SRB,
PL 9; sAttuaStat24_SRB,
PLSA; sAttuaStat25_SRB,


