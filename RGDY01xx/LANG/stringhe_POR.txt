; 
; \file stringhe_POR.c
; \brief POR strings set
; 


; week days
Domingo; sDomenica_POR
Segunda-Feira; sLunedi_POR
Ter�a-Feira; sMartedi_POR	// Ter�a-Feira
Quarta-Feira; sMercoledi_POR
Quinta-Feira; sGiovedi_POR
Sexta-Feira; sVenerdi_POR
Sabado; sSabato_POR	// S�bado

Off; sOff_POR
On ; sOn_POR
Auto; sAuto_POR
�C; sDegC_POR
�F; sDegF_POR
H2O; sH2O_POR

; stati della stufa
ATIVA�AO; Stufa_Accensione_POR	// ATIVA��O
ON; Stufa_Accesa_POR
DESATIVA�AO; Stufa_Spegnimento_POR	// DESATIVA��O
OFF; Stufa_MCZ_POR
ALARME; Stufa_Allarme_POR
DESATIVA�AO APOS BLACKOUT; Stufa_SpeRete_POR	// DESATIVA��O AP�S BLACKOUT
ATIVA�AO APOS BLACKOUT; Stufa_AccRete_POR	// ATIVA��O AP�S BLACKOUT
RESET; Stufa_Reset_POR
TESTE; Stufa_Collaudo_POR
 ; Stufa_NonDef_POR

; allarme
ALARME; Alarm_POR

; descrizione allarme
 ; AlarmSource00_POR
Falha na Ativa�ao; AlarmSource01_POR	// Falha na Ativa��o
Extin�ao Chama; AlarmSource02_POR	// Extin��o Chama
Superaquecimento Tanque Pellet; AlarmSource03_POR
Temperatura Fumos Excessiva; AlarmSource04_POR
Alarme Pressostato; AlarmSource05_POR
Alarme Ar Combustao; AlarmSource06_POR	// Alarme Ar Combust�o
Porta Aberta; AlarmSource07_POR
Exaustor Fumos Avariado; AlarmSource08_POR
Avaria Sonda Fumos; AlarmSource09_POR
Avaria Vela; AlarmSource10_POR
Avaria Alimenta�ao Pellet; AlarmSource11_POR	// Avaria Alimenta��o Pellet
 ; AlarmSource12_POR
Avaria Ficha Eletronic; AlarmSource13_POR
 ; AlarmSource14_POR
Alarme Nivel Pellet; AlarmSource15_POR
Pressao Agua Fora de Escala; AlarmSource16_POR	// Press�o Agua Fora de Escala
Portinhola Vao Pellet Aberta; AlarmSource17_POR	// Portinhola V�o Pellet Aberta
Superaquecimento Tanque Agua; AlarmSource18_POR

; ripristino allarme
 ; AlarmRecovery00_POR
Limpar Braseiro e Tentar Novamente; AlarmRecovery01_POR
Encher Tanque Pellet; AlarmRecovery02_POR
Verificar Livro Instru�oes; AlarmRecovery03_POR	// Verificar Livro Instru��es
Verificar Livro Instru�oes; AlarmRecovery04_POR	// Verificar Livro Instru��es
Liberar de uma Eventual Obstru�ao; AlarmRecovery05_POR	// Liberar de uma Eventual Obstru��o
Verificar Limpeza Braseiro/Entrada Ar/Chamine; AlarmRecovery06_POR	// Verificar Limpeza Braseiro/Entrada Ar/Chamin�	//
Verificar Fechamento Porta; AlarmRecovery07_POR
Chamar Assistencia; AlarmRecovery08_POR	// Chamar Assist�ncia
Chamar Assistencia; AlarmRecovery09_POR	// Chamar Assist�ncia
Chamar Assistencia; AlarmRecovery10_POR	// Chamar Assist�ncia
Chamar Assistencia; AlarmRecovery11_POR	// Chamar Assist�ncia
Chamar Assistencia; AlarmRecovery12_POR	// Chamar Assist�ncia
Chamar Assistencia; AlarmRecovery13_POR	// Chamar Assist�ncia
Chamar Assistencia; AlarmRecovery14_POR	// Chamar Assist�ncia
Verificar Nivel Pellet; AlarmRecovery15_POR
Restabelecer a Correta Pressao do Sistema; AlarmRecovery16_POR	// Restabelecer a Correta Press�o do Sistema
Verificar Fechamento Vao Pellet; AlarmRecovery17_POR	// Verificar Fechamento V�o Pellet
Verificar Livro Instru�oes; AlarmRecovery18_POR	// Verificar Livro Instru��es

; sleep
Sleep; Menu_Sleep_POR

; avvio
Hora; Settings_Time_POR
Data; Menu_DataOra_POR	
Potencia; Potenza_POR	// Pot�ncia
Temperatura; Temperatura_POR
Temp. Agua; Menu_Idro_POR
Fan; Fan_POR

; network
Network; Menu_Network_POR
;Wi-Fi; WiFi_POR
SSID; Ssid_POR
Keyword; Keyword_POR
;TCP Port; TCPPort_POR
;WPS PIN; WPSPin_POR

; anomalie
Anomalias; Menu_Anomalie_POR
Service; RichiestaService_POR
Sonda Temp. Ar Avariada; GuastoSondaTempAria_POR
Sonda Temp. Agua Avariada; GuastoSondaTempAcqua_POR
Pressostato Agua Avariado; GuastoPressostAcqua_POR
Pressao Agua Fora de Escala; SovraPressAcqua_POR	// Press�o Agua Fora de Escala
Sen.Cap. Ar Av.; GuastoSensorePortata_POR
Baixo Niv. Pellet; PelletEsaurito_POR	// Baixo N�v. Pellet
Porta Aberta; PortaAperta_POR

; info
Info; Menu_Infos_POR
Codigo Ficha; CodiceScheda_POR	// C�digo Ficha
Codigo Seguran�a; CodiceSicurezza_POR	// C�digo Seguran�a
Codigo Display; CodicePannello_POR	// C�digo Display
Codigo Parametros; CodiceParametri_POR	// C�digo Par�metros
Horas de Funcionamento; OreFunzionamento_POR
Horas de Servi�o; OreSat_POR	// Horas de Servi�o
Service; Service_POR
Rota�oes Expulsor 1; VentilatoreFumi_POR	// Rota��es Expulsor 1
luxo de Ar Medido; PortataAriaMisurata_POR
Oxigenio Residual; OssigenoResiduo_POR	// Oxig�nio Residual
Consumo Pellet; Consumo_POR
Temperatura Fumos; TemperaturaFumi_POR
Fotoresistencia; FotoResistenza_POR	// Fotoresist�ncia
Tempo Coclea 1; TempoCoclea_POR	// Tempo C�clea 1
Rota�oes Coclea 1; GiriCoclea_POR	// Rota��es C�clea 1 
Atua�ao Fan1; MotoreScambiatore_POR	// Atua��o Fan1
Atua�ao Fan2; MotoreScambiatore2_POR	// Atua��o Fan2
Pressao Agua; IdroPress_POR	// Press�o Agua
Numero Igni�oes; NumAccensioni_POR	// N�mero Igni��es
IP Address; IPAddress_POR
Historico de alarmes; StoricoAllarmi_POR	// Hist�rico de alarmes

; menu principale
Menu Principal; MainMenu_POR
Configura�oes; Menu_Impostazioni_POR	// Configura��es
Menu Tecnico; Menu_Tecnico_POR

; settings
Lingua; Lingua_POR
Eco; EcoMode_POR
Display; Settings_Backlight_POR
�C / �F; Settings_ShowTemp_POR
Carga Pellet; Precarica_Pellet_POR
Limpeza; Attiva_Pulizia_POR
Ativa Bomba; Attiva_Pompa_POR
Radio ID; RFPanel_POR

; ricette
Combustao Receitas; Ricette_POR	// Combust�o Receitas
Ar; OffsetEspulsore_POR
Pellet; RicettaPelletON_POR
Oxigenio; Ossigeno_POR	// Oxig�nio

; temp H2O
Set Aquecimento; TempH2O_SetRisc_POR
Set Sanitarios; TempH2O_SetSan_POR	// Set Sanit�rios

; menu nascosto
Password; HidPassword_POR

;012345678901234567890 menu tecnico - livello 1					
Configura�ao; ConfigSystem_POR	// Configura��o
Controle; Controllo_POR
Hidro; MenuIdro_POR
Ativa�ao/Desativa�ao; AttuaTransitorie_POR	// Ativa��o/Desativa��o
Potencia; AttuaPotenza_POR	// Pot�ncia
Gestao de Alarmes; GestAllarmi_POR	// Gest�o de Alarmes
Teste; Collaudo_POR
Busca por Codigo; RicercaCodParam_POR	// Busca por C�digo

;012345678901234567890 menu tecnico - livello 2							
Configura�oes; MenuParamGen_POR	// Configura��es
Blackout; Blackout_POR
Coclea; MenuCoclea_POR	// C�clea
Limpador/Sacudidor; MenuScuotitore_POR
Fan; FanAmbiente_POR
Fan 1; Fan1_POR
Fan 2; Fan2_POR
Fan 3; Fan3_POR

Fun�ao Eco; MenuEco_POR	// Fun��o Eco
Tempo de Opera�ao; TempiFunz_POR	// Tempo de Opera��o

Configura�oes; ParamTransitori_POR	// Configura��es
PreAtiva�ao 1; Preacc1_POR	// Pr�Ativa��o 1
PreAtiva�ao 2; Preacc2_POR	// Pr�Ativa��o 2
PreAtiva�ao a Quente; PreaccCaldo_POR	// Pr�Ativa��o a Quente
Ativa�ao A; AccA_POR	// Ativa��o A
Ativa�ao B; AccB_POR	// Ativa��o B
Fogo ON; FireON_POR
Desativa�ao A; SpeA_POR	// Desativa��o A
Desativa�ao B; SpeB_POR	// Desativa��o B
Resfriamento; Raff_POR

Parametros Potencia; ParamPotenza_POR	// Par�metros Pot�ncia
Ciclo de Limpeza; MenuPB_POR
Atraso Expulsor; RitardoAttuaEsp_POR
Atrasar Potencia; RitardoStepPot_POR	// Atrasar Pot�ncia
Potencia 1; Pot1_POR	// Pot�ncia 1
Potencia 2; Pot2_POR	// Pot�ncia 2
Potencia 3; Pot3_POR	// Pot�ncia 3
Potencia 4; Pot4_POR	// Pot�ncia 4
Potencia 5; Pot5_POR	// Pot�ncia 5
Potencia 6; Pot6_POR	// Pot�ncia 6
Limpeza; Pulizia_POR
Interpola�ao; Interpolazione_POR	// Interpola��o

Fumos Alarme; AllarmeFumi_POR
Sensor Pellet; SensPellet_POR
;No Flama; AssenzaFiamma_POR
Fluxo de Ar; SensAria_POR

Comandos; Comandi_POR
Cargas; Carichi_POR
Atua�oes; Attuazioni_POR	// Atua��es

; parametri generali
Tipo Estufa; TipoStufa_POR
Restaurar Tipo Estufa Originais; TipoStufaDef_POR
Tipo Motor 1; MotoreEspulsore_POR
Tipo Motor 2; MotoreEspulsore2_POR
Numero Fan; Multifan_POR	// N�mero Fan
Habilita�ao Hidro; IdroMode_POR	// Habilita��o Hidro
Vis. Agua Sanitaria; VisSanitari_POR	// Vis. Agua Sanit�ria
Vis. Tipo Estufa; VisTipoStufa_POR
Controle Fluxo de Ar; SensorePortataAria_POR
Controle Rota�oes; SensoreHall_POR	// Controle Rota��es
Controle Lambda; ControlloLambda_POR
Limitador Velocidade�Min; LimGiriMinimi_POR	// Limitador Velocidade�M�n
Termocopia; AccTermocoppia_POR	// Termoc�pia
Fotoresistencia; AccFotoResistenza_POR	// Fotoresist�ncia
Inibi�ao Ativa�ao B; InibAccB_POR	// Inibi��o Ativa��o B
;Rampa Candeletta; RampaCandeletta_POR
Termostato; Termostato_Ambiente_POR
Fogo/Temp. Mode; GestioneIbrida_POR
;Gestao Madeira/Pellet; GestioneLegna_POR	// Gest�o Madeira/Pellet
Limpeza Mecanica; FScuotitore_POR	// Limpeza Mec�nica
Coclea 2 / Limpador; Coclea2_POR	// C�clea 2 / Limpador
Expulsor 2 Habilita�ao; Esp2Enable_POR	// Expulsor 2 Habilita��o
Controle Rota�oes Expulsor 2; SensoreHall_2_POR	// Controle Rota��es Expulsor 2
Sensor Nivel Pellet; SensorePellet_POR	// Sensor N�vel Pellet

; parametri blackout
Recupera�ao Habilita�ao; BlackoutRipristino_POR	// Recupera��o Habilita��o
Recupera�ao Tempo; BlackoutDurata_POR	// Recupera��o Tempo

; parametri coclea
Freada Coclea 1; FrenataCoclea_POR		// Freada C�clea 1
Tipo Freada Coclea 1; TipoFrenataCoclea_POR	// Tipo Freada C�clea 1
Freada Coclea 2; FrenataCoclea_2_POR	// Freada C�clea 2
Tipo Frenata Coclea 2; TipoFrenataCoclea_2_POR	// Tipo Freada C�clea 2
Controle Rota�oes Coclea 1; CtrlGiriCoclea_POR	// Controle Rota��es C�clea 1 
Periodo Coclea 1; PeriodoCoclea_POR	// Per�odo C�clea 1
Periodo Coclea 2; PeriodoCoclea2_POR	// Per�odo C�clea 2
Abilitazione Periodo Coclea 1; AbilPeriodoCoclea_POR	// Habilita��o Per�odo C�clea 1
Abilitazione Periodo Coclea 2; AbilPeriodoCoclea2_PO	// Habilita��o Per�odo C�clea 2R

; parametri scuotitore
Dura�ao Semi-Ciclo; ScuotDurata_POR	// Dura��o Semi-Ciclo
Numero Ciclos; ScuotNCicli_POR	// N�mero Ciclos
;Intervalo Ciclos; ScuotPeriodo_POR

; parametri fan ambiente
De Exaustao; FanCtrlTempFumi_POR	// De Exaust�o
Temp. On Fan; FanTempFumiON_POR
Fan Min. Temp.; FanTempFumiOFF_POR

; parametri fan
Nivel 1; FanAttuaL1_POR	// N�vel 1
Nivel 2; FanAttuaL2_POR	// N�vel 2
Nivel 3; FanAttuaL3_POR	// N�vel 3
Nivel 4; FanAttuaL4_POR	// N�vel 4
Nivel 5; FanAttuaL5_POR	// N�vel 5

; parametri ricette

; parametri eco
Ecostop; EcoStop_POR
Espera On; EcoAttesaAccensione_POR
Espera Off; Settings_TShutdown_Eco_POR
Delta Temp.; EcoDeltaTemperatura_POR

; parametri tempi funzionamento

; parametri idro
Hidro Independente; IdroIndipendente_POR
Desativa�ao Hidro; SpegnimentoIdro_POR	// Desativa��o Hidro
;Accumulo; FAccumulo_POR
Inibi�ao Bomba Sensing; InibSensing_POR	// Inibi��o Bomba Sensing
Bomba Modulante; PompaModulante_POR
Pressostato Agua; FPressIdro_POR	// Press�stato Agua
;Flussostato Secondario; FlussostatoDigitale_POR
Ganho Aquecimento; GainRisc_POR
Histerese Temp. Agua; IsteresiTempAcqua_POR
Delta Sanitarios; DeltaSanitari_POR	// Delta Sanit�rios
Ganho Sanitarios; GainSanit_POR	// Ganho Sanit�rios
Ativa�ao Temp AUX 2; TempAux_POR	// Ativa��o Temp AUX 2
Pressao Agua Max; MaxPressH2O_POR	// Press�o Agua M�x
Temp. On Bomba; TSETON_Pompa_POR
Temp. Off Bomba; TSETOFF_Pompa_POR

; parametri transitori
Tempo PreAti. 1; AccTempoPreacc_POR	// Tempo Pr�Ati. 1
Tempo PreAti. 2; AccTempoPreLoad_POR	// Tempo Pr�Ati. 2
Tempo PreAtiva�ao a Quente; AccTempoPreacc2_POR	// Tempo Pr�Ativa��o a Quente
Temp. Fumos On; AccTempOnFumi_POR
Temp. Fumos Off; AccTempOffFumi_POR
Madeira Limite; AccTempSogliaFumi_POR
Tempo PreAtiva�ao a Quente; AccTempoInc_POR	// Tempo Pr�Ativa��o a Quente
Delta Temp. Quente; AccDeltaTempCaldo_POR
Dura�ao Max  Ativa�ao; AccMaxTimeWarmUp_POR	// Dura��o Max  Ativa��o
Dura�ao Fogo On; AccMaxTimeFireOn_POR	// Dura��o Fogo On
Dura�ao Desativa�ao; AccMaxTimeSpe_POR	// Dura��o Desativa��o

; parametri di attuazione
T. On Coclea 1; AttuaTempoOnCoclea1_POR	// T. On C�clea 1
T. Off Coclea 1; AttuaTempoOffCoclea1_POR	// T. Off C�clea 1
Fluxo de Ar; AttuaPortata_POR
Rota�oes Expulsor 1; AttuaGiriEsp_POR	// Rota��es Expulsor 1
Rota�oes Expulsor 2; AttuaGiriEsp2_POR	// Rota��es Expulsor 2
T. On Coclea 2; AttuaTempoOnCoclea2_POR	// T. On C�clea 2
T. Off Coclea 2; AttuaTempoOffCoclea2_POR	// T. Off C�clea 2
Expulsor 2; AttuaEspulsore2_POR

; parametri di potenza
Niveis de Potencia; LivPotenzaMax_POR	// N�veis de Pot�ncia

; parametri pulizia braciere
Intervalo; AttesaPB_POR
Tempo; DurataPB_POR
Limite; MinPowerPB_POR

; parametri ritardo espulsore
Atraso Crescimento; RitardoEspInc_POR
Atraso Diminuir; RitardoEspDec_POR

; parametri ritardo step potenza
Atraso Crescimento; RitardoPotInc_POR
Atraso Diminuir; RitardoPotDec_POR

; parametri allarme fumi
Inibi�ao de Pre-Alarme; AlmFumi_InibPreAlm_POR	// Inibi��o de Pr�-Alarme
Temp. Fumos Pre-Alarme; AlmFumi_TempPreAlm_POR	// Temp. Fumos Pr�-Alarme
Dura�ao Max Pre-Alarme; AlmFumi_DurataPreAlm_POR	// Dura��o Max Pr�-Alarme
Histerese Pre-Alarme; AlmFumi_GradientePreAlm_POR	// Histerese Pr�-Alarme
Temp. Fumos Alarme; AlmFumi_TempAlm_POR

; parametri sensore pellet
Dura�ao Pre-Alarme; SensPellet_DurataPreAlm_POR	// Dura��o Pr�-Alarme

; parametri assenza fiamma
Delta Fumos Off; NoFiamma_DeltaTemp_POR

; parametri sensore portata aria
Limite Porta Aberta; Aria_PortataCritica_POR
Dura�ao Porta Aberta; Aria_TempoAlmPorta_POR	// Dura��o Porta Aberta
Dura�ao Pre-Alarme Porta; Aria_TempoPreAlmPorta_POR	// Dura��o Pr�-Alarme Porta
Inibi�ao Alarme Ar�Combustao; Aria_InibAlmAriaCombust_POR	// Inibi��o Alarme Ar�Combust�o
Dura�ao Pre-Alarme Ar�Combustao; Aria_TempoPreAlmAriaCombust_POR	// Dura��o Pr�-Alarme Ar�Combust�o
Delta Fluxo�Ar Combustao; Aria_DeltaPortAriaCombust_POR	// Delta Fluxo�Ar Combust�o

; comandi
Teste Manual; Test_StartStop_POR
Testes Automaticos; Test_Sequenza_POR	// Testes Autom�ticos
Bypass; Test_Bypass_POR
Calibragem Termocopia; Test_CalibTC_POR	// Calibragem Termoc�pia
Calibragem Fotoresistencia On; Test_CalibFotoresOn_POR	// Calibragem Fotoresist�ncia On
Calibragem Fotoresistencia Off; Test_CalibFotoresOff_POR	// Calibragem Fotoresist�ncia Off
Dura�ao Passo de Teste; Test_DurataStep_POR	// Dura��o Passo de Teste

; carichi
Coclea 1; Test_Coclea_POR	// C�clea 1
Vela; Test_Candeletta_POR
Fan 1; Test_Fan1_POR
Fan 2; Test_Fan2_POR
Fan 3; Test_Fan3_POR
Expulsor 1; Test_Espulsore_POR
Bomba; Test_Pompa_POR
3-Way; Test_3Vie_POR
Aux 1; Test_Aux1_POR
Aux 2; Test_Aux2_POR
Aux A; Test_AuxA_POR

; attuazioni
Atua�ao Fan 1; AttuaFan1_POR	// Atua��o Fan 1
Atua�ao Fan 2; AttuaFan2_POR	// Atua��o Fan 2
Atua�ao Fan 3; AttuaFan3_POR	// Atua��o Fan 3

; richieste di conferma
Confirmar?; ReqConf_POR
Curto-Circuito JE Terminal e Depois Ativar; TC_ReqConf_POR

; Crono
Crono; Menu_Crono_POR
Habilita�ao; Abilitazione_POR	// Habilita��o
Carga Perfil; CaricaProfilo_POR
Reiniciado; Azzera_POR

; prog. crono settimanale
Programa; PrgSettimanale_POR
Habilita�ao; PrgAbilita_POR	// Habilita��o
H. Inicio; PrgStart_POR
H. Stop; PrgStop_POR
Temp. Ar; PrgTempAria_POR
Temp. Agua; PrgTempH2O_POR
Fogo; PrgPotenza_POR

;01 sigle fan
F1; sF1_POR,
F2; sF2_POR,

;0123 stati di attuazione
 OFF; sAttuaStat00_POR,
PSU1; sAttuaStat01_POR,
PSU2; sAttuaStat02_POR,
WPSU; sAttuaStat03_POR,
SU A; sAttuaStat04_POR,
SU B; sAttuaStat05_POR,
SU C; sAttuaStat06_POR,
FONA; sAttuaStat07_POR,
FONB; sAttuaStat08_POR,
SD A; sAttuaStat09_POR,
SD B; sAttuaStat10_POR,
SD C; sAttuaStat11_POR,
CD A; sAttuaStat12_POR,
CD B; sAttuaStat13_POR,
BCLA; sAttuaStat14_POR,
BCLB; sAttuaStat15_POR,
PL 1; sAttuaStat16_POR,
PL 2; sAttuaStat17_POR,
PL 3; sAttuaStat18_POR,
PL 4; sAttuaStat19_POR,
PL 5; sAttuaStat20_POR,
PL 6; sAttuaStat21_POR,
PL 7; sAttuaStat22_POR,
PL 8; sAttuaStat23_POR,
PL 9; sAttuaStat24_POR,
PLSA; sAttuaStat25_POR,


